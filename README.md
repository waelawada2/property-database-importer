# Property Database Importer

## Prerequisites
- Java 8 or newer
- Maven 3.3 or newer

## How to Build
Clone the Git repository to a local directory:
```
$ git clone http://bitbucket.org/sothebys/property-database-importer
```
Build with Maven:
```sh
$ mvn clean package
```

## How to Test
Run unit tests:
```sh
$ mvn clean test
```

## How to Run
The application can be launched with the `spring-boot-maven-plugin`:
```sh
$ mvn spring-boot:run
```

Please note that the application must be properly configured before it can run successfully.

## How to Configure
The main configuration properties are found in `application.yml`. For running the application locally with a custom set of configuration properties, create a copy named `application-local.yml`, make any necessary changes to the configuration properties, and run the application with the following command:

```sh
$ mvn -Dspring.profiles.active=local spring-boot:run
```
## Configuration Properties

| Property | Description |
| ------ | ------ |
| backend.property-service.host | URI of the property service where property data will be pushed to. The URI needs to be provided in the notation `host:port/base-path`, e.g. `localhost:9990/property-service/v1` |
| s3.bucket-name | Name of the S3 bucket where the input CSV files are stored. |
| s3.access-key | The AWS access key of an IAM user that has access to the bucket. |
| s3.secret-key | The AWS secret key of an IAM user that has access to the bucket. |
| s3.poll-delay | The delay between polling the S3 bucket in milliseconds. |
| s3.delete-after-read | `true` if the file can be deleted after reading it from S3, otherwise `false`. |

## AWS Resources

All AWS resources needed by EOS Importer are managed by the CloudFormation template [stb-eosimporter.cf.yml](aws/stb-eosimporter.cf.yml).

The stack should be called `stb-stk-{ENV}-eos-eosimporter` where `{ENV}` is the logical environment name, e.g. `stb-stk-dev-eos-eosimporter`.

*Please Note*: The CloudFormation template creates an IAM user for EOS Importer. (The IAM username is printed in the CloudFormation Outputs.) 
Credentials must be created manually and added to the appropriate configuration files in the project's [config repository](https://bitbucket.org/sothebys/property-database-importer-config).
