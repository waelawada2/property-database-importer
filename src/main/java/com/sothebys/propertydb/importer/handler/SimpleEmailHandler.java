package com.sothebys.propertydb.importer.handler;

import static com.sothebys.propertydb.importer.support.PropertyImporterConstants.UTF_8;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.Header;
import org.apache.camel.component.aws.s3.S3Constants;
import org.apache.camel.language.Simple;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public class SimpleEmailHandler {

  @Value("${s3.importer-output.aws-server-name}")
  private String awsServerName;

  @Value("${s3.importer-output.bucket-name}")
  private String bucketName;

  /**
   * This method used to construct email body
   *
   * @param originalFileName of the file name
   * @return String body message
   */
  public String prepareEmailBody(@Simple("${file:onlyname}") String originalFileName,
      @ExchangeProperty("errorCsvFileName") String errorFileName,
      @ExchangeProperty("successfulCsvFileName") String successfulCsvFileName,
      @ExchangeProperty("numberOfCSVRows") int numRecords,
      @Simple("${exchangeProperty.badCsvData.size()}") int numFailedRecords) {
    int numSuccessfulRecords = numRecords - numFailedRecords;

    StringBuilder emailBody = new StringBuilder();
    emailBody.append("Hi,").append("<br/>")
        .append(" Here is the complete summary of import action for ")
        .append(originalFileName)
        .append("<br> Total number of records in file:")
        .append(numRecords)
        .append("<br/> Total number of records successfully imported:")
        .append(numRecords - numFailedRecords)
        .append("<br/> Total number of records with errors:")
        .append(numFailedRecords);
    if (numFailedRecords > 0) {
      emailBody.append("<br/> Please download the Error records file from <a href=")
          .append(constructErrorFileUploadUrlForEmail(errorFileName)).append(">here</a>");
    }
    if (numSuccessfulRecords > 0 && successfulCsvFileName != null) {
      emailBody.append("<br/> Please download the Successful records file from <a href=")
          .append(constructErrorFileUploadUrlForEmail(successfulCsvFileName)).append(">here</a>");
    }
    
    emailBody.append("<br/><br/> Thanks, <br/> EOS Team");

    return emailBody.toString();
  }

  /**
   * This method used to construct S3 file path URL
   *
   * @param fileName the file name
   * @return String of errorFilePathS3URL
   */
  private String constructErrorFileUploadUrlForEmail(final String fileName) {
    String fileUrl = null;
    try {
      fileUrl = awsServerName + "/" + bucketName + "/" + URLEncoder.encode(fileName, UTF_8);
    } catch (UnsupportedEncodingException e) {
      log.error("Failed to construct the error file upload url for email report", e);
    }
    return fileUrl;
  }

  /**
   * This method used to construct the save list confirmation email body
   *
   * @return String body message
   */
  public String prepareSaveListStatusEmailBody(
      @Simple("file:onlyname.noext") String fileName,
      @ExchangeProperty("saveListId") String saveListId,
      @ExchangeProperty("saveListResponsePropertyCount") Integer saveListResponsePropertyCount,
      @ExchangeProperty("saveListErrorMessage") String errorMessage) {

    String emailBody = "Hi, <br/><br/>";

    if (errorMessage == null) {
      final int sanitizedPropertyCount =
          saveListResponsePropertyCount != null ? saveListResponsePropertyCount : 0;

      emailBody += "Saved list " + fileName + " has successfully been saved."
          + "<br>Saved list ID: " + saveListId
          + "<br>Saved list property count: " + sanitizedPropertyCount;
    } else {
      emailBody += "Failed to create save list for " + fileName + "."
          + "<br>Error message: " + errorMessage;
    }

    emailBody += "<br/><br/> Thanks, <br/> EOS Team";
    return emailBody;
  }

  /**
   * Create the email body telling the user that a duplicate file has been detected.
   *
   * @param fileName the name of the duplicate file
   * @return An email body
   */
  public String prepareDuplicateFileEmailBody(@Header(S3Constants.KEY) String fileName) {
    String emailBody = "Hi, <br/><br/>";
    emailBody += "We did not process the import for " + fileName;
    emailBody += " because we have already processed a file with the same content recently.";
    emailBody += "<br>Please modify the file content before trying again.";
    emailBody += "<br/><br/> Thanks, <br/> EOS Team";
    return emailBody;
  }
}
