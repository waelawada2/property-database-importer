package com.sothebys.propertydb.importer.handler;

import com.sothebys.propertydb.importer.exception.TransactionEventImportException;
import com.sothebys.propertydb.importer.model.event.TransactionEventDto;
import com.sothebys.propertydb.importer.support.TransactionEventMapper;
import java.util.List;
import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionEventCSVDataHandler {

  private final TransactionEventMapper mapper;

  @Autowired
  public TransactionEventCSVDataHandler(TransactionEventMapper mapper) {
    this.mapper = mapper;
  }

  public TransactionEventDto handle(List<String> csv, Exchange exchange) {
    TransactionEventDto dto;

    try {
      dto = mapper.map(csv);
      if (dto.getId() != null) {
        exchange.setProperty("eventId", dto.getId());
        dto.setId(null);
      }
    } catch (Exception e) {
      throw new TransactionEventImportException(
          "Cannot map transaction event from CSV data row, Due to: " + e.getMessage(), e);
    }

    return dto;
  }

}
