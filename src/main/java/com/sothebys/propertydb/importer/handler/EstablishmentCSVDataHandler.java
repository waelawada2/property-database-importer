package com.sothebys.propertydb.importer.handler;

import com.sothebys.propertydb.importer.exception.EstablishmentImportException;
import com.sothebys.propertydb.importer.model.event.EstablishmentDto;
import com.sothebys.propertydb.importer.support.EstablishmentMapper;
import java.util.List;
import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EstablishmentCSVDataHandler {

  private final EstablishmentMapper mapper;

  @Autowired
  public EstablishmentCSVDataHandler(EstablishmentMapper mapper) {
    this.mapper = mapper;
  }

  public EstablishmentDto handle(List<String> csv, Exchange exchange) {
    EstablishmentDto dto;

    try {
      dto = mapper.map(csv);
      if (dto.getId() != null) {
        exchange.setProperty("establishmentId", dto.getId());
        dto.setId(null);
      }
    } catch (Exception e) {
      throw new EstablishmentImportException(
          "Cannot map establishment from CSV data row, Due to: " + e.getMessage(), e);
    }

    return dto;
  }
}
