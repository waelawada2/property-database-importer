package com.sothebys.propertydb.importer.handler;

import static com.sothebys.propertydb.importer.support.PropertyImporterConstants.FILE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.substringAfterLast;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3Object;
import com.sothebys.propertydb.importer.exception.PropertyImportException;
import java.io.IOException;
import org.apache.camel.Exchange;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * This class handles the image upload & download to/from S3 Bucket.
 *
 * @author VenkataPrasad Tammineni
 */
@SuppressWarnings("unused")
public class ImageHandler {

  @Autowired
  private AmazonS3Client images3Client;

  @Value("${s3.images.bucket-name}")
  private String bucketName;

  /**
   * This method reads an image from S3 and sets it in the {@code exchange} body as MultiPart file.
   * If {@code imageName} is blank the {@code exchange} body will be set as null.
   *
   * @param imageName present on the images S3 bucket
   * @param exchange to be used for carrying the multipart body
   * @throws PropertyImportException if the imageName is not found or it is an empty file.
   */
  public void setS3ImageToBodyAsMultiPart(String imageName, Exchange exchange) {
    if (isNotBlank(imageName)) {
      try {
        S3Object s3Object = images3Client.getObject(bucketName, imageName);
        long imageSizeInBytes = s3Object.getObjectMetadata().getContentLength();
        if (imageSizeInBytes <= 0) {
          s3Object.close();
          throw new PropertyImportException("Image file not valid");
        }
        exchange.getIn().setBody(MultipartEntityBuilder.create()
            .addPart(FILE, new InputStreamBody(
                s3Object.getObjectContent(), substringAfterLast("/".concat(imageName), "/")))
            .build());
      } catch (SdkClientException e) {
        throw new PropertyImportException(
            String.format("Image named '%s' doesn't exist", imageName), e);
      } catch (IOException e) {
        throw new PropertyImportException(
            String.format("Exception while writing the image '%s'", imageName), e);
      }
    } else {
      exchange.getIn().setBody(null);
    }
  }

}
