package com.sothebys.propertydb.importer.handler;

import static com.sothebys.propertydb.importer.support.PropertyImporterConstants.CSV;
import static com.sothebys.propertydb.importer.support.PropertyImporterConstants.ERRORS;
import static com.sothebys.propertydb.importer.support.PropertyImporterConstants.NULL;
import static com.sothebys.propertydb.importer.support.PropertyImporterConstants.TEMP_PATH;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.jayway.jsonpath.JsonPath;
import com.sothebys.propertydb.importer.exception.PropertyImportException;
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * This class handles the transform JSON data into CSV data and upload CSV file into S3 Bucket.
 *
 * @author VenkataPrasad Tammineni
 */
@Slf4j
@SuppressWarnings("unused")
public class ArtistErrorHandler {

  private static List<List<String>> failRecordList = Collections
      .synchronizedList(new ArrayList<List<String>>());

  @Autowired
  private AmazonS3Client importerFileS3Client;

  @Value("${s3.importer-output.bucket-name}")
  private String bucketName;

  /**
   * This method is used to add artist failed record into list with failure error message
   *
   * @param failedRecordData list of values from the failed artist record
   * @param exchange of the Exchange object
   */
  public void addFailedRecordToList(@ExchangeProperty("csvRowData") List<String> failedRecordData,
      Exchange exchange) {
    String errorMessage = EMPTY;
    Exception exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
    try {
      if (exception instanceof HttpOperationFailedException) {
        HttpOperationFailedException httpException = (HttpOperationFailedException) exception;
        String responseBody = httpException.getResponseBody();
        if (isNotBlank(responseBody)) {
          errorMessage = JsonPath.parse(responseBody).read("$.details");
        }
      } else {
        errorMessage = exception.getMessage();
      }
    } catch (Exception e) {
      log.error("Exception occurred while adding failed record to list", e);
      errorMessage = "Error occurred while processing " + exception.getClass().getSimpleName();
    }
    if (isEmpty(errorMessage)) {
      errorMessage = "Unknown error (" + exception.getClass().getSimpleName() + ")";
    }
    failedRecordData.add(errorMessage);
    Collections.replaceAll(failedRecordData, NULL, EMPTY);
    failRecordList.add(failedRecordData);
  }

  /**
   * This method returns fail records list
   */
  public void getFailRecordsList(Exchange exchange) {
    exchange.getIn().setBody(failRecordList);
  }

  /**
   * This method used to prepare error file name
   *
   * @param fileName of the imported file into S3
   * @return of the error file name
   */
  public String prepareErrorFileName(final String fileName) {
    String originalFileName = fileName;
    StringBuilder errorFileName = new StringBuilder();
    if (isNotBlank(originalFileName) && originalFileName.contains("-")) {
      String[] fileNameArray = originalFileName.split("-");
      errorFileName.append(fileNameArray[1]).append("_").append(ERRORS).append("_")
          .append(LocalDateTime.now().format(ofPattern("yyyyMMddHHmmss"))).append(CSV);
    }
    return errorFileName.toString();
  }

  /**
   * This method used to read the csv file from tmp and stores file into S3
   *
   * @param errorFileName of the error csv file name
   */
  public void uploadArtistErrorFileIntoS3(final String errorFileName) {
    try {
      File tempFile = new File(System.getProperty(TEMP_PATH));
      if (isNotBlank(errorFileName)) {
        File errorCsvFile = new File(tempFile, errorFileName);
        if (errorCsvFile != null && errorCsvFile.exists()) {
          importerFileS3Client
              .putObject(new PutObjectRequest(bucketName, errorFileName, errorCsvFile)
                  .withCannedAcl(CannedAccessControlList.PublicRead));
        }
      }
    } catch (Exception e) {
      throw new PropertyImportException(
          String.format("Exception while import %s file into S3", errorFileName));
    }
  }

  /**
   * This method use the clear fail records List once importer job processed
   */
  public void clearFailRecordsList() {
    failRecordList.clear();
  }

}
