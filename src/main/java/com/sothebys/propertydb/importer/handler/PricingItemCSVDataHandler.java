package com.sothebys.propertydb.importer.handler;

import java.util.List;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.exception.PricingItemImportException;
import com.sothebys.propertydb.importer.model.item.PricingItemDto;
import com.sothebys.propertydb.importer.support.PricingItemMapper;

@Component
public class PricingItemCSVDataHandler {

  private final PricingItemMapper mapper;

  @Autowired
  public PricingItemCSVDataHandler(PricingItemMapper mapper) {
    this.mapper = mapper;
  }

  public PricingItemDto handle(List<String> csv, Exchange exchange) {
    
    PricingItemDto dto;
    
    try {
      dto = mapper.map(csv);
      if(dto.getId() != null) {
        exchange.getProperties().put("itemId", dto.getId());
        dto.setId(null);
      }

    } catch (Exception e) {
      throw new PricingItemImportException(
          "Cannot map pricing item from CSV data row, Due to:  " + e.getMessage(), e);
    }

    return dto;
  }

}
