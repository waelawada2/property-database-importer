package com.sothebys.propertydb.importer.handler;

import com.sothebys.propertydb.importer.exception.PublicationItemImportException;
import com.sothebys.propertydb.importer.model.item.PublicationItemDto;
import com.sothebys.propertydb.importer.support.PublicationItemMapper;
import java.util.List;
import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class PublicationItemCSVDataHandler {

  public PublicationItemMapper mapper;

  @Autowired
  public PublicationItemCSVDataHandler(PublicationItemMapper mapper) {
    this.mapper = mapper;
  }

  public PublicationItemDto handle(List<String> csv, Exchange exchange) {
    PublicationItemDto dto;

    try {
      dto = mapper.map(csv);
      if(dto.getId() != null) {
        exchange.getProperties().put("itemId", dto.getId());
        dto.setId(null);
      }
    }
    catch (Exception e) {
      throw new PublicationItemImportException(
          "Cannot map Publication item from CSV data row, Due to: "+e.getMessage(), e);
    }

    return dto;
  }
}
