package com.sothebys.propertydb.importer.handler;

import java.util.List;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.exception.PublicationEventImportException;
import com.sothebys.propertydb.importer.model.event.PublicationEventDto;
import com.sothebys.propertydb.importer.support.PublicationEventMapper;


@Component
public class PublicationEventCSVDataHandler {

  public PublicationEventMapper mapper;

  @Autowired
  public PublicationEventCSVDataHandler(PublicationEventMapper mapper) {
    this.mapper = mapper;
  }

  public PublicationEventDto handle(List<String> csv, Exchange exchange) {
    PublicationEventDto dto;

    try {
      dto = mapper.map(csv);
      if(dto.getId() != null) {
        exchange.getProperties().put("eventId", dto.getId());
        dto.setId(null);
      }
    } catch (Exception e) {
      throw new PublicationEventImportException(
          "Cannot map Publication event from CSV data row, Due to: " + e.getMessage(), e);
    }

    return dto;
  }
}
