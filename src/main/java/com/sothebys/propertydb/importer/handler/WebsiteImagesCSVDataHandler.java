package com.sothebys.propertydb.importer.handler;

import com.sothebys.propertydb.importer.model.WebsiteImagesCreateRequestDTO;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sothebys.propertydb.importer.exception.WebsiteImageImportException;
import com.sothebys.propertydb.importer.support.WebsiteImagesMapper;

/**
 * @author SrinivasaRao Batthula
 */
public class WebsiteImagesCSVDataHandler {

  private static final Logger log = LoggerFactory.getLogger(WebsiteImagesCSVDataHandler.class);

  private final WebsiteImagesMapper mapper = new WebsiteImagesMapper();

  public WebsiteImagesCreateRequestDTO handle(List<String> csv) {
    log.info("Received website images CSV data row: {}", csv);

    WebsiteImagesCreateRequestDTO dto;

    try {
      dto = mapper.map(csv);
    } catch (Exception e) {
      log.error("Cannot map website images data from CSV", e);
      throw new WebsiteImageImportException("Cannot map website images data from CSV", e);
    }

    return dto;
  }

}
