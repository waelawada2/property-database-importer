/**
 *
 */
package com.sothebys.propertydb.importer.handler;

import com.sothebys.propertydb.importer.model.SaveListCreateRequestDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.Header;
import org.apache.camel.PropertyInject;
import org.apache.commons.lang3.StringUtils;

/**
 * This class handles the creation of SaveList DTO. It also handles population & clear actions for
 * propertyId list.
 *
 * @author Manju.Rana
 */
public class SaveListHandler {

  private static final String CSV_FORMAT = ".csv$";

  private static List<String> propertyIdsList = Collections
      .synchronizedList(new ArrayList<String>());


  private static final String FILE_NAME = "Import ";
  
  @PropertyInject("savedlist.shared-users")
  private String saveListSharedUsers;

  /**
   * This method used to add successfully inserted property object id values into list.
   *
   * @param propertyId of the property id
   */
  public void addPropertyIdToList(String propertyId) {
    if (StringUtils.isNotBlank(propertyId)) {
      propertyIdsList.add(propertyId);
    }
  }

  /**
   * This method builds {@code SaveListCreateRequestDTO}.
   *
   * @param originalFileName of the uploaded file name
   * @return saveListCreateRequestDTO SaveListCreateRequest object
   */
  public SaveListCreateRequestDTO createSaveListDTO(
      @Header(Exchange.FILE_NAME) String originalFileName,
      @ExchangeProperty("saveListPropertyIds") List<String> propertyIds) {
    SaveListCreateRequestDTO saveListCreateRequestDTO = new SaveListCreateRequestDTO();
    saveListCreateRequestDTO.setName(FILE_NAME + getOriginalFileName(originalFileName));
    // by default notes are blank
    saveListCreateRequestDTO.setNotes(StringUtils.EMPTY);
    saveListCreateRequestDTO.setOpenSaveList(false);
    saveListCreateRequestDTO.setPropertyIds(propertyIds);
    saveListCreateRequestDTO.setDate(new Date());
    saveListCreateRequestDTO.setSharedUsersList(null);
    return saveListCreateRequestDTO;
  }

  /**
   * This method replaces the file name with the CSV_FORMAT value.
   */
  private String getOriginalFileName(final String originalFileName) {
    String fileName = originalFileName;
    return fileName.replaceAll(CSV_FORMAT, StringUtils.EMPTY);
  }

  /**
   * This method clears the propertyId List
   */
  public void clearPropertyIdsList() {
    propertyIdsList.clear();
  }

  /**
   * This method used return the propertyIdsListCount
   *
   * @return int propertyIdsCount
   */
  public synchronized List<String> propertyIdsList() {
    
    return new ArrayList<>(propertyIdsList);
  }
  
  /**
   * This method used to prepare save list shared users list
   *
   * @return List<String> save list shared users
   */
  public synchronized List<String> createSaveListSharedUsers() {

    return Arrays.asList(saveListSharedUsers.split(","));
  }

  
}
