package com.sothebys.propertydb.importer.handler;

import java.util.List;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.exception.TransactionItemImportException;
import com.sothebys.propertydb.importer.model.item.TransactionItemDto;
import com.sothebys.propertydb.importer.support.TransactionItemMapper;

@Component
public class TransactionItemCSVDataHandler {

  private final TransactionItemMapper mapper;

  @Autowired
  public TransactionItemCSVDataHandler(TransactionItemMapper mapper) {
    this.mapper = mapper;
  }

  public TransactionItemDto handle(List<String> csv, Exchange exchange) {
    
    TransactionItemDto dto;
    
    try {
      dto = mapper.map(csv);
      if(dto.getId() != null) {
        exchange.getProperties().put("itemId", dto.getId());
        dto.setId(null);
      }

    } catch (Exception e) {
      throw new TransactionItemImportException(
          "Cannot map transaction item from CSV data row, Due to: " + e.getMessage(), e);
    }

    return dto;
  }

}
