package com.sothebys.propertydb.importer.handler;

import com.sothebys.propertydb.importer.model.ArtistCreateRequestDTO;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sothebys.propertydb.importer.exception.ArtistImportException;
import com.sothebys.propertydb.importer.support.ArtistMapper;

/**
 * @author VenkataPrasad Tammineni
 */
public class ArtistCSVDataHandler {

  private static final Logger log = LoggerFactory.getLogger(ArtistCSVDataHandler.class);

  private final ArtistMapper mapper = new ArtistMapper();

  public ArtistCreateRequestDTO handle(List<String> csv) {
    log.info("Received artist CSV data row: {}", csv);

    ArtistCreateRequestDTO dto;

    try {
      List<String> errorMeassgesList = new ArrayList<>();
      StringBuffer stringBuffer = new StringBuffer();
      dto = mapper.map(csv, errorMeassgesList);
      errorMeassgesList = dto.getErrorDetailsList();
      if (!errorMeassgesList.isEmpty()) {
        for (String error : errorMeassgesList) {
          stringBuffer.append(" | " +error);
        }
        throw new RuntimeException((StringUtils.isBlank(stringBuffer)) ? ""
            : stringBuffer.substring(2, stringBuffer.length()));
      }
    } catch (Exception e) {
      log.error("Cannot map artist data from CSV", e);
      throw new ArtistImportException(e.getMessage(), e);
    }

    return dto;
  }

}
