package com.sothebys.propertydb.importer.handler;

public class StringHandler {

  /**
   * Trim the given string and replace all whitespaces with an underscore.
   *
   * @param string a string
   * @return A trimmed string with all whitespaces replaced with an underscore.
   */
  public String trimAndReplaceWhitespace(String string) {
    if (string == null) {
      return null;
    }
    return string.trim().replaceAll(" ", "_");
  }
  
  /**
   * Gets the actual File name from Error Csv file name.
   *
   * @param fileName a string
   * @param fileExt a string
   * @return Actual CSV File Name String.
   */
  public String getActualCsvFileName(String fileName, String fileExt) {
    if (fileName == null) {
      return null;
    }
    int beginIndex = fileName.indexOf("-");
    int lastIndex = fileName.lastIndexOf("-");
    if (beginIndex != -1 && lastIndex != -1 && (beginIndex + 1) < lastIndex) {
      fileName = fileName.substring(beginIndex + 1, lastIndex).concat(".").concat(fileExt);
    } else {
      fileName = fileName.concat(".").concat(fileExt);
    }
    return fileName;
  }
}
