package com.sothebys.propertydb.importer.handler;

import com.sothebys.propertydb.importer.exception.PricingEventImportException;
import com.sothebys.propertydb.importer.model.event.PricingEventDto;
import com.sothebys.propertydb.importer.support.PricingEventMapper;
import java.util.List;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PricingEventCSVDataHandler {

  private final PricingEventMapper mapper;

  @Autowired
  public PricingEventCSVDataHandler(PricingEventMapper mapper) {
    this.mapper = mapper;
  }

  public PricingEventDto handle(List<String> csv, Exchange exchange) {
    
    PricingEventDto dto;
    
    try {
      dto = mapper.map(csv);
      if (dto.getId() != null) {
        exchange.getProperties().put("eventId", dto.getId());
        dto.setId(null);
      }

    } catch (Exception e) {
      throw new PricingEventImportException(
          "Cannot map pricing event from CSV data row, Due to: " + e.getMessage(), e);
    }

    return dto;
  }

}
