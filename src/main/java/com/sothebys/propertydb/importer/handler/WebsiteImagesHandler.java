package com.sothebys.propertydb.importer.handler;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.sothebys.propertydb.importer.exception.WebsiteImageImportException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;

/**
 * This class handles the creation of fail record/ success records list with error message or file
 * path by using WebsiteImages DTO.
 * 
 * @author SrinivasaRao.Batthula
 *
 */
public class WebsiteImagesHandler {

  private static final Logger log = LoggerFactory.getLogger(WebsiteImagesHandler.class);

  private static List<String> failRecordList =
      Collections.synchronizedList(new ArrayList<String>());

  private static final String[] header = {"ITEM ID", "SALE_SALE_ID", "LOTS_LOT_ID_TXT", "Details"};

  private static final String WEBSITE_IMAGE_FILE_PATH = "images/web/website-image-";

  private static final String TEMP_PATH = "java.io.tmpdir";

  private static final String WEBSITE_ERROR_MESSAGE =
      "Failed to get imagePath from lotsImages service";

  private static final String OUT_PUT = "output";

  @Autowired
  @Qualifier("imageS3TransferManager")
  private TransferManager transferManager;

  @Autowired
  private AmazonS3Client importerOutputCSVFileS3Client;

  @Value("${s3.images.bucket-name}")
  private String imagesBucketName;

  @Value("${s3.importer-output.bucket-name}")
  private String bucketName;

  /**
   * This method used to add fail record and success records into list with error message or file
   * path
   * 
   * @param details of the fail/success record details
   * @param exchange of the {@link Exchange}
   */
  public void addWebsiteImagesRecordToFile(String itemId, String saleId, String lotsNumber,
      String details, Exchange exchange) {

    String[] row = new String[header.length];
    int index = 0;
    boolean isError = false;

    if (StringUtils.isBlank(details)) {
      isError = true;
      HttpOperationFailedException httpOperationFailedException =
          (HttpOperationFailedException) exchange.getProperty(Exchange.EXCEPTION_CAUGHT,
              Exception.class);
      if (httpOperationFailedException != null) {
        DocumentContext parsedJson = JsonPath.parse(httpOperationFailedException.getResponseBody());
        details = parsedJson.read("$.message");
      } else {
        // failed to get the imagePath from lotsImages service
        details = WEBSITE_ERROR_MESSAGE;
      }
    }

    row[index++] = itemId;
    row[index++] = saleId;
    row[index++] = lotsNumber;
    if (isError) {
      row[index++] = details;
    } else {
      row[index++] = getWebsiteImagePath(saleId, lotsNumber, details);
    }
    failRecordList.add(Arrays.toString(row).trim().replace("[", "").replace("]", ""));

  }

  private String getWebsiteImagePath(String saleId, String lotsNumber, String details) {
    String[] fileNameArray = details.split("/");
    String imagePath = WEBSITE_ERROR_MESSAGE;
    if (fileNameArray != null && fileNameArray.length > 0) {
      imagePath = new StringBuilder(WEBSITE_IMAGE_FILE_PATH).append(saleId).append("-")
          .append(lotsNumber).append("-").append(String.format("%s", fileNameArray[fileNameArray.length-1]))
          .toString();
    }
    return imagePath;
  }

  public void getFailRecordsList(Exchange exchange) {
    exchange.getIn().setBody(failRecordList);
  }

  /**
   * Handler for downloading the provided image ({@code imageUrl}) and uploading it into S3.
   *
   * @param imageUrl URL of the original image
   * @param saleId sale ID of the image
   * @param lotsNumber lot number of the image
   * @throws WebsiteImageImportException If errors occur during image download or upload.
   */
  public void uploadWebsiteImageIntoS3(String imageUrl, String saleId, String lotsNumber) {
    String fileName = getWebsiteImagePath(saleId, lotsNumber, imageUrl);
    File tempFile = null;
    try {
      URL url = new URL(imageUrl);
      try (InputStream imageStream = url.openStream()) {
        // create temporary file, so we don't need to expand file into memory for size calculation
        tempFile = File.createTempFile("website-image", saleId + "-" + lotsNumber);
        FileUtils.copyInputStreamToFile(imageStream, tempFile);

        ObjectMetadata objectMetadata = createObjectMetadataForImage(tempFile);
        PutObjectRequest putObjectRequest = new PutObjectRequest(imagesBucketName, fileName,
            tempFile).withMetadata(objectMetadata);

        transferManager.upload(putObjectRequest).waitForCompletion(); // block until upload completes
        transferManager.shutdownNow(false);
      }
    } catch (Exception e) {
      throw new WebsiteImageImportException(
          String.format("Failed to upload website image '%s' into S3 bucket '%s'", imageUrl, fileName), e);
    } finally {
      if (tempFile != null) {
        try {
          Files.delete(tempFile.toPath());
        } catch (IOException e) {
          log.error("Failed to clean up temporary image file '%s'", tempFile.getPath());
        }
      }
    }
  }

  /**
   * Creates an {@link ObjectMetadata} instance for the given {@code imageFile}
   * for uploading an image into S3.
   */
  private ObjectMetadata createObjectMetadataForImage(File imageFile) {
    ObjectMetadata objectMetadata = new ObjectMetadata();
    objectMetadata.setContentLength(imageFile.length());
    objectMetadata.setContentType(MediaType.IMAGE_JPEG_VALUE);
    objectMetadata.setCacheControl("no-cache");
    return objectMetadata;
  }

  /**
   * This method used to upload website images updated csv file from temp location into S3
   * 
   * @param fileName of website images csv file
   */
  public void uploadWebsiteImageFileIntoS3(final String fileName) {
    try {
      File tempFile = new File(System.getProperty(TEMP_PATH));
      if (StringUtils.isNotBlank(fileName)) {
        File detailsCsvFile = new File(tempFile, fileName);
        if (detailsCsvFile != null && detailsCsvFile.exists()) {
          importerOutputCSVFileS3Client
              .putObject(new PutObjectRequest(bucketName, fileName, detailsCsvFile)
                  .withCannedAcl(CannedAccessControlList.Private));
        }
      }
    } catch (Exception e) {
      throw new WebsiteImageImportException(
          String.format("Exception while import website images %s file into S3", fileName));
    }
  }

  /**
   * This method used to prepare website image file name with image path/error message details with
   * respect to sale and lots numbers
   * 
   * @param fileName of the imported file into S3
   * @return details of the website image file name
   */
  public String prepareWebsiteImageFileName(final String fileName) {
    String originalFileName = null;
    StringBuilder updatedFileName = new StringBuilder();
    if (StringUtils.isNotBlank(fileName) && fileName.contains("-")) {
      String[] fileNameArray = fileName.split("-");
      originalFileName = fileName.substring(0, fileName.lastIndexOf("-"));
      updatedFileName.append(originalFileName).append("_").append(OUT_PUT)
          .append("_").append(String.format("%s", fileNameArray[fileNameArray.length-1]));
       }
    return updatedFileName.toString();
  }

  /**
   * This method clears the Website upload images list once csv file upload into s3 bucket
   */
  public void clearWebsiteImagesList() {
    failRecordList.clear();
  }

}
