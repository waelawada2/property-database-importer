package com.sothebys.propertydb.importer.handler;

import com.sothebys.propertydb.importer.exception.PropertyImportException;
import com.sothebys.propertydb.importer.model.PropertyCreateRequestDTO;
import com.sothebys.propertydb.importer.model.PropertyCreateRequestDTO.CatalogueRaisonee;
import com.sothebys.propertydb.importer.support.PropertyMapper;
import java.util.ArrayList;
import java.util.List;
import org.apache.camel.ExchangeProperty;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Gregor Zurowski
 */
public class PropertyCSVDataHandler {

  private static final Logger log = LoggerFactory.getLogger(PropertyCSVDataHandler.class);

  private final PropertyMapper mapper = new PropertyMapper();

  public PropertyCreateRequestDTO handle(List<String> csv) {
    log.info("Received CSV data row: {}", csv);
    PropertyCreateRequestDTO dto;
    try {
      List<String> errorMessagesList = new ArrayList<>();
      StringBuffer stringBuffer = new StringBuffer();
      dto = mapper.map(csv, errorMessagesList);
      errorMessagesList = dto.getErrorDetailsList();
      if (!errorMessagesList.isEmpty()) {
        for (String error : errorMessagesList) {
          stringBuffer.append(" | " + error);
        }
        throw new RuntimeException(StringUtils.isBlank(stringBuffer) ? ""
            : stringBuffer.substring(2, stringBuffer.length()));
      }
      return dto;
    } catch (Exception e) {
      log.error("Cannot map data from CSV", e);
      throw new PropertyImportException(e.getMessage(), e);
    }
  }

  public PropertyCreateRequestDTO addArtistCRIds(
      @ExchangeProperty("propertyDto") PropertyCreateRequestDTO originalPropertyDto,
      @ExchangeProperty("artistCatalogueRaisoneeIds") List<Integer> artistCatalogueRaisoneeIds) {
    List<CatalogueRaisonee> propertyCatalogueRaisonees = originalPropertyDto
        .getPropertyCatalogueRaisonees();

    if (propertyCatalogueRaisonees == null) {
      return originalPropertyDto;
    }

    if (artistCatalogueRaisoneeIds == null) {
      artistCatalogueRaisoneeIds = new ArrayList<>();
    }

    if (propertyCatalogueRaisonees.size() > artistCatalogueRaisoneeIds.size()) {
      throw new PropertyImportException(
          "Too many property catalogue raisonee values defined (expected at most "
              + artistCatalogueRaisoneeIds.size() + ")");
    }

    for (int i = 0; i < propertyCatalogueRaisonees.size(); i++) {
      propertyCatalogueRaisonees.get(i)
          .setArtistCatalogueRaisoneeId(Long.valueOf(artistCatalogueRaisoneeIds.get(i)));
    }

    return originalPropertyDto;
  }

}
