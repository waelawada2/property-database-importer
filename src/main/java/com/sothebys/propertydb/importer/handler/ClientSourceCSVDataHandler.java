package com.sothebys.propertydb.importer.handler;

import java.util.List;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.exception.ClientSourceImportException;
import com.sothebys.propertydb.importer.model.event.ClientSourceDto;
import com.sothebys.propertydb.importer.support.ClientSourceMapper;

@Component
public class ClientSourceCSVDataHandler {

  private final ClientSourceMapper mapper;

  @Autowired
  public ClientSourceCSVDataHandler(ClientSourceMapper mapper) {
    this.mapper = mapper;
  }

  public ClientSourceDto handle(List<String> csv, Exchange exchange) {
    
    ClientSourceDto dto;
        
    try {
      dto = mapper.map(csv);
      if(dto.getId() != null) {
        exchange.getProperties().put("clientSourceId", dto.getId());
        dto.setId(null);
      }
    } catch (Exception e) {
      throw new ClientSourceImportException(
          "Cannot map Client Source data from CSV data row, Due to: " + e.getMessage(), e);
    }

    return dto;
  }

}
