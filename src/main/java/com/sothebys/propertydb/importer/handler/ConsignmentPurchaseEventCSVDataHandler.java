package com.sothebys.propertydb.importer.handler;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.sothebys.propertydb.importer.exception.ConsignmentPurchaseEventImportException;
import com.sothebys.propertydb.importer.model.event.ConsignmentPurchaseEventDto;
import com.sothebys.propertydb.importer.support.ConsignmentPurchaseEventMapper;

import java.util.List;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConsignmentPurchaseEventCSVDataHandler {

  private ConsignmentPurchaseEventMapper mapper;

  @Autowired
  public ConsignmentPurchaseEventCSVDataHandler(ConsignmentPurchaseEventMapper mapper) {
    this.mapper = mapper;
  }

  public ConsignmentPurchaseEventDto handle(List<String> csv, Exchange exchange) {
    ConsignmentPurchaseEventDto dto;

    try {
      
      dto = mapper.map(csv);
      
      /* If the row in the CSV file contains an ID it must perform a PUT */
      if( isNotBlank(dto.getId()) ) {
        exchange.setProperty("id", dto.getId());
        exchange.getIn().setHeader(Exchange.HTTP_PATH, dto.getId());
        dto.setId(null);
      }
    } catch (Exception e) {
      throw new ConsignmentPurchaseEventImportException(
          "Cannot map Consignment/Purchase event from CSV data row, Due to: " + e.getMessage(), e);
    }

    return dto;
  }



}
