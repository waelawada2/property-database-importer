package com.sothebys.propertydb.importer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Gregor Zurowski
 */
@SpringBootApplication
public class PropertyImportApplication {

    public static void main(String[] args) {
        SpringApplication.run(PropertyImportApplication.class, args);
    }

}
