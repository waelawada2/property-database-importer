package com.sothebys.propertydb.importer.model.item;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = false)
@EqualsAndHashCode(callSuper = false)
public class ItemObjectDto {

  @NotNull
  private String objectId;
}
