package com.sothebys.propertydb.importer.model.item;

import com.sothebys.propertydb.importer.model.event.DocumentBaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AuctionTermDto extends DocumentBaseDto {

  private String name;
}