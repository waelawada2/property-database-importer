package com.sothebys.propertydb.importer.model.event;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
public class ValuationTypeDto extends DocumentBaseDto {

  private String name;
}
