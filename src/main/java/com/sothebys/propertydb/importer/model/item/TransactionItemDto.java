package com.sothebys.propertydb.importer.model.item;


import com.sothebys.propertydb.importer.model.event.ConsignmentPurchaseEventDto;
import com.sothebys.propertydb.importer.model.event.TransactionType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TransactionItemDto extends ItemDto {

  public TransactionItemDto() {
    this.type = ItemType.TRANSACTION;
  }

  private TransactionType transactionType;

  private String lotNumber;

  private TransactionStatusDto status;
  
  private ConsignmentPurchaseEventDto consignmentEvent;

  private ConsignmentPurchaseEventDto purchaseEvent;

  private AuctionDto auction;

  private PrivateSaleDto privateSale;

}
