package com.sothebys.propertydb.importer.model.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class EventDto extends DocumentBaseDto {

  protected String code;

  protected EventType type;

  protected String notes;
}
