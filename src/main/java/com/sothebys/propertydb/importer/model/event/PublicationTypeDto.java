package com.sothebys.propertydb.importer.model.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PublicationTypeDto extends DocumentBaseDto {

  private String code;

  private String value;
}
