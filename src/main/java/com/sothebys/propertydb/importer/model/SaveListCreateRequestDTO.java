package com.sothebys.propertydb.importer.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * This class getters & setters for all the properties needed to create a Save List.
 * 
 * @author VenkataPrasd Tammineni
 *
 */
public class SaveListCreateRequestDTO {

  @NotNull
  private String name;

  @NotNull
  private String notes;

  @NotNull
  private boolean openSaveList;

  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date date;

  @NotNull
  @Size(min = 1)
  private List<String> propertyIds;

  @NotNull
  @Size(min = 1)
  private List<String> sharedUsersList;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public boolean isOpenSaveList() {
    return openSaveList;
  }

  public void setOpenSaveList(boolean openSaveList) {
    this.openSaveList = openSaveList;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public List<String> getPropertyIds() {
    return propertyIds;
  }

  public void setPropertyIds(List<String> propertyIds) {
    this.propertyIds = propertyIds;
  }

  public List<String> getSharedUsersList() {
    return sharedUsersList;
  }

  public void setSharedUsersList(List<String> sharedUsersList) {
    this.sharedUsersList = sharedUsersList;
  }
}
