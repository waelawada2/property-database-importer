package com.sothebys.propertydb.importer.model.event;

public enum EventType {
  TRANSACTION,
  PURCHASE,
  CONSIGNMENT,
  PUBLICATION,
  PRICING,
  EXHIBITION
}
