package com.sothebys.propertydb.importer.model.event;

import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ExhibitionEventDto extends EventDto {

  public ExhibitionEventDto() {
    this.type = EventType.EXHIBITION;
  }

  private List<PublicationEventDto> publicationEvents;

}
