package com.sothebys.propertydb.importer.model.item;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ItemType {
  PRICING("Pricing"),
  TRANSACTION("Transaction"),
  PUBLICATION("Publication");

  private final String value;
}
