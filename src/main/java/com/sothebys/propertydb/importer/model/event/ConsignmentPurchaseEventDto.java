package com.sothebys.propertydb.importer.model.event;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ConsignmentPurchaseEventDto extends EventDto {

  public ConsignmentPurchaseEventDto(EventType type) {
    this.type = type;
  }
  private MainClientDto mainClient;

  private AffiliatedClientDto affiliatedClient;

}
