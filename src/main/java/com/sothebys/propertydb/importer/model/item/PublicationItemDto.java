package com.sothebys.propertydb.importer.model.item;

import com.sothebys.propertydb.importer.model.event.ConsignmentPurchaseEventDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class PublicationItemDto extends ItemDto {

  public PublicationItemDto() {
    this.type = ItemType.PUBLICATION;
  }

  private PublicationItemTypeDto publicationItemType;

  private String cataloguingPage;

  private String number;

  private String figure;

  private String plate;

  private ColourTypeDto colourType;

  private ConsignmentPurchaseEventDto consignmentEvent;

  private String illustration;

}
