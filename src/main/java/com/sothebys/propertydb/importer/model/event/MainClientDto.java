package com.sothebys.propertydb.importer.model.event;

import lombok.Data;

@Data
public class MainClientDto {

  private String clientId;

  private ClientSourceDto clientSource;
  
  private ClientSourceTypeDto clientSourceType;

  private ClientStatusDto clientStatus;

  private String display;

}
