package com.sothebys.propertydb.importer.model.event;

import lombok.Data;

@Data
public class AffiliatedClientDto {

  private String clientId;

  private ClientSourceDto clientSource;

  private ClientSourceTypeDto clientSourceType;

  private AffiliatedTypeDto affiliatedType;

}
