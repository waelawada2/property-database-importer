package com.sothebys.propertydb.importer.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * A DTO representation of a property that is used to create a Artist using the Artist service API.
 * <p/>
 * This DTO implements version 0.1.0 of the interface specification documented in
 * <a href="https://sothebys.jira.com/wiki/display/CORE/Property+Database+Artist+Import+CSV">Property Database Artist Import CSV</a>.
 *
 * @author Gregor Zurowski
 */
public class ArtistCreateRequestDTO {

  private Long ulanId;

  private String id;

  private String firstName;

  private String lastName;

  private Integer birthYear;

  private Integer deathYear;

  private String display;
  
  private String externalId;

  private List<CountryDTO> countries = new ArrayList<>();

  private String gender;

  private List<ArtistCategory> categories = new ArrayList<>();

  private List<ArtistCatalogueRaisonee> artistCatalogueRaisonees = new ArrayList<>();

  private String notes;

  private List<String> errorDetailsList;

  public Long getUlanId() {
    return ulanId;
  }

  public void setUlanId(Long ulanId) {
    this.ulanId = ulanId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Integer getBirthYear() {
    return birthYear;
  }

  public void setBirthYear(Integer birthYear) {
    this.birthYear = birthYear;
  }

  public Integer getDeathYear() {
    return deathYear;
  }

  public void setDeathYear(Integer deathYear) {
    this.deathYear = deathYear;
  }

  public String getDisplay() {
    return display;
  }

  public void setDisplay(String display) {
    this.display = display;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public List<ArtistCategory> getCategories() {
    return categories;
  }

  public void setCategories(List<ArtistCategory> categories) {
    this.categories = categories;
  }

  public List<ArtistCatalogueRaisonee> getArtistCatalogueRaisonees() {
    return artistCatalogueRaisonees;
  }

  public void setArtistCatalogueRaisonees(List<ArtistCatalogueRaisonee> artistCatalogueRaisonees) {
    this.artistCatalogueRaisonees = artistCatalogueRaisonees;
  }

  public List<CountryDTO> getCountries() {
    return countries;
  }

  public void setCountries(List<CountryDTO> countries) {
    this.countries = countries;
  }

  public String getExternalId() {
    return externalId;
  }

  public void setExternalId(String externalId) {
    this.externalId = externalId;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public List<String> getErrorDetailsList() {
    return errorDetailsList;
  }

  public void setErrorDetailsList(List<String> errorDetailsList) {
    this.errorDetailsList = errorDetailsList;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  public static final class ArtistCatalogueRaisonee { 


    private String author;

    private String type;

    private String volumes;

    private Integer year;
    
    public ArtistCatalogueRaisonee(){
      
    }


    public ArtistCatalogueRaisonee(String author, String type, String volumes, Integer year) {
      this.author = author;
      this.type = type;
      this.volumes = volumes;
      this.year = year;
    }

    public String getAuthor() {
      return author;
    }

    public void setAuthor(String author) {
      this.author = author;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getVolumes() {
      return volumes;
    }

    public void setVolumes(String volumes) {
      this.volumes = volumes;
    }

    public Integer getYear() {
      return year;
    }

    public void setYear(Integer year) {
      this.year = year;
    }

    @Override
    public String toString() {
      return ToStringBuilder.reflectionToString(this);
    }

  }

  public static final class ArtistCategory {

    private String name;
    
    public ArtistCategory(){
      
    }

    public ArtistCategory(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    @Override
    public String toString() {
      return ToStringBuilder.reflectionToString(this);
    }
  }
  
  public static final class CountryDTO {

    public CountryDTO(String countryName) {
      this.countryName = countryName;
    }
    
    public CountryDTO(){
      
    }

    private String countryName;


    public String getCountryName() {
      return countryName;
    }


    public void setCountryName(String countryName) {
      this.countryName = countryName;
    }

    @Override
    public String toString() {
      return ToStringBuilder.reflectionToString(this);
    }

  }

}

