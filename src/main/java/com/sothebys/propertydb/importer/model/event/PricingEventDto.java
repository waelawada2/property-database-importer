package com.sothebys.propertydb.importer.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PricingEventDto extends EventDto {

  public PricingEventDto() {
    this.type = EventType.PRICING;
  }
  
  private ValuationOriginDto valuationOrigin;
  
  private ValuationTypeDto valuationType;
  
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  protected Date date;
  
  private String valuationId;
  
  private String proposal;
  
  private MainClientDto mainClient;
  
  private AffiliatedClientDto affiliatedClient;  


}
