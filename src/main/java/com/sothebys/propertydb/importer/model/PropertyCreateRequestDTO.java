package com.sothebys.propertydb.importer.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * A DTO representation of a property that is used to create a property using the property service
 * API.
 * <p/>
 * This DTO implements version 0.2.4 of the interface specification documented in <a
 * href="https://sothebys.jira.com/wiki/display/CORE/Property+Database+Import+CSV">Property Database
 * Import CSV</a>.
 *
 * @author Gregor Zurowski
 */
@Data
public class PropertyCreateRequestDTO {


  private Integer itemId;

  private String sortCode;

  private String artistCode;

  private List<String> artistIds = new ArrayList<>();

  private List<Long> ulanIds = new ArrayList<>();

  private String artistName;

  private List<CatalogueRaisonee> propertyCatalogueRaisonees;

  private String title;

  private String originalTitle;

  private String signature;

  private String medium;

  private String category;

  private String subCategory;

  private Boolean yearCirca;

  private Integer yearEnd;

  private Integer yearStart;

  private Boolean castYearCirca;

  private Integer castYearStart;

  private Integer castYearEnd;

  private Boolean posthumous;

  private String yearText;

  private String presizeName;

  private BigDecimal heightIn;

  private BigDecimal widthIn;

  private BigDecimal depthIn;

  private BigDecimal heightCm;

  private BigDecimal widthCm;

  private BigDecimal depthCm;

  private BigDecimal weight;

  private Integer parts;

  private String editionSizeTypeName;

  private String editionNumber;

  private String editionSizeNotes;

  private Integer seriesTotal;

  private Integer artistProofTotal;

  private Integer trialProofTotal;

  private Integer bonATirerTotal;

  private Integer printersProofTotal;

  private Integer horsCommerceProofTotal;

  private Integer cancellationProofTotal;

  private Integer colorTrialProofTotal;

  private Integer dedicatedProofTotal;

  private Integer museumProofTotal;

  private Integer progressiveProofTotal;

  private Integer rightToProduceProofTotal;

  private Integer workingProofTotal;

  private String editionOverride;

  private String foundry;

  private Boolean stamped;

  private Boolean signed;

  private Boolean certificate;

  private Boolean authentication;

  private List<ArchiveNumberDTO> archiveNumbers = new ArrayList<>();

  private String researcherName;

  private Date researcherNotesDate;

  private String researcherNotesText;
  
  private Boolean researchCompleteCheck;

  private String imagePath;

  private String editionName;

  private FlagsRequestDTO flags;

  private TagsRequestDTO tags;

  private String sizeText;

  private List<String> objectCodes = new ArrayList<>();

  private String id;

  private String externalId;

  private List<String> errorDetailsList;

  @Data
  @NoArgsConstructor
  public static final class ArchiveNumberDTO {

    private String archiveNumber;
    private Integer id;

    public ArchiveNumberDTO(Integer id, String archiveNumber) {
      this.id = id;
      this.archiveNumber = archiveNumber;
    }
  }

  @Data
  public static final class FlagsRequestDTO {

    private Boolean flagAuthenticity;

    private Boolean flagCondition;

    private Boolean flagMedium;

    private Boolean flagRestitution;

    private Boolean flagSfs;

    private String flagsCountry;
  }

  @Data
  @NoArgsConstructor
  public static final class CatalogueRaisonee {

    private Long artistCatalogueRaisoneeId;
    private Long id;
    private String number;
    private String volume;

    public CatalogueRaisonee(String number, String volume) {
      this.number = number;
      this.volume = volume;
    }
  }

  @Data
  public static final class TagsRequestDTO {

    private String imageText;

    private String imageFigure;

    private String expertiseLocation;

    private List<String> imageSubjects;

    private List<String> imageGenders;

    private List<String> imagePositions;

    private List<String> imageAnimals;

    private List<String> imageMainColours;

    private List<ExpertiseSitterDTO> expertiseSitters;
    
    @Data
    @NoArgsConstructor
    public static final class ExpertiseSitterDTO {

      private String displayName;
      private String firstName;
      private String lastName;
      private Long id;

      public ExpertiseSitterDTO(Long id, String displayName) {
        this.id = id;
        this.displayName = displayName;
      }
    }

    private List<String> otherTags;
  }
}
