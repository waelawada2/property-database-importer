package com.sothebys.propertydb.importer.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PublicationEventDto extends EventDto {

  public PublicationEventDto() {
    this.type = EventType.PUBLICATION;
  }

  private String title;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  protected Date date;

  private String subTitle;

  private String author;

  private EstablishmentDto establishment;

  private String volume;

  private String edition;

  private int pages;

  private boolean verified;

  private String isbn;

  private PublicationTypeDto publicationType;

  private List<PublicationEventDto> publicationEvents;

  //TODO: This should be changed to a list of ExhibitionEvents once Exhibition events are
  // implemented in this service.
  private List<String> exhibitionEvents;
  
  private String libraryCode;
   
  private String imageName;
  
  private String originalImageName;

  private boolean updateImage;

}
