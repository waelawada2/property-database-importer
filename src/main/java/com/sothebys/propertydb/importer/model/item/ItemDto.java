package com.sothebys.propertydb.importer.model.item;

import java.util.List;

import com.sothebys.propertydb.importer.model.event.DocumentBaseDto;
import com.sothebys.propertydb.importer.model.event.EventDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ItemDto extends DocumentBaseDto {
  
  protected String code;

  protected ItemType type;

  protected EventDto event;

  protected List<ItemObjectDto> objects;

  protected String legacyId;

  protected String comments;

  protected String description;

  protected String provenance;

  protected String exhibition;

  protected String literature;

  protected String condition;

  protected String cataloguingNotes;

}
