package com.sothebys.propertydb.importer.model.item;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AuctionDto {

  private boolean estimateOnRequest;

  private BigDecimal lowEstimate;

  private BigDecimal highEstimate;

  private AuctionTermDto term;

  private BigDecimal hammer;

  private BigDecimal premium;
  
  private BigDecimal unsoldHammer;
}