package com.sothebys.propertydb.importer.model.item;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class PrivateSaleDto {

  private BigDecimal netBuyer;

  private BigDecimal netVendor;
}