package com.sothebys.propertydb.importer.model.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class TransactionEventDto extends EventDto {

  public TransactionEventDto() {
    this.type = EventType.TRANSACTION;
  }

  private String title;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  protected Date date;

  private String saleNumber;

  private Double totalLots;

  private TransactionType transactionType;

  private CurrencyDto currency;

  private EstablishmentDto establishment;

  private DepartmentDto department;
}
