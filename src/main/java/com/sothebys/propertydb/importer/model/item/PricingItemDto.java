package com.sothebys.propertydb.importer.model.item;

import java.math.BigDecimal;

import com.sothebys.propertydb.importer.model.event.CurrencyDto;
import com.sothebys.propertydb.importer.model.event.ExhibitionEventDto;
import com.sothebys.propertydb.importer.model.event.TransactionEventDto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PricingItemDto extends ItemDto {

  public PricingItemDto() {
    this.setType(ItemType.PRICING);
  }

  private CurrencyDto auctionCurrency;

  private BigDecimal auctionLowValue;

  private BigDecimal auctionHighValue;

  private boolean auctionPlusOne;

  private CurrencyDto privateSaleCurrency;

  private BigDecimal privateSaleNetSeller;

  private CurrencyDto insuranceCurrency;

  private BigDecimal insurancePrice;

  private CurrencyDto fairMarketValueCurrency;
  
  private BigDecimal fairMarketValuePrice;

  private TransactionEventDto pipelineAuction;

  private boolean pipelineAuctionTarget;

  private TransactionEventDto pipelinePrivateSale;

  private boolean pipelinePrivateSaleTarget;
  
  private ExhibitionEventDto pipelineExhibition;

  private boolean pipelineExhibitionTarget;

}
