package com.sothebys.propertydb.importer.model.event;

import com.sothebys.propertydb.importer.exception.EnumNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TransactionType {
  AUCTION("Auction"),
  PRIVATE_SALE("Private Sale");


  private String value;

  public static TransactionType findByValue(String value) throws EnumNotFoundException {
    for (TransactionType transactionType : values()) {
      if (transactionType.value.equals(value)) {
        return transactionType;
      }
    }
    throw new EnumNotFoundException("Cannot find transaction type enum from value " + value);
  }
}
