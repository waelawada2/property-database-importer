package com.sothebys.propertydb.importer.model;

/**
 * A DTO representation of a WebsiteImages that is used to create a WebsiteImages using the
 * lotsImages service API.
 * <p/>
 * This DTO implements version 0.1.0 of the interface specification documented in <a href=
 * "https://sothebys.jira.com/wiki/spaces/CORE/pages/180752002/Website+Images+Import+CSV">Property
 * Database WebsiteImages Import CSV</a>.
 *
 * @author SrinivasaRao Batthula
 */
public class WebsiteImagesCreateRequestDTO {

  private String itemId;

  private String saleId;

  private String lotsNumber;

  public String getSaleId() {
    return saleId;
  }

  public void setSaleId(String saleId) {
    this.saleId = saleId;
  }

  public String getLotsNumber() {
    return lotsNumber;
  }

  public void setLotsNumber(String lotsNumber) {
    this.lotsNumber = lotsNumber;
  }

  public String getItemId() {
    return itemId;
  }

  public void setItemId(String itemId) {
    this.itemId = itemId;
  }

}

