package com.sothebys.propertydb.importer.model.item;

import com.sothebys.propertydb.importer.model.event.DocumentBaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PublicationItemTypeDto extends DocumentBaseDto {

  private String value;
}
