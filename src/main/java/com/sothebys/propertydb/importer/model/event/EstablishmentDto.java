package com.sothebys.propertydb.importer.model.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class EstablishmentDto extends DocumentBaseDto {

  private String code;

  private String name;

  private String cityCode;

  private String cityName;

  private String countryCode;

  private String countryName;

  private String state;

  private String region;
}
