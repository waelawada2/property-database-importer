package com.sothebys.propertydb.importer.model.event;

import lombok.Data;

@Data
public class ImageDto {
  
  public String imageName;

}
