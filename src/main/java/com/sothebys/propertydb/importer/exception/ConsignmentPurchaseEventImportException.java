package com.sothebys.propertydb.importer.exception;

public class ConsignmentPurchaseEventImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public ConsignmentPurchaseEventImportException() {
    super();
  }

  public ConsignmentPurchaseEventImportException(String message) {
    super(message);
  }

  public ConsignmentPurchaseEventImportException(String message, Throwable cause) {
    super(message, cause);
  }

}
