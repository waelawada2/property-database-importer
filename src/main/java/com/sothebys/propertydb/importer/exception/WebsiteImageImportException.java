package com.sothebys.propertydb.importer.exception;

/**
 * Exception that is thrown when an error occurs while importing Website images.
 *
 * @author SrinivasaRao Batthula
 */
public class WebsiteImageImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public WebsiteImageImportException() {
    super();
  }

  public WebsiteImageImportException(String message) {
    super(message);
  }

  public WebsiteImageImportException(String message, Throwable cause) {
    super(message, cause);
  }

}
