package com.sothebys.propertydb.importer.exception;

public class ClientSourceImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public ClientSourceImportException() {
    super();
  }

  public ClientSourceImportException(String message) {
    super(message);
  }

  public ClientSourceImportException(String message, Throwable cause) {
    super(message, cause);
  }
}
