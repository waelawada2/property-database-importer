package com.sothebys.propertydb.importer.exception;

/**
 * Base exception for property service.
 *
 * @author VenkataPrasad Tammineni
 */
public class ImporterException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public ImporterException() {
    super();
  }

  public ImporterException(String message) {
    super(message);
  }

  public ImporterException(String message, Throwable cause) {
    super(message, cause);
  }

}
