package com.sothebys.propertydb.importer.exception;

public class PublicationItemImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public PublicationItemImportException() {
    super();
  }

  public PublicationItemImportException(String message) {
    super(message);
  }

  public PublicationItemImportException(String message, Throwable cause) {
    super(message, cause);
  }
}
