package com.sothebys.propertydb.importer.exception;

public class EstablishmentImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public EstablishmentImportException() {
    super();
  }

  public EstablishmentImportException(String message) {
    super(message);
  }

  public EstablishmentImportException(String message, Throwable cause) {
    super(message, cause);
  }
}
