package com.sothebys.propertydb.importer.exception;

public class TransactionEventImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public TransactionEventImportException() {
    super();
  }

  public TransactionEventImportException(String message) {
    super(message);
  }

  public TransactionEventImportException(String message, Throwable cause) {
    super(message, cause);
  }
}
