package com.sothebys.propertydb.importer.exception;

public class PublicationEventImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public PublicationEventImportException() {
    super();
  }

  public PublicationEventImportException(String message) {
    super(message);
  }

  public PublicationEventImportException(String message, Throwable cause) {
    super(message, cause);
  }
}
