package com.sothebys.propertydb.importer.exception;

/**
 * Exception that is thrown when an error occurs while importing Artist data.
 *
 * @author VenkataPrasad Tammineni
 */
public class ArtistImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public ArtistImportException() {
    super();
  }

  public ArtistImportException(String message) {
    super(message);
  }

  public ArtistImportException(String message, Throwable cause) {
    super(message, cause);
  }

}
