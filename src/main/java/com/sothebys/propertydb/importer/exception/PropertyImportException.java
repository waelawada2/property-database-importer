package com.sothebys.propertydb.importer.exception;


/**
 * Exception that is thrown when an error occurs while importing data.
 *
 * @author Gregor Zurowski
 */
public class PropertyImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public PropertyImportException() {
    super();
  }

  public PropertyImportException(String message) {
    super(message);
  }

  public PropertyImportException(String message, Throwable cause) {
    super(message, cause);
  }

}
