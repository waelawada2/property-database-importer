package com.sothebys.propertydb.importer.exception;

public class PricingItemImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public PricingItemImportException() {
    super();
  }

  public PricingItemImportException(String message) {
    super(message);
  }

  public PricingItemImportException(String message, Throwable cause) {
    super(message, cause);
  }
}
