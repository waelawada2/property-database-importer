package com.sothebys.propertydb.importer.exception;

public class TransactionItemImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public TransactionItemImportException() {
    super();
  }

  public TransactionItemImportException(String message) {
    super(message);
  }

  public TransactionItemImportException(String message, Throwable cause) {
    super(message, cause);
  }
}
