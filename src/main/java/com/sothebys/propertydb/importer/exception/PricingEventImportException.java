package com.sothebys.propertydb.importer.exception;

public class PricingEventImportException extends ImporterException {

  private static final long serialVersionUID = 1L;

  public PricingEventImportException() {
    super();
  }

  public PricingEventImportException(String message) {
    super(message);
  }

  public PricingEventImportException(String message, Throwable cause) {
    super(message, cause);
  }
}
