package com.sothebys.propertydb.importer.exception;

/**
 * Enum not found exception.
 *
 * @author Minhtri Tran
 */
public class EnumNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public EnumNotFoundException() {
    super();
  }

  public EnumNotFoundException(String message) {
    super(message);
  }

  public EnumNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }
}
