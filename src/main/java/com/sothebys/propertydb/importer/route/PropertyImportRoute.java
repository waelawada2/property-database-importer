package com.sothebys.propertydb.importer.route;

import static com.sothebys.propertydb.importer.support.PropertyImporterConstants.TEMP_PATH;
import static org.apache.camel.util.toolbox.AggregationStrategies.flexible;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.importer.exception.PropertyImportException;
import com.sothebys.propertydb.importer.exception.WebsiteImageImportException;
import com.sothebys.propertydb.importer.handler.ImageHandler;
import com.sothebys.propertydb.importer.handler.PropertyCSVDataHandler;
import com.sothebys.propertydb.importer.handler.PropertyErrorHandler;
import com.sothebys.propertydb.importer.handler.SaveListHandler;
import com.sothebys.propertydb.importer.handler.SimpleEmailHandler;
import com.sothebys.propertydb.importer.handler.WebsiteImagesCSVDataHandler;
import com.sothebys.propertydb.importer.handler.WebsiteImagesHandler;
import com.sothebys.propertydb.importer.support.ArtistCatalogueRaisoneeIdAggregationStrategy;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.PropertyInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

/**
 * Camel route that fetches CSV files that contain property data and pushes the data into the
 * property database using the property API.
 *
 * @author Gregor Zurowski
 */
@Component
public class PropertyImportRoute extends RouteBuilder {

  @Autowired
  protected String propertyServiceAuthorization;

  @Autowired
  @Qualifier("executorServiceWebsiteImages")
  protected ExecutorService executorServiceWebsiteImages;

  @Autowired
  @Qualifier("executorServicePushCsv")
  protected ExecutorService executorServicePushCsv;

  private static final String[] WEBSITE_IMAGE_IMPORTER_HEADER =
      {"ITEM ID", "SALE_SALE_ID", "LOTS_LOT_ID_TXT", "Details"};

  private static final String TMP_PATH = System.getProperty(TEMP_PATH);

  private static final String[] HEADER = {"Object_Code", "EOS_Object_ID", "Sort_Code", "Artist_ID",
      "Artist_Code", "Artist_Name", "CR_Volume", "CR_Number", "Title_(Orig)", "Title_(EN)",
      "Signature_Details", "Medium", "Category", "Subcategory", "Year_Circa_check", "Year1",
      "Year2", "Cast_Circa_check", "Cast_Year1", "Cast_Year2", "PH", "Year_Override",
      "Pre-size_Text", "H_in", "W_in", "D_in", "H_cm", "W_cm", "D_cm", "Measurement_Override",
      "Weight_kg", "Parts", "Edition_Status", "Edition_#", "Edition_Type", "Series_Total",
      "AP_Total", "BTP_Total", "CP_Total", "CTP_Total", "DP_Total", "FP/PP_Total", "HCP_Total",
      "MP_Total", "PRP_Total", "RTP_Total", "TP_Total", "WP_Total", "Foundry", "Edition_Override",
      "Edition_Notes", "Stamped_check", "Signed_check", "Certificate_check", "Authentication_check",
      "Archive_#", "Flag_Authenticity_check", "Flag_Restitution_check", "Flag_Condition_check",
      "Flag_SFS_check", "Flag_Medium_check", "Flag_Export_Country", "Tag_Subject", "Tag_Figure",
      "Tag_Gender", "Tag_Position", "Tag_Animal", "Tag_Main_Colour", "Tag_Text", "Tag_Location",
      "Tag_Sitter", "Tag_Other", "Auto_Orientation", "Auto_Image", "Auto_Scale", "Auto_Area_cm",
      "Research_Complete_check", "Researcher_Name", "Researcher_Notes_Date",
      "Researcher_Notes_Text", "Image_File_Name", "Image_Update_Y/N", "Error_Details"};

  @PropertyInject("router.time-delay")
  private int routerTimeDelay;

  @Override
  public void configure() throws Exception {

    // @formatter:off
    from("file:propertyData?delete={{routes.push-csv-to-service.delete-source-file}}")
        .streamCaching()
        .routeId("push-csv-to-service")
        .onException(HttpOperationFailedException.class)
          .handled(true)
          .maximumRedeliveries(0)
          .log(LoggingLevel.ERROR, "Service error (HTTP ${exception.statusCode}) - Error: ${exception.responseBody} - message: ${exchangeProperty.csvRowData} -- parentBody: ${property.parentBody}")
          .bean(PropertyErrorHandler.class,"addFailedRecordToList")
          .choice()
            .when().simple("${property.createProperty} == true")
              .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.DELETE))
              .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic " + propertyServiceAuthorization))
              .setHeader(Exchange.HTTP_URI, simple("http://{{backend.property-service.host}}/properties/${property.propertyId}"))
              .to("http4:deleteProperty")
              .log(LoggingLevel.INFO, "Deleting created property ${property.propertyId}")
            .otherwise()
              .log(LoggingLevel.INFO, "Property couldn't be created or it was an update request, No delete action required!!!")
          .end()
        .end()
        .onException(PropertyImportException.class)
          .handled(true)
          .maximumRedeliveries(0)
          .log(LoggingLevel.ERROR, "PropertyImportException Exception thrown while importing data - Error: ${exception.message} - message: ${exchangeProperty.csvRowData}")
          .bean(PropertyErrorHandler.class,"addFailedRecordToList")
        .end()
        .onException(HttpHostConnectException.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Error connecting to artist service host: ${exception.host}")
          .bean(PropertyErrorHandler.class,"addFailedRecordToList")
        .end()
        .onException(Exception.class)
          .handled(true)
          .maximumRedeliveries(0)
          .log(LoggingLevel.ERROR, "Exception thrown while importing data - Error: ${exception.message} - message: ${exception}")
          .bean(PropertyErrorHandler.class,"addFailedRecordToList")
        .end()
        .unmarshal(new CsvDataFormat().setSkipHeaderRecord(true).setNullString(EMPTY).setLazyLoad(true))
        .setProperty("headersBeforeCsvSplit", simple("${headers}"))
        .setProperty("numberOfCSVRows", constant(0))
        .delay(routerTimeDelay)
        .split(body(), flexible()
            .condition(exchangeProperty("csvSplitComplete"))
            .pick(exchangeProperty("csvSplitSize"))
            .storeInProperty("numberOfCSVRows")
            .completionAware(exchange-> {
              if (exchange == null) {
                return;
              }

              exchange.removeProperties("CamelFailure*");
              exchange.removeProperties("CamelException*");
              exchange.removeProperties("CamelError*");

              @SuppressWarnings("unchecked")
              Map<String, Object> headersBeforeCsvSplit = exchange.getProperty("headersBeforeCsvSplit", Map.class);
              exchange.getIn().setHeaders(headersBeforeCsvSplit);
            }))
          .streaming()
          .parallelProcessing().executorService(executorServicePushCsv)
          .setProperty("csvRowData").simple("${body}", List.class)
          .setProperty("csvSplitComplete", exchangeProperty(Exchange.SPLIT_COMPLETE))
          .setProperty("csvSplitSize", exchangeProperty(Exchange.SPLIT_SIZE))
          .bean(PropertyCSVDataHandler.class, "handle")
          .filter(simple("${body.propertyCatalogueRaisonees} != null"))
            .enrich("direct:addArtistCRIds", flexible().pick(body()).storeInBody())
          .end()
          .marshal().json(JsonLibrary.Jackson)
          .log(LoggingLevel.INFO, "Sending message: ${body}")
          .setProperty("imageName").jsonpath("$.imagePath")
          .setProperty("id").jsonpath("$.externalId")
          .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic " + propertyServiceAuthorization))
          .setProperty("imageDrop").simple("${property.imageName} == ''", Boolean.class)
          .log(LoggingLevel.INFO, "Image drop: ${property.imageDrop}")
          .setProperty("parentBody").simple("${body}")
          .log(LoggingLevel.INFO, "Parent body: ${property.parentBody}")
          .choice()
            .when().simple("${property.id} != null")
              .setHeader(Exchange.HTTP_QUERY, simple("dropImage=${property.imageDrop}"))
              .setHeader(Exchange.HTTP_PATH, simple("${property.id}"))
              .to("rest:PUT:properties?host={{backend.property-service.host}}")
            .otherwise()
              .to("rest:POST:properties?host={{backend.property-service.host}}")
              .setProperty("createProperty").simple("true")
          .end()
          .log(LoggingLevel.INFO, "Property response: ${body}")
          .setProperty("propertyId").jsonpath("$.id", true)
          .choice()
            .when(exchangeProperty("propertyId").isNull())
              .throwException(PropertyImportException.class, "Unexpected rest response (no id returned)")
            .otherwise()
          .end()
          .bean(ImageHandler.class, "setS3ImageToBodyAsMultiPart('${property.imageName}')")
          .log(LoggingLevel.INFO, "Setting Multipart Image from S3: ${property.imageName}")
          .choice()
            .when().simple("${in.body} != null")
              .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic " + propertyServiceAuthorization))
              .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.POST))
              .setHeader(Exchange.HTTP_PATH, simple(""))
              .setHeader(Exchange.HTTP_URI, simple("http://{{backend.property-service.host}}/properties/${property.propertyId}/images"))
              .to("http4:uploadImage")
              .log(LoggingLevel.INFO, "Image Endpoint response : ${body}")
              .bean(SaveListHandler.class, "addPropertyIdToList('${property.propertyId}')")
            .otherwise()
              .log("MultipartFormEntity request as empty")
              .bean(SaveListHandler.class, "addPropertyIdToList('${property.propertyId}')")
          .end()
        .end()
        .bean(PropertyErrorHandler.class,"getFailRecordsList")
        .setProperty("badCsvData", body())
        .choice()
          .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
            .setBody(simple("${exchangeProperty.badCsvData}"))
            .marshal(new CsvDataFormat().setHeader(HEADER))
            .setProperty("badRowsBody").simple("${body}")
          .end()
        .to("direct:sendImportReport")
        .end()
        .bean(PropertyErrorHandler.class, "clearFailRecordsList")
        .setProperty("saveListPropertyIds").method(SaveListHandler.class, "propertyIdsList")
        .bean(SaveListHandler.class, "clearPropertyIdsList")
        .to("direct:createSaveListAndSendStatusEmail")
        .end()
        .log("Completed import of '${header.CamelFileName}'");

    from("direct:addArtistCRIds")
        .streamCaching()
        .routeId("property-add-artist-cr-ids")
        .errorHandler(noErrorHandler())
        .setProperty("propertyDto", body())
        .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+ propertyServiceAuthorization))
        .split(simple("${body.artistIds}"), new ArtistCatalogueRaisoneeIdAggregationStrategy())
          .setHeader(Exchange.HTTP_PATH, body())
          .setBody(constant(null))
          .to("rest:GET:artists?host={{backend.property-service.host}}")
          .setProperty("raisoneeIds").jsonpath("$.artistCatalogueRaisonees[*].id")
          .setProperty("raisoneeSortIds").jsonpath("$.artistCatalogueRaisonees[*].sortId")
        .end()
        .bean(PropertyCSVDataHandler.class, "addArtistCRIds");

    from("direct:createSaveListAndSendStatusEmail")
        .streamCaching()
        .routeId("create-save-list-and-send-status-email")
        .onException(HttpOperationFailedException.class)
          .continued(true)
          .setProperty("saveListErrorMessage", simple("Service error when trying to create save list (HTTP ${exception.statusCode}) - Error: ${exception.responseBody}"))
          .log(LoggingLevel.ERROR, "${exchangeProperty.saveListErrorMessage}")
        .end()
        .onException(Exception.class)
          .continued(true)
          .setProperty("saveListErrorMessage", simple("Error occurred when trying to create save list: ${exception}"))
          .log(LoggingLevel.ERROR, "${exchangeProperty.saveListErrorMessage}: ${exception.stacktrace}")
        .end()
        .choice()
          .when().simple("${exchangeProperty.saveListPropertyIds.size} > 0")
            .bean(SaveListHandler.class, "createSaveListDTO")
            .marshal().json(JsonLibrary.Jackson)
            .log(LoggingLevel.INFO, "SaveList Sending message: ${body}")
            .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic " + propertyServiceAuthorization))
            .to("rest:POST:users/{{backend.property-service.username}}/lists?host={{backend.property-service.host}}")
            .log(LoggingLevel.INFO, "SaveList response: ${body}")
            .setProperty("saveListResponsePropertyCount").jsonpath("$.propertyIds.length()", true, Integer.class)
            .setProperty("saveListId").jsonpath("$.id", true)
            .filter(simple("${exchangeProperty.saveListId} == null && ${exchangeProperty.saveListErrorMessage} == null"))
              .setProperty("saveListErrorMessage", constant("Unexpected rest response (no id returned) when trying to create list"))
              .log(LoggingLevel.ERROR, "${exchangeProperty.saveListErrorMessage}")
            .endChoice()
          .otherwise()
            .setProperty("saveListErrorMessage", constant("Did not create save list because property count is 0"))
            .log(LoggingLevel.INFO, "${exchangeProperty.saveListErrorMessage}")
        .end()
        .bean(SimpleEmailHandler.class, "prepareSaveListStatusEmailBody")
        .setHeader(Exchange.CONTENT_TYPE, simple("{{routes.email.content-type}}"))
        .setHeader("subject", simple("{{routes.email.save-list-status-subject}}"))
        .log("Save List Status Email Body message -- ${body}")
        .to("smtp://{{routes.email.host}}?from={{routes.email.from}}&to={{routes.email.to}}")
        .choice()
          .when().simple("${exchangeProperty.saveListId} != null")
            .bean(SaveListHandler.class, "createSaveListSharedUsers")
            .marshal().json(JsonLibrary.Jackson)
            .log(LoggingLevel.INFO, "SaveList shared users list : ${body}")
            .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic " + propertyServiceAuthorization))
            .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.PUT))
            .setHeader(Exchange.HTTP_URI, simple("http://{{backend.property-service.host}}/users/{{backend.property-service.username}}/lists/${exchangeProperty.saveListId}/shareusers")) 
            .to("http4:saveListSharedUsers")
            .endChoice()
          .otherwise()
            .log(LoggingLevel.INFO, "Did not create save list shared users because save list as empty")
        .end();

      from("file:websiteImagesData?delete={{routes.push-csv-to-service.delete-source-file}}")
        .streamCaching()
        .routeId("push-csv-website-Images-to-service")
        .onException(HttpOperationFailedException.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Website Images Service error (HTTP ${exception.statusCode}) - Error: ${exception.responseBody} - message: ${body}")
          .bean(WebsiteImagesHandler.class,"addWebsiteImagesRecordToFile('${property.itemId}','${property.saleId}','${property.lotsNumber}','${property.originalImagePath}')")
        .end()
        .onException(WebsiteImageImportException.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Error importing Website Images data - Error: ${exception.message}")
        .end()
        .unmarshal(new CsvDataFormat().setSkipHeaderRecord(true).setNullString(EMPTY).setLazyLoad(true))
        .setProperty("WebsiteImageFileName", method(WebsiteImagesHandler.class,"prepareWebsiteImageFileName('${header.CamelFileName}')"))
        .split(body()).streaming().parallelProcessing().executorService(executorServiceWebsiteImages)
          .bean(WebsiteImagesCSVDataHandler.class)
          .marshal().json(JsonLibrary.Jackson)
          .log(LoggingLevel.INFO, "Sending Website Images message: ${body}")
          .setProperty("itemId").jsonpath("$.itemId")
          .setProperty("saleId").jsonpath("$.saleId")
          .setProperty("lotsNumber").jsonpath("$.lotsNumber")
          .setHeader(Exchange.HTTP_QUERY, simple("saleNumber=${property.saleId}&lotNumbers=${property.lotsNumber}"))
          .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.GET))
          .setHeader(Exchange.HTTP_URI, simple("{{backend.lotsimages-service.url}}"))
          .to("http4:websiteImages")
          .choice()
            .when().jsonpath("$..lots[?(@.originalImagePath != null)]")
              .setProperty("originalImagePath").jsonpath("$.lots[*].originalImagePath")
              .log("Received originalImagePath='${property.originalImagePath}' for saleId='${property.saleId}', lotsNumber='${property.lotsNumber}'")
              .bean(WebsiteImagesHandler.class,"uploadWebsiteImageIntoS3('${property.originalImagePath}','${property.saleId}','${property.lotsNumber}')")
            .otherwise()
              .log("No image path received for saleId='${property.saleId}', lotsNumber='${property.lotsNumber}'")
          .end()
          .bean(WebsiteImagesHandler.class,"addWebsiteImagesRecordToFile('${property.itemId}','${property.saleId}','${property.lotsNumber}','${property.originalImagePath}')")
          .end()
          .bean(WebsiteImagesHandler.class,"getFailRecordsList")
            .marshal(new CsvDataFormat().setDelimiter(',').setHeader(WEBSITE_IMAGE_IMPORTER_HEADER))
            .log("File name for tmp location '${property.WebsiteImageFileName}'.")
            .to("file:"+TMP_PATH+"?fileName=${property.WebsiteImageFileName}")
          .end()
          .bean(WebsiteImagesHandler.class,"uploadWebsiteImageFileIntoS3('${property.WebsiteImageFileName}')")
          .log("Uploaded Website images File Name '${property.WebsiteImageFileName}' into S3")
          .bean(WebsiteImagesHandler.class,"clearWebsiteImagesList")
        .end()
        .log("Completed import of '${header.CamelFileName}'.");
        // @formatter:on
  }

}
