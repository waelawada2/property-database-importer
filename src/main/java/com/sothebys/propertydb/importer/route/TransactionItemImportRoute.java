package com.sothebys.propertydb.importer.route;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.exception.TransactionItemImportException;
import com.sothebys.propertydb.importer.handler.TransactionItemCSVDataHandler;
import com.sothebys.propertydb.importer.support.CsvAggregationStrategy;
import com.sothebys.propertydb.importer.support.TransactionItemCsvFields;

@Component
public class TransactionItemImportRoute extends RouteBuilder {

  @BeanInject
  private TransactionItemCSVDataHandler transactionItemCSVDataHandler;
  
  @Autowired
  private String propertyServiceAuthorization;
  
  @Qualifier("executorServiceTransactionItemCsv")
  @Autowired
  private ExecutorService executorService;

  @Override
  public void configure() throws Exception {
    String[] header = Arrays.stream(TransactionItemCsvFields.values())
        .map(TransactionItemCsvFields::getText).toArray(String[]::new);

    Map<String, String> csvFieldsByEntityAttribute = new HashMap<>();

    mapFields(csvFieldsByEntityAttribute);

    // @formatter:off
    from("file:transactionItemData?delete={{routes.push-csv-to-service.delete-source-file}}")
    .streamCaching()
    .routeId("push-transaction-item-csv-to-service")
    .onException(TransactionItemImportException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error importing transaction item data: ${exception.message} ${exception.stacktrace}")
      .end()
    .onException(HttpHostConnectException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error connecting to transaction item service host: ${exception.host}. Request body: ${body}")
      .end()
    .onException(HttpOperationFailedException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error received from transaction item service: HTTP ${exception.statusCode}. Response body: ${exception.responseBody}. Request body: ${body}")
      .end()
    .onException(Exception.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error: ${exception.message} ${exception.stacktrace}")
      .end()
    .log(LoggingLevel.INFO, "Beginning to import transaction item CSV: ${file:onlyname}")
    .unmarshal(new CsvDataFormat()
        .setSkipHeaderRecord(true)
        .setNullString(EMPTY)
        .setLazyLoad(true))
    .split(body(), new CsvAggregationStrategy())
      .streaming()
      .parallelProcessing().executorService(executorService)
      .setProperty("csvRowData").simple("${body}", List.class)
      .setProperty("csvFieldsByEntityAttribute").constant(csvFieldsByEntityAttribute)
      .bean(transactionItemCSVDataHandler)
      .marshal().json(JsonLibrary.Jackson)
      .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
      .choice()
        .when().simple("${property.itemId} != null")
          .setHeader(Exchange.HTTP_PATH, simple("${property.itemId}"))
          .to("rest:PUT:items?host={{backend.event-service.host}}")
        .otherwise()
          .to("rest:POST:items?host={{backend.event-service.host}}")
        .end()
      .setProperty("responseId").jsonpath("$.id", true)
      .setProperty("idColumnPosition").constant(TransactionItemCsvFields.TRANSACTION_ITEM_ID.getNumber())
      .choice()
        .when(exchangeProperty("responseId").isNull())
          .throwException(TransactionItemImportException.class, "Unexpected rest response (no id returned)")
        .otherwise()
          .log(LoggingLevel.INFO, "Successful rest response, item id: ${property.responseId}")
        .end()
      .end()
    .choice()
      .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
        .setBody(simple("${exchangeProperty.badCsvData}"))
        .marshal(new CsvDataFormat().setHeader(header))
        .setProperty("badRowsBody").simple("${body}")
      .end()
    .choice()
      .when(simple("${exchangeProperty.successfulRecords.size()} > 0"))
        .setBody(simple("${exchangeProperty.successfulRecords}"))
        .marshal(new CsvDataFormat().setHeader(header))
        .setProperty("sucessfulRowsBody").simple("${body}")
      .end()
    .to("direct:sendImportReport").end()
    .log("Completed import for transaction item CSV: '${file:onlyname}'");
 
    // @formatter:on
  }

  private void mapFields(Map<String, String> csvFieldsByEntityAttribute) {
    csvFieldsByEntityAttribute.put("entity.id",
        TransactionItemCsvFields.TRANSACTION_ITEM_ID.getText());
    csvFieldsByEntityAttribute.put("event",
        TransactionItemCsvFields.TRANSACTION_EVENT_ID.getText());
    csvFieldsByEntityAttribute.put("event.id",
        TransactionItemCsvFields.TRANSACTION_EVENT_ID.getText());
    csvFieldsByEntityAttribute.put("consignmentEvent.id",
        TransactionItemCsvFields.CONSIGNMENT_ID.getText());
    csvFieldsByEntityAttribute.put("purchaseEvent.id",
        TransactionItemCsvFields.PURCHASE_ID.getText());
    csvFieldsByEntityAttribute.put("lotNumber", TransactionItemCsvFields.PURCHASE_ID.getText());
    csvFieldsByEntityAttribute.put("auction.term", TransactionItemCsvFields.TERM.getText());
    csvFieldsByEntityAttribute.put("auction.unsoldHammer",
        TransactionItemCsvFields.UNSOLD_HAMMER.getText());
    csvFieldsByEntityAttribute.put("auction.hammer",
        TransactionItemCsvFields.UNSOLD_HAMMER.getText());
    csvFieldsByEntityAttribute.put("auction.premium", TransactionItemCsvFields.PREMIUM.getText());
    csvFieldsByEntityAttribute.put("status", TransactionItemCsvFields.SALE_STATUS.getText());
    csvFieldsByEntityAttribute.put("status.id", TransactionItemCsvFields.SALE_STATUS.getText());
    csvFieldsByEntityAttribute.put("privateSale.netVendor",
        TransactionItemCsvFields.PS_NET_VENDOR.getText());
    csvFieldsByEntityAttribute.put("privateSale.netBuyer",
        TransactionItemCsvFields.PS_NET_BUYER.getText());
    csvFieldsByEntityAttribute.put("legacyId", TransactionItemCsvFields.OLD_ITEM_ID.getText());
    csvFieldsByEntityAttribute.put("comments", TransactionItemCsvFields.ITEM_NOTES.getText());
    csvFieldsByEntityAttribute.put("description", TransactionItemCsvFields.DESCRIPTION.getText());
    csvFieldsByEntityAttribute.put("provenance", TransactionItemCsvFields.PROVENANCE.getText());
    csvFieldsByEntityAttribute.put("exhibition", TransactionItemCsvFields.EXHIBITION.getText());
    csvFieldsByEntityAttribute.put("literature", TransactionItemCsvFields.LITERATURE.getText());
    csvFieldsByEntityAttribute.put("condition", TransactionItemCsvFields.CONDITION.getText());
    csvFieldsByEntityAttribute.put("cataloguingNotes", TransactionItemCsvFields.OD_NOTES.getText());
  }
}
