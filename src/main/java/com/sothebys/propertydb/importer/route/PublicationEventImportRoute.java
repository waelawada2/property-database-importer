package com.sothebys.propertydb.importer.route;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.importer.exception.PublicationEventImportException;
import com.sothebys.propertydb.importer.handler.PublicationEventCSVDataHandler;
import com.sothebys.propertydb.importer.support.CsvAggregationStrategy;
import com.sothebys.propertydb.importer.support.PublicationEventImageAggregationStrategy;
import com.sothebys.propertydb.importer.support.PublicationEventCsvFields;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class PublicationEventImportRoute extends RouteBuilder {

  @BeanInject
  private PublicationEventCSVDataHandler publicationEventCSVDataHandler;

  @Autowired
  private String propertyServiceAuthorization;
  
  @Qualifier("executorServicePublicationEventCsv")
  @Autowired
  private ExecutorService executorService;

  @Override
  public void configure() throws Exception {
    String[] header = Arrays.stream(PublicationEventCsvFields.values())
        .map(PublicationEventCsvFields::getText).toArray(String[]::new);

    Map<String, String> csvFieldsByEntityAttribute = new HashMap<>();

    mapFields(csvFieldsByEntityAttribute);


    //@formatter:off
    from("file:publicationEventData?delete={{routes.push-csv-to-service.delete-source-file}}")
        .streamCaching()
        .routeId("push-publication-event-csv-to-service")
        .onException(PublicationEventImportException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error importing publication event data: ${exception.message} ${exception.stacktrace}")
            .end()
        .onException(HttpHostConnectException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error connecting to publication event service host: ${exception.host}. Request body: ${body}")
            .end()
        .onException(HttpOperationFailedException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error received from publication event service: HTTP ${exception.statusCode}. Response body: ${exception.responseBody}. Request body: ${body}")
            .end()
        .onException(Exception.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error: ${exception.message} ${exception.stacktrace}")
            .end()
        .log(LoggingLevel.INFO, "Beginning to import publication event CSV: ${file:onlyname}")
        .unmarshal(new CsvDataFormat()
            .setSkipHeaderRecord(true)
            .setNullString(EMPTY)
            .setLazyLoad(true))
        .split(body(), new CsvAggregationStrategy())
            .streaming()
            .parallelProcessing().executorService(executorService)
            .setProperty("csvRowData").simple("${body}", List.class)
            .setProperty("csvFieldsByEntityAttribute").constant(csvFieldsByEntityAttribute)
            .bean(publicationEventCSVDataHandler)
            .setProperty("updateImage").simple("${body.isUpdateImage}")
            .setProperty("imageName").simple("${body.getImageName}")
            .choice()
                .when().simple("${property.updateImage} && ${property.imageName} != null && ${property.imageName.trim()} != '' ")
                  .enrich("direct:post-image-to-event-service", new PublicationEventImageAggregationStrategy())
            .end()
            .marshal().json(JsonLibrary.Jackson)
            .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
            .log("Event ID: ${property.eventId}")
            .choice()
                .when().simple("${property.eventId} != null")
                    .setHeader(Exchange.HTTP_PATH, simple("${property.eventId}"))
                    .to("rest:PUT:events?host={{backend.event-service.host}}")
                .otherwise()
                    .to("rest:POST:events?host={{backend.event-service.host}}")
                .end()
            .setProperty("responseId").jsonpath("$.id", true)
            .setProperty("idColumnPosition").constant(PublicationEventCsvFields.ID.getNumber())
            .choice()
                .when(exchangeProperty("responseId").isNull())
                    .throwException(PublicationEventImportException.class, "Unexpected rest response (no id returned)")
                .otherwise()
                .end()
        .end()
        .choice()
          .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
            .setBody(simple("${exchangeProperty.badCsvData}"))
            .marshal(new CsvDataFormat().setHeader(header))
            .setProperty("badRowsBody").simple("${body}")
          .end()
        .choice()
          .when(simple("${exchangeProperty.successfulRecords.size()} > 0"))
            .setBody(simple("${exchangeProperty.successfulRecords}"))
            .marshal(new CsvDataFormat().setHeader(header))
            .setProperty("sucessfulRowsBody").simple("${body}")
          .end()      
        .to("direct:sendImportReport").end()
        .log("Completed import for publication event CSV: '${file:onlyname}'");

    //@formatter:on

  }


  private void mapFields(Map<String, String> csvFieldsByEntityAttribute) {
    csvFieldsByEntityAttribute.put("publicationType", PublicationEventCsvFields.TYPE.getText());
    csvFieldsByEntityAttribute.put("publicationType.id", PublicationEventCsvFields.TYPE.getText());
    csvFieldsByEntityAttribute.put("date", PublicationEventCsvFields.DATE.getText());
    csvFieldsByEntityAttribute.put("title", PublicationEventCsvFields.TITLE.getText());
    csvFieldsByEntityAttribute.put("subTitle", PublicationEventCsvFields.SUB_TITLE.getText());
    csvFieldsByEntityAttribute.put("author", PublicationEventCsvFields.AUTHOR.getText());
    csvFieldsByEntityAttribute.put("establishment",
        PublicationEventCsvFields.ESTABLISHMENT_ID.getText());
    csvFieldsByEntityAttribute.put("establishment.id",
        PublicationEventCsvFields.ESTABLISHMENT_ID.getText());
    csvFieldsByEntityAttribute.put("volume", PublicationEventCsvFields.VOLUME.getText());
    csvFieldsByEntityAttribute.put("edition", PublicationEventCsvFields.EDITION.getText());
    csvFieldsByEntityAttribute.put("pages", PublicationEventCsvFields.PAGES.getText());
    csvFieldsByEntityAttribute.put("verified", PublicationEventCsvFields.PAGES.getText());
    csvFieldsByEntityAttribute.put("isbn", PublicationEventCsvFields.PAGES.getText());
    csvFieldsByEntityAttribute.put("libraryCode", PublicationEventCsvFields.LIBRARY_CODE.getText());
    csvFieldsByEntityAttribute.put("notes", PublicationEventCsvFields.PUBLICATION_NOTES.getText());
    csvFieldsByEntityAttribute.put("exhibitionEvents",
        PublicationEventCsvFields.EXHIBITIONS.getText());
    csvFieldsByEntityAttribute.put("publicationEvents",
        PublicationEventCsvFields.PUBLICATIONS.getText());
  }
}