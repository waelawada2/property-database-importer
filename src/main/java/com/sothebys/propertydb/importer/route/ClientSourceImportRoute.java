package com.sothebys.propertydb.importer.route;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.importer.exception.ClientSourceImportException;
import com.sothebys.propertydb.importer.handler.ClientSourceCSVDataHandler;
import com.sothebys.propertydb.importer.support.ClientSourceCsvFields;
import com.sothebys.propertydb.importer.support.CsvAggregationStrategy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class ClientSourceImportRoute extends RouteBuilder {

  @BeanInject
  private ClientSourceCSVDataHandler clientSourceCSVDataHandler;
  
  @Autowired
  private String propertyServiceAuthorization;
  
  @Qualifier("executorServiceClientSourceCsv")
  @Autowired
  private ExecutorService executorService;

  @Override
  public void configure() throws Exception {
    String[] header = Arrays.stream(ClientSourceCsvFields.values())
        .map(ClientSourceCsvFields::getText).toArray(String[]::new);

    Map<String, String> csvFieldsByEntityAttribute = new HashMap<>();

    mapFields(csvFieldsByEntityAttribute);

    // @formatter:off
    from("file:clientSourceData?delete={{routes.push-csv-to-service.delete-source-file}}")
    .streamCaching()
    .routeId("push-client-source-csv-to-service")
    .onException(ClientSourceImportException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error importing client source data: ${exception.message} ${exception.stacktrace}")
      .end()
    .onException(HttpHostConnectException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error connecting to client source service host: ${exception.host}. Request body: ${body}")
      .end()
    .onException(HttpOperationFailedException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error received from client source service: HTTP ${exception.statusCode}. Response body: ${exception.responseBody}. Request body: ${body}")
      .end()
    .onException(Exception.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error: ${exception.message} ${exception.stacktrace}")
      .end()
    .log(LoggingLevel.INFO, "Beginning to import client source CSV: ${file:onlyname}")
    .unmarshal(new CsvDataFormat()
        .setSkipHeaderRecord(true)
        .setNullString(EMPTY)
        .setLazyLoad(true))
    .split(body(), new CsvAggregationStrategy())
      .streaming()
      .parallelProcessing().executorService(executorService)
      .setProperty("csvRowData").simple("${body}", List.class)
      .setProperty("csvFieldsByEntityAttribute").constant(csvFieldsByEntityAttribute)
      .bean(clientSourceCSVDataHandler)
      .marshal().json(JsonLibrary.Jackson)
      .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
      .choice()
        .when().simple("${property.clientSourceId} != null")
          .setHeader(Exchange.HTTP_PATH, simple("${property.clientSourceId}"))
          .to("rest:PUT:client-sources?host={{backend.event-service.host}}")
        .otherwise()
          .to("rest:POST:client-sources?host={{backend.event-service.host}}")
        .end()
      .setProperty("responseId").jsonpath("$.id", true)
      .setProperty("idColumnPosition").constant(ClientSourceCsvFields.SOURCE_ID.getNumber())
     .choice()
       .when(exchangeProperty("responseId").isNull())
         .throwException(ClientSourceImportException.class, "Unexpected rest response (no id returned)")
         .otherwise()
         .end()
      .end()
    .choice()
      .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
        .setBody(simple("${exchangeProperty.badCsvData}"))
        .marshal(new CsvDataFormat().setHeader(header))
        .setProperty("badRowsBody").simple("${body}")
      .end()
    .choice()
      .when(simple("${exchangeProperty.successfulRecords.size()} > 0"))
        .setBody(simple("${exchangeProperty.successfulRecords}"))
        .marshal(new CsvDataFormat().setHeader(header))
        .setProperty("sucessfulRowsBody").simple("${body}")
      .end()
    .to("direct:sendImportReport").end()
    .log("Completed import for client source CSV: '${file:onlyname}'");
 
    // @formatter:on
  }

  private void mapFields(Map<String, String> csvFieldsByEntityAttribute) {
    csvFieldsByEntityAttribute.put("entity.id", ClientSourceCsvFields.SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("name", ClientSourceCsvFields.SOURCE_ID.getText());
  }
}
