package com.sothebys.propertydb.importer.route;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.exception.PricingEventImportException;
import com.sothebys.propertydb.importer.handler.PricingEventCSVDataHandler;
import com.sothebys.propertydb.importer.support.CsvAggregationStrategy;
import com.sothebys.propertydb.importer.support.PricingEventCsvFields;

@Component
public class PricingEventImportRoute extends RouteBuilder {

  @BeanInject
  private PricingEventCSVDataHandler pricingEventCSVDataHandler;
  
  @Autowired
  private String propertyServiceAuthorization;
  
  @Qualifier("executorServicePricingEventCsv")
  @Autowired
  private ExecutorService executorService;

  @Override
  public void configure() throws Exception {
    String[] header = Arrays.stream(PricingEventCsvFields.values())
        .map(PricingEventCsvFields::getText).toArray(String[]::new);

    Map<String, String> csvFieldsByEntityAttribute = new HashMap<>();

    mapFields(csvFieldsByEntityAttribute);

    // @formatter:off
    from("file:pricingEventData?delete={{routes.push-csv-to-service.delete-source-file}}")
    .streamCaching()
    .routeId("push-pricing-event-csv-to-service")
    .onException(PricingEventImportException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error importing pricing event data: ${exception.message} ${exception.stacktrace}")
      .end()
    .onException(HttpHostConnectException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error connecting to pricing event service host: ${exception.host}. Request body: ${body}")
      .end()
    .onException(HttpOperationFailedException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error received from pricing event service: HTTP ${exception.statusCode}. Response body: ${exception.responseBody}. Request body: ${body}")
      .end()
    .onException(Exception.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error: ${exception.message} ${exception.stacktrace}")
      .end()
    .log(LoggingLevel.INFO, "Beginning to import pricing event CSV: ${file:onlyname}")
    .unmarshal(new CsvDataFormat()
        .setSkipHeaderRecord(true)
        .setNullString(EMPTY)
        .setLazyLoad(true))
    .split(body(), new CsvAggregationStrategy())
      .streaming()
      .parallelProcessing().executorService(executorService)
      .setProperty("csvRowData").simple("${body}", List.class)
      .setProperty("csvFieldsByEntityAttribute").constant(csvFieldsByEntityAttribute)
      .bean(pricingEventCSVDataHandler)
      .marshal().json(JsonLibrary.Jackson)
      .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
      .choice()
        .when().simple("${property.eventId} != null")
          .setHeader(Exchange.HTTP_PATH, simple("${property.eventId}"))
          .to("rest:PUT:events?host={{backend.event-service.host}}")
        .otherwise()
          .to("rest:POST:events?host={{backend.event-service.host}}")
        .end()
      .setProperty("responseId").jsonpath("$.id", true)
      .setProperty("idColumnPosition").constant(PricingEventCsvFields.PRICING_ID.getNumber())
      .choice()
       .when(exchangeProperty("responseId").isNull())
         .throwException(PricingEventImportException.class, "Unexpected rest response (no id returned)")
       .otherwise()
       .end()
    .end()
    .choice()
      .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
        .setBody(simple("${exchangeProperty.badCsvData}"))
        .marshal(new CsvDataFormat().setHeader(header))
        .setProperty("badRowsBody").simple("${body}")
      .end()
    .choice()
      .when(simple("${exchangeProperty.successfulRecords.size()} > 0"))
        .setBody(simple("${exchangeProperty.successfulRecords}"))
        .marshal(new CsvDataFormat().setHeader(header))
        .setProperty("sucessfulRowsBody").simple("${body}")
      .end()
    .to("direct:sendImportReport").end()
    .log("Completed import for pricing event CSV: '${file:onlyname}'");
 
    // @formatter:on
  }

  private void mapFields(Map<String, String> csvFieldsByEntityAttribute) {
    csvFieldsByEntityAttribute.put("entity.id", PricingEventCsvFields.PRICING_ID.getText());
    csvFieldsByEntityAttribute.put("valuationOrigin", PricingEventCsvFields.ORIGIN.getText());
    csvFieldsByEntityAttribute.put("valuationOrigin.id", PricingEventCsvFields.ORIGIN.getText());
    csvFieldsByEntityAttribute.put("date", PricingEventCsvFields.DATE.getText());
    csvFieldsByEntityAttribute.put("valuationId", PricingEventCsvFields.VAL_SYSTEM_ID.getText());
    csvFieldsByEntityAttribute.put("valuationType", PricingEventCsvFields.TYPE.getText());
    csvFieldsByEntityAttribute.put("valuationType.id", PricingEventCsvFields.TYPE.getText());
    csvFieldsByEntityAttribute.put("proposal", PricingEventCsvFields.PROPOSAL_CODE.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientId",
        PricingEventCsvFields.CLIENT_ID.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientStatus",
        PricingEventCsvFields.CLIENT_STATUS.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientStatus.id",
        PricingEventCsvFields.CLIENT_STATUS.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientSourceType",
        PricingEventCsvFields.CLIENT_SOURCE_TYPE.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientSourceType.id",
        PricingEventCsvFields.CLIENT_SOURCE_TYPE.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientSource",
        PricingEventCsvFields.CLIENT_SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientSource.id",
        PricingEventCsvFields.CLIENT_SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientSource.id",
        PricingEventCsvFields.CLIENT_SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("mainClient.display",
        PricingEventCsvFields.DESIGNATION.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientId",
        PricingEventCsvFields.AFFILIATION_CLIENT_ID.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientSourceType",
        PricingEventCsvFields.AFFILIATION_SOURCE_TYPE.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientSourceType.id",
        PricingEventCsvFields.AFFILIATION_SOURCE_TYPE.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientSource",
        PricingEventCsvFields.AFFILIATION_SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientSource.id",
        PricingEventCsvFields.AFFILIATION_SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.affiliatedType",
        PricingEventCsvFields.AFFILIATION_TYPE.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.affiliatedType.id",
        PricingEventCsvFields.AFFILIATION_TYPE.getText());
    csvFieldsByEntityAttribute.put("notes", PricingEventCsvFields.PRICING_NOTES.getText());
  }

}