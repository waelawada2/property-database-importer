package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.handler.SimpleEmailHandler;
import java.io.File;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.PropertyInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.processor.idempotent.FileIdempotentRepository;
import org.springframework.stereotype.Component;

/**
 * Camel route that fetches CSV files from S3 and puts them in the correct folder in the filesystem.
 *
 * @author Minhtri Tran
 */
@Component
public class ImportFromS3Route extends RouteBuilder {

  protected static final String FILE_REPO_PATH = "file-repository.data";

  @PropertyInject("routes.import-from-s3.file-cache-size")
  private int fileCacheSize;

  @Override
  public void configure() throws Exception {

    // @formatter:off
    from("aws-s3://{{s3.imports.bucket-name}}?amazonS3Client=#amazonS3Client&deleteAfterRead={{s3.imports.delete-after-read}}&maxMessagesPerPoll=1&delay={{s3.imports.poll-delay}}")
        .routeId("import-from-s3")
        .autoStartup("{{routes.import-from-s3.enabled}}")
        .onException(Exception.class)
            .log(LoggingLevel.ERROR, "Error importing file from s3: ${exception.stacktrace}")
            .continued(true)
        .end()
        .log(LoggingLevel.INFO, "Getting object '${headers.CamelAwsS3Key}' from S3")
        .idempotentConsumer(header("CamelAwsS3ETag"),
            FileIdempotentRepository.fileIdempotentRepository(new File(FILE_REPO_PATH), fileCacheSize))
        .skipDuplicate(false)
        .filter(exchangeProperty(Exchange.DUPLICATE_MESSAGE).isEqualTo(true))
          .log(LoggingLevel.WARN, "Duplicate object '${headers.CamelAwsS3Key}' detected.")
          .bean(SimpleEmailHandler.class, "prepareDuplicateFileEmailBody")
          .log(LoggingLevel.INFO, "Sending duplicate file email: ${body}")
          .setHeader(Exchange.CONTENT_TYPE, simple("{{routes.email.content-type}}"))
          .to("smtp://{{routes.email.host}}?from={{routes.email.from}}&to={{routes.email.to}}&subject={{routes.email.duplicate-file-subject}}")
          .stop()
        .end()
        .log(LoggingLevel.INFO, "Saving to file ${in.header.CamelAwsS3Key}")
        .choice()
          .when().simple("${in.header.CamelAwsS3Key} contains 'property'")
            .to("file:propertyData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'artist'")
            .to("file:artistData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'website_images'")
            .to("file:websiteImagesData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'establishment'")
            .to("file:establishmentData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'consignment_event' || ${in.header.CamelAwsS3Key} contains 'purchase_event'")
            .to("file:consignmentPurchaseEventData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'transaction_event'")
            .to("file:transactionEventData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'pricing_event'")
            .to("file:pricingEventData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'publication_event'")
            .to("file:publicationEventData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'pricing_item'")
            .to("file:pricingItemData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'publication_item'")
              .to("file:publicationItemData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key} contains 'transaction_item'")
            .to("file:transactionItemData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
          .when().simple("${in.header.CamelAwsS3Key.toLowerCase()} contains 'source' && ${in.header.CamelAwsS3Key.toLowerCase()} contains 'name'")
            .to("file:clientSourceData?fileName=${in.header.CamelAwsS3Key}&fileExist=Ignore")
         .otherwise()
           .log(LoggingLevel.ERROR, "Import file name ${in.header.CamelAwsS3Key} not matching");
    // @formatter:on
  }
}
