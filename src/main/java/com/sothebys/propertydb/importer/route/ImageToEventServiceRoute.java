package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.handler.ImageHandler;
import com.sothebys.propertydb.importer.model.event.ImageDto;

import org.apache.camel.component.http4.*;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class ImageToEventServiceRoute extends RouteBuilder{
  
  @Autowired
  private String propertyServiceAuthorization;
  
  @Override
  public void configure() throws Exception {
    
    //@formatter:off
    from("direct:post-image-to-event-service")
      .log("Sending image to event service")
      .streamCaching()
      .routeId("post-image-to-event-service")
      .errorHandler(noErrorHandler())
      .bean(ImageHandler.class, "setS3ImageToBodyAsMultiPart('${property.imageName}')")
      .choice()
        .when().simple("${in.body} != null")
           .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic " + propertyServiceAuthorization))
           .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.POST))
           .setHeader(Exchange.HTTP_PATH, simple(""))
           .setHeader(Exchange.HTTP_URI, simple("http://{{backend.event-service.host}}/images"))
           .to("http4:uploadImage")
           .unmarshal().json(JsonLibrary.Jackson, ImageDto.class)
           .setProperty("finalImageName").simple("${body.imageName}")
      .end()
    .log("finish sending image to event service"); 
    //@formatter:off
    
  }

}