package com.sothebys.propertydb.importer.route;

import static org.apache.camel.util.toolbox.AggregationStrategies.flexible;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.importer.exception.ArtistImportException;
import com.sothebys.propertydb.importer.handler.ArtistCSVDataHandler;
import com.sothebys.propertydb.importer.handler.ArtistErrorHandler;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class ArtistImportRoute extends RouteBuilder {

  @Autowired
  private String propertyServiceAuthorization;

  private static final String[] HEADER = {"Artist_Code", "Artist_ID", "First_Name", "Last_Name",
      "Birth_Year", "Death_Year", "Display (free text)", "Nationality (Country)", "Gender",
      "Unknown_check", "Painting_check", "Work_on_Paper_Check", "Mounted_check", "Sculpture_check",
      "Photograph_check", "Print_check", "Installation_check", "Video_check", "Other_check",
      "CR_Author", "CR_Year", "CR_Volumes", "CR_Type", "Notes", "Error_Details"};

  @Override
  public void configure() throws Exception {
    // @formatter:off
    from("file:artistData?delete={{routes.push-csv-to-service.delete-source-file}}")
        .streamCaching()
        .routeId("push-artist-csv-to-service")
        .onException(HttpOperationFailedException.class)
         .handled(true)
          .log(LoggingLevel.ERROR, "Artist Service error (HTTP ${exception.statusCode}) - Error: ${exception.responseBody} - message: ${exchangeProperty.csvRowData}")
          .bean(ArtistErrorHandler.class,"addFailedRecordToList")
        .end()
        .onException(ArtistImportException.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Error importing artist data - Error: ${exception.message}")
          .bean(ArtistErrorHandler.class,"addFailedRecordToList")
        .end()
        .onException(HttpHostConnectException.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Error connecting to artist service host: ${exception.host}")
          .bean(ArtistErrorHandler.class,"addFailedRecordToList")
        .end()
        .onException(Exception.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Exception thrown while importing data - Error: ${exception.message} - message: ${exception}")
          .bean(ArtistErrorHandler.class,"addFailedRecordToList")
        .end()
        .unmarshal(new CsvDataFormat().setSkipHeaderRecord(true).setNullString(EMPTY).setLazyLoad(true))
        .setProperty("numberOfCSVRows", constant(0))
        .split(body(), flexible()
            .condition(exchangeProperty(Exchange.SPLIT_COMPLETE))
            .pick(exchangeProperty(Exchange.SPLIT_SIZE))
            .storeInProperty("numberOfCSVRows")
            .completionAware(exchange-> {
              if (exchange == null) {
                return;
              }
              exchange.removeProperties("CamelFailure*");
              exchange.removeProperties("CamelException*");
              exchange.removeProperties("CamelError*");
            }))
          .streaming()
          .parallelProcessing()
          .setProperty("csvRowData").simple("${body}", List.class)
          .bean(ArtistCSVDataHandler.class)
          .marshal().json(JsonLibrary.Jackson)
          .log(LoggingLevel.INFO, "Sending Artist message: ${body}")
          .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
          .setProperty("artistId").jsonpath("$.externalId")
            .choice()
              .when().simple("${property.artistId} != null")
                .setHeader(Exchange.HTTP_PATH, simple("${property.artistId}"))
                .to("rest:PUT:artists?host={{backend.property-service.host}}")
              .otherwise()
                .to("rest:POST:artists?host={{backend.property-service.host}}")
            .end()
          .setProperty("responseId").jsonpath("$.id", true)
          .choice()
            .when(exchangeProperty("responseId").isNull())
              .throwException(ArtistImportException.class, "Unexpected rest response (no id returned)")
            .otherwise()
          .end()
        .end()
        .bean(ArtistErrorHandler.class,"getFailRecordsList")
        .setProperty("badCsvData", body())
        .choice()
          .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
            .setBody(simple("${exchangeProperty.badCsvData}"))
            .marshal(new CsvDataFormat().setHeader(HEADER))
            .setProperty("badRowsBody").simple("${body}")
          .end()
        .to("direct:sendImportReport")
        .end()
        .bean(ArtistErrorHandler.class, "clearFailRecordsList")
        .log("Completed import of '${header.CamelFileName}'.");
    // @formatter:on
  }

}
