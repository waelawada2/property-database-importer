package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.handler.SimpleEmailHandler;
import com.sothebys.propertydb.importer.handler.StringHandler;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.aws.s3.S3Constants;
import org.springframework.stereotype.Component;

/**
 * Camel route that sends an email report of the import. In the case that the import failed, it
 * uploads an error CSV file to S3.
 *
 * @author Minhtri Tran
 */
@Component
public class SendImportReportRoute extends RouteBuilder {

  private static final char UTF8_BOM = '\ufeff';

  @Override
  public void configure() throws Exception {
    // @formatter:off
    from("direct:sendImportReport")
        .routeId("send-import-report")
        .onException(Exception.class)
            .log(LoggingLevel.ERROR, "Error sending the email with the import results: ${exception.stacktrace}")
            .continued(true)
            .maximumRedeliveries(5)
            .redeliveryDelay(30000)
            .end()
        .filter(body().not().startsWith(UTF8_BOM))
            .transform(body().prepend(UTF8_BOM))
        .end()
        .choice()
            .when(simple("${exchangeProperty.badRowsBody} != null"))
                .setBody(simple("${exchangeProperty.badRowsBody}"))
                .setProperty("actualCsvFileName", method(StringHandler.class, "getActualCsvFileName(${file:onlyname.noext}, ${file:name.ext.single})"))
                .setProperty("errorCsvFileName", method(StringHandler.class, "trimAndReplaceWhitespace(${file:onlyname.noext}_errors_${date:now:yyyyMMddHHmmss}.${file:name.ext})"))
                .log(LoggingLevel.INFO, "Uploading csv file detailing errors to S3: ${exchangeProperty.errorCsvFileName}")
                .setHeader(S3Constants.KEY, exchangeProperty("errorCsvFileName"))
                .setHeader(S3Constants.CANNED_ACL, constant("PublicRead"))
                .setHeader("subject", simple("{{routes.email.subject}}: ${exchangeProperty.actualCsvFileName}"))
                .to("aws-s3:{{s3.importer-output.bucket-name}}?amazonS3Client=#importerFileS3Client")
            .otherwise()
                .setHeader("subject", simple("{{routes.email.subject}}"))
            .end()
        .choice()
            .when(simple("${exchangeProperty.sucessfulRowsBody} != null"))
                .setBody(simple("${exchangeProperty.sucessfulRowsBody}"))
                .setProperty("actualCsvFileName", method(StringHandler.class, "getActualCsvFileName(${file:onlyname.noext}, ${file:name.ext.single})"))
                .setProperty("successfulCsvFileName", method(StringHandler.class, "trimAndReplaceWhitespace(${file:onlyname.noext}_successful_${date:now:yyyyMMddHHmmss}.${file:name.ext})"))
                .log(LoggingLevel.INFO, "Uploading csv file detailing succed to S3: ${exchangeProperty.successfulCsvFileName}")
                .setHeader(S3Constants.KEY, exchangeProperty("successfulCsvFileName"))
                .setHeader(S3Constants.CANNED_ACL, constant("PublicRead"))
                .setHeader("subject", simple("{{routes.email.subject}}: ${exchangeProperty.actualCsvFileName}"))
                .to("aws-s3:{{s3.importer-output.bucket-name}}?amazonS3Client=#importerFileS3Client")
            .end()       
        .log(LoggingLevel.INFO, "Number of rows: ${exchangeProperty.numberOfCSVRows}")
        .log(LoggingLevel.INFO, "Number of bad rows: ${exchangeProperty.badCsvData.size()}")
        .bean(SimpleEmailHandler.class, "prepareEmailBody")
        .log(LoggingLevel.INFO, "Sending email: ${body}")
        .setHeader(Exchange.CONTENT_TYPE, simple("{{routes.email.content-type}}"))
        .to("smtp://{{routes.email.host}}?from={{routes.email.from}}&to={{routes.email.to}}");

    // @formatter:on
  }
}
