package com.sothebys.propertydb.importer.route;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.importer.exception.TransactionEventImportException;
import com.sothebys.propertydb.importer.handler.TransactionEventCSVDataHandler;
import com.sothebys.propertydb.importer.support.CsvAggregationStrategy;
import com.sothebys.propertydb.importer.support.TransactionEventCsvFields;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

/**
 * Camel route that fetches CSV files that contain transaction event data and pushes the data to the
 * event service.
 *
 * @author Minhtri Tran
 */
@Component
public class TransactionEventImportRoute extends RouteBuilder {

  @BeanInject
  private TransactionEventCSVDataHandler transactionEventCSVDataHandler;
  
  @Autowired
  private String propertyServiceAuthorization;
  
  @Qualifier("executorServiceTransactionEventCsv")
  @Autowired
  private ExecutorService executorService;

  @Override
  public void configure() throws Exception {
    String[] header = Arrays.stream(TransactionEventCsvFields.values())
        .map(TransactionEventCsvFields::getText).toArray(String[]::new);

    Map<String, String> csvFieldsByEntityAttribute = new HashMap<>();

    mapFields(csvFieldsByEntityAttribute);

    // @formatter:off
    from("file:transactionEventData?delete={{routes.push-csv-to-service.delete-source-file}}")
        .streamCaching()
        .routeId("push-transaction-event-csv-to-service")
        .onException(TransactionEventImportException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error importing transaction event data: ${exception.message} ${exception.stacktrace}")
            .end()
        .onException(HttpHostConnectException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error connecting to transaction event service host: ${exception.host}. Request body: ${body}")
            .end()
        .onException(HttpOperationFailedException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error received from transaction event service: HTTP ${exception.statusCode}. Response body: ${exception.responseBody}. Request body: ${body}")
            .end()
        .onException(Exception.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error: ${exception.message} ${exception.stacktrace}")
            .end()
        .log(LoggingLevel.INFO, "Beginning to import transaction event CSV: ${file:onlyname}")
        .unmarshal(new CsvDataFormat()
            .setSkipHeaderRecord(true)
            .setNullString(EMPTY)
            .setLazyLoad(true))
        .split(body(), new CsvAggregationStrategy())
            .streaming()
            .parallelProcessing().executorService(executorService)
            .setProperty("csvRowData").simple("${body}", List.class)
            .setProperty("csvFieldsByEntityAttribute").constant(csvFieldsByEntityAttribute)
            .bean(transactionEventCSVDataHandler)
            .marshal().json(JsonLibrary.Jackson)
            .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
            .choice()
                .when().simple("${property.eventId} != null")
                    .setHeader(Exchange.HTTP_PATH, simple("${property.eventId}"))
                    .to("rest:PUT:events?host={{backend.event-service.host}}")
                .otherwise()
                    .to("rest:POST:events?host={{backend.event-service.host}}")
                .end()
            .setProperty("responseId").jsonpath("$.id", true)
            .setProperty("idColumnPosition").constant(TransactionEventCsvFields.TRANSACTION_ID.getNumber())
            .choice()
                .when(exchangeProperty("responseId").isNull())
                    .throwException(TransactionEventImportException.class, "Unexpected rest response (no id returned)")
                .otherwise()
                .end()
        .end()        
        .choice()
          .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
            .setBody(simple("${exchangeProperty.badCsvData}"))
            .marshal(new CsvDataFormat().setHeader(header))
            .setProperty("badRowsBody").simple("${body}")
          .end()
        .choice()
          .when(simple("${exchangeProperty.successfulRecords.size()} > 0"))
            .setBody(simple("${exchangeProperty.successfulRecords}"))
            .marshal(new CsvDataFormat().setHeader(header))
            .setProperty("sucessfulRowsBody").simple("${body}")
          .end()      
        .to("direct:sendImportReport").end()
        .log("Completed import for transaction event CSV: '${file:onlyname}'");

    // @formatter:on
  }

  private void mapFields(Map<String, String> csvFieldsByEntityAttribute) {
    csvFieldsByEntityAttribute.put("entity.id", TransactionEventCsvFields.TRANSACTION_ID.getText());
    csvFieldsByEntityAttribute.put("title", TransactionEventCsvFields.TITLE.getText());
    csvFieldsByEntityAttribute.put("establishment",
        TransactionEventCsvFields.ESTABLISHMENT_ID.getText());
    csvFieldsByEntityAttribute.put("establishment.id",
        TransactionEventCsvFields.ESTABLISHMENT_ID.getText());
    csvFieldsByEntityAttribute.put("department.id", TransactionEventCsvFields.DEPARTMENT.getText());
    csvFieldsByEntityAttribute.put("date", TransactionEventCsvFields.DATE.getText());
    csvFieldsByEntityAttribute.put("saleNumber", TransactionEventCsvFields.SALE_NUMBER.getText());
    csvFieldsByEntityAttribute.put("currency", TransactionEventCsvFields.CURRENCY.getText());
    csvFieldsByEntityAttribute.put("currency.id", TransactionEventCsvFields.CURRENCY.getText());
    csvFieldsByEntityAttribute.put("totalLots", TransactionEventCsvFields.TOTAL_LOTS.getText());
    csvFieldsByEntityAttribute.put("notes", TransactionEventCsvFields.NOTES.getText());
  }
}
