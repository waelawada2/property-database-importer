package com.sothebys.propertydb.importer.route;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.importer.exception.ConsignmentPurchaseEventImportException;
import com.sothebys.propertydb.importer.handler.ConsignmentPurchaseEventCSVDataHandler;
import com.sothebys.propertydb.importer.support.ConsignmentPurchaseEventCsvFields;
import com.sothebys.propertydb.importer.support.CsvAggregationStrategy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.camel.BeanInject;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class ConsignmentPurchaseEventImportRoute extends RouteBuilder {

  @BeanInject
  private ConsignmentPurchaseEventCSVDataHandler consignmentPurchaseEventCSVDataHandler;

  @Autowired
  private String propertyServiceAuthorization;
  
  @Qualifier("executorServiceConsignmentPurchaseEventCsv")
  @Autowired
  private ExecutorService executorService;
  
  @Override
  public void configure() throws Exception {
    String[] header = Arrays.stream(ConsignmentPurchaseEventCsvFields.values())
        .map(ConsignmentPurchaseEventCsvFields::getText).toArray(String[]::new);

    Map<String, String> csvFieldsByEntityAttribute = new HashMap<>();

    mapFields(csvFieldsByEntityAttribute);

 // @formatter:off
    from("file:consignmentPurchaseEventData?delete={{routes.push-csv-to-service.delete-source-file}}")
      .streamCaching()
      .routeId("push-consignment-purchase-event-csv-to-service")
      .onException(ConsignmentPurchaseEventImportException.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Error importing Purchase/Consignment event data: ${exception.message} ${exception.stacktrace}")
          .end()
      .onException(HttpHostConnectException.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Error connecting to event service host: ${exception.host}. Request body: ${body}")
          .end()
      .onException(HttpOperationFailedException.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Error performing a PUT/POST to the event service: Request body: ${body}. ${exception.message}")
          .end()
      .onException(Exception.class)
          .handled(true)
          .log(LoggingLevel.ERROR, "Error (Consignment/Purchase import): ${exception.message} ${exception.stacktrace}")
          .end()
      .log(LoggingLevel.INFO, "Beginning to import Consignment/Purchase event CSV: ${file:onlyname}")
      .unmarshal(new CsvDataFormat()
          .setSkipHeaderRecord(true)
          .setNullString(EMPTY)
          .setLazyLoad(true))
      .split(body(), new CsvAggregationStrategy())
          .streaming()
          .parallelProcessing().executorService(executorService)
          .setProperty("csvRowData").simple("${body}", List.class)
          .setProperty("csvFieldsByEntityAttribute").constant(csvFieldsByEntityAttribute)
          .bean(consignmentPurchaseEventCSVDataHandler)
          .marshal().json(JsonLibrary.Jackson)
          .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
          .choice()
              .when().simple("${property.id} != null")
                  .to("rest:PUT:events?host={{backend.event-service.host}}")
              .otherwise()
                  .to("rest:POST:events?host={{backend.event-service.host}}")
              .end()
          .setProperty("responseId").jsonpath("$.id", true)
          .setProperty("idColumnPosition").constant(ConsignmentPurchaseEventCsvFields.PURCHASE_CONSIGNMENT_ID.getNumber())
          .choice()
              .when(exchangeProperty("responseId").isNull())
                  .throwException(ConsignmentPurchaseEventImportException.class, "Unexpected rest response (no id returned)")
              .otherwise()
              .end()
        .end()
        .choice()
          .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
            .setBody(simple("${exchangeProperty.badCsvData}"))
            .marshal(new CsvDataFormat().setHeader(header))
            .setProperty("badRowsBody").simple("${body}")
          .end()
        .choice()
          .when(simple("${exchangeProperty.successfulRecords.size()} > 0"))
            .setBody(simple("${exchangeProperty.successfulRecords}"))
            .marshal(new CsvDataFormat().setHeader(header))
            .setProperty("sucessfulRowsBody").simple("${body}")
          .end() 
      .to("direct:sendImportReport")
      .log("Completed import for Consignment/Purchase event CSV: '${file:onlyname}'");

    // @formatter:on
  }

  private void mapFields(Map<String, String> csvFieldsByEntityAttribute) {
    csvFieldsByEntityAttribute.put("entity.id",
        ConsignmentPurchaseEventCsvFields.PURCHASE_CONSIGNMENT_ID.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientId",
        ConsignmentPurchaseEventCsvFields.CLIENT_ID.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientStatus",
        ConsignmentPurchaseEventCsvFields.OWNERSHIP_STATUS.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientStatus.id",
        ConsignmentPurchaseEventCsvFields.OWNERSHIP_STATUS.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientSourceType",
        ConsignmentPurchaseEventCsvFields.OWNERSHIP_CLIENT_SOURCE_TYPE.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientSourceType.id",
        ConsignmentPurchaseEventCsvFields.OWNERSHIP_CLIENT_SOURCE_TYPE.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientSource",
        ConsignmentPurchaseEventCsvFields.OWNERSHIP_CLIENT_SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("mainClient.clientSource.id",
        ConsignmentPurchaseEventCsvFields.OWNERSHIP_CLIENT_SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("mainClient.display",
        ConsignmentPurchaseEventCsvFields.DESIGNATION.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientId",
        ConsignmentPurchaseEventCsvFields.AFFILIATION_ID.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientSourceType",
        ConsignmentPurchaseEventCsvFields.AFFILIATION_SOURCE_TYPE.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientSourceType.id",
        ConsignmentPurchaseEventCsvFields.AFFILIATION_SOURCE_TYPE.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientSource",
        ConsignmentPurchaseEventCsvFields.AFFILIATION_SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.clientSource.id",
        ConsignmentPurchaseEventCsvFields.AFFILIATION_SOURCE_ID.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.affiliatedType",
        ConsignmentPurchaseEventCsvFields.AFFILIATION_TYPE.getText());
    csvFieldsByEntityAttribute.put("affiliatedClient.affiliatedType.id",
        ConsignmentPurchaseEventCsvFields.AFFILIATION_TYPE.getText());
  }
}
