package com.sothebys.propertydb.importer.route;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.importer.exception.EstablishmentImportException;
import com.sothebys.propertydb.importer.handler.EstablishmentCSVDataHandler;
import com.sothebys.propertydb.importer.support.CsvAggregationStrategy;
import com.sothebys.propertydb.importer.support.EstablishmentCsvFields;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

/**
 * Camel route that fetches CSV files that contain establishment data and pushes the data to the
 * event service.
 *
 * @author Minhtri Tran
 */
@Component
public class EstablishmentImportRoute extends RouteBuilder {

  @BeanInject
  private EstablishmentCSVDataHandler establishmentCSVDataHandler;
  
  @Autowired
  private String propertyServiceAuthorization;
  
  @Qualifier("executorServiceEstablishmentCsv")
  @Autowired
  private ExecutorService executorService;

  @Override
  public void configure() throws Exception {
    String[] header = Arrays.stream(EstablishmentCsvFields.values())
        .map(EstablishmentCsvFields::getText).toArray(String[]::new);

    Map<String, String> csvFieldsByEntityAttribute = new HashMap<>();

    mapFields(csvFieldsByEntityAttribute);

    // @formatter:off
    from("file:establishmentData?delete={{routes.push-csv-to-service.delete-source-file}}")
        .streamCaching()
        .routeId("push-establishment-csv-to-service")
        .onException(EstablishmentImportException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error importing establishment data: ${exception.message} ${exception.stacktrace}")
            .end()
        .onException(HttpHostConnectException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error connecting to establishment service host: ${exception.host}. Request body: ${body}")
            .end()
        .onException(HttpOperationFailedException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error received from establishment service: HTTP ${exception.statusCode}. Response body: ${exception.responseBody}. Request body: ${body}")
            .end()
        .onException(Exception.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error: ${exception.message} ${exception.stacktrace}")
            .end()
        .log(LoggingLevel.INFO, "Beginning to import establishment CSV: ${file:onlyname}")
        .unmarshal(new CsvDataFormat()
            .setSkipHeaderRecord(true)
            .setNullString(EMPTY)
            .setLazyLoad(true))
        .split(body(), new CsvAggregationStrategy())
            .streaming()
            .parallelProcessing().executorService(executorService)
            .setProperty("csvRowData").simple("${body}", List.class)
            .setProperty("csvFieldsByEntityAttribute").constant(csvFieldsByEntityAttribute)
            .bean(establishmentCSVDataHandler)
            .marshal().json(JsonLibrary.Jackson)
            .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
            .choice()
                .when().simple("${property.establishmentId} != null")
                    .setHeader(Exchange.HTTP_PATH, simple("${property.establishmentId}"))
                    .to("rest:PUT:establishments?host={{backend.event-service.host}}")
                .otherwise()
                    .to("rest:POST:establishments?host={{backend.event-service.host}}")
                .end()
            .setProperty("responseId").jsonpath("$.id", true)
            .setProperty("idColumnPosition").constant(EstablishmentCsvFields.ESTABLISHMENT_ID.getNumber())
            .choice()
                .when(exchangeProperty("responseId").isNull())
                    .throwException(EstablishmentImportException.class, "Unexpected rest response (no id returned)")
                .otherwise()
                .end()
           .end()
          .choice()
            .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
              .setBody(simple("${exchangeProperty.badCsvData}"))
              .marshal(new CsvDataFormat().setHeader(header))
              .setProperty("badRowsBody").simple("${body}")
            .end()
          .choice()
            .when(simple("${exchangeProperty.successfulRecords.size()} > 0"))
              .setBody(simple("${exchangeProperty.successfulRecords}"))
              .marshal(new CsvDataFormat().setHeader(header))
              .setProperty("sucessfulRowsBody").simple("${body}")
            .end() 
        .to("direct:sendImportReport").end()
        .log("Completed import for establishment CSV: '${file:onlyname}'");

    // @formatter:on
  }

  private void mapFields(Map<String, String> csvFieldsByEntityAttribute) {
    csvFieldsByEntityAttribute.put("code", EstablishmentCsvFields.ESTABLISHMENT_CODE.getText());
    csvFieldsByEntityAttribute.put("entity.id", EstablishmentCsvFields.ESTABLISHMENT_ID.getText());
    csvFieldsByEntityAttribute.put("name", EstablishmentCsvFields.ESTABLISHMENT_NAME.getText());
    csvFieldsByEntityAttribute.put("cityName", EstablishmentCsvFields.CITY.getText());
    csvFieldsByEntityAttribute.put("state", EstablishmentCsvFields.STATE.getText());
    csvFieldsByEntityAttribute.put("countryName", EstablishmentCsvFields.COUNTRY.getText());
    csvFieldsByEntityAttribute.put("region", EstablishmentCsvFields.REGION.getText());
  }
}
