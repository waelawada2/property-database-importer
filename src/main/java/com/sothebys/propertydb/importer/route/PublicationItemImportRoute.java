package com.sothebys.propertydb.importer.route;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.importer.exception.PublicationItemImportException;
import com.sothebys.propertydb.importer.handler.PublicationItemCSVDataHandler;
import com.sothebys.propertydb.importer.support.CsvAggregationStrategy;
import com.sothebys.propertydb.importer.support.PublicationItemCsvFields;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class PublicationItemImportRoute extends RouteBuilder {

  @BeanInject
  private PublicationItemCSVDataHandler publicationItemCSVDataHandler;

  @Autowired
  private String propertyServiceAuthorization;

  @Qualifier("executorServicePublicationItemCsv")
  @Autowired
  private ExecutorService executorService;

  @Override
  public void configure() throws Exception {
    String[] header = Arrays.stream(PublicationItemCsvFields.values())
        .map(PublicationItemCsvFields::getText).toArray(String[]::new);

    Map<String, String> csvFieldsByEntityAttribute = new HashMap<>();

    mapFields(csvFieldsByEntityAttribute);

    //@formatter:off
    from("file:publicationItemData?delete={{routes.push-csv-to-service.delete-source-file}}")
        .streamCaching()
        .routeId("push-publication-item-csv-to-service")
        .onException(PublicationItemImportException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error importing publication item data: ${exception.message} ${exception.stacktrace}")
            .end()
        .onException(HttpHostConnectException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error connecting to publication item service host: ${exception.host}. Request body: ${body}")
            .end()
        .onException(HttpOperationFailedException.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error received from publication item service: HTTP ${exception.statusCode}. Response body: ${exception.responseBody}. Request body: ${body}")
            .end()
        .onException(Exception.class)
            .handled(true)
            .log(LoggingLevel.ERROR, "Error: ${exception.message} ${exception.stacktrace}")
            .end()
        .log(LoggingLevel.INFO, "Beginning to import publication item CSV: ${file:onlyname}")
        .unmarshal(new CsvDataFormat()
            .setSkipHeaderRecord(true)
            .setNullString(EMPTY)
            .setLazyLoad(true))
        .split(body(), new CsvAggregationStrategy())
            .streaming()
            .parallelProcessing().executorService(executorService)
            .setProperty("csvRowData").simple("${body}", List.class)
            .setProperty("csvFieldsByEntityAttribute").constant(csvFieldsByEntityAttribute)
            .bean(publicationItemCSVDataHandler)
            .marshal().json(JsonLibrary.Jackson)
            .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
            .log("Item ID: ${property.itemId}")
            .choice()
                .when().simple("${property.itemId} != null")
                    .setHeader(Exchange.HTTP_PATH, simple("${property.itemId}"))
                    .to("rest:PUT:items?host={{backend.event-service.host}}")
                .otherwise()
                    .to("rest:POST:items?host={{backend.event-service.host}}")
                .end()
            .setProperty("responseId").jsonpath("$.id", true)
            .setProperty("idColumnPosition").constant(PublicationItemCsvFields.ID.getNumber())
            .choice()
                .when(exchangeProperty("responseId").isNull())
                    .throwException(PublicationItemImportException.class, "Unexpected rest "
                        + "response (no id returned)")
                .otherwise()
                .end()
        .end()
        .choice()
            .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
              .setBody(simple("${exchangeProperty.badCsvData}"))
              .marshal(new CsvDataFormat().setHeader(header))
              .setProperty("badRowsBody").simple("${body}")
            .end()
        .choice()
            .when(simple("${exchangeProperty.successfulRecords.size()} > 0"))
              .setBody(simple("${exchangeProperty.successfulRecords}"))
              .marshal(new CsvDataFormat().setHeader(header))
              .setProperty("sucessfulRowsBody").simple("${body}")
            .end() 
        .to("direct:sendImportReport").end()
        .log("Completed import for publication item CSV: '${file:onlyname}'");


    //@formatter:on

  }

  private void mapFields(Map<String, String> csvFieldsByEntityAttribute) {
    csvFieldsByEntityAttribute.put("entity.id", PublicationItemCsvFields.ID.getText());
    csvFieldsByEntityAttribute.put("event",
        PublicationItemCsvFields.PUBLICATION_EVENT_ID.getText());
    csvFieldsByEntityAttribute.put("event.id",
        PublicationItemCsvFields.PUBLICATION_EVENT_ID.getText());
    csvFieldsByEntityAttribute.put("consignmentEvent.id",
        PublicationItemCsvFields.CONSIGNMENT_ID.getText());
    csvFieldsByEntityAttribute.put("cataloguingPage", PublicationItemCsvFields.CAT_PAGE.getText());
    csvFieldsByEntityAttribute.put("number", PublicationItemCsvFields.NUMBER.getText());
    csvFieldsByEntityAttribute.put("plate", PublicationItemCsvFields.PLATE.getText());
    csvFieldsByEntityAttribute.put("figure", PublicationItemCsvFields.FIGURE.getText());
    csvFieldsByEntityAttribute.put("illustration", PublicationItemCsvFields.Ill_PAGE.getText());
    csvFieldsByEntityAttribute.put("colourType", PublicationItemCsvFields.COLOUR.getText());
    csvFieldsByEntityAttribute.put("colourType.id", PublicationItemCsvFields.COLOUR.getText());
    csvFieldsByEntityAttribute.put("publicationItemType", PublicationItemCsvFields.TYPE.getText());
    csvFieldsByEntityAttribute.put("publicationItemType.id",
        PublicationItemCsvFields.TYPE.getText());
    csvFieldsByEntityAttribute.put("comments", PublicationItemCsvFields.ITEM_NOTES.getText());
    csvFieldsByEntityAttribute.put("description", PublicationItemCsvFields.DESCRIPTION.getText());
    csvFieldsByEntityAttribute.put("provenance", PublicationItemCsvFields.PROVENANCE.getText());
    csvFieldsByEntityAttribute.put("exhibition", PublicationItemCsvFields.EXHIBITION.getText());
    csvFieldsByEntityAttribute.put("literature", PublicationItemCsvFields.LITERATURE.getText());
    csvFieldsByEntityAttribute.put("condition", PublicationItemCsvFields.CONDITION_TEXT.getText());
    csvFieldsByEntityAttribute.put("cataloguingNotes", PublicationItemCsvFields.OD_NOTES.getText());
  }
}
