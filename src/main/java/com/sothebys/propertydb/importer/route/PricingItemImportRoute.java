package com.sothebys.propertydb.importer.route;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.importer.exception.PricingItemImportException;
import com.sothebys.propertydb.importer.handler.PricingItemCSVDataHandler;
import com.sothebys.propertydb.importer.support.CsvAggregationStrategy;
import com.sothebys.propertydb.importer.support.PricingItemCsvFields;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.csv.CsvDataFormat;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class PricingItemImportRoute extends RouteBuilder {

  @BeanInject
  private PricingItemCSVDataHandler pricingItemCSVDataHandler;
  
  @Autowired
  private String propertyServiceAuthorization;

  @Qualifier("executorServicePricingItemCsv")
  @Autowired
  private ExecutorService executorService;
  
  @Override
  public void configure() throws Exception {
    String[] header = Arrays.stream(PricingItemCsvFields.values()).map
        (PricingItemCsvFields::getText)
        .toArray(String[]::new);
    
    Map<String, String> csvFieldsByEntityAttribute = new HashMap<>();
    
    mapFields(csvFieldsByEntityAttribute);
    
    // @formatter:off
    from("file:pricingItemData?delete={{routes.push-csv-to-service.delete-source-file}}")
    .streamCaching()
    .routeId("push-pricing-item-csv-to-service")
    .onException(PricingItemImportException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error importing pricing item data: ${exception.message} ${exception.stacktrace}")
      .end()
    .onException(HttpHostConnectException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error connecting to pricing item service host: ${exception.host}. Request body: ${body}")
      .end()
    .onException(HttpOperationFailedException.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error received from pricing item service: HTTP ${exception.statusCode}. Response body: ${exception.responseBody}. Request body: ${body}")
      .end()
    .onException(Exception.class)
      .handled(true)
      .log(LoggingLevel.ERROR, "Error: ${exception.message} ${exception.stacktrace}")
      .end()
    .log(LoggingLevel.INFO, "Beginning to import pricing item CSV: ${file:onlyname}")
    .unmarshal(new CsvDataFormat()
        .setSkipHeaderRecord(true)
        .setNullString(EMPTY)
        .setLazyLoad(true))
    .split(body(), new CsvAggregationStrategy())
      .streaming()
      .parallelProcessing().executorService(executorService)
      .setProperty("csvRowData").simple("${body}", List.class)
      .setProperty("csvFieldsByEntityAttribute").constant(csvFieldsByEntityAttribute)
      .bean(pricingItemCSVDataHandler)
      .marshal().json(JsonLibrary.Jackson)
      .setHeader(HttpHeaders.AUTHORIZATION, simple("Basic "+propertyServiceAuthorization))
      .choice()
        .when().simple("${property.itemId} != null")
          .setHeader(Exchange.HTTP_PATH, simple("${property.itemId}"))
          .to("rest:PUT:items?host={{backend.event-service.host}}")
        .otherwise()
          .to("rest:POST:items?host={{backend.event-service.host}}")
        .end()
      .setProperty("responseId").jsonpath("$.id", true)
      .setProperty("idColumnPosition").constant(PricingItemCsvFields.PRICING_ITEM_ID.getNumber())
      .choice()
        .when(exchangeProperty("responseId").isNull())
          .throwException(PricingItemImportException.class, "Unexpected rest response (no id returned)")
        .otherwise()
          .log(LoggingLevel.INFO, "Successful rest response, item id: ${property.responseId}")
        .end()
    .end()
    .choice()
      .when(simple("${exchangeProperty.badCsvData.size()} > 0"))
        .setBody(simple("${exchangeProperty.badCsvData}"))
        .marshal(new CsvDataFormat().setHeader(header))
        .setProperty("badRowsBody").simple("${body}")
      .end()
    .choice()
      .when(simple("${exchangeProperty.successfulRecords.size()} > 0"))
        .setBody(simple("${exchangeProperty.successfulRecords}"))
        .marshal(new CsvDataFormat().setHeader(header))
        .setProperty("sucessfulRowsBody").simple("${body}")
      .end() 
    .to("direct:sendImportReport").end()
    .log("Completed import for pricing item CSV: '${file:onlyname}'");
 
    // @formatter:on
  }

  private void mapFields(Map<String, String> csvFieldsByEntityAttribute) {
    csvFieldsByEntityAttribute.put("entity.id", PricingItemCsvFields.PRICING_ITEM_ID.getText());
    csvFieldsByEntityAttribute.put("event", PricingItemCsvFields.PRICING_EVENT_ID.getText());
    csvFieldsByEntityAttribute.put("event.id", PricingItemCsvFields.PRICING_EVENT_ID.getText());
    csvFieldsByEntityAttribute.put("auctionCurrency", PricingItemCsvFields.PRICING_AUCTION_CURRENCY.getText());
    csvFieldsByEntityAttribute.put("auctionCurrency.id", PricingItemCsvFields.PRICING_AUCTION_CURRENCY.getText());
    csvFieldsByEntityAttribute.put("auctionLowValue", PricingItemCsvFields.PRICING_AUCTION_LOW.getText());
    csvFieldsByEntityAttribute.put("auctionHighValue", PricingItemCsvFields.PRICING_AUCTION_HIGH.getText());
    csvFieldsByEntityAttribute.put("privateSaleCurrency", PricingItemCsvFields.PRICING_PS_CURRENCY.getText());
    csvFieldsByEntityAttribute.put("privateSaleCurrency.id", PricingItemCsvFields.PRICING_PS_CURRENCY.getText());
    csvFieldsByEntityAttribute.put("privateSaleNetSeller", PricingItemCsvFields.PRICING_PS_VALUE.getText());
    csvFieldsByEntityAttribute.put("insuranceCurrency", PricingItemCsvFields.PRICING_INSURANCE_CURRENCY.getText());
    csvFieldsByEntityAttribute.put("insuranceCurrency.id", PricingItemCsvFields.PRICING_INSURANCE_CURRENCY.getText());
    csvFieldsByEntityAttribute.put("insurancePrice", PricingItemCsvFields.PRICING_INSURANCE_VALUE.getText());
    csvFieldsByEntityAttribute.put("fairMarketValueCurrency", PricingItemCsvFields.PRICING_FMV_CURRENCY.getText());
    csvFieldsByEntityAttribute.put("fairMarketValueCurrency.id", PricingItemCsvFields.PRICING_FMV_CURRENCY.getText());
    csvFieldsByEntityAttribute.put("fairMarketValuePrice", PricingItemCsvFields.PRICING_FMV_VALUE.getText());
    csvFieldsByEntityAttribute.put("pipelineAuction.id", PricingItemCsvFields.PIPELINE_AUCTION.getText());
    csvFieldsByEntityAttribute.put("pipelinePrivateSale.id", PricingItemCsvFields.PIPELINE_PS.getText());
    csvFieldsByEntityAttribute.put("pipelineExhibition.id", PricingItemCsvFields.PIPELINE_EXHIBITION.getText());
    csvFieldsByEntityAttribute.put("legacyId", PricingItemCsvFields.OLD_ITEM_ID.getText());
    csvFieldsByEntityAttribute.put("comments", PricingItemCsvFields.ITEM_NOTES.getText());
    csvFieldsByEntityAttribute.put("description", PricingItemCsvFields.ITEM_DESCRIPTION.getText());
    csvFieldsByEntityAttribute.put("provenance", PricingItemCsvFields.ITEM_PROVENANCE.getText());
    csvFieldsByEntityAttribute.put("exhibition", PricingItemCsvFields.ITEM_EXHIBITION.getText());
    csvFieldsByEntityAttribute.put("literature", PricingItemCsvFields.ITEM_LITERATURE.getText());
    csvFieldsByEntityAttribute.put("condition", PricingItemCsvFields.ITEM_CONDITION.getText());
    csvFieldsByEntityAttribute.put("cataloguingNotes", PricingItemCsvFields.ITEM_ODNOTES.getText());
  }

}
