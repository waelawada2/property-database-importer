package com.sothebys.propertydb.importer.config;

import org.apache.camel.builder.ThreadPoolBuilder;
import org.apache.camel.CamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;

/**
 * Configuration of thread pools used by the application.
 *
 * @author Gregor Zurowski
 */
@Configuration
public class ThreadPoolConfiguration {

  @Autowired
  private CamelContext camelContext;

  @Bean("executorServiceWebsiteImages")
  public ExecutorService executorServiceWebsiteImages(
      @Value("${thread-pool.website-images.pool-size:5}") int poolSize,
      @Value("${thread-pool.website-images.max-pool-size:5}") int maxPoolSize) throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("website-images");
    // @formatter:on
  }

  @Bean("executorServicePushCsv")
  public ExecutorService executorServicePushCsv(
      @Value("${thread-pool.property-service.pool-size:2}") int poolSize,
      @Value("${thread-pool.property-service.max-pool-size:5}") int maxPoolSize) throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("property-service");
    // @formatter:on
  }

  @Bean("executorServiceTransactionEventCsv")
  public ExecutorService executorServiceTransactionEventCsv(
      @Value("${thread-pool.event-service.events.transaction.pool-size:2}") int poolSize,
      @Value("${thread-pool.event-service.events.transaction.max-pool-size:5}") int maxPoolSize)
      throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("transaction-event");
    // @formatter:on
  }

  @Bean("executorServicePublicationEventCsv")
  public ExecutorService executorServicePublicationEventCsv(
      @Value("${thread-pool.event-service.events.publication.pool-size:2}") int poolSize,
      @Value("${thread-pool.event-service.events.publication.max-pool-size:5}") int maxPoolSize)
      throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("publication-event");
    // @formatter:on
  }

  @Bean("executorServicePricingEventCsv")
  public ExecutorService executorServicePricingEventCsv(
      @Value("${thread-pool.event-service.events.pricing.pool-size:2}") int poolSize,
      @Value("${thread-pool.event-service.events.pricing.max-pool-size:5}") int maxPoolSize)
      throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("pricing-event");
    // @formatter:on
  }

  @Bean("executorServiceConsignmentPurchaseEventCsv")
  public ExecutorService executorServiceConsignmentPurchaseEventCsv(
      @Value("${thread-pool.event-service.events.consignment-purchase.pool-size:2}") int poolSize,
      @Value("${thread-pool.event-service.events.consignment-purchase.max-pool-size:5}") int maxPoolSize)
      throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("consignment-purchase-event");
    // @formatter:on
  }
  
  @Bean("executorServiceTransactionItemCsv")
  public ExecutorService executorServiceTransactionItemCsv(
      @Value("${thread-pool.event-service.items.transaction.pool-size:2}") int poolSize,
      @Value("${thread-pool.event-service.items.transaction.max-pool-size:5}") int maxPoolSize)
      throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("transaction-item");
    // @formatter:on
  }
  
  @Bean("executorServicePublicationItemCsv")
  public ExecutorService executorServicePublicationItemCsv(
      @Value("${thread-pool.event-service.items.publication.pool-size:2}") int poolSize,
      @Value("${thread-pool.event-service.items.publication.max-pool-size:5}") int maxPoolSize)
      throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("publication-item");
    // @formatter:on
  }
  
  @Bean("executorServicePricingItemCsv")
  public ExecutorService executorServicePricingItemCsv(
      @Value("${thread-pool.event-service.items.pricing.pool-size:2}") int poolSize,
      @Value("${thread-pool.event-service.items.pricing.max-pool-size:5}") int maxPoolSize)
      throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("pricing-item");
    // @formatter:on
  }
  
  @Bean("executorServiceEstablishmentCsv")
  public ExecutorService executorServiceEstablishmentCsv(
      @Value("${thread-pool.event-service.establishment.pool-size:2}") int poolSize,
      @Value("${thread-pool.event-service.establishment.max-pool-size:5}") int maxPoolSize)
      throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("establishment");
    // @formatter:on
  }
  
  @Bean("executorServiceClientSourceCsv")
  public ExecutorService executorServiceClientSourceCsv(
      @Value("${thread-pool.event-service.client-source.pool-size:2}") int poolSize,
      @Value("${thread-pool.event-service.client-source.max-pool-size:5}") int maxPoolSize)
      throws Exception {
    // @formatter:off
    return new ThreadPoolBuilder(camelContext)
        .poolSize(poolSize)
        .maxPoolSize(maxPoolSize)
        .build("client-source");
    // @formatter:on
  }

}
