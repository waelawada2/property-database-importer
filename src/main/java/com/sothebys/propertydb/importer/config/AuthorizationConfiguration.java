package com.sothebys.propertydb.importer.config;

import java.util.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Minhtri Tran
 */
@Configuration
public class AuthorizationConfiguration {
  /**
   * Sets the basic authentication credentials for the property service back-end.
   */
  @Bean
  public String propertyServiceAuthorization(
      @Value("${backend.property-service.username}") String username,
      @Value("${backend.property-service.password}") String password) {
    return Base64.getEncoder().encodeToString(String.format("%s:%s", username, password).getBytes());
  }
}
