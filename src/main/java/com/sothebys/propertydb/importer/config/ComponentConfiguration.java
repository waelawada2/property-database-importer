package com.sothebys.propertydb.importer.config;

import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

/**
 * @author Gregor Zurowski
 */
@Configuration
public class ComponentConfiguration {

  @Bean("amazonS3Client")
  public AmazonS3 amazonS3Client(@Value("${s3.imports.access-key}") String accessKey,
      @Value("${s3.imports.secret-key}") String secretKey, @Value("${s3.region}") String region) {

    BasicAWSCredentials basicAwsCredentials = new BasicAWSCredentials(accessKey, secretKey);
    AWSCredentialsProvider credentialsProvider =
        new AWSStaticCredentialsProvider(basicAwsCredentials);
    return AmazonS3ClientBuilder.standard().withRegion(region).withCredentials(credentialsProvider)
        .build();
  }

  @Bean("images3Client")
  public AmazonS3 amazonImagesS3Client(@Value("${s3.images.access-key}") String accessKey,
      @Value("${s3.images.secret-key}") String secretKey,
      @Value("${s3.region}") String region) {

    BasicAWSCredentials basicAwsCredentials = new BasicAWSCredentials(accessKey, secretKey);
    AWSCredentialsProvider credentialsProvider =
        new AWSStaticCredentialsProvider(basicAwsCredentials);
    return AmazonS3ClientBuilder.standard().withRegion(region).withCredentials(credentialsProvider)
        .build();
  }

  @Bean("imageS3TransferManager")
  public TransferManager imagesS3TransferManager(@Qualifier("images3Client") AmazonS3 imageS3Client) {
    return TransferManagerBuilder.standard().withS3Client(imageS3Client)
        .withShutDownThreadPools(false).build();
  }

  @Bean("importerFileS3Client")
  public AmazonS3 amazonImporterFileS3Client(
      @Value("${s3.importer-output.access-key}") String accessKey,
      @Value("${s3.importer-output.secret-key}") String secretKey,
      @Value("${s3.region}") String region) {

    BasicAWSCredentials basicAwsCredentials = new BasicAWSCredentials(accessKey, secretKey);
    AWSCredentialsProvider credentialsProvider =
        new AWSStaticCredentialsProvider(basicAwsCredentials);
    return AmazonS3ClientBuilder.standard().withRegion(region).withCredentials(credentialsProvider)
        .build();
  }
  
  @Bean("importerOutputCSVFileS3Client")
  public AmazonS3 amazonImporterOutputS3Client(@Value("${s3.importer-output.access-key}") String accessKey,
      @Value("${s3.importer-output.secret-key}") String secretKey, @Value("${s3.region}") String region) {

    BasicAWSCredentials basicAwsCredentials =
        new BasicAWSCredentials(accessKey, secretKey);
    AWSCredentialsProvider credentialsProvider =
        new AWSStaticCredentialsProvider(basicAwsCredentials);
    return AmazonS3ClientBuilder.standard().withRegion(region)
        .withCredentials(credentialsProvider).build();
  }

}
