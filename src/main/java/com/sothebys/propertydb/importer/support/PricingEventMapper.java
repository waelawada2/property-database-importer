package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.AFFILIATION_CLIENT_ID;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.AFFILIATION_SOURCE_ID;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.AFFILIATION_SOURCE_TYPE;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.AFFILIATION_TYPE;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.CLIENT_ID;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.CLIENT_SOURCE_ID;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.CLIENT_SOURCE_TYPE;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.CLIENT_STATUS;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.DATE;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.DESIGNATION;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.ORIGIN;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.PRICING_CODE;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.PRICING_ID;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.PRICING_NOTES;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.PROPOSAL_CODE;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.TYPE;
import static com.sothebys.propertydb.importer.support.PricingEventCsvFields.VAL_SYSTEM_ID;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.time.format.DateTimeParseException;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.exception.PricingEventImportException;
import com.sothebys.propertydb.importer.model.event.AffiliatedClientDto;
import com.sothebys.propertydb.importer.model.event.AffiliatedTypeDto;
import com.sothebys.propertydb.importer.model.event.ClientSourceDto;
import com.sothebys.propertydb.importer.model.event.ClientSourceTypeDto;
import com.sothebys.propertydb.importer.model.event.ClientStatusDto;
import com.sothebys.propertydb.importer.model.event.MainClientDto;
import com.sothebys.propertydb.importer.model.event.PricingEventDto;
import com.sothebys.propertydb.importer.model.event.ValuationOriginDto;
import com.sothebys.propertydb.importer.model.event.ValuationTypeDto;

@Component
public class PricingEventMapper {

  public PricingEventDto map(List<String> csv) {
    
    ValuationOriginDto valuationOriginDto = null;
    if(isNotBlank(csv.get(ORIGIN.getNumber()))) {
      valuationOriginDto = new ValuationOriginDto();
      valuationOriginDto.setId(csv.get(ORIGIN.getNumber()));
    }
    
    ValuationTypeDto valuationTypeDto = null;
    if(isNotBlank(csv.get(TYPE.getNumber()))) {
      valuationTypeDto = new ValuationTypeDto();
      valuationTypeDto.setId(csv.get(TYPE.getNumber()));
    }
    
    MainClientDto mainClientDto = new MainClientDto();
    
    ClientSourceDto clientSourceDto = null;
    if(isNotBlank(csv.get(CLIENT_SOURCE_ID.getNumber()))) {
      clientSourceDto = new ClientSourceDto();
      clientSourceDto.setId(csv.get(CLIENT_SOURCE_ID.getNumber()));
    }
    
    ClientSourceTypeDto clientSourceTypeDto = null;
    if(isNotBlank(csv.get(CLIENT_SOURCE_TYPE.getNumber()))) {
      clientSourceTypeDto = new ClientSourceTypeDto();
      clientSourceTypeDto.setId(csv.get(CLIENT_SOURCE_TYPE.getNumber()));
    }
    
    ClientStatusDto clientStatusDto = null;
    if(isNotBlank(csv.get(CLIENT_STATUS.getNumber()))) {
      clientStatusDto = new ClientStatusDto();
      clientStatusDto.setId(csv.get(CLIENT_STATUS.getNumber()));
    }
    
    mainClientDto.setClientSource(clientSourceDto);
    mainClientDto.setClientSourceType(clientSourceTypeDto);
    mainClientDto.setClientStatus(clientStatusDto);
    mainClientDto.setClientId(csv.get(CLIENT_ID.getNumber()));
    mainClientDto.setDisplay(csv.get(DESIGNATION.getNumber()));
    
    ClientSourceDto affiliatedClientSourceDto = null;
    if(isNotBlank(csv.get(AFFILIATION_SOURCE_ID.getNumber()))) {
      affiliatedClientSourceDto = new ClientSourceDto();
      affiliatedClientSourceDto.setId(csv.get(AFFILIATION_SOURCE_ID.getNumber()));
    }
    
    ClientSourceTypeDto affiliatedClientSourceTypeDto = null;
    if(isNotBlank(csv.get(AFFILIATION_SOURCE_TYPE.getNumber()))) {
      affiliatedClientSourceTypeDto = new ClientSourceTypeDto();
      affiliatedClientSourceTypeDto.setId(csv.get(AFFILIATION_SOURCE_TYPE.getNumber()));
    }
    
    AffiliatedTypeDto affiliatedTypeDto = null;
    if(isNotBlank(csv.get(AFFILIATION_TYPE.getNumber()))) {
      affiliatedTypeDto = new AffiliatedTypeDto();
      affiliatedTypeDto.setId(csv.get(AFFILIATION_TYPE.getNumber()));
    }
    
    PricingEventDto pricingEventDto = new PricingEventDto();
    
    
    if (affiliatedClientSourceDto != null || affiliatedClientSourceTypeDto != null
        || affiliatedTypeDto != null || isNotBlank(csv.get(AFFILIATION_CLIENT_ID.getNumber()))) {
      AffiliatedClientDto affiliatedClientDto = new AffiliatedClientDto();
      
      
      affiliatedClientDto.setClientSource(affiliatedClientSourceDto);
      affiliatedClientDto.setClientSourceType(affiliatedClientSourceTypeDto);
      affiliatedClientDto.setAffiliatedType(affiliatedTypeDto);
      affiliatedClientDto.setClientId(csv.get(AFFILIATION_CLIENT_ID.getNumber()));
      
      pricingEventDto.setAffiliatedClient(affiliatedClientDto);
    }
    
    pricingEventDto.setId(csv.get(PRICING_ID.getNumber()));
    pricingEventDto.setCode(csv.get(PRICING_CODE.getNumber()));
    
    pricingEventDto.setValuationOrigin(valuationOriginDto);
    pricingEventDto.setValuationType(valuationTypeDto);
    pricingEventDto.setMainClient(mainClientDto);
    
    try {
      pricingEventDto.setDate(CsvStringConverter.toDate(csv.get(DATE.getNumber()), "d/MMM/yyyy"));
    } catch (DateTimeParseException e) {
      throw new PricingEventImportException("Date: "+e.getMessage()+" expected format is \"d/MMM/yyyy\"", e);
    }
    
    pricingEventDto.setProposal(csv.get(PROPOSAL_CODE.getNumber()));
    pricingEventDto.setNotes(csv.get(PRICING_NOTES.getNumber()));
    pricingEventDto.setValuationId(csv.get(VAL_SYSTEM_ID.getNumber()));
    
    
    return pricingEventDto;
  }
}
