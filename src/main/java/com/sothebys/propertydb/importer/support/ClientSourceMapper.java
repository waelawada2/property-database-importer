package com.sothebys.propertydb.importer.support;


import static com.sothebys.propertydb.importer.support.ClientSourceCsvFields.SOURCE_ID;
import static com.sothebys.propertydb.importer.support.ClientSourceCsvFields.SOURCE_NAME;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.model.event.ClientSourceDto;

@Component
public class ClientSourceMapper {
  public ClientSourceDto map(List<String> csv) {
    ClientSourceDto clientSourceDto = new ClientSourceDto();
    clientSourceDto.setId(csv.get(SOURCE_ID.getNumber()));
    clientSourceDto.setName(csv.get(SOURCE_NAME.getNumber()));
    
    return clientSourceDto;
  }

}
