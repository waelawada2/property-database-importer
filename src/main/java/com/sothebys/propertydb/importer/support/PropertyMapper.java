package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.CsvFields.ARCHIVE_NUMBERS;
import static com.sothebys.propertydb.importer.support.CsvFields.ARTIST_CODE;
import static com.sothebys.propertydb.importer.support.CsvFields.ARTIST_ID;
import static com.sothebys.propertydb.importer.support.CsvFields.ARTIST_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.AUTHENTICATION;
import static com.sothebys.propertydb.importer.support.CsvFields.BON_A_TIRER_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.CANCELLATION_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.CAST_YEAR_CIRCA;
import static com.sothebys.propertydb.importer.support.CsvFields.CAST_YEAR_END;
import static com.sothebys.propertydb.importer.support.CsvFields.CAST_YEAR_START;
import static com.sothebys.propertydb.importer.support.CsvFields.CATALOGUE_RAISONEE_NUMBER;
import static com.sothebys.propertydb.importer.support.CsvFields.CATALOGUE_RAISONEE_VOLUME;
import static com.sothebys.propertydb.importer.support.CsvFields.CATEGORY;
import static com.sothebys.propertydb.importer.support.CsvFields.CERTIFICATE;
import static com.sothebys.propertydb.importer.support.CsvFields.COLOR_TRIAL_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.DEDICATED_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.DEPTH_CM;
import static com.sothebys.propertydb.importer.support.CsvFields.DEPTH_IN;
import static com.sothebys.propertydb.importer.support.CsvFields.EDITION_NOTES;
import static com.sothebys.propertydb.importer.support.CsvFields.EDITION_NUMBER;
import static com.sothebys.propertydb.importer.support.CsvFields.EDITION_OVERRIDE;
import static com.sothebys.propertydb.importer.support.CsvFields.EDITION_STATUS;
import static com.sothebys.propertydb.importer.support.CsvFields.EDITION_TYPE;
import static com.sothebys.propertydb.importer.support.CsvFields.EOS_OBJECT_ID;
import static com.sothebys.propertydb.importer.support.CsvFields.FLAGS_AUTHENTICITY;
import static com.sothebys.propertydb.importer.support.CsvFields.FLAGS_CONDITION;
import static com.sothebys.propertydb.importer.support.CsvFields.FLAGS_EXPORT_COUNTRY;
import static com.sothebys.propertydb.importer.support.CsvFields.FLAGS_MEDIUM;
import static com.sothebys.propertydb.importer.support.CsvFields.FLAGS_RESTITUTION;
import static com.sothebys.propertydb.importer.support.CsvFields.FLAGS_SFS;
import static com.sothebys.propertydb.importer.support.CsvFields.FOUNDRY;
import static com.sothebys.propertydb.importer.support.CsvFields.HEIGHT_CM;
import static com.sothebys.propertydb.importer.support.CsvFields.HEIGHT_IN;
import static com.sothebys.propertydb.importer.support.CsvFields.HORS_COMMERCE_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.IMAGE_FILE_NAME;
import static com.sothebys.propertydb.importer.support.CsvFields.IMAGE_UPDATE_Y_N;
import static com.sothebys.propertydb.importer.support.CsvFields.MEASUREMENT_OVERRIDE;
import static com.sothebys.propertydb.importer.support.CsvFields.MEDIUM;
import static com.sothebys.propertydb.importer.support.CsvFields.MUSEUM_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.OBJECT_CODE;
import static com.sothebys.propertydb.importer.support.CsvFields.ORIGINAL_TITLE;
import static com.sothebys.propertydb.importer.support.CsvFields.PARTS;
import static com.sothebys.propertydb.importer.support.CsvFields.POSTHUMOUS;
import static com.sothebys.propertydb.importer.support.CsvFields.PRESIZE_DESCRIPTION;
import static com.sothebys.propertydb.importer.support.CsvFields.PRINTERS_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.PROGRESSIVE_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.RESEARCHER_NAME;
import static com.sothebys.propertydb.importer.support.CsvFields.RESEARCHER_NOTES_DATE;
import static com.sothebys.propertydb.importer.support.CsvFields.RESEARCHER_NOTES_TEXT;
import static com.sothebys.propertydb.importer.support.CsvFields.RESEARCH_COMPLETE_CHECK;
import static com.sothebys.propertydb.importer.support.CsvFields.RIGHT_TO_PRODUCE_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.SERIES_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.SIGNATURE;
import static com.sothebys.propertydb.importer.support.CsvFields.SIGNED;
import static com.sothebys.propertydb.importer.support.CsvFields.SORT_CODE;
import static com.sothebys.propertydb.importer.support.CsvFields.STAMPED;
import static com.sothebys.propertydb.importer.support.CsvFields.SUB_CATEGORY;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_ANIMAL;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_FIGURE;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_GENDER;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_LOCATION;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_MAIN_COLOUR;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_OTHER;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_POSITION;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_SITTER;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_SUBJECT;
import static com.sothebys.propertydb.importer.support.CsvFields.TAG_TEXT;
import static com.sothebys.propertydb.importer.support.CsvFields.TITLE;
import static com.sothebys.propertydb.importer.support.CsvFields.TRIAL_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.WEIGHT;
import static com.sothebys.propertydb.importer.support.CsvFields.WIDTH_CM;
import static com.sothebys.propertydb.importer.support.CsvFields.WIDTH_IN;
import static com.sothebys.propertydb.importer.support.CsvFields.WORKING_PROOF_TOTAL;
import static com.sothebys.propertydb.importer.support.CsvFields.YEAR_CIRCA;
import static com.sothebys.propertydb.importer.support.CsvFields.YEAR_END;
import static com.sothebys.propertydb.importer.support.CsvFields.YEAR_OVERRIDE;
import static com.sothebys.propertydb.importer.support.CsvFields.YEAR_START;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import com.sothebys.propertydb.importer.exception.PropertyImportException;
import com.sothebys.propertydb.importer.model.PropertyCreateRequestDTO;
import com.sothebys.propertydb.importer.model.PropertyCreateRequestDTO.FlagsRequestDTO;
import com.sothebys.propertydb.importer.model.PropertyCreateRequestDTO.TagsRequestDTO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Mapper class for mapping between the CSV representation and the DTO representation of a property.
 *
 * @author Gregor Zurowski
 */
public class PropertyMapper {

  protected static final String NESTED_VALUE_SEPARATOR = "\\|";
  protected static final String DATE_FORMAT = "MM/dd/yyyy";
  protected static Logger log = LoggerFactory.getLogger(PropertyMapper.class);

  public PropertyCreateRequestDTO map(List<String> csv, List<String> errorDetailsList) {
    PropertyCreateRequestDTO dto = new PropertyCreateRequestDTO();
    mapNumericFields(csv, dto, errorDetailsList);
    mapStringFields(csv, dto, errorDetailsList);
    mapBooleanFields(csv, dto, errorDetailsList);
    mapDateFields(csv, dto, errorDetailsList);
    mapComplexFields(csv, dto, errorDetailsList);
    dto.setErrorDetailsList(Collections.unmodifiableList(errorDetailsList));
    return dto;
  }

  private void mapComplexFields(List<String> csv, PropertyCreateRequestDTO dto, List<String> errorDetailsList) {
    if (isNotBlank(csv.get(ARTIST_ID))) {
      dto.setArtistIds(convertMultiValueStringToList(csv.get(ARTIST_ID)));
    } else {
      errorDetailsList.add("Artist Id cannot be empty.");
    }
    if (isNotBlank(csv.get(ARTIST_CODE))) {
      if (csv.get(ARTIST_CODE).length() >= 20) {
        errorDetailsList.add(
            String.format("Maximum value of Artist Code is  99,99,99,99,99,99,99,99,999"));
      }
      try {
        dto.setUlanIds(convertMultiValueStringToList(csv.get(ARTIST_CODE), Long::parseLong));
      } catch (NumberFormatException nfe) {
        errorDetailsList.add(String
            .format("Artist Code should always be a Numerical value - '%s'", csv.get(ARTIST_CODE)));
      }
    }
    if (isNotEmpty(csv.get(ARCHIVE_NUMBERS))) {
      dto.setArchiveNumbers(convertMultiValueStringToList(csv.get(ARCHIVE_NUMBERS),
          n -> new PropertyCreateRequestDTO.ArchiveNumberDTO(null, n)));
    }
    if (isNotEmpty(csv.get(CATALOGUE_RAISONEE_NUMBER))
        || isNotEmpty(csv.get(CATALOGUE_RAISONEE_VOLUME))) {
      translateCataloguesRaisonees(csv.get(CATALOGUE_RAISONEE_NUMBER),
          csv.get(CATALOGUE_RAISONEE_VOLUME), dto, errorDetailsList);
    }
    if (isNotEmpty(csv.get(FLAGS_EXPORT_COUNTRY))) {
      translateFlagCountry(csv.get(FLAGS_EXPORT_COUNTRY), dto);
    }
    if (isNotEmpty(csv.get(OBJECT_CODE))) {
      dto.setObjectCodes(convertMultiValueStringToList(csv.get(OBJECT_CODE)));
    }

    if (isNotEmpty(csv.get(TAG_SUBJECT))) {
      translateImageSubjects(csv.get(TAG_SUBJECT), dto);
    }

    if (isNotEmpty(csv.get(TAG_FIGURE))) {
      translateImageFigure(csv.get(TAG_FIGURE), dto);
    }

    if (isNotEmpty(csv.get(TAG_GENDER))) {
      translateImageGenders(csv.get(TAG_GENDER), dto);
    }

    if (isNotEmpty(csv.get(TAG_POSITION))) {
      translateImagePositions(csv.get(TAG_POSITION), dto);
    }

    if (isNotEmpty(csv.get(TAG_ANIMAL))) {
      translateImageAnimals(csv.get(TAG_ANIMAL), dto);
    }

    if (isNotEmpty(csv.get(TAG_MAIN_COLOUR))) {
      translateImageMainColours(csv.get(TAG_MAIN_COLOUR), dto, errorDetailsList);
    }

    if (isNotEmpty(csv.get(TAG_TEXT))) {
      translateImageText(csv.get(TAG_TEXT), dto, errorDetailsList);
    }

    if (isNotEmpty(csv.get(TAG_LOCATION))) {
      translateExpertiseLocation(csv.get(TAG_LOCATION), dto);
    }

    if (isNotEmpty(csv.get(TAG_SITTER))) {
      translateExpertiseSitters(csv.get(TAG_SITTER), dto);
    }

    if (isNotEmpty(csv.get(TAG_OTHER))) {
      translateOtherTags(csv.get(TAG_OTHER), dto);
    }
  }

  private void translateImageSubjects(String imageSubjects, PropertyCreateRequestDTO dto) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);
    tagsDTO.setImageSubjects(convertMultiValueStringToList(imageSubjects));
    dto.setTags(tagsDTO);
  }

  private void translateImageFigure(String imageFigure, PropertyCreateRequestDTO dto) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);
    tagsDTO.setImageFigure(imageFigure);
    dto.setTags(tagsDTO);
  }

  private void translateImageGenders(String imageGenders, PropertyCreateRequestDTO dto) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);
    tagsDTO.setImageGenders(convertMultiValueStringToList(imageGenders));
    dto.setTags(tagsDTO);
  }

  private void translateImagePositions(String imagePositions, PropertyCreateRequestDTO dto) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);
    tagsDTO.setImagePositions(convertMultiValueStringToList(imagePositions));
    dto.setTags(tagsDTO);
  }

  private void translateImageAnimals(String imageAnimals, PropertyCreateRequestDTO dto) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);
    tagsDTO.setImageAnimals(convertMultiValueStringToList(imageAnimals));
    dto.setTags(tagsDTO);
  }

  private void translateImageMainColours(String imageMainColours, PropertyCreateRequestDTO dto,
      List<String> errorDetailsList) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);
    List<String> imageMainColorsList = convertMultiValueStringToList(imageMainColours);
    if (imageMainColorsList != null && imageMainColorsList.size() <= 3) {
      tagsDTO.setImageMainColours(imageMainColorsList);
    } else {
      errorDetailsList.add(
          String.format("Cannot add more than 3 main colors under TAG_MAIN_COLOUR property - %s",
              imageMainColours));
    }
    dto.setTags(tagsDTO);
  }

  private void translateImageText(String imageText, PropertyCreateRequestDTO dto, List<String> errorDetailsList) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);
    if(imageText == null || imageText.trim().length() <= 300) {
      tagsDTO.setImageText(imageText);
    } else {
      errorDetailsList.add("Image Text cannot be more than 300 characters");
    }
    dto.setTags(tagsDTO);
  }

  private void translateExpertiseLocation(String expertiseLocation, PropertyCreateRequestDTO dto) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);
    tagsDTO.setExpertiseLocation(expertiseLocation);
    dto.setTags(tagsDTO);
  }

  private void translateExpertiseSitters(String expertiseSitters, PropertyCreateRequestDTO dto) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);

    Objects.requireNonNull(expertiseSitters);

    tagsDTO.setExpertiseSitters(Arrays.stream(expertiseSitters.split(NESTED_VALUE_SEPARATOR))
        .map(n -> new PropertyCreateRequestDTO.TagsRequestDTO.ExpertiseSitterDTO(null, n))
        .collect(Collectors.toList()));

    dto.setTags(tagsDTO);
  }

  private void translateOtherTags(String otherTags, PropertyCreateRequestDTO dto) {
    TagsRequestDTO tagsDTO = Optional.ofNullable(dto.getTags()).orElseGet(TagsRequestDTO::new);
    tagsDTO.setOtherTags(convertMultiValueStringToList(otherTags));
    dto.setTags(tagsDTO);
  }

  private void translateFlagCountry(final String country, PropertyCreateRequestDTO dto) {
    Objects.requireNonNull(country);
    dto.getFlags().setFlagsCountry(country);
  }

  private void translateCataloguesRaisonees(String catalogueRaisoneeNumber,
      String catalogueRaisoneeVolume, PropertyCreateRequestDTO dto, List<String> errorDetailsList) {
    if (isNotBlank(catalogueRaisoneeVolume) || isNotBlank(catalogueRaisoneeNumber)) {

      String[] catalogueRaisoneeNumbers = !StringUtils.isEmpty(catalogueRaisoneeNumber)
          ? catalogueRaisoneeNumber.split(NESTED_VALUE_SEPARATOR, -1)
          : new String[0];
      String[] catalogueRaisoneeVolumes = !StringUtils.isEmpty(catalogueRaisoneeVolume)
          ? catalogueRaisoneeVolume.split(NESTED_VALUE_SEPARATOR, -1)
          : new String[0];

      int crNumbersLength =
          catalogueRaisoneeNumbers.length == 0 ? 1 : catalogueRaisoneeNumbers.length;
      int crVolumesLength =
          catalogueRaisoneeVolumes.length == 0 ? 1 : catalogueRaisoneeVolumes.length;

      StringBuilder crNumber = new StringBuilder(
          isNotEmpty(catalogueRaisoneeNumber) ? catalogueRaisoneeNumber : StringUtils.EMPTY);
      StringBuilder crVolume = new StringBuilder(
          isNotEmpty(catalogueRaisoneeVolume) ? catalogueRaisoneeVolume : StringUtils.EMPTY);

      if (crNumbersLength > crVolumesLength) {
        for (int i = 0; i < crNumbersLength - crVolumesLength; i++) {
          crVolume.append("|");
        }
      } else if (crVolumesLength > crNumbersLength) {
        for (int i = 0; i < crVolumesLength - crNumbersLength; i++) {
          crNumber.append("|");
        }
      }
      String[] finalCatalogueRaisoneeNumbers = crNumber.toString().split(NESTED_VALUE_SEPARATOR, -1);
      String[] finalCatalogueRaisoneeVolumes = crVolume.toString().split(NESTED_VALUE_SEPARATOR, -1);
      for (String finalCatalogueRaisoneeNumber : finalCatalogueRaisoneeNumbers) {
        if (finalCatalogueRaisoneeNumber.trim().length() > 20) {
          errorDetailsList.add(String
              .format("PropertyName CR_Number should not have more than 20 characters - '%s'",
                  finalCatalogueRaisoneeNumber.trim()));
        }
      }
      for (String finalCatalogueRaisoneeVolume : finalCatalogueRaisoneeVolumes) {
        if (finalCatalogueRaisoneeVolume.trim().length() > 10) {
          errorDetailsList.add(String
              .format("PropertyName CR_Volume should not have more than 10 characters - '%s'",
                  finalCatalogueRaisoneeVolume.trim()));
        }
      }
      // @formatter:off
      dto.setPropertyCatalogueRaisonees(IntStream.range(0, Math.min(finalCatalogueRaisoneeNumbers.length, finalCatalogueRaisoneeVolumes.length))
          .mapToObj(i -> new PropertyCreateRequestDTO.CatalogueRaisonee(
              finalCatalogueRaisoneeNumbers[i].trim(), finalCatalogueRaisoneeVolumes[i].trim()))
          .collect(Collectors.toList()));
      // @formatter:on
    }
  }

  private void mapBooleanFields(List<String> csv, PropertyCreateRequestDTO dto, List<String> errorDetailsList) {
    dto.setPosthumous(convertStringToBoolean("Posthumous", csv.get(POSTHUMOUS), errorDetailsList));
    dto.setYearCirca(convertStringToBoolean("YearCirca", csv.get(YEAR_CIRCA), errorDetailsList));
    dto.setCastYearCirca(convertStringToBoolean("CastYearCirca", csv.get(CAST_YEAR_CIRCA), errorDetailsList));
    dto.setStamped(convertStringToBoolean("Stamped", csv.get(STAMPED), errorDetailsList));
    dto.setSigned(convertStringToBoolean("Signed", csv.get(SIGNED), errorDetailsList));
    dto.setCertificate(convertStringToBoolean("Certificate", csv.get(CERTIFICATE), errorDetailsList));
    dto.setAuthentication(convertStringToBoolean("Authentication", csv.get(AUTHENTICATION), errorDetailsList));
    dto.setResearchCompleteCheck(convertStringToBoolean("Research_Complete_check", csv.get(RESEARCH_COMPLETE_CHECK), errorDetailsList));
    FlagsRequestDTO flagsRequest = new FlagsRequestDTO();
    flagsRequest.setFlagAuthenticity(
        convertStringToBoolean("FlagAuthenticity", csv.get(FLAGS_AUTHENTICITY), errorDetailsList));
    flagsRequest
        .setFlagRestitution(convertStringToBoolean("FlagRestitution", csv.get(FLAGS_RESTITUTION), errorDetailsList));
    flagsRequest
        .setFlagCondition(convertStringToBoolean("FlagCondition", csv.get(FLAGS_CONDITION), errorDetailsList));
    flagsRequest
        .setFlagSfs(convertStringToBoolean("FlagSfs", csv.get(FLAGS_SFS), errorDetailsList));
    flagsRequest.setFlagMedium(convertStringToBoolean("FlagMedium", csv.get(FLAGS_MEDIUM), errorDetailsList));
    dto.setFlags(flagsRequest);
  }

  private void mapStringFields(List<String> csv, PropertyCreateRequestDTO dto, List<String> errorDetailsList) {
    dto.setExternalId(csv.get(EOS_OBJECT_ID));
    dto.setCategory(csv.get(CATEGORY));
    dto.setFoundry(csv.get(FOUNDRY));
    String medium = csv.get(MEDIUM) != null ? csv.get(MEDIUM).trim() : "";
    if (medium.length() <= 1000) {
      dto.setMedium(medium);
    } else {
      errorDetailsList.add(String
          .format("PropertyName Medium should not have more than 1000 characters - '%s'", medium));
    }
    String originalTitle = csv.get(ORIGINAL_TITLE) != null ? csv.get(ORIGINAL_TITLE).trim() : "";
    if (originalTitle.length() <= 500) {
      dto.setOriginalTitle(originalTitle);
    } else {
      errorDetailsList.add(String.format(
          "PropertyName OriginalTitle should not have more than 500 characters - '%s'",
          originalTitle));
    }
    dto.setResearcherName(csv.get(RESEARCHER_NAME));
    String researcherNotesText =
        csv.get(RESEARCHER_NOTES_TEXT) != null ? csv.get(RESEARCHER_NOTES_TEXT).trim() : "";
    if (researcherNotesText.length() <= 1000) {
      dto.setResearcherNotesText(researcherNotesText);
    } else {
      errorDetailsList.add(String.format(
          "PropertyName ResearcherNotesText should not have more than 1000 characters - '%s'",
          researcherNotesText));
    }
    String signature = csv.get(SIGNATURE) != null ? csv.get(SIGNATURE).trim() : "";
    if (signature.length() <= 1000) {
      dto.setSignature(signature);
    } else {
      errorDetailsList.add(String.format(
          "PropertyName Signature should not have more than 1000 characters - '%s'", signature));
    }
    dto.setSortCode(csv.get(SORT_CODE));
    dto.setSubCategory(csv.get(SUB_CATEGORY));
    String title = csv.get(TITLE) != null ? csv.get(TITLE).trim() : "";
    if (title.length() <= 500) {
      dto.setTitle(title);
    } else {
      errorDetailsList.add(String
          .format("PropertyName Title should not have more than 500 characters - '%s'", title));
    }
    dto.setYearText(csv.get(YEAR_OVERRIDE));
    dto.setPresizeName(csv.get(PRESIZE_DESCRIPTION));
    dto.setEditionSizeTypeName(
        isNotBlank(csv.get(EDITION_STATUS)) ? csv.get(EDITION_STATUS).trim() : "Unspecified");
    dto.setEditionName(csv.get(EDITION_TYPE));
    if (StringUtils.isBlank(dto.getExternalId())
        || "Y".equalsIgnoreCase(csv.get(IMAGE_UPDATE_Y_N))) {
      dto.setImagePath(StringUtils.defaultString(csv.get(IMAGE_FILE_NAME)));
    } else if (!"N".equalsIgnoreCase(csv.get(IMAGE_UPDATE_Y_N))) {
      errorDetailsList.add("Image Update field needs to be set to Y or N");
    }
    String sizeText =
        csv.get(MEASUREMENT_OVERRIDE) != null ? csv.get(MEASUREMENT_OVERRIDE).trim() : "";
    if (sizeText.length() <= 500) {
      dto.setSizeText(sizeText);
    } else {
      errorDetailsList.add(String.format(
          "PropertyName SizeText should not have more than 500 characters - '%s'", sizeText));
    }
    dto.setEditionNumber(csv.get(EDITION_NUMBER));
    dto.setEditionOverride(csv.get(EDITION_OVERRIDE));
    dto.setEditionSizeNotes(csv.get(EDITION_NOTES));
  }

  private void mapNumericFields(List<String> csv, PropertyCreateRequestDTO dto, List<String> errorDetailsList) {
    IntPredicate rangeMinusOneTo99000 = x -> x >= -1 && x <= 99000;
    String rangeInvalid = "Value for the propertyName='%s' should be between -1 to 99000";
    IntPredicate positiveOnly = x -> x >= 0;
    String positiveInvalid = "couldn't allow negative value for the propertyName='%s'";

    dto.setArtistProofTotal(convertStringToInt("ArtistProofTotal", csv.get(ARTIST_PROOF_TOTAL),
        rangeMinusOneTo99000, rangeInvalid, errorDetailsList));
    dto.setBonATirerTotal(convertStringToInt("BonATirerTotal", csv.get(BON_A_TIRER_TOTAL),
        rangeMinusOneTo99000, rangeInvalid, errorDetailsList));
    dto.setCastYearStart(parseYearToInt("CastYearStart", csv.get(CAST_YEAR_START), errorDetailsList));
    dto.setCastYearEnd(parseYearToInt("CastYearEnd", csv.get(CAST_YEAR_END), errorDetailsList));
    if (dto.getCastYearStart() != null && dto.getCastYearEnd() != null) {
      if (dto.getCastYearStart() > dto.getCastYearEnd() && dto.getCastYearEnd() > 0) {
        errorDetailsList.add("CastYear2 must be greater than or equal to CastYear1");
      }
    } else if (dto.getCastYearStart() == null && dto.getCastYearEnd() != null) {
      errorDetailsList.add("CastYear1 is mandatory, if CastYear2 is given");
    } else if (dto.getCastYearStart() != null && dto.getCastYearEnd() == null) {
      errorDetailsList.add("CastYear2 is mandatory, if CastYear1 is given");
    }
    dto.setDepthCm(convertStringToBigDecimal("DepthCm", csv.get(DEPTH_CM), errorDetailsList));
    dto.setHeightCm(convertStringToBigDecimal("HeightCm", csv.get(HEIGHT_CM), errorDetailsList));
    dto.setHorsCommerceProofTotal(convertStringToInt("HorsCommerceProofTotal", csv.get(HORS_COMMERCE_TOTAL),
        rangeMinusOneTo99000, rangeInvalid, errorDetailsList));
    dto.setParts(convertStringToInt("Parts", csv.get(PARTS), positiveOnly, positiveInvalid,
        errorDetailsList));
    dto.setPrintersProofTotal(convertStringToInt("PrintersProofTotal", csv.get(PRINTERS_PROOF_TOTAL),
        rangeMinusOneTo99000, rangeInvalid, errorDetailsList));
    dto.setSeriesTotal(convertStringToInt("SeriesTotal", csv.get(SERIES_TOTAL),
        rangeMinusOneTo99000, rangeInvalid, errorDetailsList));
    dto.setTrialProofTotal(convertStringToInt("TrialProofTotal", csv.get(TRIAL_PROOF_TOTAL),
        rangeMinusOneTo99000, rangeInvalid, errorDetailsList));
    dto.setWeight(convertStringToBigDecimal("Weight", csv.get(WEIGHT), errorDetailsList));
    dto.setWidthCm(convertStringToBigDecimal("WidthCm", csv.get(WIDTH_CM), errorDetailsList));
    dto.setYearStart(parseYearToInt("YearStart", csv.get(YEAR_START), errorDetailsList));
    dto.setYearEnd(parseYearToInt("YearEnd", csv.get(YEAR_END), errorDetailsList));
    if (dto.getYearStart() != null && dto.getYearEnd() != null) {
      if (dto.getYearStart() > dto.getYearEnd() && dto.getYearEnd() > 0) {
        errorDetailsList.add("Year2 must be greater than or equal to Year1");
      }
    } else if (dto.getYearStart() == null && dto.getYearEnd() != null) {
      errorDetailsList.add("Year1 is mandatory, if Year2 is given");
    } else if (dto.getYearStart() != null && dto.getYearEnd() == null) {
      errorDetailsList.add("Year2 is mandatory, if Year1 is given");
    }
    dto.setHeightIn(convertStringToBigDecimal("HeightIn", csv.get(HEIGHT_IN), errorDetailsList));
    dto.setWidthIn(convertStringToBigDecimal("WidthIn", csv.get(WIDTH_IN), errorDetailsList));
    dto.setDepthIn(convertStringToBigDecimal("DepthIn", csv.get(DEPTH_IN), errorDetailsList));
    dto.setCancellationProofTotal(convertStringToInt("CancellationProofTotal", csv.get(CANCELLATION_PROOF_TOTAL),
        errorDetailsList));
    dto.setColorTrialProofTotal(convertStringToInt("ColorTrialProofTotal", csv.get(COLOR_TRIAL_PROOF_TOTAL),
        errorDetailsList));
    dto.setDedicatedProofTotal(convertStringToInt("DedicatedProofTotal", csv.get(DEDICATED_PROOF_TOTAL),
        rangeMinusOneTo99000, rangeInvalid, errorDetailsList));
    dto.setMuseumProofTotal(convertStringToInt("MuseumProofTotal", csv.get(MUSEUM_PROOF_TOTAL),
        rangeMinusOneTo99000, rangeInvalid, errorDetailsList));
    dto.setProgressiveProofTotal(convertStringToInt("ProgressiveProofTotal", csv.get(PROGRESSIVE_PROOF_TOTAL),
        errorDetailsList));
    dto.setRightToProduceProofTotal(convertStringToInt("RightToProduceProofTotal", csv.get(RIGHT_TO_PRODUCE_PROOF_TOTAL),
        errorDetailsList));
    dto.setWorkingProofTotal(convertStringToInt("WorkingProofTotal", csv.get(WORKING_PROOF_TOTAL),
        errorDetailsList));
  }

  private void mapDateFields(List<String> csv, PropertyCreateRequestDTO dto,
      List<String> errorDetailsList) {
    if (StringUtils.isNotBlank(dto.getResearcherNotesText())
        && StringUtils.isBlank(csv.get(RESEARCHER_NOTES_DATE))) {
      dto.setResearcherNotesDate(new Date());
    } else {
      dto.setResearcherNotesDate(isNotBlank(csv.get(RESEARCHER_NOTES_DATE))
          ? parseDateQuietly(csv.get(RESEARCHER_NOTES_DATE).trim(), errorDetailsList)
          : null);
    }
  }

  /**
   * This method used to format the string to Date Object
   *
   * @param dateString of the String value
   * @param errorDetailsList list of messages
   * @return date object
   * @throws PropertyImportException if the dateString fail to fomat into Date
   */
  private Date parseDateQuietly(String dateString, List<String> errorDetailsList) {
    SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
    Date date = null;
    try {
      date = format.parse(dateString);
    } catch (ParseException e) {
      log.warn("Could not parse date '{}'", dateString);
      errorDetailsList.add(String.format("%s Could not parse date", dateString));
    }
    return date;
  }

  /**
   * This method used to verify given propertyValue equals 'Y' return otherwise return boolean
   * 'false'
   *
   * @param propertyName of the property name.
   * @param propertyValue of the property object.
   * @param errorDetailsList list of messages
   * @return boolean <code>true</code> if the given <code>propertyValue</code> equals 'Y', otherwise
   * <code>false</code>.
   * @throws PropertyImportException if the propertyValue is other than "Y or empty"
   */
  private boolean convertStringToBoolean(String propertyName, String propertyValue,
      List<String> errorDetailsList) {
    boolean flag = false;
    if ("Y".equalsIgnoreCase(propertyValue)) {
      flag = true;
    } else if (StringUtils.isNotEmpty(propertyValue)) {
      errorDetailsList
          .add(String.format("Couldn't convert property='%s' to boolean for the propertyName='%s'",
              propertyValue, propertyName));
    }
    return flag;
  }

  /**
   * This method used to convert given propertyValue into integer
   *
   * @param propertyName of the property name.
   * @param propertyValue of the property object.
   * @param errorDetailsList list of messages.
   * @return int value or null if {@code propertyValue} is blank.
   * @throws PropertyImportException if the propertyValue fail to convert into int
   */
  private Integer convertStringToInt(String propertyName, String propertyValue, List<String> errorDetailsList) {
    return convertStringToInt(propertyName, propertyValue, x -> true, null, errorDetailsList);
  }

  /**
   * This method used to convert given propertyValue into integer
   *
   * @param propertyName of the property name.
   * @param propertyValue of the property object.
   * @param valid predicate function to test parsed value.
   * @param invalidMessage to use if not valid value (negative validation).
   * @param errorDetailsList list of messages.
   * @return int value or null if {@code propertyValue} is blank.
   * @throws PropertyImportException if the propertyValue fail to convert into int or invalid
   */
  private Integer convertStringToInt(String propertyName, String propertyValue, IntPredicate valid,
      String invalidMessage, List<String> errorDetailsList) {
    Integer convertedValue = null;
    if (isNotBlank(propertyValue)) {
      try {
        convertedValue = Integer.parseInt(propertyValue.trim());
        if (valid.negate().test(convertedValue)) {
          errorDetailsList.add(String.format(invalidMessage, propertyName));
        }
      } catch (NumberFormatException nfe) {
        errorDetailsList.add(String
            .format("Couldn't convert property='%s' to int for the propertyName='%s'",
                propertyValue.trim(), propertyName));
      }
    }
    return convertedValue;
  }

  /**
   * This method used to convert given propertyValue into BigDecimal value
   *
   * @param propertyName of the property name.
   * @param propertyValue of the property object.
   * @param errorDetailsList list of messages.
   * @return big decimal value or null if {@code propertyValue} is blank.
   * @throws PropertyImportException if the propertyValue fail to convert into BigDecimal
   */
  private BigDecimal convertStringToBigDecimal(String propertyName, String propertyValue,
      List<String> errorDetailsList) {
    BigDecimal convertedValue = null;
    if (isNotBlank(propertyValue)) {
      try {
        int propertyValueLength = propertyValue.trim().split("\\.")[0].length();
        if (propertyValueLength > 5) {
          errorDetailsList.add(String
              .format("Couldn't allow maximum length property='%s' for the propertyName='%s'",
                  propertyValue.trim(), propertyName));
        }
        convertedValue = new BigDecimal(propertyValue.trim());
        if (convertedValue.signum() < 0) {
          errorDetailsList.add(String
              .format("Couldn't allow negative value for the propertyName='%s'", propertyName));
        }
      } catch (NumberFormatException nfe) {
        errorDetailsList.add(String
            .format("Couldn't convert property='%s' to bigdecimal for the propertyName='%s'",
                propertyValue.trim(), propertyName));
      }
    }
    return convertedValue;
  }

  /**
   * Convert a {@value NESTED_VALUE_SEPARATOR}-delimited string to a list of strings. Only non-blank
   * values are processed and they are trimmed.
   *
   * @param multiValueString {@value NESTED_VALUE_SEPARATOR}-delimited string
   * @return a list of strings
   */
  private List<String> convertMultiValueStringToList(String multiValueString) {
    return convertMultiValueStringToList(multiValueString, String::toString);
  }

  /**
   * Convert a {@value NESTED_VALUE_SEPARATOR}-delimited string to a list of strings.  Only
   * non-blank values are processed and they are trimmed.
   *
   * @param multiValueString {@value NESTED_VALUE_SEPARATOR}-delimited string
   * @return a list of strings
   */
  private <R> List<R> convertMultiValueStringToList(String multiValueString,
      Function<? super String, R> mapper) {
    Objects.requireNonNull(multiValueString);
    Objects.requireNonNull(mapper);
    return Arrays.stream(multiValueString.split(NESTED_VALUE_SEPARATOR))
        .filter(StringUtils::isNotBlank).map(String::trim).map(mapper).collect(Collectors.toList());
  }

  /**
   * This method used to check if given dateValue is a valid year
   *
   * @param propertyName of the property name.
   * @param dateString of the property object.
   * @param errorDetailsList list of messages.
   * @return int value or null if {@code dateString} is blank.
   * @throws PropertyImportException if the propertyValue is not a valid date year
   */
  private Integer parseYearToInt(String propertyName, String dateString,
      List<String> errorDetailsList) {
    Integer convertedValue = null;
    if (isNotBlank(dateString)) {
      try {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy");
        convertedValue = Year.parse(dateString.trim(), format).getValue();
      } catch (DateTimeParseException ex) {
        errorDetailsList.add(String.format("%s Could not parse year value", propertyName));
        convertedValue = 0;
      }
    }
    return convertedValue;
  }

}
