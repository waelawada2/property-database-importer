package com.sothebys.propertydb.importer.support;

/**
 * Contains constants that represent column positions of the source CSV file.
 *
 * @author SrinivasaRao Batthula
 */
public class WebsiteImagesCsvFields {

  public static final int ITEM_ID = 0; // A

  public static final int SALE_SALE_ID = 1; // B

  public static final int LOTS_LOT_ID_TXT = 2; // C

  private WebsiteImagesCsvFields() {
    throw new AssertionError();
  }

}
