package com.sothebys.propertydb.importer.support;

/**
 * Contains constants that represent column positions of the source CSV file.
 *
 * @author Gregor Zurowski
 */
public class CsvFields {
  
  public static final int OBJECT_CODE = 0; // A

  public static final int EOS_OBJECT_ID = 1; // B

  public static final int SORT_CODE = 2; // C

  public static final int ARTIST_ID = 3; // D
  
  public static final int ARTIST_CODE = 4; // E

  public static final int ARTIST_NAME = 5; // F

  public static final int CATALOGUE_RAISONEE_VOLUME = 6; // G

  public static final int CATALOGUE_RAISONEE_NUMBER = 7; // H

  public static final int ORIGINAL_TITLE = 8; // I

  public static final int TITLE = 9; // J

  public static final int SIGNATURE = 10; // K

  public static final int MEDIUM = 11; // L

  public static final int CATEGORY = 12; // M

  public static final int SUB_CATEGORY = 13; // N

  public static final int YEAR_CIRCA = 14; // O

  public static final int YEAR_START = 15; // P

  public static final int YEAR_END = 16; // Q

  public static final int CAST_YEAR_CIRCA = 17; // R

  public static final int CAST_YEAR_START = 18; // S

  public static final int CAST_YEAR_END = 19; // T

  public static final int POSTHUMOUS = 20; // U

  public static final int YEAR_OVERRIDE = 21; // V

  public static final int PRESIZE_DESCRIPTION = 22; // W

  public static final int HEIGHT_IN = 23; // X

  public static final int WIDTH_IN = 24; // Y

  public static final int DEPTH_IN = 25; // Z

  public static final int HEIGHT_CM = 26; // AA

  public static final int WIDTH_CM = 27; // AB

  public static final int DEPTH_CM = 28; // AC
  
  public static final int MEASUREMENT_OVERRIDE = 29; // AD

  public static final int WEIGHT = 30; // AE

  public static final int PARTS = 31; // AF

  public static final int EDITION_STATUS = 32; //  AG

  public static final int EDITION_NUMBER = 33; // AH
  
  public static final int EDITION_TYPE = 34; // AI

  public static final int SERIES_TOTAL = 35; // AJ

  public static final int ARTIST_PROOF_TOTAL = 36; // AK

  public static final int BON_A_TIRER_TOTAL = 37; // AL

  public static final int CANCELLATION_PROOF_TOTAL = 38; // AM

  public static final int COLOR_TRIAL_PROOF_TOTAL = 39; // AN

  public static final int DEDICATED_PROOF_TOTAL = 40; // AO

  public static final int PRINTERS_PROOF_TOTAL = 41; // AP

  public static final int HORS_COMMERCE_TOTAL = 42; // AQ

  public static final int MUSEUM_PROOF_TOTAL = 43; // AR

  public static final int PROGRESSIVE_PROOF_TOTAL = 44; // AS

  public static final int RIGHT_TO_PRODUCE_PROOF_TOTAL = 45; // AT

  public static final int TRIAL_PROOF_TOTAL = 46; // AU

  public static final int WORKING_PROOF_TOTAL = 47; // AV
  
  public static final int FOUNDRY = 48; // AW

  public static final int EDITION_OVERRIDE = 49; // AX

  public static final int EDITION_NOTES = 50; // AY

  public static final int STAMPED = 51; // AZ
  
  public static final int SIGNED = 52; // BA
  
  public static final int CERTIFICATE = 53; // BB
  
  public static final int AUTHENTICATION = 54; // BC
  
  public static final int ARCHIVE_NUMBERS = 55; // BD
    
  public static final int FLAGS_AUTHENTICITY = 56; // BE
  
  public static final int FLAGS_RESTITUTION = 57; // BF
  
  public static final int FLAGS_CONDITION = 58; // BG
  
  public static final int FLAGS_SFS = 59; // BH
  
  public static final int FLAGS_MEDIUM = 60; // BI
  
  public static final int FLAGS_EXPORT_COUNTRY = 61; // BJ

  public static final int TAG_SUBJECT = 62; // BK

  public static final int TAG_FIGURE = 63; // BL

  public static final int TAG_GENDER = 64; // BM

  public static final int TAG_POSITION = 65; // BN

  public static final int TAG_ANIMAL = 66; // BO

  public static final int TAG_MAIN_COLOUR = 67; // BP

  public static final int TAG_TEXT = 68; // BQ

  public static final int TAG_LOCATION = 69; // BR

  public static final int TAG_SITTER = 70; // BS

  public static final int TAG_OTHER = 71; // BT

  public static final int AUTO_ORIENTATION = 72; // BU

  public static final int AUTO_IMAGE = 73; // BV

  public static final int AUTO_SCALE = 74; // BW

  public static final int AUTO_AREA_CM = 75; // BX
  
  public static final int RESEARCH_COMPLETE_CHECK = 76; // BY
  
  public static final int RESEARCHER_NAME = 77; // BZ

  public static final int RESEARCHER_NOTES_DATE = 78; // CA

  public static final int RESEARCHER_NOTES_TEXT = 79; // CB

  public static final int IMAGE_FILE_NAME = 80; // CC

  public static final int IMAGE_UPDATE_Y_N = 81; // CD

  private CsvFields() {
    throw new AssertionError();
  }

}
