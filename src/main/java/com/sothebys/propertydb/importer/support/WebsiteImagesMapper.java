package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.WebsiteImagesCsvFields.ITEM_ID;
import static com.sothebys.propertydb.importer.support.WebsiteImagesCsvFields.LOTS_LOT_ID_TXT;
import static com.sothebys.propertydb.importer.support.WebsiteImagesCsvFields.SALE_SALE_ID;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sothebys.propertydb.importer.exception.WebsiteImageImportException;
import com.sothebys.propertydb.importer.model.WebsiteImagesCreateRequestDTO;

/**
 * Mapper class for mapping between the CSV representation and the DTO representation of a Website
 * Images.
 *
 * @author SrinivasaRao Batthula
 */
public class WebsiteImagesMapper {

  protected static Logger log = LoggerFactory.getLogger(WebsiteImagesMapper.class);

  public WebsiteImagesCreateRequestDTO map(List<String> csv) {
    WebsiteImagesCreateRequestDTO dto = new WebsiteImagesCreateRequestDTO();
    mapStringFields(csv, dto);
    return dto;
  }

  private void mapStringFields(List<String> csv, WebsiteImagesCreateRequestDTO dto) {
    dto.setItemId(csv.get(ITEM_ID));
    if (StringUtils.isEmpty(csv.get(SALE_SALE_ID))
        && StringUtils.isNotEmpty(csv.get(LOTS_LOT_ID_TXT))) {
      throw new WebsiteImageImportException(
          String.format("Empty Sale ID Value for Lots Number value %s", csv.get(LOTS_LOT_ID_TXT)));
    } else if (StringUtils.isNotEmpty(csv.get(SALE_SALE_ID))
        && StringUtils.isEmpty(csv.get(LOTS_LOT_ID_TXT))) {
      throw new WebsiteImageImportException(
          String.format("Empty Lots Number for Sale ID value %s", csv.get(SALE_SALE_ID)));
    } else {
      dto.setSaleId(csv.get(SALE_SALE_ID));
      dto.setLotsNumber(csv.get(LOTS_LOT_ID_TXT));
    }
  }
}
