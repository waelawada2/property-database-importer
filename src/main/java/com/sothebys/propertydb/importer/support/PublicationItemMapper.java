package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.CAT_PAGE;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.CODE;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.COLOUR;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.CONDITION_TEXT;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.CONSIGNMENT_CODE;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.CONSIGNMENT_ID;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.DESCRIPTION;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.EXHIBITION;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.FIGURE;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.ID;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.ITEM_NOTES;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.Ill_PAGE;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.LITERATURE;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.NUMBER;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.OBJECT_ID;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.OD_NOTES;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.PLATE;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.PROVENANCE;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.PUBLICATION_EVENT_CODE;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.PUBLICATION_EVENT_ID;
import static com.sothebys.propertydb.importer.support.PublicationItemCsvFields.TYPE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.sothebys.propertydb.importer.model.event.ConsignmentPurchaseEventDto;
import com.sothebys.propertydb.importer.model.event.EventType;
import com.sothebys.propertydb.importer.model.event.PublicationEventDto;
import com.sothebys.propertydb.importer.model.item.ColourTypeDto;
import com.sothebys.propertydb.importer.model.item.ItemObjectDto;
import com.sothebys.propertydb.importer.model.item.PublicationItemDto;
import com.sothebys.propertydb.importer.model.item.PublicationItemTypeDto;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class PublicationItemMapper {

  private static final Pattern PIPE_PATTERN = Pattern.compile("\\|");

  public PublicationItemDto map(List<String> csv) {
    PublicationItemDto dto = new PublicationItemDto();

    dto.setCode(csv.get(CODE.getNumber()));
    dto.setId(csv.get(ID.getNumber()));
    PublicationEventDto eventDto = new PublicationEventDto();
    eventDto.setId(csv.get(PUBLICATION_EVENT_ID.getNumber()));
    eventDto.setCode(csv.get(PUBLICATION_EVENT_CODE.getNumber()));
    dto.setEvent(eventDto);

    if (isNotBlank(csv.get(OBJECT_ID.getNumber()))) {
      List<String> objectIds = Arrays.asList(PIPE_PATTERN.split(csv.get(OBJECT_ID.getNumber())));
      List<ItemObjectDto> objectList = MapperUtils.itemObjectsToString(objectIds);
      if (objectList != null) {
        dto.setObjects(objectList);
      }
    }

    dto.setCataloguingPage(csv.get(CAT_PAGE.getNumber()));

    dto.setNumber(csv.get(NUMBER.getNumber()));
    dto.setPlate(csv.get(PLATE.getNumber()));
    dto.setFigure(csv.get(FIGURE.getNumber()));
    dto.setIllustration(csv.get(Ill_PAGE.getNumber()));

    if (StringUtils.isNotEmpty(csv.get(COLOUR.getNumber()))) {
      ColourTypeDto colourTypeDto = new ColourTypeDto();
      colourTypeDto.setId(csv.get(COLOUR.getNumber()));
      dto.setColourType(colourTypeDto);
    }

    if (StringUtils.isNotEmpty(csv.get(TYPE.getNumber()))) {
      PublicationItemTypeDto publicationItemTypeDto = new PublicationItemTypeDto();
      publicationItemTypeDto.setId(csv.get(TYPE.getNumber()));
      dto.setPublicationItemType(publicationItemTypeDto);
    }

    if (StringUtils.isNotEmpty(csv.get(CONSIGNMENT_ID.getNumber()))) {
      ConsignmentPurchaseEventDto consignmentPurchaseEventDto = new ConsignmentPurchaseEventDto(
          EventType.CONSIGNMENT);
      consignmentPurchaseEventDto.setId(csv.get(CONSIGNMENT_ID.getNumber()));
      consignmentPurchaseEventDto.setCode(csv.get(CONSIGNMENT_CODE.getNumber()));
      dto.setConsignmentEvent(consignmentPurchaseEventDto);
    }

    dto.setComments(csv.get(ITEM_NOTES.getNumber()));
    dto.setCataloguingNotes(csv.get(OD_NOTES.getNumber()));
    dto.setDescription(csv.get(DESCRIPTION.getNumber()));
    dto.setProvenance(csv.get(PROVENANCE.getNumber()));
    dto.setExhibition(csv.get(EXHIBITION.getNumber()));
    dto.setLiterature(csv.get(LITERATURE.getNumber()));
    dto.setCondition(csv.get(CONDITION_TEXT.getNumber()));

    //TODO: Publication_Image_Path is missing as it is not currently supported by
    // Event Service

    return dto;
  }
}
