package com.sothebys.propertydb.importer.support;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum PricingItemCsvFields {
  PRICING_ITEM_CODE(0, "Pricing_Item_Code"), // A
  PRICING_ITEM_ID(1, "Pricing_Item_ID"), // B
  PRICING_EVENT_CODE(2, "Pricing_Event_Code"), // C
  PRICING_EVENT_ID(3, "Pricing_Event_ID"), // D
  OBJECT_CODE(4, "Object_Code"), // E
  OBJECT_ID(5, "Object_ID"), // F
  PRICING_AUCTION_CURRENCY(6, "Pricing_Auction_Currency"), // G
  PRICING_AUCTION_LOW(7, "Pricing_Auction_Low"), // H
  PRICING_AUCTION_HIGH(8, "Pricing_Auction_High"), // I
  PRICING_AUCTION_PLUS_1_CHECK(9, "Pricing_Auction+1_check"), // J
  PRICING_PS_CURRENCY(10, "Pricing_PS_Currency"), // K
  PRICING_PS_VALUE(11, "Pricing_PS_Value"), // L
  PRICING_INSURANCE_CURRENCY(12, "Pricing_Insurance_Currency"), // M
  PRICING_INSURANCE_VALUE(13, "Pricing_Insurance_Value"), // N
  PRICING_FMV_CURRENCY(14, "Pricing_FMV_Currency"), // O
  PRICING_FMV_VALUE(15, "Pricing_FMV_Value"), // P
  PIPELINE_AUCTION(16, "Pipeline_Auction"), // Q
  AUCTION_TARGET_CHECK(17, "Auction_Target_check"), // R
  PIPELINE_PS(18, "Pipeline_PS"), // S
  PS_TARGET_CHECK(19, "PS_Target_check"), // T
  PIPELINE_EXHIBITION(20, "Pipeline_Exhibition"), // U
  EXHIBITION_TARGET_CHECK(21, "Exhibition_Target_check"), // V
  OLD_ITEM_ID(22, "Old_Item_ID"), // W
  ITEM_NOTES(23, "Item_Notes"), // X
  ITEM_DESCRIPTION(24, "Item_Description"), // Y
  ITEM_PROVENANCE(25, "Item_Provenance"), // Z
  ITEM_EXHIBITION(26, "Item_Exhibition"), // AA
  ITEM_LITERATURE(27, "Item_Literature"), // AB
  ITEM_CONDITION(28, "Item_Condition"), // AC
  ITEM_ODNOTES(29, "Item_ODNotes"), // AD
  ERROR_DETAILS (30, "Error_Details");
  
  private int number;
  private String text;

}
