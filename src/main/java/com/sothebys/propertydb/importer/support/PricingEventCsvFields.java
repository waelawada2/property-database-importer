package com.sothebys.propertydb.importer.support;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum PricingEventCsvFields {
  PRICING_CODE(0,"Pricing_Code"), // A
  PRICING_ID(1, "Pricing_ID"), // B
  ORIGIN(2, "Origin"), // C
  DATE(3, "Date"), // D
  VAL_SYSTEM_ID(4, "Val_System_ID"), // E
  PROPOSAL_CODE(5, "Proposal_Code"), // F
  TYPE(6, "Type"), // G
  PRICING_NOTES(7, "Pricing_Notes"), // H
  CLIENT_SOURCE_TYPE(8, "Client_Source_Type"), // I
  CLIENT_SOURCE_NAME(9, "Client_Source_Name"), // J
  CLIENT_SOURCE_ID(10, "Client_Source_ID"), // K
  CLIENT_STATUS(11, "Client_Status"), // L
  CLIENT_NAME(12, "Client_Name"), // M
  CLIENT_ID(13, "Client_ID"), // N
  DESIGNATION(14, "Designation"), // O
  AFFILIATION_SOURCE_TYPE(15, "Affiliation_Source_Type"), // P
  AFFILIATION_SOURCE_NAME(16, "Affiliation_Source_Name"), // Q
  AFFILIATION_SOURCE_ID(17, "Affiliation_Source_ID"), // R
  AFFILIATION_CLIENT_NAME(18, "Affiliation_Client_Name"), // S
  AFFILIATION_CLIENT_ID(19, "Affiliation_Client_ID"), // T
  AFFILIATION_TYPE(20, "Affiliation_Type"), // U
  ERROR_DETAILS (21, "Error_Details");
  
  private int number;
  private String text;

}
