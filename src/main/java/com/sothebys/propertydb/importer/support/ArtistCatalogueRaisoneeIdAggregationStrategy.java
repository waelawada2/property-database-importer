package com.sothebys.propertydb.importer.support;

import com.sothebys.propertydb.importer.exception.PropertyImportException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class ArtistCatalogueRaisoneeIdAggregationStrategy implements AggregationStrategy {

  @Override
  @SuppressWarnings("unchecked")
  public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
    if (oldExchange == null) {
      oldExchange = newExchange;
    }

    List<Integer> raisoneeIds = newExchange
        .getProperty("raisoneeIds", new ArrayList<>(), List.class);
    List<Integer> raisoneeSortIds = newExchange
        .getProperty("raisoneeSortIds", new ArrayList<>(), List.class);

    Map<Integer, Integer> raisoneeIdMap = new TreeMap<>();
    for (int i = 0; i < raisoneeIds.size(); i++) {
      if (raisoneeSortIds.get(i) == null) {
        throw new PropertyImportException(
            "Catalogue raisonnes' sort id must not be null. Please contact support.");
      }

      raisoneeIdMap.put(raisoneeSortIds.get(i), raisoneeIds.get(i));
    }

    Collection<Integer> sortedRaisoneeIds = raisoneeIdMap.values();

    List<Integer> artistCatalogueRaisoneeIds = oldExchange
        .getProperty("artistCatalogueRaisoneeIds", new ArrayList<>(), List.class);
    artistCatalogueRaisoneeIds.addAll(sortedRaisoneeIds);
    oldExchange.setProperty("artistCatalogueRaisoneeIds", artistCatalogueRaisoneeIds);

    return oldExchange;
  }
}
