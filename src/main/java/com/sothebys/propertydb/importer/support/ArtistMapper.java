package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.ArtistCsvFields.*;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.countMatches;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.repeat;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sothebys.propertydb.importer.exception.ArtistImportException;
import com.sothebys.propertydb.importer.model.ArtistCreateRequestDTO;
import com.sothebys.propertydb.importer.model.ArtistCreateRequestDTO.ArtistCategory;

/**
 * Mapper class for mapping between the CSV representation and the
 * DTO representation of a property.
 *
 * @author Venkata Prasad Tammineni
 */
public class ArtistMapper {

  private static final String NESTED_VALUE_SEPARATOR = "\\|";
  private static final String UNKNOWN = "Unknown";
  private static final String PAINTING = "Painting";
  private static final String WORK_ON_PAPER = "Work on Paper";
  private static final String MOUNTED = "Mounted";
  private static final String SCULPTURE = "Sculpture";
  private static final String PHOTOGRAPH = "Photograph";
  private static final String PRINT = "Print";
  private static final String INSTALLATION = "Installation";
  private static final String VIDEO = "Video";
  private static final String OTHER = "Other";
  private static final String MALE = "Male";
  private static final String FEMALE = "Female";

  @SuppressWarnings("unused")
  private static Logger log = LoggerFactory.getLogger(ArtistMapper.class);

  public ArtistCreateRequestDTO map(List<String> csv, List<String> errorDetailsList) {
    ArtistCreateRequestDTO dto = new ArtistCreateRequestDTO();
    mapNumericFields(csv, dto, errorDetailsList);
    mapStringFields(csv, dto, errorDetailsList);
    mapComplexFields(csv, dto, errorDetailsList);
    generateDisplayName(csv, dto);
    dto.setErrorDetailsList(Collections.unmodifiableList(errorDetailsList));
    return dto;
  }

  private void mapComplexFields(List<String> csv, ArtistCreateRequestDTO dto,
      List<String> errorDetailsList) {
    if (isNotBlank(csv.get(CATALOGUE_RAISONEE_AUTHOR))
        || isNotBlank(csv.get(CATALOGUE_RAISONEE_YEAR))
        || isNotBlank(csv.get(CATALOGUE_RAISONEE_VOLUMES))
        || isNotBlank(csv.get(CATALOGUE_RAISONEE_TYPE))) {
      translateCataloguesRaisonees(csv.get(CATALOGUE_RAISONEE_AUTHOR),
          csv.get(CATALOGUE_RAISONEE_YEAR), csv.get(CATALOGUE_RAISONEE_VOLUMES),
          csv.get(CATALOGUE_RAISONEE_TYPE), dto, errorDetailsList);
    }
    translateCategory(csv.get(UNKNOWN_CHECK), csv.get(PAINTING_CHECK), csv.get(WORK_ON_PAPER_CHECK),
        csv.get(MOUNTED_CHECK), csv.get(SCULPTURE_CHECK), csv.get(PHOTOGRAPH_CHECK),
        csv.get(PRINT_CHECK), csv.get(INSTALLATION_CHECK), csv.get(VIDEO_CHECK),
        csv.get(OTHER_CHECK), dto, errorDetailsList);
    if (isNotBlank(csv.get(NATIONALITY))) {
      translateCountry(csv.get(NATIONALITY), dto);
    }
  }

  private void translateCataloguesRaisonees(String catalogueRaisoneeAuthor,
      String catalogueRaisoneeYear, String catalogueRaisoneeVolume, String catalogueRaisoneeType,
      ArtistCreateRequestDTO dto, List<String> errorDetailsList) {
    if (isNotBlank(catalogueRaisoneeAuthor) || isNotBlank(catalogueRaisoneeYear)
        || isNotBlank(catalogueRaisoneeVolume) || isNotBlank(catalogueRaisoneeType)) {
      int crAuthors = countMatches(catalogueRaisoneeAuthor, "|") + 1;
      int crYears = countMatches(catalogueRaisoneeYear, "|") + 1;
      int crVolumes = countMatches(catalogueRaisoneeVolume, "|") + 1;
      int crTypes = countMatches(catalogueRaisoneeType, "|") + 1;
      int maxQuantity = ObjectUtils.max(crAuthors, crYears, crVolumes, crTypes);
      Queue<String> crFinalFields = Stream
          .of(Pair.of(crAuthors, defaultString(catalogueRaisoneeAuthor)),
              Pair.of(crYears, defaultString(catalogueRaisoneeYear)),
              Pair.of(crVolumes, defaultString(catalogueRaisoneeVolume)),
              Pair.of(crTypes, defaultString(catalogueRaisoneeType)))
          .map(field -> field.getValue() + repeat("|", maxQuantity - field.getKey()))
          .collect(toCollection(ArrayDeque::new));
      String[] finalCatalogueRaisoneeAuthors = crFinalFields.poll()
          .split(NESTED_VALUE_SEPARATOR, -1);
      Integer[] finalCatalogueRaisoneeYears = Arrays
          .stream(crFinalFields.poll().split(NESTED_VALUE_SEPARATOR, -1))
          .map(year -> convertToInteger("CRYear", year, errorDetailsList)).toArray(Integer[]::new);
      String[] finalCatalogueRaisoneeVolumes = crFinalFields.poll()
          .split(NESTED_VALUE_SEPARATOR, -1);
      if (finalCatalogueRaisoneeVolumes != null && finalCatalogueRaisoneeVolumes.length > 0) {
        for (int i = 0; i < finalCatalogueRaisoneeVolumes.length; i++) {
          if (finalCatalogueRaisoneeVolumes[i].trim().length() > 10) {
            errorDetailsList.add(String.format(
                "Artist CR_Volume should not have more than 10 characters - '%s'",
                finalCatalogueRaisoneeVolumes[i].trim()));
          }
        }
      }
      String[] finalCatalogueRaisoneeTypes = crFinalFields.poll().split(NESTED_VALUE_SEPARATOR, -1);
      // @formatter:off
      dto.setArtistCatalogueRaisonees(IntStream.range(0,
          Math.min(finalCatalogueRaisoneeVolumes.length, finalCatalogueRaisoneeTypes.length))
          .mapToObj(i -> new ArtistCreateRequestDTO.ArtistCatalogueRaisonee(
              finalCatalogueRaisoneeAuthors[i].trim(), finalCatalogueRaisoneeTypes[i].trim(),
              finalCatalogueRaisoneeVolumes[i].trim(), finalCatalogueRaisoneeYears[i]))
          .collect(toList()));
      // @formatter:on
    }
  }

  private void translateCountry(String country, ArtistCreateRequestDTO dto) {
    requireNonNull(country);
    // @formatter:off
    dto.setCountries(
        Arrays.stream(country.split(NESTED_VALUE_SEPARATOR)).filter(StringUtils::isNotBlank)
            .map(ArtistCreateRequestDTO.CountryDTO::new).collect(toList()));
    // @formatter:on
  }

  private void translateCategory(String unknown, String painting, String workOnPaper,
      String mounted, String sculpture, String photograph, String print, String installation,
      String video, String other, ArtistCreateRequestDTO dto, List<String> errorDetailsList) {
    Supplier<Stream<Pair<String, String>>> streamSupplier = () -> Stream
        .of(Pair.of(unknown, UNKNOWN),
            Pair.of(painting, PAINTING), Pair.of(workOnPaper, WORK_ON_PAPER),
            Pair.of(mounted, MOUNTED),
            Pair.of(sculpture, SCULPTURE), Pair.of(photograph, PHOTOGRAPH), Pair.of(print, PRINT),
            Pair.of(installation, INSTALLATION), Pair.of(video, VIDEO), Pair.of(other, OTHER));
    if (streamSupplier.get().anyMatch(category -> !(
        "Y".equalsIgnoreCase((category.getKey() != null ? category.getKey() : "").trim()) || ""
            .equals((category.getKey() != null ? category.getKey() : "").trim())))) {
      errorDetailsList.add(String.format("Improper data provided in CSV - %s", streamSupplier.get()
          .filter(category -> !(
              "Y".equalsIgnoreCase((category.getKey() != null ? category.getKey() : "").trim())
                  || "".equals((category.getKey() != null ? category.getKey() : "").trim())))
          .collect(toList()).toString()));
    }
    List<ArtistCategory> categories = streamSupplier.get()
        .filter(category -> "Y"
            .equalsIgnoreCase((category.getKey() != null ? category.getKey() : "").trim()))
        .map(Pair::getValue).map(ArtistCategory::new).collect(toList());
    dto.setCategories(categories);
  }

  @SuppressWarnings("FeatureEnvy")
  private static void mapStringFields(List<String> csv, ArtistCreateRequestDTO dto,
      List<String> errorDetailsList) {
    dto.setExternalId(csv.get(ARTIST_ID));
    if(isBlank(csv.get(LAST_NAME))) {
      errorDetailsList.add("Last Name cannot be blank");
    }
    dto.setFirstName(csv.get(FIRST_NAME));
    dto.setLastName(csv.get(LAST_NAME));
    dto.setDisplay(csv.get(DISPLAY));
    String gender = csv.get(GENDER) != null ? csv.get(GENDER) : "";
    if ("".equals(gender) || MALE.equalsIgnoreCase(gender)
			|| FEMALE.equalsIgnoreCase(gender)) {
    	dto.setGender(csv.get(GENDER));
	} else {
      errorDetailsList.add(String.format("Improper Gender data provided in CSV - %s", csv.get(GENDER)));
	}
    dto.setNotes(csv.get(NOTES));
  }
  
  private void generateDisplayName(List<String> csv, ArtistCreateRequestDTO dto) {
    if (!isNotBlank(csv.get(DISPLAY))) {
      String name = "";
      if (isNotBlank(csv.get(FIRST_NAME)) && isNotBlank(csv.get(LAST_NAME))) {
        name = csv.get(FIRST_NAME).trim() + " " + csv.get(LAST_NAME).trim();
      } else if (isNotBlank(csv.get(LAST_NAME))) {
        name = csv.get(LAST_NAME).trim();
      }
      Integer birthYear = null;
      Integer deathYear = null;
      String year = null;
      try {
        birthYear =
            isNotBlank(csv.get(BIRTH_YEAR)) ? Integer.parseInt(csv.get(BIRTH_YEAR).trim()) : null;
      } catch (NumberFormatException nfe) {
        birthYear = null;
      }
      try {
        deathYear =
            isNotBlank(csv.get(DEATH_YEAR)) ? Integer.parseInt(csv.get(DEATH_YEAR).trim()) : null;
      } catch (NumberFormatException nfe) {
        deathYear = null;
      }
      if (birthYear != null) {
        year = String.valueOf(birthYear.intValue());
        if (deathYear != null) {
          year = year + "-" + String.valueOf(deathYear.intValue());
        } else {
          year = "b. " + year;
        }
      }
      if (!"".equals(name)) {
        if (year != null) {
          dto.setDisplay(name + " (" + year + ")");
        } else {
          dto.setDisplay(name);
        }
      }
    }
  }

  @SuppressWarnings("FeatureEnvy")
  private static void mapNumericFields(List<String> csv, ArtistCreateRequestDTO dto,
      List<String> errorDetailsList) {
    dto.setBirthYear(convertToInteger("BirthYear", csv.get(BIRTH_YEAR), errorDetailsList));
    dto.setDeathYear(convertToInteger("DeathYear", csv.get(DEATH_YEAR), errorDetailsList));
    if (csv.get(ULAN_ID) != null && csv.get(ULAN_ID).length() >= 20) {
      errorDetailsList.add(
          String.format("Maximum value of Artist Code is  99,99,99,99,99,99,99,99,999"));
    }
    dto.setUlanId(convertToLong("Artist Code", csv.get(ULAN_ID), errorDetailsList));
  }

  /**
   * This method used to convert given String value into Integer
   *
   * @param field name of the value to parse.
   * @param value of the field to parse.
   * @param errorDetailsList
   * @return value in the String.
   * @throws ArtistImportException if the value fail to convert into int
   */
  @SuppressWarnings("ThrowInsideCatchBlockWhichIgnoresCaughtException")
  private static Integer convertToInteger(String field, String value,
      List<String> errorDetailsList) throws ArtistImportException {
    try {
      return isNotBlank(value) ? Integer.parseInt(value.trim()) : null;
    } catch (NumberFormatException nfe) {
      errorDetailsList.add(
          String.format("couldn't convert value='%s' to int for the field='%s'", value, field));
    }
    return null;
  }
  
  /**
   * This method used to convert given String value into Long
   *
   * @param field name of the value to parse.
   * @param value of the field to parse.
   * @param errorDetailsList
   * @return value in the long.
   * @throws ArtistImportException if the value fail to convert into long
   */
  @SuppressWarnings("ThrowInsideCatchBlockWhichIgnoresCaughtException")
  protected static Long convertToLong(String field, String value,
      List<String> errorDetailsList) throws ArtistImportException {
    try {
      return isNotBlank(value) ? Long.parseLong(value.trim()) : null;
    } catch (NumberFormatException nfe) {
      errorDetailsList.add(
          String.format("Artist Code should always be a Numerical value - '%s'", value));
    }
    return null;
  }

}
