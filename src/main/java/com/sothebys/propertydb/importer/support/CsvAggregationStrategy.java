package com.sothebys.propertydb.importer.support;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import lombok.extern.slf4j.Slf4j;

import org.apache.camel.Exchange;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.processor.aggregate.CompletionAwareAggregationStrategy;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;

@Slf4j
public class CsvAggregationStrategy implements CompletionAwareAggregationStrategy {

  @Override
  public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {

    Map<String, String> csvFieldsByEntityAttribute =
        newExchange.getProperty("csvFieldsByEntityAttribute", Map.class);

    if (oldExchange == null) {
      oldExchange = newExchange;
      oldExchange.setProperty("badCsvData", new TreeMap<>()); // TreeMap ensures a sorted order
      oldExchange.setProperty("successfulRecords", new TreeMap<>());
    }

    if (newExchange.getProperty(Exchange.SPLIT_COMPLETE, boolean.class)) {
      oldExchange.setProperty("numberOfCSVRows", newExchange.getProperty(Exchange.SPLIT_SIZE));
    }

    Exception exception = newExchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
    if (exception != null) {
      @SuppressWarnings("unchecked")
      Map<Integer, Object> badCsvData = oldExchange.getProperty("badCsvData", Map.class);

      @SuppressWarnings("unchecked")
      List<String> csvRowData = newExchange.getProperty("csvRowData", List.class);

      if (exception instanceof HttpOperationFailedException) {
        String responseBody = ((HttpOperationFailedException) exception).getResponseBody();

        String errorMessage = getErrorMessage(responseBody, csvFieldsByEntityAttribute,
            ((HttpOperationFailedException) exception).getStatusCode());

        csvRowData.add(errorMessage);
      } else {
        csvRowData.add(exception.getMessage());
      }

      badCsvData.put(newExchange.getProperty(Exchange.SPLIT_INDEX, Integer.class), csvRowData);
      oldExchange.setProperty("badCsvData", badCsvData);
    }else {
      @SuppressWarnings("unchecked")
      Map<Integer, Object> sucessRecords = oldExchange.getProperty("successfulRecords", Map.class);
      
      @SuppressWarnings("unchecked")
      List<String> csvRowData = newExchange.getProperty("csvRowData", List.class);
      
      Integer idPosition = (Integer) newExchange.getProperty("idColumnPosition");
      if(idPosition != null) {
        csvRowData.set(idPosition, (String)newExchange.getProperty("responseId"));
      }else {
        csvRowData.add((String)newExchange.getProperty("responseId"));
      }
      sucessRecords.put(newExchange.getProperty(Exchange.SPLIT_INDEX, Integer.class), csvRowData);
      oldExchange.setProperty("successfulRecords", sucessRecords);
    }


    return oldExchange;
  }

  private String getErrorMessage(String responseBody,
      Map<String, String> csvFieldsByEntityAttribute, int statusCode) {
    String errorMessage = "";
    try {
      if (statusCode == HttpStatus.BAD_REQUEST.value()) {
        JSONObject jsonObject = new JSONObject(responseBody);
        JSONArray messages = (JSONArray) jsonObject.get("messages");

        for (Object parsedMessage : messages) {
          if (!errorMessage.equals("")) {
            errorMessage = errorMessage + " | ";
          }

          JSONObject lineMessage = (JSONObject) parsedMessage;
          String fieldString =
              csvFieldsByEntityAttribute.get(lineMessage.getString("field")) != null
                  ? csvFieldsByEntityAttribute.get(lineMessage.getString("field"))
                  : lineMessage.getString("field");

          errorMessage = errorMessage + fieldString + ": " + lineMessage.getString("message") + " ";
        }
      } else if (statusCode == HttpStatus.NOT_FOUND.value()) {
        JSONArray jsonArray = new JSONArray(responseBody);

        for (Object parsedMessage : jsonArray) {
          if (!errorMessage.equals("")) {
            errorMessage = errorMessage + " | ";
          }

          JSONObject lineMessage = (JSONObject) parsedMessage;
          if (lineMessage.getString("logref").equals("Not found")) {
            errorMessage = errorMessage + csvFieldsByEntityAttribute.get("entity.id") + ": "
                + lineMessage.getString("message") + " ";
          } else {
            errorMessage = errorMessage + lineMessage.getString("message") + " ";
          }
        }
      } else {
        errorMessage = responseBody;
      }
    } catch (Exception e) {
      log.error("Failed parsing the responseBody message: " + responseBody, e);;
    }
    return errorMessage;
  }

  @Override
  public void onCompletion(Exchange exchange) {
    @SuppressWarnings("unchecked")
    Map<Integer, List<String>> badCsvData = exchange.getProperty("badCsvData", Map.class);
    exchange.setProperty("badCsvData", new ArrayList<>(badCsvData.values()));
    
    @SuppressWarnings("unchecked")
    Map<Integer, List<String>> succesfulCsvData = exchange.getProperty("successfulRecords", Map.class);
    exchange.setProperty("successfulRecords", new ArrayList<>(succesfulCsvData.values()));

    /* Removing Exception/Failure properties if any occurred while processing the CSV rows. */
    exchange.removeProperties("CamelFailure*");
    exchange.removeProperties("CamelException*");
    exchange.removeProperties("CamelError*");

  }
}
