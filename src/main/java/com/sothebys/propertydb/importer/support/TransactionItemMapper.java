package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.CONDITION;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.CONSIGNMENT_ID;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.DESCRIPTION;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.EOR_CHECKED;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.EOS_OBJECT_ID;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.ESTIMATE_HIGH;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.ESTIMATE_LOW;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.EXHIBITION;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.HAMMER;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.ITEM_NOTES;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.LITERATURE;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.LOT_NUMBER;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.OD_NOTES;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.OLD_ITEM_ID;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.PREMIUM;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.PROVENANCE;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.PS_NET_BUYER;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.PS_NET_VENDOR;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.PURCHASE_ID;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.SALE_STATUS;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.TERM;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.TRANSACTION_EVENT_ID;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.TRANSACTION_ITEM_CODE;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.TRANSACTION_ITEM_ID;
import static com.sothebys.propertydb.importer.support.TransactionItemCsvFields.UNSOLD_HAMMER;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.exception.TransactionItemImportException;
import com.sothebys.propertydb.importer.model.event.ConsignmentPurchaseEventDto;
import com.sothebys.propertydb.importer.model.event.EventDto;
import com.sothebys.propertydb.importer.model.event.EventType;
import com.sothebys.propertydb.importer.model.event.TransactionEventDto;
import com.sothebys.propertydb.importer.model.event.TransactionType;
import com.sothebys.propertydb.importer.model.item.AuctionDto;
import com.sothebys.propertydb.importer.model.item.AuctionTermDto;
import com.sothebys.propertydb.importer.model.item.ItemObjectDto;
import com.sothebys.propertydb.importer.model.item.PrivateSaleDto;
import com.sothebys.propertydb.importer.model.item.TransactionItemDto;
import com.sothebys.propertydb.importer.model.item.TransactionStatusDto;

@Component
public class TransactionItemMapper {
  public TransactionItemDto map(List<String> csv) {
    TransactionItemDto transactionItemDto = new TransactionItemDto();

    transactionItemDto.setCode(csv.get(TRANSACTION_ITEM_CODE.getNumber()));
    transactionItemDto.setId(csv.get(TRANSACTION_ITEM_ID.getNumber()));

    EventDto transactionEvent = null;
    if (isNotBlank(csv.get(TRANSACTION_EVENT_ID.getNumber()))) {
      transactionEvent = new TransactionEventDto();
      transactionEvent.setId(csv.get(TRANSACTION_EVENT_ID.getNumber()));
    }
    transactionItemDto.setEvent(transactionEvent);

    ConsignmentPurchaseEventDto consignmentEvent = null;
    if (isNotBlank(csv.get(CONSIGNMENT_ID.getNumber()))) {
      consignmentEvent = new ConsignmentPurchaseEventDto(EventType.CONSIGNMENT);
      consignmentEvent.setId(csv.get(CONSIGNMENT_ID.getNumber()));
    }
    transactionItemDto.setConsignmentEvent(consignmentEvent);

    ConsignmentPurchaseEventDto purchaseEvent = null;
    if (isNotBlank(csv.get(PURCHASE_ID.getNumber()))) {
      purchaseEvent = new ConsignmentPurchaseEventDto(EventType.PURCHASE);
      purchaseEvent.setId(csv.get(PURCHASE_ID.getNumber()));
    }
    transactionItemDto.setPurchaseEvent(purchaseEvent);

    if (isNotBlank(csv.get(EOS_OBJECT_ID.getNumber()))) {
      List<String> objectIds =
          Arrays.asList(Pattern.compile("\\|").split(csv.get(EOS_OBJECT_ID.getNumber())));
      List<ItemObjectDto> objectList = MapperUtils.itemObjectsToString(objectIds);
      if (objectList != null)
        transactionItemDto.setObjects(objectList);
    }

    transactionItemDto.setLotNumber(csv.get(LOT_NUMBER.getNumber()));

    AuctionDto auction = new AuctionDto();
    if (isNotBlank(csv.get(EOR_CHECKED.getNumber()))) {
      if (csv.get(EOR_CHECKED.getNumber()).toUpperCase().equals("Y"))
        auction.setEstimateOnRequest(true);
      else {
        throw new TransactionItemImportException(
            "EoR_checked: The value " + csv.get(EOR_CHECKED.getNumber())
                + "  is not valid, expected Y for true or empty for false");
      }
    } else {
      auction.setEstimateOnRequest(false);
    }
    if (isNotBlank(csv.get(ESTIMATE_LOW.getNumber()))) {
      try {
        auction.setLowEstimate(new BigDecimal(csv.get(ESTIMATE_LOW.getNumber())));
      } catch (NumberFormatException e) {
        throw new TransactionItemImportException("Estimate_Low: The Value "
            + csv.get(ESTIMATE_LOW.getNumber()) + " is not a valid number");
      }
    }
    if (isNotBlank(csv.get(ESTIMATE_HIGH.getNumber()))) {
      try {
        auction.setHighEstimate(new BigDecimal(csv.get(ESTIMATE_HIGH.getNumber())));
      } catch (NumberFormatException e) {
        throw new TransactionItemImportException("Estimate_High: The Value "
            + csv.get(ESTIMATE_HIGH.getNumber()) + " is not a valid number");
      }
    }
    if (isNotBlank(csv.get(TERM.getNumber()))) {
      AuctionTermDto term = new AuctionTermDto();
      term.setId(csv.get(TERM.getNumber()));
      auction.setTerm(term);
    }
    if (isNotBlank(csv.get(UNSOLD_HAMMER.getNumber()))) {
      try {
        auction.setUnsoldHammer(new BigDecimal(csv.get(UNSOLD_HAMMER.getNumber())));
      } catch (NumberFormatException e) {
        throw new TransactionItemImportException("Unsold Hammer: The Value "
            + csv.get(UNSOLD_HAMMER.getNumber()) + " is not a valid number");
      }
    }
    if (isNotBlank(csv.get(HAMMER.getNumber()))) {
      try {
        auction.setHammer(new BigDecimal(csv.get(HAMMER.getNumber())));
      } catch (NumberFormatException e) {
        throw new TransactionItemImportException(
            "Hammer: The Value " + csv.get(HAMMER.getNumber()) + " is not a valid number");
      }
    }
    if (isNotBlank(csv.get(PREMIUM.getNumber()))) {
      try {
        auction.setPremium(new BigDecimal(csv.get(PREMIUM.getNumber())));
      } catch (NumberFormatException e) {
        throw new TransactionItemImportException(
            "Premium: The Value " + csv.get(PREMIUM.getNumber()) + " is not a valid number");
      }
    }

    TransactionStatusDto status = new TransactionStatusDto();
    if (isNotBlank(csv.get(SALE_STATUS.getNumber()))) {
      status.setId(csv.get(SALE_STATUS.getNumber()));
    }
    transactionItemDto.setStatus(status);


    PrivateSaleDto privateSale = new PrivateSaleDto();
    if (isNotBlank(csv.get(PS_NET_VENDOR.getNumber()))) {
      try {
        privateSale.setNetVendor(new BigDecimal(csv.get(PS_NET_VENDOR.getNumber())));
      } catch (NumberFormatException e) {
        throw new TransactionItemImportException("PS_Net_Vendor: The Value "
            + csv.get(PS_NET_VENDOR.getNumber()) + " is not a valid number");
      }
    }
    if (isNotBlank(csv.get(PS_NET_BUYER.getNumber()))) {
      try {
        privateSale.setNetBuyer(new BigDecimal(csv.get(PS_NET_BUYER.getNumber())));
      } catch (NumberFormatException e) {
        throw new TransactionItemImportException("PS_Net_Buyer: The Value "
            + csv.get(PS_NET_BUYER.getNumber()) + " is not a valid number");
      }
    }

    if (privateSale.getNetBuyer() != null || privateSale.getNetVendor() != null) {
      transactionItemDto.setTransactionType(TransactionType.PRIVATE_SALE);
      transactionItemDto.setPrivateSale(privateSale);
      // Validate if auction fields has values
      if (auction.isEstimateOnRequest() || auction.getLowEstimate() != null
          || auction.getHighEstimate() != null || auction.getTerm() != null
          || auction.getUnsoldHammer() != null || auction.getHammer() != null
          || auction.getUnsoldHammer() != null) {
        throw new TransactionItemImportException(
            "This transaction item should be either Auction Type or Private Sale Type");
      }
    } else {
      transactionItemDto.setTransactionType(TransactionType.AUCTION);
      transactionItemDto.setAuction(auction);
    }

    transactionItemDto.setLegacyId(csv.get(OLD_ITEM_ID.getNumber()));
    transactionItemDto.setComments(csv.get(ITEM_NOTES.getNumber()));
    transactionItemDto.setDescription(csv.get(DESCRIPTION.getNumber()));
    transactionItemDto.setProvenance(csv.get(PROVENANCE.getNumber()));
    transactionItemDto.setExhibition(csv.get(EXHIBITION.getNumber()));
    transactionItemDto.setLiterature(csv.get(LITERATURE.getNumber()));
    transactionItemDto.setCondition(csv.get(CONDITION.getNumber()));
    transactionItemDto.setCataloguingNotes(csv.get(OD_NOTES.getNumber()));

    return transactionItemDto;
  }

}
