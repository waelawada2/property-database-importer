package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.EstablishmentCsvFields.CITY;
import static com.sothebys.propertydb.importer.support.EstablishmentCsvFields.COUNTRY;
import static com.sothebys.propertydb.importer.support.EstablishmentCsvFields.ESTABLISHMENT_CODE;
import static com.sothebys.propertydb.importer.support.EstablishmentCsvFields.ESTABLISHMENT_ID;
import static com.sothebys.propertydb.importer.support.EstablishmentCsvFields.ESTABLISHMENT_NAME;
import static com.sothebys.propertydb.importer.support.EstablishmentCsvFields.REGION;
import static com.sothebys.propertydb.importer.support.EstablishmentCsvFields.STATE;

import com.sothebys.propertydb.importer.model.event.EstablishmentDto;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class EstablishmentMapper {

  public EstablishmentDto map(List<String> csv) {
    EstablishmentDto dto = new EstablishmentDto();
    dto.setCode(csv.get(ESTABLISHMENT_CODE.getNumber()));
    dto.setId(csv.get(ESTABLISHMENT_ID.getNumber()));
    dto.setName(csv.get(ESTABLISHMENT_NAME.getNumber()));
    dto.setCityName(csv.get(CITY.getNumber()));
    dto.setState(csv.get(STATE.getNumber()));
    dto.setCountryName(csv.get(COUNTRY.getNumber()));
    dto.setRegion(csv.get(REGION.getNumber()));
    return dto;
  }
}
