package com.sothebys.propertydb.importer.support;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum TransactionItemCsvFields {
  TRANSACTION_ITEM_CODE(0, "Transaction_Item_Code"), // A
  TRANSACTION_ITEM_ID(1, "Transaction_Item_ID"), // B
  TRANSACTION_EVENT_CODE(2, "Transaction_Event_Code"), // C
  TRANSACTION_EVENT_ID(3, "Transaction_Event_ID"), // D
  CONSIGNMENT_CODE(4, "Consignment_Code"), // E
  CONSIGNMENT_ID(5, "Consignment_ID"), // F
  PURCHASE_CODE(6, "Purchase_Code"), // G
  PURCHASE_ID(7, "Purchase_ID"), // H
  OBJECT_CODE(8, "Object_Code"), // I
  EOS_OBJECT_ID(9, "EOS_Object_ID"), // J
  LOT_NUMBER(10, "Lot_Number"), // K
  EOR_CHECKED(11, "EoR_checked"), // L
  ESTIMATE_LOW(12, "Estimate_Low"), // M
  ESTIMATE_HIGH(13, "Estimate_High"), // N
  TERM(14, "Term"), // O
  UNSOLD_HAMMER(15, "Unsold Hammer"), // P
  HAMMER(16, "Hammer"), // Q
  PREMIUM(17, "Premium"), // R
  SALE_STATUS(18, "Sale_Status"), // S
  PS_NET_VENDOR(19, "PS_Net_Vendor"), // T
  PS_NET_BUYER(20, "PS_Net_Buyer"), // U
  OLD_ITEM_ID(21, "Old_item_ID"), // V
  ITEM_NOTES(22, "Item_Notes"), // W
  DESCRIPTION(23, "Description"), // X
  PROVENANCE(24, "Provenance"), // Y
  EXHIBITION(25, "Exhibition"), // Z
  LITERATURE(26, "Literature"), // AA
  CONDITION(27, "Condition"), // AB
  OD_NOTES(28, "OD_Notes"), // AC
  ERROR_DETAILS (29, "Error_Details");
  
  private int number;
  private String text;

}
