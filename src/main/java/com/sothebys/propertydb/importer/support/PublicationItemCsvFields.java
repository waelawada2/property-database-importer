package com.sothebys.propertydb.importer.support;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum PublicationItemCsvFields {

   CODE ( 0, "Publication_Item_Code"),
   ID ( 1, "Publication_Item_ID"),
   PUBLICATION_EVENT_CODE(2, "Publication_Event_Code"),
   PUBLICATION_EVENT_ID(3, "Publication_Event_ID"),
   CONSIGNMENT_CODE(4, "Consignment_Code"),
   CONSIGNMENT_ID(5, "Consignment_ID"),
   OBJECT_CODE(6, "Object_Code"),
   OBJECT_ID(7, "Object_ID"),
   CAT_PAGE(8, "Cat._Page"),
   NUMBER(9, "Number"),
   PLATE(10, "Plate"),
   FIGURE(11, "Figure"),
   Ill_PAGE(12, "Ill._Page"),
   COLOUR(13, "Colour"),
   TYPE(14, "Type"),
   ITEM_NOTES(15, "Item_Notes"),
   DESCRIPTION(16, "Description"),
   PROVENANCE(17, "Provenance"),
   EXHIBITION(18, "Exhibition"),
   LITERATURE(19, "Literature"),
   CONDITION_TEXT(20, "Condition_text"),
   OD_NOTES(21, "OD_Notes"),
   ERROR_DETAILS ( 22, "Error_Details");

   private int number;
   private String text;

}
