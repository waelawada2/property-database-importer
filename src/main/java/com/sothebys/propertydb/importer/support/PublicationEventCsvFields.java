package com.sothebys.propertydb.importer.support;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum PublicationEventCsvFields {

   CODE ( 0, "Publication_Code"),
   ID ( 1, "Publication_ID"),
   TYPE ( 2, "Publication_Type"),
   DATE ( 3, "Publication_Date"),
   TITLE ( 4, "Title"),
   SUB_TITLE ( 5, "Sub-title"),
   AUTHOR ( 6, "Author"),
   ESTABLISHMENT_CODE ( 7, "Establishment_Code" ),
   ESTABLISHMENT_ID( 8, "Establishment_ID"),
   VOLUME ( 9, "Volume"),
   EDITION ( 10, "Edition_Number"),
   PAGES ( 11, "Pages"),
   VERIFIED ( 12, "Verified" ),
   ISBN ( 13, "ISBN" ),
   LIBRARY_CODE(14, "Library_Code"),
   EXHIBITIONS ( 15, "Associated_Exhibition_ID"),
   PUBLICATIONS ( 16, "Associated_Publications_ID"),
   PUBLICATION_NOTES(17, "Publication_Notes"),
   PUBLICATION_IMAGE(18, "Publication_Image"),
   IMAGE_UPDATE_Y_N(19, "Image_Update_Y/N"),
   ERROR_DETAILS ( 20, "Error_Details");

   private int number;
   private String text;

}