package com.sothebys.propertydb.importer.support;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ClientSourceCsvFields {
  SOURCE_ID(0, "Source ID"), // A
  SOURCE_NAME(1, "Source Name"), // B
  ERROR_DETAILS (3, "Error_Details");
  
  private int number;
  private String text;

}
