package com.sothebys.propertydb.importer.support;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ConsignmentPurchaseEventCsvFields {
  
  PURCHASE_CONSIGNMENT_CODE(0, "Consignment_Purchase_Code"),
  PURCHASE_CONSIGNMENT_ID(1, "Consignment_Purchase_ID"),
  EVENT_TYPE(2, "Event_Type"),
  OWNERSHIP_CLIENT_SOURCE_TYPE(3, "Ownership_Client_Source_Type"),
  OWNERSHIP_CLIENT_SOURCE_NAME(4, "Ownership_Client_Source_Name"),
  OWNERSHIP_CLIENT_SOURCE_ID(5, "Ownership_Client_Source_ID"),
  OWNERSHIP_STATUS(6, "Ownership_Status"),
  OWNERSHIP_CLIENT_NAME(7, "Ownership_Client_Name"),
  CLIENT_ID(8, "Client_ID"),
  DESIGNATION(9, "Designation"),
  AFFILIATION_SOURCE_TYPE(10, "Affiliation_Source_Type"),
  AFFILIATION_SOURCE_NAME(11, "Affiliation_Source_Name"),
  AFFILIATION_SOURCE_ID(12, "Affiliation_Source_ID"),
  AFFILIATION_NAME(13, "Affiliation_Name"),
  AFFILIATION_ID(14, "Affiliation_ID"),
  AFFILIATION_TYPE(15, "Affiliation_Type"),
  ERROR_DETAILS (16, "Error_Details");
  
  private int number;
  private String text;
  
}
