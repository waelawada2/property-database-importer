package com.sothebys.propertydb.importer.support;

/**
 * Contains constants of the property database importer.
 *
 * @author VenkataPrasad Tammineni
 */
public class PropertyImporterConstants {

  public static final String TEMP_PATH = "java.io.tmpdir";

  public static final String FILE = "file";

  public static final String NULL = "null";

  public static final String CSV = ".csv";

  public static final String PAINTING = "Painting";

  public static final String WORK_ON_PAPER = "Work on Paper";

  public static final String PAPER_LAID_DOWN = "Paper laid down";

  public static final String PHOTOGRAPH = "Photograph";

  public static final String SCULPTURE = "Sculpture";

  public static final String PRINT = "Print";

  public static final String OTHER = "Other";

  public static final String VIDEO = "Video";

  public static final String INSTALLATION = "Installation";

  public static final String ERRORS = "errors";

  public static final String UTF_8 = "UTF-8";

  private PropertyImporterConstants() {
    throw new AssertionError();
  }

}
