package com.sothebys.propertydb.importer.support;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum TransactionEventCsvFields {
  TRANSACTION_CODE(0, "Transaction_Code"), // A
  TRANSACTION_ID(1, "Transaction_ID"), // B
  DATE(2, "Date"), // C
  TYPE(3, "Type"), // D
  TITLE(4, "Title"), // E
  ESTABLISHMENT_CODE(5, "Establishment_Code"), // F
  ESTABLISHMENT_ID(6, "Establishment_ID"), // G
  DEPARTMENT(7, "Department"), // H
  SALE_NUMBER(8, "Sale_#"), // I
  CURRENCY(9, "Currency"), // J
  TOTAL_LOTS(10, "Total_Lots"), // K
  NOTES(11, "Transaction_Notes"), // L
  ERROR_DETAILS (12, "Error_Details");
  
  private int number;
  private String text;
}
