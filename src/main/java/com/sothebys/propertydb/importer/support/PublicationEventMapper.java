package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.AUTHOR;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.CODE;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.DATE;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.EDITION;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.ESTABLISHMENT_CODE;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.ESTABLISHMENT_ID;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.ID;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.ISBN;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.LIBRARY_CODE;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.PUBLICATION_NOTES;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.PAGES;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.PUBLICATIONS;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.SUB_TITLE;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.TITLE;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.TYPE;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.VERIFIED;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.VOLUME;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.IMAGE_UPDATE_Y_N;
import static com.sothebys.propertydb.importer.support.PublicationEventCsvFields.PUBLICATION_IMAGE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.sothebys.propertydb.importer.exception.PricingEventImportException;
import com.sothebys.propertydb.importer.exception.PublicationEventImportException;
import com.sothebys.propertydb.importer.model.event.EstablishmentDto;
import com.sothebys.propertydb.importer.model.event.PublicationEventDto;
import com.sothebys.propertydb.importer.model.event.PublicationTypeDto;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class PublicationEventMapper {

  public static final String VERIFIED_VALUE = "VERIFIED";
  public static final String UNVERIFIED_VALUE = "UNVERIFIED";

  public PublicationEventDto map(List<String> csv) {
    PublicationEventDto dto = new PublicationEventDto();

    dto.setCode(csv.get(CODE.getNumber()));
    dto.setId(csv.get(ID.getNumber()));
    
    try {
      dto.setDate(CsvStringConverter.toDate(csv.get(DATE.getNumber()), "d/MMM/yyyy"));
    } catch (Exception e) {
      throw new PricingEventImportException("Date: "+e.getMessage()+" expected format is \"d/MMM/yyyy\"", e);
    }

    dto.setTitle(csv.get(TITLE.getNumber()));
    dto.setSubTitle(csv.get(SUB_TITLE.getNumber()));
    dto.setAuthor(csv.get(AUTHOR.getNumber()));

    EstablishmentDto establishmentDto = null;
    if (isNotBlank(csv.get(ESTABLISHMENT_ID.getNumber()))) {
      establishmentDto = new EstablishmentDto();
      establishmentDto.setId(csv.get(ESTABLISHMENT_ID.getNumber()));
      establishmentDto.setCode(csv.get(ESTABLISHMENT_CODE.getNumber()));
    }

    dto.setEstablishment(establishmentDto);

    dto.setVolume(csv.get(VOLUME.getNumber()));
    dto.setEdition(csv.get(EDITION.getNumber()));

    if(isNotBlank(csv.get(PAGES.getNumber()))) {
      try {
        dto.setPages(Integer.parseInt(csv.get(PAGES.getNumber())));
      } catch (NumberFormatException e) {
        throw new PublicationEventImportException(
            "Pages: The value "
                + csv.get(PAGES.getNumber()) + " is not a valid number");
      }
    }
    
    if (isNotBlank(csv.get(VERIFIED.getNumber()))) {
      if (csv.get(VERIFIED.getNumber()).toUpperCase().equals(VERIFIED_VALUE)) {
        dto.setVerified(true);
      } else if (csv.get(VERIFIED.getNumber()).toUpperCase().equals(UNVERIFIED_VALUE)) {
        dto.setVerified(false);
      } else {
        throw new PublicationEventImportException(VERIFIED.getText() + ": is not valid, expected Verified or Unverified");
      }
    } else {
      throw new PublicationEventImportException(VERIFIED.getText() + ": Empty Value");
    }

    dto.setIsbn(csv.get(ISBN.getNumber()));
    dto.setNotes(csv.get(PUBLICATION_NOTES.getNumber()));
    dto.setPublicationType(new PublicationTypeDto());
    dto.getPublicationType().setId(csv.get(TYPE.getNumber()));
    dto.setLibraryCode(csv.get(LIBRARY_CODE.getNumber()));


    // TODO: Exhibitions relationship mapping is missing as Exhibition Events
    // are still not implemented.


    String publications = csv.get(PUBLICATIONS.getNumber());
    if (StringUtils.isNotEmpty(publications)) {
      String publicationIds[] = publications.split("\\|");
      dto.setPublicationEvents(new ArrayList<>());
      for (String id : publicationIds) {
        PublicationEventDto pub = new PublicationEventDto();
        pub.setId(id);
        dto.getPublicationEvents().add(pub);
      }

    }
    
    String updateImage = csv.get(IMAGE_UPDATE_Y_N.getNumber());
    String publicationImage = csv.get(PUBLICATION_IMAGE.getNumber());
    if(!StringUtils.isBlank(csv.get(ID.getNumber()))) {
      if("Y".equals(updateImage)) {
        dto.setImageName(publicationImage);
        dto.setOriginalImageName(publicationImage);
        dto.setUpdateImage(true);
      }else if("N".equals(updateImage)) {
        dto.setUpdateImage(false);
      }else {
        throw new PublicationEventImportException(IMAGE_UPDATE_Y_N.getText() + ": should be Y or N");
      }
    }else {
      dto.setImageName(publicationImage);
      dto.setOriginalImageName(publicationImage);
      dto.setUpdateImage(true);
    }


    return dto;
  }
}