package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.CURRENCY;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.DATE;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.DEPARTMENT;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.ESTABLISHMENT_ID;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.NOTES;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.SALE_NUMBER;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.TITLE;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.TOTAL_LOTS;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.TRANSACTION_CODE;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.TRANSACTION_ID;
import static com.sothebys.propertydb.importer.support.TransactionEventCsvFields.TYPE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.sothebys.propertydb.importer.exception.EnumNotFoundException;
import com.sothebys.propertydb.importer.exception.TransactionEventImportException;
import com.sothebys.propertydb.importer.model.event.CurrencyDto;
import com.sothebys.propertydb.importer.model.event.DepartmentDto;
import com.sothebys.propertydb.importer.model.event.EstablishmentDto;
import com.sothebys.propertydb.importer.model.event.TransactionEventDto;
import com.sothebys.propertydb.importer.model.event.TransactionType;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class TransactionEventMapper {

  public TransactionEventDto map(List<String> csv) {

    EstablishmentDto establishmentDto = null;
    if (isNotBlank(csv.get(ESTABLISHMENT_ID.getNumber()))) {
      establishmentDto = new EstablishmentDto();
      establishmentDto.setId(csv.get(ESTABLISHMENT_ID.getNumber()));
    }

    DepartmentDto departmentDto = null;
    if (isNotBlank(csv.get(DEPARTMENT.getNumber()))) {
      departmentDto = new DepartmentDto();
      departmentDto.setId(csv.get(DEPARTMENT.getNumber()));
    }

    CurrencyDto currencyDto = null;
    if (isNotBlank(csv.get(CURRENCY.getNumber()))) {
      currencyDto = new CurrencyDto();
      currencyDto.setId(csv.get(CURRENCY.getNumber()));
    }

    TransactionEventDto transactionEventDto = new TransactionEventDto();
    transactionEventDto.setCode(csv.get(TRANSACTION_CODE.getNumber()));
    transactionEventDto.setId(csv.get(TRANSACTION_ID.getNumber()));
    try {
      transactionEventDto.setDate(CsvStringConverter.toDate(csv.get(DATE.getNumber()), "d/MMM/yyyy"));
    } catch (Exception e) {
      throw new TransactionEventImportException("Date: "+e.getMessage()+" expected format is \"d/MMM/yyyy\"", e);
    }
    
    try {
      transactionEventDto.setTransactionType(TransactionType.findByValue(csv.get(TYPE.getNumber())));
    } catch (EnumNotFoundException e) {
      throw new TransactionEventImportException("transactionType: "+e.getMessage(), e);
    }
    
    
    transactionEventDto.setTitle(csv.get(TITLE.getNumber()));
    transactionEventDto.setEstablishment(establishmentDto);
    transactionEventDto.setDepartment(departmentDto);
    transactionEventDto.setSaleNumber(csv.get(SALE_NUMBER.getNumber()));
    transactionEventDto.setCurrency(currencyDto);
    try {
      transactionEventDto.setTotalLots(CsvStringConverter.toDouble(csv.get(TOTAL_LOTS.getNumber())));
    } catch (NumberFormatException e) {
      throw new TransactionEventImportException(
          "Number format issue (" + e.getMessage() + ") ", e);
    }
    transactionEventDto.setNotes(csv.get(NOTES.getNumber()));

    return transactionEventDto;
  }
}
