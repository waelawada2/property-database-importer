package com.sothebys.propertydb.importer.support;

import java.util.ArrayList;
import java.util.List;

import com.sothebys.propertydb.importer.model.item.ItemObjectDto;

public class MapperUtils {
  /**
   * Changes a list of Item IDs into its corresponding Item Object DTOs
   *
   * @param itemObjectIds List of object Ids.
   * @return List of Item Objects DTOs
   */
  public static List<ItemObjectDto> itemObjectsToString(List<String> itemObjectIds) {
    if (itemObjectIds == null) {
      return null;
    }

    List<ItemObjectDto> itemObjectDtos = new ArrayList<>();
    for (String objectId : itemObjectIds) {
      ItemObjectDto itemObjectDto = new ItemObjectDto();
      itemObjectDto.setObjectId(objectId);
      itemObjectDtos.add(itemObjectDto);
    }
    return itemObjectDtos;
  }
}
