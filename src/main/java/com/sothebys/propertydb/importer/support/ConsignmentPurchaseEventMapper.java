package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.ConsignmentPurchaseEventCsvFields.*;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.sothebys.propertydb.importer.model.event.MainClientDto;
import com.sothebys.propertydb.importer.exception.ConsignmentPurchaseEventImportException;
import com.sothebys.propertydb.importer.model.event.AffiliatedClientDto;
import com.sothebys.propertydb.importer.model.event.AffiliatedTypeDto;
import com.sothebys.propertydb.importer.model.event.ClientSourceDto;
import com.sothebys.propertydb.importer.model.event.ClientSourceTypeDto;
import com.sothebys.propertydb.importer.model.event.ClientStatusDto;
import com.sothebys.propertydb.importer.model.event.ConsignmentPurchaseEventDto;
import com.sothebys.propertydb.importer.model.event.EventType;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ConsignmentPurchaseEventMapper {

  public ConsignmentPurchaseEventDto map(List<String> csv) {
    EventType eventType = null;
    ConsignmentPurchaseEventDto consignmentPurchaseEventDto = null;

    if (isNotBlank(csv.get(EVENT_TYPE.getNumber()))) {
      try {
        eventType = EventType.valueOf(csv.get(EVENT_TYPE.getNumber()).toUpperCase());
      } catch (IllegalArgumentException iae) {
        throw new ConsignmentPurchaseEventImportException(
            "Event_Type: The event type " + csv.get(EVENT_TYPE.getNumber()).toUpperCase()
                + " is not valid, the valid event types are: " + EventType.CONSIGNMENT + " and "
                + EventType.PURCHASE);
      }
    } else {
      throw new ConsignmentPurchaseEventImportException("Event_Type: may not be null ");
    }



    consignmentPurchaseEventDto = new ConsignmentPurchaseEventDto(eventType);

    MainClientDto mainClientDto = new MainClientDto();

    ClientSourceDto clientSourceDto = null;
    if (isNotBlank(csv.get(OWNERSHIP_CLIENT_SOURCE_ID.getNumber()))) {
      clientSourceDto = new ClientSourceDto();
      clientSourceDto.setId(csv.get(OWNERSHIP_CLIENT_SOURCE_ID.getNumber()));
    }

    ClientSourceTypeDto clientSourceTypeDto = null;
    if (isNotBlank(csv.get(OWNERSHIP_CLIENT_SOURCE_TYPE.getNumber()))) {
      clientSourceTypeDto = new ClientSourceTypeDto();
      clientSourceTypeDto.setId(csv.get(OWNERSHIP_CLIENT_SOURCE_TYPE.getNumber()));
    }

    ClientStatusDto clientStatusDto = null;
    if (isNotBlank(csv.get(OWNERSHIP_STATUS.getNumber()))) {
      clientStatusDto = new ClientStatusDto();
      clientStatusDto.setId(csv.get(OWNERSHIP_STATUS.getNumber()));
    }

    mainClientDto.setClientSource(clientSourceDto);
    mainClientDto.setClientSourceType(clientSourceTypeDto);
    mainClientDto.setClientStatus(clientStatusDto);
    mainClientDto.setClientId(csv.get(CLIENT_ID.getNumber()));
    mainClientDto.setDisplay(csv.get(DESIGNATION.getNumber()));

    ClientSourceDto affiliatedClientSourceDto = null;
    if (isNotBlank(csv.get(AFFILIATION_SOURCE_ID.getNumber()))) {
      affiliatedClientSourceDto = new ClientSourceDto();
      affiliatedClientSourceDto.setId(csv.get(AFFILIATION_SOURCE_ID.getNumber()));
    }

    ClientSourceTypeDto affiliatedClientSourceTypeDto = null;
    if (isNotBlank(csv.get(AFFILIATION_SOURCE_TYPE.getNumber()))) {
      affiliatedClientSourceTypeDto = new ClientSourceTypeDto();
      affiliatedClientSourceTypeDto.setId(csv.get(AFFILIATION_SOURCE_TYPE.getNumber()));
    }

    AffiliatedTypeDto affiliatedTypeDto = null;
    if (isNotBlank(csv.get(AFFILIATION_TYPE.getNumber()))) {
      affiliatedTypeDto = new AffiliatedTypeDto();
      affiliatedTypeDto.setId(csv.get(AFFILIATION_TYPE.getNumber()));
    }

    if (affiliatedClientSourceDto != null || affiliatedClientSourceTypeDto != null
        || affiliatedTypeDto != null || isNotBlank(csv.get(AFFILIATION_ID.getNumber()))) {
      AffiliatedClientDto affiliatedClientDto = new AffiliatedClientDto();

      affiliatedClientDto.setClientSource(affiliatedClientSourceDto);
      affiliatedClientDto.setClientSourceType(affiliatedClientSourceTypeDto);
      affiliatedClientDto.setAffiliatedType(affiliatedTypeDto);
      affiliatedClientDto.setClientId(csv.get(AFFILIATION_ID.getNumber()));
      consignmentPurchaseEventDto.setAffiliatedClient(affiliatedClientDto);
    }

    consignmentPurchaseEventDto.setMainClient(mainClientDto);
    consignmentPurchaseEventDto.setCode(csv.get(PURCHASE_CONSIGNMENT_CODE.getNumber()));
    consignmentPurchaseEventDto.setId(csv.get(PURCHASE_CONSIGNMENT_ID.getNumber()));

    return consignmentPurchaseEventDto;
  }

}
