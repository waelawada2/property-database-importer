package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.OBJECT_ID;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_AUCTION_CURRENCY;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_AUCTION_HIGH;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_AUCTION_LOW;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_AUCTION_PLUS_1_CHECK;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_EVENT_ID;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_ITEM_CODE;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_ITEM_ID;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_PS_CURRENCY;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_PS_VALUE;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_INSURANCE_CURRENCY;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_INSURANCE_VALUE;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_FMV_CURRENCY;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PRICING_FMV_VALUE;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PIPELINE_AUCTION;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.AUCTION_TARGET_CHECK;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PIPELINE_PS;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PS_TARGET_CHECK;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.PIPELINE_EXHIBITION;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.EXHIBITION_TARGET_CHECK;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.OLD_ITEM_ID;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.ITEM_NOTES;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.ITEM_DESCRIPTION;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.ITEM_PROVENANCE;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.ITEM_EXHIBITION;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.ITEM_LITERATURE;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.ITEM_CONDITION;
import static com.sothebys.propertydb.importer.support.PricingItemCsvFields.ITEM_ODNOTES;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.sothebys.propertydb.importer.exception.PricingItemImportException;
import com.sothebys.propertydb.importer.model.event.CurrencyDto;
import com.sothebys.propertydb.importer.model.event.EventDto;
import com.sothebys.propertydb.importer.model.event.ExhibitionEventDto;
import com.sothebys.propertydb.importer.model.event.PricingEventDto;
import com.sothebys.propertydb.importer.model.event.TransactionEventDto;
import com.sothebys.propertydb.importer.model.item.ItemObjectDto;
import com.sothebys.propertydb.importer.model.item.PricingItemDto;

@Component
public class PricingItemMapper {
  public PricingItemDto map(List<String> csv) {
    PricingItemDto pricingItemDto = new PricingItemDto();
    pricingItemDto.setCode(csv.get(PRICING_ITEM_CODE.getNumber()));
    pricingItemDto.setId(csv.get(PRICING_ITEM_ID.getNumber()));

    EventDto pricingEvent = null;
    if (isNotBlank(csv.get(PRICING_EVENT_ID.getNumber()))) {
      pricingEvent = new PricingEventDto();
      pricingEvent.setId(csv.get(PRICING_EVENT_ID.getNumber()));
    }
    pricingItemDto.setEvent(pricingEvent);

    if (isNotBlank(csv.get(OBJECT_ID.getNumber()))) {
      List<String> objectIds =
          Arrays.asList(Pattern.compile("\\|").split(csv.get(OBJECT_ID.getNumber())));
      List<ItemObjectDto> objectList = MapperUtils.itemObjectsToString(objectIds);
      if (objectList != null)
        pricingItemDto.setObjects(objectList);
    }

    CurrencyDto auctionCurrency = null;
    if (isNotBlank(csv.get(PRICING_AUCTION_CURRENCY.getNumber()))) {
      auctionCurrency = new CurrencyDto();
      auctionCurrency.setId(csv.get(PRICING_AUCTION_CURRENCY.getNumber()));
    }
    pricingItemDto.setAuctionCurrency(auctionCurrency);

    if (isNotBlank(csv.get(PRICING_AUCTION_LOW.getNumber()))) {
      try {
        pricingItemDto.setAuctionLowValue(new BigDecimal(csv.get(PRICING_AUCTION_LOW.getNumber())));
      } catch (NumberFormatException e) {
        throw new PricingItemImportException("Pricing_Auction_Low: The value "
            + csv.get(PRICING_AUCTION_LOW.getNumber()) + " is not a valid number");
      }

    }

    if (isNotBlank(csv.get(PRICING_AUCTION_HIGH.getNumber()))) {
      try {
        pricingItemDto
            .setAuctionHighValue(new BigDecimal(csv.get(PRICING_AUCTION_HIGH.getNumber())));
      } catch (NumberFormatException e) {
        throw new PricingItemImportException("Pricing_Auction_High: The value "
            + csv.get(PRICING_AUCTION_HIGH.getNumber()) + " is not a valid number");
      }
    }

    if (isNotBlank(csv.get(PRICING_AUCTION_PLUS_1_CHECK.getNumber()))) {
      if (csv.get(PRICING_AUCTION_PLUS_1_CHECK.getNumber()).toUpperCase().equals("Y"))
        pricingItemDto.setAuctionPlusOne(true);
      else {
        throw new PricingItemImportException("Pricing_Auction+1_check: The value "
            + csv.get(PRICING_AUCTION_PLUS_1_CHECK.getNumber()) + " is not a valid number");
      }
    } else {
      pricingItemDto.setAuctionPlusOne(false);
    }

    CurrencyDto privateSaleCurrency = null;
    if (isNotBlank(csv.get(PRICING_PS_CURRENCY.getNumber()))) {
      privateSaleCurrency = new CurrencyDto();
      privateSaleCurrency.setId(csv.get(PRICING_PS_CURRENCY.getNumber()));
    }
    pricingItemDto.setPrivateSaleCurrency(privateSaleCurrency);

    if (isNotBlank(csv.get(PRICING_PS_VALUE.getNumber()))) {
      try {
        pricingItemDto
            .setPrivateSaleNetSeller(new BigDecimal(csv.get(PRICING_PS_VALUE.getNumber())));
      } catch (NumberFormatException e) {
        throw new PricingItemImportException("Pricing_PS_Value: The value "
            + csv.get(PRICING_PS_VALUE.getNumber()) + " is not a valid number");
      }
    }

    CurrencyDto insuranceCurrency = null;
    if (isNotBlank(csv.get(PRICING_INSURANCE_CURRENCY.getNumber()))) {
      insuranceCurrency = new CurrencyDto();
      insuranceCurrency.setId(csv.get(PRICING_INSURANCE_CURRENCY.getNumber()));
    }
    pricingItemDto.setInsuranceCurrency(insuranceCurrency);

    if (isNotBlank(csv.get(PRICING_INSURANCE_VALUE.getNumber()))) {
      try {
        pricingItemDto
            .setInsurancePrice(new BigDecimal(csv.get(PRICING_INSURANCE_VALUE.getNumber())));
      } catch (NumberFormatException e) {
        throw new PricingItemImportException("Pricing_Insurance_Value: The value "
            + csv.get(PRICING_INSURANCE_VALUE.getNumber()) + " is not a valid number");
      }
    }

    CurrencyDto fairMarketValueCurrency = null;
    if (isNotBlank(csv.get(PRICING_FMV_CURRENCY.getNumber()))) {
      fairMarketValueCurrency = new CurrencyDto();
      fairMarketValueCurrency.setId(csv.get(PRICING_FMV_CURRENCY.getNumber()));
    }
    pricingItemDto.setFairMarketValueCurrency(fairMarketValueCurrency);

    if (isNotBlank(csv.get(PRICING_FMV_VALUE.getNumber()))) {
      try {
        pricingItemDto
            .setFairMarketValuePrice(new BigDecimal(csv.get(PRICING_FMV_VALUE.getNumber())));
      } catch (Exception e) {
        throw new PricingItemImportException("Pricing_FMV_Value: The value "
            + csv.get(PRICING_FMV_VALUE.getNumber()) + " is not a valid number");
      }

    }

    TransactionEventDto pipelineAuction = null;
    if (isNotBlank(csv.get(PIPELINE_AUCTION.getNumber()))) {
      pipelineAuction = new TransactionEventDto();
      pipelineAuction.setId(csv.get(PIPELINE_AUCTION.getNumber()));
    }
    pricingItemDto.setPipelineAuction(pipelineAuction);

    if (isNotBlank(csv.get(AUCTION_TARGET_CHECK.getNumber()))) {
      if (csv.get(AUCTION_TARGET_CHECK.getNumber()).toUpperCase().equals("Y"))
        pricingItemDto.setPipelineAuctionTarget(true);
      else {
        throw new PricingItemImportException(
            "Auction_Target_check: The value " + csv.get(AUCTION_TARGET_CHECK.getNumber())
                + " is not valid, expected Y for true or empty for false");
      }
    } else {
      pricingItemDto.setPipelineAuctionTarget(false);
    }

    TransactionEventDto pipelinePrivateSale = null;
    if (isNotBlank(csv.get(PIPELINE_PS.getNumber()))) {
      pipelinePrivateSale = new TransactionEventDto();
      pipelinePrivateSale.setId(csv.get(PIPELINE_PS.getNumber()));
    }
    pricingItemDto.setPipelinePrivateSale(pipelinePrivateSale);

    if (isNotBlank(csv.get(PS_TARGET_CHECK.getNumber()))) {
      if (csv.get(PS_TARGET_CHECK.getNumber()).toUpperCase().equals("Y"))
        pricingItemDto.setPipelinePrivateSaleTarget(true);
      else {
        throw new PricingItemImportException(
            "PS_Target_check: The value " + csv.get(PS_TARGET_CHECK.getNumber())
                + " is not valid, expected Y for true or empty for false");
      }
    } else {
      pricingItemDto.setPipelinePrivateSaleTarget(false);
    }

    ExhibitionEventDto pipelineExhibition = null;
    if (isNotBlank(csv.get(PIPELINE_EXHIBITION.getNumber()))) {
      pipelineExhibition = new ExhibitionEventDto();
      pipelineExhibition.setId(csv.get(PIPELINE_EXHIBITION.getNumber()));
    }
    pricingItemDto.setPipelineExhibition(pipelineExhibition);

    if (isNotBlank(csv.get(EXHIBITION_TARGET_CHECK.getNumber()))) {
      if (csv.get(EXHIBITION_TARGET_CHECK.getNumber()).toUpperCase().equals("Y"))
        pricingItemDto.setPipelineExhibitionTarget(true);
      else {
        throw new PricingItemImportException(
            "Exhibition_Target_check: The value " + csv.get(EXHIBITION_TARGET_CHECK.getNumber())
                + " is not valid, expected Y for true or empty for false");
      }
    } else {
      pricingItemDto.setPipelineExhibitionTarget(false);
    }

    pricingItemDto.setLegacyId(csv.get(OLD_ITEM_ID.getNumber()));
    pricingItemDto.setComments(csv.get(ITEM_NOTES.getNumber()));
    pricingItemDto.setDescription(csv.get(ITEM_DESCRIPTION.getNumber()));
    pricingItemDto.setProvenance(csv.get(ITEM_PROVENANCE.getNumber()));
    pricingItemDto.setExhibition(csv.get(ITEM_EXHIBITION.getNumber()));
    pricingItemDto.setLiterature(csv.get(ITEM_LITERATURE.getNumber()));
    pricingItemDto.setCondition(csv.get(ITEM_CONDITION.getNumber()));
    pricingItemDto.setCataloguingNotes(csv.get(ITEM_ODNOTES.getNumber()));

    return pricingItemDto;
  }

}
