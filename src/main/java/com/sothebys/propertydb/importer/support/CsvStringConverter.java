package com.sothebys.propertydb.importer.support;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CsvStringConverter {


  /**
   * Convert a date string to a {@link Date} in UTC.
   *
   * @param value The string to be converted
   * @param pattern The date pattern (refer to {@link DateTimeFormatter} for the format)
   */
  public static Date toDate(String value, String pattern) {
    if (isBlank(value)) {
      return null;
    }

    DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
    builder.parseCaseInsensitive();
    builder.appendPattern(pattern);

    DateTimeFormatter formatter = builder.toFormatter();

    LocalDate localDate = LocalDate.parse(value, formatter);

    return Date.from(localDate.atStartOfDay(ZoneId.of("UTC")).toInstant());
  }

  public static Double toDouble(String value) {
    return isNotBlank(value) ? Double.valueOf(value.trim()) : null;
  }
}
