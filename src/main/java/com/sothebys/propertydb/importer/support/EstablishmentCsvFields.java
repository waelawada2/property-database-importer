package com.sothebys.propertydb.importer.support;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum EstablishmentCsvFields {
  
  ESTABLISHMENT_CODE(0, "Establishment_Code"), // A
  ESTABLISHMENT_ID(1, "Establishment_ID"), // B
  ESTABLISHMENT_NAME(2, "Establishment_Name"), // C
  CITY(3, "City"), // D
  STATE(4, "State"), // E
  COUNTRY(5, "Country"), // F
  REGION(6, "Region"), // G
  ERROR_DETAILS (7, "Error_Details");
  
  private int number;
  private String text;
}
