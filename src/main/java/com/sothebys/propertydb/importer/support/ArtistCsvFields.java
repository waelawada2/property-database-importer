package com.sothebys.propertydb.importer.support;

/**
 * Contains constants that represent column positions of the source CSV file.
 *
 * @author VenkataPrasad Tammineni
 */
public class ArtistCsvFields {

  public static final int ULAN_ID = 0; // A
  public static final int ARTIST_ID = 1; // B
  public static final int FIRST_NAME = 2; // C
  public static final int LAST_NAME = 3; // D
  public static final int BIRTH_YEAR = 4; // E
  public static final int DEATH_YEAR = 5; // F
  public static final int DISPLAY = 6; // G
  public static final int NATIONALITY = 7; // H
  public static final int GENDER = 8; // I
  public static final int UNKNOWN_CHECK = 9; // J
  public static final int PAINTING_CHECK = 10; // K
  public static final int WORK_ON_PAPER_CHECK = 11; // L
  public static final int MOUNTED_CHECK = 12; // M
  public static final int SCULPTURE_CHECK = 13; // N
  public static final int PHOTOGRAPH_CHECK = 14; // O
  public static final int PRINT_CHECK = 15; // P
  public static final int INSTALLATION_CHECK = 16; // Q
  public static final int VIDEO_CHECK = 17; // R
  public static final int OTHER_CHECK = 18; // S
  public static final int CATALOGUE_RAISONEE_AUTHOR = 19; // T
  public static final int CATALOGUE_RAISONEE_YEAR = 20; // U
  public static final int CATALOGUE_RAISONEE_VOLUMES = 21; // V
  public static final int CATALOGUE_RAISONEE_TYPE = 22; // W
  public static final int NOTES = 23; // W

  private ArtistCsvFields() {
    throw new AssertionError();
  }

}
