package com.sothebys.propertydb.importer.support;

import com.sothebys.propertydb.importer.model.event.PublicationEventDto;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.CompletionAwareAggregationStrategy;

public class PublicationEventImageAggregationStrategy implements CompletionAwareAggregationStrategy {

  @Override
  public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
    String imageName = (String) newExchange.getProperty("finalImageName");
    PublicationEventDto message = oldExchange.getIn().getBody(PublicationEventDto.class);

    message.setImageName(imageName);

    return oldExchange;
  }

  @Override
  public void onCompletion(Exchange exchange) {
    exchange.removeProperties("CamelFailure*");
    exchange.removeProperties("CamelException*");
    exchange.removeProperties("CamelError*");
  }

}