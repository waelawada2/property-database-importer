package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.PropertyImportApplication;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

/**
 * @author Minhtri Tran
 */
@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class EstablishmentImportRouteTest extends CamelTestSupport {

  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:postEstablishment")
  private MockEndpoint postEstablishmentMock;

  @EndpointInject(uri = "mock:putEstablishment")
  private MockEndpoint putEstablishmentMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  private byte[] establishmentCsvContent;

  @Before
  public void setUp() throws Exception {
    camelContext.getRouteDefinition("push-establishment-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:establishments*").skipSendToOriginalEndpoint()
                .to("mock:postEstablishment");
            interceptSendToEndpoint("rest:PUT:establishments*").skipSendToOriginalEndpoint()
                .to("mock:putEstablishment");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });

    File csvFile = new File("src/test/resources/establishment-import-test.csv");
    establishmentCsvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));
  }

  @After
  public void teardown() {
    postEstablishmentMock.reset();
    putEstablishmentMock.reset();
    sendImportReportMock.reset();
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testPostPutEstablishment() throws Exception {
    camelContext.start();

    postEstablishmentMock.expectedMessageCount(1);
    postEstablishmentMock.message(0).jsonpath("$.name").isEqualTo("Thames and Hudson Ltd., London");
    postEstablishmentMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    putEstablishmentMock.expectedMessageCount(1);
    putEstablishmentMock.message(0).jsonpath("$.name").isEqualTo("Sotheby's New York");
    putEstablishmentMock.expectedHeaderReceived(Exchange.HTTP_PATH, "SE-75NNX4W04QQ");
    putEstablishmentMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Establishment_Name");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate
        .sendBodyAndHeader("direct:start", establishmentCsvContent, Exchange.FILE_NAME, "test.csv");

    postEstablishmentMock.assertIsSatisfied();
    putEstablishmentMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 0);
    assertEquals(successfulRecords.size(), 2);
  }

  @Test
  public void testPostPutEstablishmentFailure() throws Exception {
    camelContext.start();

    postEstablishmentMock.expectedMessageCount(1);
    postEstablishmentMock.message(0).jsonpath("$.name").isEqualTo("Thames and Hudson Ltd., London");
    postEstablishmentMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    putEstablishmentMock.expectedMessageCount(1);
    putEstablishmentMock.message(0).jsonpath("$.name").isEqualTo("Sotheby's New York");
    putEstablishmentMock.expectedHeaderReceived(Exchange.HTTP_PATH, "SE-75NNX4W04QQ");
    putEstablishmentMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Establishment_Name");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate
        .sendBodyAndHeader("direct:start", establishmentCsvContent, Exchange.FILE_NAME, "test.csv");

    postEstablishmentMock.assertIsSatisfied();
    putEstablishmentMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 2);
    assertEquals(successfulRecords.size(), 0);

  }

}
