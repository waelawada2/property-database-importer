package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.PropertyImportApplication;
import com.sothebys.propertydb.importer.exception.PropertyImportException;
import com.sothebys.propertydb.importer.exception.WebsiteImageImportException;
import com.sothebys.propertydb.importer.model.PropertyCreateRequestDTO;
import com.sothebys.propertydb.importer.model.PropertyCreateRequestDTO.CatalogueRaisonee;
import java.io.File;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.impl.DefaultMessage;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.apache.http.conn.HttpHostConnectException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.annotation.DirtiesContext;


/**
 * @author Gregor Zurowski
 */
@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
public class PropertyImportRouteTest extends CamelTestSupport {

  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:result")
  private MockEndpoint resultMock;

  @EndpointInject(uri = "mock:addArtistCRIds")
  private MockEndpoint addArtistCRIdsMock;

  @EndpointInject(uri = "mock:properties")
  private MockEndpoint propertiesMock;

  @EndpointInject(uri = "mock:updateProperties")
  private MockEndpoint updatePropertiesMock;

  @EndpointInject(uri = "mock:uploadImage")
  private MockEndpoint uploadImageMock;

  @EndpointInject(uri = "mock:errorDeleteProperty")
  private MockEndpoint errorDeletePropertyMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  @EndpointInject(uri = "mock:createSaveListAndSendStatusEmail")
  private MockEndpoint createSaveListAndSendStatusEmailMock;

  @EndpointInject(uri = "mock:website")
  private MockEndpoint websiteMock;

  @EndpointInject(uri = "mock:websiteOutputFile")
  private MockEndpoint websiteOutputFileMock;

  @EndpointInject(uri = "mock:artists")
  private MockEndpoint artistsMock;

  @EndpointInject(uri = "mock:postSaveList")
  private MockEndpoint postSaveListMock;

  @EndpointInject(uri = "mock:sendEmail")
  private MockEndpoint sendEmailMock;

  @Before
  public void setUp() {
  }

  @After
  public void teardown() throws Exception {
    MockEndpoint.resetMocks(camelContext);
  }

  private void setupPropertyImportCamelContext() throws Exception {
    camelContext.getRouteDefinition("push-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start1");
            interceptSendToEndpoint("direct:addArtistCRIds").skipSendToOriginalEndpoint()
                .to("mock:addArtistCRIds");
            interceptSendToEndpoint("rest:POST:properties*").skipSendToOriginalEndpoint()
                .to("mock:properties");
            interceptSendToEndpoint("rest:PUT:properties*").skipSendToOriginalEndpoint()
                .to("mock:updateProperties");
            interceptSendToEndpoint("http4:uploadImage").skipSendToOriginalEndpoint()
                .to("mock:uploadImage");
            interceptSendToEndpoint("http4:deleteProperty").skipSendToOriginalEndpoint()
                .to("mock:errorDeleteProperty");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
            interceptSendToEndpoint("direct:createSaveListAndSendStatusEmail")
                .skipSendToOriginalEndpoint()
                .to("mock:createSaveListAndSendStatusEmail");
          }
        });
    camelContext.start();
  }

  private void setupWebsiteImageImportCamelContext() throws Exception {
    camelContext.getRouteDefinition("push-csv-website-Images-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start2");
            interceptSendToEndpoint("http4:websiteImages").skipSendToOriginalEndpoint()
                .to("mock:website");
            interceptSendToEndpoint("file:*").skipSendToOriginalEndpoint()
                .to("mock:websiteOutputFile");
          }
        });
    camelContext.start();
  }

  private void setupAddArtistCRIdsCamelContext() throws Exception {
    camelContext.getRouteDefinition("property-add-artist-cr-ids").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            interceptSendToEndpoint("rest:GET:artists*").skipSendToOriginalEndpoint()
                .to("mock:artists");
            weaveAddLast().to("mock:result");
          }
        });
    camelContext.start();
  }

  private void setupCreateSaveListAndSendStatusEmailCamelContext() throws Exception {
    camelContext.getRouteDefinition("create-save-list-and-send-status-email")
        .adviceWith(camelContext,
            new AdviceWithRouteBuilder() {
              @Override
              public void configure() throws Exception {
                interceptSendToEndpoint("rest:POST:users*").skipSendToOriginalEndpoint()
                    .to("mock:postSaveList");
                interceptSendToEndpoint("smtp*").skipSendToOriginalEndpoint().to("mock:sendEmail");
              }
            });
    camelContext.start();
  }

  /**
   * Tests the route that pushes property data to the property service. Verifies that all records
   * from the input CSV file are processed.
   */
  @Test
  @DirtiesContext
  public void testPostToServiceRoute() throws Exception {
    setupPropertyImportCamelContext();

    File csvFile = new File("src/test/resources/property-import-test.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    addArtistCRIdsMock.expectedMessageCount(0);

    propertiesMock.expectedMessageCount(35);
    propertiesMock.expectedPropertyReceived("imageName", "DJI_0164.JPG");
    propertiesMock.message(1).body().convertToString().contains("Unspecified");
    propertiesMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    updatePropertiesMock.expectedMessageCount(3);
    updatePropertiesMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    uploadImageMock.expectedMessageCount(38);

    errorDeletePropertyMock.expectedMessageCount(0);

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "property-import-test.csv");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 38);

    createSaveListAndSendStatusEmailMock.expectedMessageCount(1);

    producerTemplate.sendBodyAndHeader("direct:start1", csvContent, Exchange.FILE_NAME,
        "property-import-test.csv");

    addArtistCRIdsMock.assertIsSatisfied();
    propertiesMock.assertIsSatisfied();
    updatePropertiesMock.assertIsSatisfied();
    uploadImageMock.assertIsSatisfied();
    errorDeletePropertyMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
    createSaveListAndSendStatusEmailMock.assertIsSatisfied();
  }

  /**
   * Tests the route that pushes property data to the properties service. Verifies that all records
   * from the input CSV file are handled in case of PropertyImportException.
   */
  @Test
  @DirtiesContext
  public void testPostToPropertyImportRouteForPropertyImportException() throws Exception {
    setupPropertyImportCamelContext();

    File csvFile = new File("src/test/resources/property-import-test-import-exception.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    addArtistCRIdsMock.expectedMessageCount(0);

    propertiesMock.expectedMessageCount(35);
    propertiesMock.whenAnyExchangeReceived(e -> {
      throw new PropertyImportException("Couldn't process the request");
    });

    updatePropertiesMock.expectedMessageCount(3);
    updatePropertiesMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    uploadImageMock.expectedMessageCount(3);

    errorDeletePropertyMock.expectedMessageCount(0);

    createSaveListAndSendStatusEmailMock.expectedMessageCount(1);

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock
        .expectedHeaderReceived(Exchange.FILE_NAME, "property-import-test-import-exception.csv");
    sendImportReportMock.allMessages().body().convertToString().contains("EOS_Object_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 38);

    producerTemplate.sendBodyAndHeader("direct:start1", csvContent, Exchange.FILE_NAME,
        "property-import-test-import-exception.csv");

    addArtistCRIdsMock.assertIsSatisfied();
    propertiesMock.assertIsSatisfied();
    updatePropertiesMock.assertIsSatisfied();
    uploadImageMock.assertIsSatisfied();
    errorDeletePropertyMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
    createSaveListAndSendStatusEmailMock.assertIsSatisfied();
  }

  /**
   * Tests the route that downloads images from website for the sales provided in CSV file & upload
   * them to S3 bucket. Verifies that all records from the input CSV file are handled.
   */
  @Test
  @DirtiesContext
  public void testPostToPropertyImportRouteForHttpException() throws Exception {
    setupPropertyImportCamelContext();

    JSONObject responseBody = new JSONObject();
    responseBody.put("statusCode", "500");
    responseBody.put("details", "couldn't process the request due to Interal server error");

    File csvFile = new File("src/test/resources/property-import-test-http-exception.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    addArtistCRIdsMock.expectedMessageCount(0);

    propertiesMock.expectedMessageCount(35);
    propertiesMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    updatePropertiesMock.expectedMessageCount(3);
    updatePropertiesMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    uploadImageMock.expectedMessageCount(38);
    uploadImageMock.whenAnyExchangeReceived(e -> {
      throw new HttpOperationFailedException("mock:uploadImage", 500,
          "Internal Server error", null, null, responseBody.toString());
    });

    errorDeletePropertyMock.expectedMessageCount(35);

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock
        .expectedHeaderReceived(Exchange.FILE_NAME, "property-import-test-http-exception.csv");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 38);

    createSaveListAndSendStatusEmailMock.expectedMessageCount(1);

    producerTemplate.sendBodyAndHeader("direct:start1", csvContent, Exchange.FILE_NAME,
        "property-import-test-http-exception.csv");

    addArtistCRIdsMock.assertIsSatisfied();
    propertiesMock.assertIsSatisfied();
    updatePropertiesMock.assertIsSatisfied();
    uploadImageMock.assertIsSatisfied();
    errorDeletePropertyMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
    createSaveListAndSendStatusEmailMock.assertIsSatisfied();
  }

  @Test
  @DirtiesContext
  public void testPostToPropertyImportRouteForHttpHostConnectException() throws Exception {
    setupPropertyImportCamelContext();

    File csvFile = new File("src/test/resources/property-import-test-http-exception.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    addArtistCRIdsMock.expectedMessageCount(0);

    propertiesMock.expectedMessageCount(35);
    propertiesMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpHostConnectException(null, null, (InetAddress) null);
    });

    updatePropertiesMock.expectedMessageCount(3);
    updatePropertiesMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpHostConnectException(null, null, (InetAddress) null);
    });

    uploadImageMock.expectedMessageCount(0);
    errorDeletePropertyMock.expectedMessageCount(0);
    createSaveListAndSendStatusEmailMock.expectedMessageCount(1);

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock
        .expectedHeaderReceived(Exchange.FILE_NAME, "property-import-test-http-exception.csv");
    sendImportReportMock.allMessages().body().convertToString().contains("EOS_Object_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 38);

    producerTemplate.sendBodyAndHeader("direct:start1", csvContent, Exchange.FILE_NAME,
        "property-import-test-http-exception.csv");

    addArtistCRIdsMock.assertIsSatisfied();
    propertiesMock.assertIsSatisfied();
    updatePropertiesMock.assertIsSatisfied();
    uploadImageMock.assertIsSatisfied();
    errorDeletePropertyMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
    createSaveListAndSendStatusEmailMock.assertIsSatisfied();
  }

  @Test
  @DirtiesContext
  public void testPostToPropertyImportRouteForUnknownException() throws Exception {
    setupPropertyImportCamelContext();

    File csvFile = new File("src/test/resources/property-import-test-http-exception.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    addArtistCRIdsMock.expectedMessageCount(0);

    propertiesMock.expectedMessageCount(35);
    propertiesMock.whenAnyExchangeReceived(exchange -> {
      throw new Exception();
    });

    updatePropertiesMock.expectedMessageCount(3);
    updatePropertiesMock.whenAnyExchangeReceived(exchange -> {
      throw new Exception();
    });

    uploadImageMock.expectedMessageCount(0);
    errorDeletePropertyMock.expectedMessageCount(0);
    createSaveListAndSendStatusEmailMock.expectedMessageCount(1);

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock
        .expectedHeaderReceived(Exchange.FILE_NAME, "property-import-test-http-exception.csv");
    sendImportReportMock.allMessages().body().convertToString().contains("EOS_Object_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 38);

    producerTemplate.sendBodyAndHeader("direct:start1", csvContent, Exchange.FILE_NAME,
        "property-import-test-http-exception.csv");

    addArtistCRIdsMock.assertIsSatisfied();
    propertiesMock.assertIsSatisfied();
    updatePropertiesMock.assertIsSatisfied();
    uploadImageMock.assertIsSatisfied();
    errorDeletePropertyMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
    createSaveListAndSendStatusEmailMock.assertIsSatisfied();
  }

  @Test
  @DirtiesContext
  public void testPostToServiceRouteWithCRsButErrorsOutWhileInAddArtistCRIdsSplit()
      throws Exception {
    setupPropertyImportCamelContext();

    File csvFile = new File("src/test/resources/property-import-test-with-crs.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    addArtistCRIdsMock.expectedMessageCount(1);
    addArtistCRIdsMock.whenAnyExchangeReceived(exchange -> {
      exchange.setProperty(Exchange.SPLIT_COMPLETE, true);
      exchange.setProperty(Exchange.SPLIT_SIZE, "123");
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 1);

    producerTemplate.sendBody("direct:start1", csvContent);

    addArtistCRIdsMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
  }

  @Test
  @DirtiesContext
  public void testPostToServiceRouteUnchangedHeaderAfterCsvSplit() throws Exception {
    setupPropertyImportCamelContext();

    File csvFile = new File("src/test/resources/property-import-test.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    propertiesMock.expectedMessageCount(35);
    propertiesMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
      exchange.getIn().setHeader(HttpHeaders.TRANSFER_ENCODING, "chunked");
    });

    updatePropertiesMock.expectedMessageCount(3);
    updatePropertiesMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
      exchange.getIn().setHeader(HttpHeaders.TRANSFER_ENCODING, "chunked");
    });

    createSaveListAndSendStatusEmailMock.expectedMessageCount(1);
    createSaveListAndSendStatusEmailMock.message(0).header(HttpHeaders.TRANSFER_ENCODING).isNull();

    producerTemplate.sendBodyAndHeader("direct:start1", csvContent, Exchange.FILE_NAME,
        "property-import-test.csv");

    propertiesMock.assertIsSatisfied();
    updatePropertiesMock.assertIsSatisfied();
    createSaveListAndSendStatusEmailMock.assertIsSatisfied();
  }

  @Test
  @DirtiesContext
  public void testPostToServiceRouteEmptyCsvFile() throws Exception {
    setupPropertyImportCamelContext();

    addArtistCRIdsMock.expectedMessageCount(0);
    propertiesMock.expectedMessageCount(0);
    updatePropertiesMock.expectedMessageCount(0);
    uploadImageMock.expectedMessageCount(0);
    errorDeletePropertyMock.expectedMessageCount(0);

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "empty-file.csv");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 0);

    createSaveListAndSendStatusEmailMock.expectedMessageCount(1);

    producerTemplate.sendBodyAndHeader("direct:start1", new byte[0], Exchange.FILE_NAME,
        "empty-file.csv");

    addArtistCRIdsMock.assertIsSatisfied();
    propertiesMock.assertIsSatisfied();
    updatePropertiesMock.assertIsSatisfied();
    uploadImageMock.assertIsSatisfied();
    errorDeletePropertyMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
    createSaveListAndSendStatusEmailMock.assertIsSatisfied();
  }

  /**
   * Tests the route that download images from website for the sales provided in CSV file & upload
   * them to S3 bucket. Verifies that all records from the input CSV file are handled in case of
   * WebsiteImageImportException.
   */
  @Test
  @DirtiesContext
  public void testImportWebsiteImagesRoute() throws Exception {
    setupWebsiteImageImportCamelContext();

    File csvFile = new File("src/test/resources/websiteImages-import-test.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    websiteMock.expectedMessageCount(3);
    websiteMock.expectedPropertyReceived("WebsiteImageFileName",
        "website-images-CAT_Website_Images_output_1234.csv");
    websiteMock.allMessages().body().convertToString().contains("N09343");

    websiteOutputFileMock.expectedMessageCount(1);
    websiteOutputFileMock.allMessages().body().convertToString().contains("Details");

    producerTemplate.sendBodyAndHeader("direct:start2", csvContent, Exchange.FILE_NAME,
        "website-images-CAT_Website_Images-1234.csv");

    websiteMock.assertIsSatisfied();
    websiteOutputFileMock.assertIsSatisfied();
  }


  /**
   * Tests the route that download images from website for the sales provided in CSV file & upload
   * them to S3 bucket. Verifies that all records from the input CSV file are handled in case of
   * WebsiteImageImportException.
   */
  @Test
  @DirtiesContext
  public void testImportWebsiteImagesRouteWithWebsiteImportException() throws Exception {
    setupWebsiteImageImportCamelContext();

    File csvFile = new File("src/test/resources/websiteImages-import-test.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    websiteMock.expectedMessageCount(3);
    websiteMock.whenAnyExchangeReceived(exchange -> {
      throw new WebsiteImageImportException("Couldn't process the request");
    });

    websiteOutputFileMock.expectedMessageCount(1);
    websiteOutputFileMock.allMessages().body().convertToString().contains("Details");

    producerTemplate.sendBodyAndHeader("direct:start2", csvContent, Exchange.FILE_NAME,
        "website-images-CAT_Website_Images-1234.csv");

    websiteMock.assertIsSatisfied();
    websiteOutputFileMock.assertIsSatisfied();
  }

  /**
   * Tests the route that download images from web-site for the sales provided in CSV file & upload
   * them to S3 bucket. Verifies that all records from the input CSV file are handled in case of
   * HttpOperationFailedException.
   */
  @Test
  @DirtiesContext
  public void testImportWebsiteImagesRouteWithHttpOperationException() throws Exception {
    setupWebsiteImageImportCamelContext();

    JSONObject responseBody = new JSONObject();
    responseBody.put("message", "internal server error");

    File csvFile = new File("src/test/resources/websiteImages-import-test.csv");
    byte[] csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    websiteMock.expectedMessageCount(3);
    websiteMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException("http4:websiteImages", 500,
          "Internal Server error", null, null, responseBody.toString());
    });

    websiteOutputFileMock.expectedMessageCount(1);
    websiteOutputFileMock.allMessages().body().convertToString().contains("Details");

    producerTemplate.sendBodyAndHeader("direct:start2", csvContent, Exchange.FILE_NAME,
        "website-images-CAT_Website_Images-1234.csv");

    websiteMock.assertIsSatisfied();
    websiteOutputFileMock.assertIsSatisfied();
  }

  /**
   * Tests the route that adds artist catalogue raisonee ids to a property's catalogue raisonees.
   */
  @Test
  @DirtiesContext
  public void testAddArtistCRIds() throws Exception {
    setupAddArtistCRIdsCamelContext();

    CatalogueRaisonee catalogueRaisoneeFirst = new CatalogueRaisonee();
    catalogueRaisoneeFirst.setNumber("1A");
    catalogueRaisoneeFirst.setVolume("345");

    CatalogueRaisonee catalogueRaisoneeSecond = new CatalogueRaisonee();
    catalogueRaisoneeSecond.setNumber("2B");
    catalogueRaisoneeSecond.setVolume("987");

    PropertyCreateRequestDTO propertyCreateRequestDTO = new PropertyCreateRequestDTO();
    propertyCreateRequestDTO.setArtistIds(Collections.singletonList("AA-1234"));
    propertyCreateRequestDTO.setPropertyCatalogueRaisonees(
        Arrays.asList(catalogueRaisoneeFirst, catalogueRaisoneeSecond));

    artistsMock.expectedMessageCount(1);
    artistsMock.message(0).body().isNull();
    artistsMock.expectedHeaderReceived(Exchange.HTTP_PATH, "AA-1234");
    artistsMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody(
          "{\"artistCatalogueRaisonees\": [{\"id\": 5678, \"sortId\": 2}, {\"id\": 1234, \"sortId\": 1}]}");
    });

    resultMock.expectedMessageCount(1);

    producerTemplate.sendBody("direct:addArtistCRIds", propertyCreateRequestDTO);

    artistsMock.assertIsSatisfied();
    resultMock.assertIsSatisfied();

    PropertyCreateRequestDTO returnDto =
        resultMock.getExchanges().get(0).getIn().getBody(PropertyCreateRequestDTO.class);
    CatalogueRaisonee returnCatalogueRaisoneeFirst =
        returnDto.getPropertyCatalogueRaisonees().get(0);
    CatalogueRaisonee returnCatalogueRaisoneeSecond =
        returnDto.getPropertyCatalogueRaisonees().get(1);

    assertEquals(new Long(1234), returnCatalogueRaisoneeFirst.getArtistCatalogueRaisoneeId());
    assertEquals("1A", returnCatalogueRaisoneeFirst.getNumber());
    assertEquals("345", returnCatalogueRaisoneeFirst.getVolume());
    assertEquals(new Long(5678), returnCatalogueRaisoneeSecond.getArtistCatalogueRaisoneeId());
    assertEquals("2B", returnCatalogueRaisoneeSecond.getNumber());
    assertEquals("987", returnCatalogueRaisoneeSecond.getVolume());
  }

  @Test
  @DirtiesContext
  public void testCreateSaveListAndSendStatusEmail() throws Exception {
    setupCreateSaveListAndSendStatusEmailCamelContext();

    postSaveListMock.expectedMessageCount(1);
    postSaveListMock.message(0).jsonpath("$.propertyIds[0]").isEqualTo("OO-1234");
    postSaveListMock.message(0).header(HttpHeaders.AUTHORIZATION).startsWith("Basic ");
    postSaveListMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"SL-9999\", \"propertyIds\": [\"OO-1234\"]}");
    });

    sendEmailMock.expectedMessageCount(1);
    sendEmailMock.allMessages().header(Exchange.CONTENT_TYPE).isEqualToIgnoreCase("text/html");
    sendEmailMock.message(0).body().convertToString()
        .contains("Saved list testFileName has successfully been saved.");

    Message message = new DefaultMessage(camelContext);
    message.setHeader(Exchange.FILE_NAME, "testFileName.csv");

    Exchange exchange = new DefaultExchange(camelContext);
    exchange.setProperty("saveListPropertyIds", Collections.singleton("OO-1234"));
    exchange.setIn(message);
    producerTemplate.send("direct:createSaveListAndSendStatusEmail", exchange);

    postSaveListMock.assertIsSatisfied();
    sendEmailMock.assertIsSatisfied();
  }

  @Test
  @DirtiesContext
  public void testCreateSaveListAndSendStatusEmailHttpOperationFailure() throws Exception {
    setupCreateSaveListAndSendStatusEmailCamelContext();

    postSaveListMock.expectedMessageCount(1);
    postSaveListMock.message(0).jsonpath("$.propertyIds[0]").isEqualTo("OO-1234");
    postSaveListMock.message(0).header(HttpHeaders.AUTHORIZATION).startsWith("Basic ");
    postSaveListMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, "something went wrong");
    });

    sendEmailMock.expectedMessageCount(1);
    sendEmailMock.allMessages().header(Exchange.CONTENT_TYPE).isEqualToIgnoreCase("text/html");
    sendEmailMock.message(0).body().convertToString().contains("something went wrong");

    Message message = new DefaultMessage(camelContext);
    message.setHeader(Exchange.FILE_NAME, "testFileName.csv");

    Exchange exchange = new DefaultExchange(camelContext);
    exchange.setProperty("saveListPropertyIds", Collections.singleton("OO-1234"));
    exchange.setIn(message);
    producerTemplate.send("direct:createSaveListAndSendStatusEmail", exchange);

    postSaveListMock.assertIsSatisfied();
    sendEmailMock.assertIsSatisfied();
  }

  @Test
  @DirtiesContext
  public void testCreateSaveListAndSendStatusEmailUnexpectedRestResponse() throws Exception {
    setupCreateSaveListAndSendStatusEmailCamelContext();

    postSaveListMock.expectedMessageCount(1);
    postSaveListMock.message(0).jsonpath("$.propertyIds[0]").isEqualTo("OO-1234");
    postSaveListMock.message(0).header(HttpHeaders.AUTHORIZATION).startsWith("Basic ");
    postSaveListMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"bogus\": \"nonsense\"}");
    });

    sendEmailMock.expectedMessageCount(1);
    sendEmailMock.allMessages().header(Exchange.CONTENT_TYPE).isEqualToIgnoreCase("text/html");
    sendEmailMock.message(0).body().convertToString()
        .contains("Unexpected rest response (no id returned) when trying to create list");

    Message message = new DefaultMessage(camelContext);
    message.setHeader(Exchange.FILE_NAME, "testFileName.csv");

    Exchange exchange = new DefaultExchange(camelContext);
    exchange.setProperty("saveListPropertyIds", Collections.singleton("OO-1234"));
    exchange.setIn(message);
    producerTemplate.send("direct:createSaveListAndSendStatusEmail", exchange);

    postSaveListMock.assertIsSatisfied();
    sendEmailMock.assertIsSatisfied();
  }

  @Test
  @DirtiesContext
  public void testCreateSaveListAndSendStatusEmailNoPropertyIds() throws Exception {
    setupCreateSaveListAndSendStatusEmailCamelContext();

    postSaveListMock.expectedMessageCount(0);

    sendEmailMock.expectedMessageCount(1);
    sendEmailMock.allMessages().header(Exchange.CONTENT_TYPE).isEqualToIgnoreCase("text/html");
    sendEmailMock.message(0).body().convertToString()
        .contains("Did not create save list because property count is 0");

    Message message = new DefaultMessage(camelContext);
    message.setHeader(Exchange.FILE_NAME, "testFileName.csv");

    Exchange exchange = new DefaultExchange(camelContext);
    exchange.setProperty("saveListPropertyIds", Collections.emptyList());
    exchange.setIn(message);
    producerTemplate.send("direct:createSaveListAndSendStatusEmail", exchange);

    postSaveListMock.assertIsSatisfied();
    sendEmailMock.assertIsSatisfied();
  }

  @Test
  @DirtiesContext
  public void testCreateSaveListAndSendStatusEmailUnknownExceptionFailure() throws Exception {
    setupCreateSaveListAndSendStatusEmailCamelContext();

    postSaveListMock.expectedMessageCount(1);
    postSaveListMock.message(0).jsonpath("$.propertyIds[0]").isEqualTo("OO-1234");
    postSaveListMock.message(0).header(HttpHeaders.AUTHORIZATION).startsWith("Basic ");
    postSaveListMock.whenAnyExchangeReceived(exchange -> {
      throw new Exception("something went terribly wrong");
    });

    sendEmailMock.expectedMessageCount(1);
    sendEmailMock.allMessages().header(Exchange.CONTENT_TYPE).isEqualToIgnoreCase("text/html");
    sendEmailMock.message(0).body().convertToString().contains("something went terribly wrong");

    Message message = new DefaultMessage(camelContext);
    message.setHeader(Exchange.FILE_NAME, "testFileName.csv");

    Exchange exchange = new DefaultExchange(camelContext);
    exchange.setProperty("saveListPropertyIds", Collections.singleton("OO-1234"));
    exchange.setIn(message);
    producerTemplate.send("direct:createSaveListAndSendStatusEmail", exchange);

    postSaveListMock.assertIsSatisfied();
    sendEmailMock.assertIsSatisfied();
  }

}
