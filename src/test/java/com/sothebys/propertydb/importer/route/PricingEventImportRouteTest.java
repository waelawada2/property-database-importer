package com.sothebys.propertydb.importer.route;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import com.sothebys.propertydb.importer.PropertyImportApplication;

@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class PricingEventImportRouteTest extends CamelTestSupport {
  
  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:postPricingEvent")
  private MockEndpoint postPricingEventMock;

  @EndpointInject(uri = "mock:putPricingEvent")
  private MockEndpoint putPricingEventMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  private byte[] PricingEventCsvContent;

  @Before
  public void setUp() throws Exception {
    File csvFile = new File("src/test/resources/pricing-event-import-test.csv");
    PricingEventCsvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));
  }

  @After
  public void teardown() {
    postPricingEventMock.reset();
    putPricingEventMock.reset();
    sendImportReportMock.reset();
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testPostPutPricingEvent() throws Exception {
    camelContext.getRouteDefinition("push-pricing-event-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:events*").skipSendToOriginalEndpoint()
                .to("mock:postPricingEvent");
            interceptSendToEndpoint("rest:PUT:events*").skipSendToOriginalEndpoint()
                .to("mock:putPricingEvent");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postPricingEventMock.expectedMessageCount(1);
    postPricingEventMock.message(0).jsonpath("$.valuationId").isEqualTo("123456");
    postPricingEventMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    putPricingEventMock.expectedMessageCount(1);
    putPricingEventMock.message(0).jsonpath("$.valuationId").isEqualTo("789012");
    putPricingEventMock.expectedHeaderReceived(Exchange.HTTP_PATH, "EV-02641214L9Z");
    putPricingEventMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Val_System_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", PricingEventCsvContent, Exchange.FILE_NAME, "test.csv");

    postPricingEventMock.assertIsSatisfied();
    putPricingEventMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 0);
    assertEquals(successfulRecords.size(), 2);
  }

  @Test
  public void testPostPutPricingEventFailure() throws Exception {
    camelContext.getRouteDefinition("push-pricing-event-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:events*").skipSendToOriginalEndpoint()
                .to("mock:postPricingEvent");
            interceptSendToEndpoint("rest:PUT:events*").skipSendToOriginalEndpoint()
                .to("mock:putPricingEvent");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postPricingEventMock.expectedMessageCount(1);
    postPricingEventMock.message(0).jsonpath("$.valuationId").isEqualTo("123456");
    postPricingEventMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    putPricingEventMock.expectedMessageCount(1);
    putPricingEventMock.message(0).jsonpath("$.valuationId").isEqualTo("789012");
    putPricingEventMock.expectedHeaderReceived(Exchange.HTTP_PATH, "EV-02641214L9Z");
    putPricingEventMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Val_System_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", PricingEventCsvContent, Exchange.FILE_NAME, "test.csv");

    postPricingEventMock.assertIsSatisfied();
    putPricingEventMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 2);
    assertEquals(successfulRecords.size(), 0);

  }

}
