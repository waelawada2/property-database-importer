package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.PropertyImportApplication;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

/**
 * @author Minhtri Tran
 */
@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class PublicationEventImportRouteTest extends CamelTestSupport {

  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:postPublicationEvent")
  private MockEndpoint postPublicationEventMock;

  @EndpointInject(uri = "mock:putPublicationEvent")
  private MockEndpoint putPublicationEventMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;
  
  @EndpointInject(uri = "mock:post-image-to-event-service")
  private MockEndpoint imageToEventServiceMock;

  private byte[] publicationCsvContent;

  @Before
  public void setUp() throws Exception {
    File csvFile = new File("src/test/resources/publication-event-import-test.csv");
    publicationCsvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));
  }

  @After
  public void teardown() {
    postPublicationEventMock.reset();
    putPublicationEventMock.reset();
    sendImportReportMock.reset();
    imageToEventServiceMock.reset();
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testPostPutPublicationEvent() throws Exception {
    camelContext.getRouteDefinition("push-publication-event-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("direct:post-image-to-event-service").skipSendToOriginalEndpoint()
                .to("mock:post-image-to-event-service");
            interceptSendToEndpoint("rest:POST:events*").skipSendToOriginalEndpoint()
                .to("mock:postPublicationEvent");
            interceptSendToEndpoint("rest:PUT:events*").skipSendToOriginalEndpoint()
                .to("mock:putPublicationEvent");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();
    
    imageToEventServiceMock.expectedMessageCount(2);
    imageToEventServiceMock.message(0).exchangeProperty("imageName").isEqualTo("VAL_standard/44KRT.jpg");
    imageToEventServiceMock.message(0).exchangeProperty("updateImage").isEqualTo(true);

    postPublicationEventMock.expectedMessageCount(1);
    postPublicationEventMock.message(0).jsonpath("$.title").isEqualTo("Test Title");
    postPublicationEventMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    putPublicationEventMock.expectedMessageCount(1);
    putPublicationEventMock.message(0).jsonpath("$.title").isEqualTo("Test Title");
    putPublicationEventMock.expectedHeaderReceived(Exchange.HTTP_PATH, "EP-123456");
    putPublicationEventMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Publication_Type");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", publicationCsvContent, Exchange.FILE_NAME, "test.csv");

    imageToEventServiceMock.assertIsSatisfied();
    postPublicationEventMock.assertIsSatisfied();
    putPublicationEventMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 0);
    assertEquals(successfulRecords.size(), 2);
  }

  @Test
  public void testPostPutPublicationEventFailure() throws Exception {
    camelContext.getRouteDefinition("push-publication-event-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("direct:post-image-to-event-service").skipSendToOriginalEndpoint()
                .to("mock:post-image-to-event-service");
            interceptSendToEndpoint("rest:POST:events*").skipSendToOriginalEndpoint()
                .to("mock:postPublicationEvent");
            interceptSendToEndpoint("rest:PUT:events*").skipSendToOriginalEndpoint()
                .to("mock:putPublicationEvent");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();
    
    imageToEventServiceMock.expectedMessageCount(2);
    imageToEventServiceMock.message(0).exchangeProperty("imageName").isEqualTo("VAL_standard/44KRT.jpg");
    imageToEventServiceMock.message(0).exchangeProperty("updateImage").isEqualTo(true);
    
    postPublicationEventMock.expectedMessageCount(1);
    postPublicationEventMock.message(0).jsonpath("$.title").isEqualTo("Test Title");
    postPublicationEventMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    putPublicationEventMock.expectedMessageCount(1);
    putPublicationEventMock.message(0).jsonpath("$.title").isEqualTo("Test Title");
    putPublicationEventMock.expectedHeaderReceived(Exchange.HTTP_PATH, "EP-123456");
    putPublicationEventMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Publication_Type");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", publicationCsvContent, Exchange.FILE_NAME, "test.csv");

    imageToEventServiceMock.assertIsSatisfied();
    postPublicationEventMock.assertIsSatisfied();
    putPublicationEventMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 2);
    assertEquals(successfulRecords.size(), 0);

  }

}