package com.sothebys.propertydb.importer.route;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import com.sothebys.propertydb.importer.PropertyImportApplication;

@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class PricingItemImportRouteTest extends CamelTestSupport {
  
  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:postPricingItem")
  private MockEndpoint postPricingItemMock;

  @EndpointInject(uri = "mock:putPricingItem")
  private MockEndpoint putPricingItemMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  private byte[] PricingItemCsvContent;

  @Before
  public void setUp() throws Exception {
    File csvFile = new File("src/test/resources/pricing-item-import-test.csv");
    PricingItemCsvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));
  }

  @After
  public void teardown() {
    postPricingItemMock.reset();
    putPricingItemMock.reset();
    sendImportReportMock.reset();
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testPostPutPricingEvent() throws Exception {
    camelContext.getRouteDefinition("push-pricing-item-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:items*").skipSendToOriginalEndpoint()
                .to("mock:postPricingItem");
            interceptSendToEndpoint("rest:PUT:items*").skipSendToOriginalEndpoint()
                .to("mock:putPricingItem");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postPricingItemMock.expectedMessageCount(1);
    postPricingItemMock.message(0).jsonpath("$.exhibition").isEqualTo("post_exhibition");
    postPricingItemMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    putPricingItemMock.expectedMessageCount(1);
    putPricingItemMock.message(0).jsonpath("$.exhibition").isEqualTo("put_exhibition");
    putPricingItemMock.expectedHeaderReceived(Exchange.HTTP_PATH, "IV-XL482WLKMXM");
    putPricingItemMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Item_Exhibition");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", PricingItemCsvContent, Exchange.FILE_NAME, "test.csv");

    postPricingItemMock.assertIsSatisfied();
    putPricingItemMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 0);
    assertEquals(successfulRecords.size(), 2);
  }

  @Test
  public void testPostPutPricingEventFailure() throws Exception {
    camelContext.getRouteDefinition("push-pricing-item-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:items*").skipSendToOriginalEndpoint()
                .to("mock:postPricingItem");
            interceptSendToEndpoint("rest:PUT:items*").skipSendToOriginalEndpoint()
                .to("mock:putPricingItem");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postPricingItemMock.expectedMessageCount(1);
    postPricingItemMock.message(0).jsonpath("$.exhibition").isEqualTo("post_exhibition");
    postPricingItemMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    putPricingItemMock.expectedMessageCount(1);
    putPricingItemMock.message(0).jsonpath("$.exhibition").isEqualTo("put_exhibition");
    putPricingItemMock.expectedHeaderReceived(Exchange.HTTP_PATH, "IV-XL482WLKMXM");
    putPricingItemMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Item_Exhibition");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", PricingItemCsvContent, Exchange.FILE_NAME, "test.csv");

    postPricingItemMock.assertIsSatisfied();
    putPricingItemMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 2);
    assertEquals(successfulRecords.size(), 0);

  }

}
