package com.sothebys.propertydb.importer.route;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import com.sothebys.propertydb.importer.PropertyImportApplication;

@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class TransactionItemImportRouteTest extends CamelTestSupport {
  
  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:postTransactionItem")
  private MockEndpoint postTransactionItemMock;

  @EndpointInject(uri = "mock:putTransactionItem")
  private MockEndpoint putTransactionItemMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  private byte[] transactionItemCsvContent;

  @Before
  public void setUp() throws Exception {
    File csvFile = new File("src/test/resources/transaction-item-import-test.csv");
    transactionItemCsvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));
  }

  @After
  public void teardown() {
    postTransactionItemMock.reset();
    putTransactionItemMock.reset();
    sendImportReportMock.reset();
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testPostPutTransactionEvent() throws Exception {
    camelContext.getRouteDefinition("push-transaction-item-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:items*").skipSendToOriginalEndpoint()
                .to("mock:postTransactionItem");
            interceptSendToEndpoint("rest:PUT:items*").skipSendToOriginalEndpoint()
                .to("mock:putTransactionItem");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postTransactionItemMock.expectedMessageCount(1);
    postTransactionItemMock.message(0).jsonpath("$.exhibition").isEqualTo("post_exhibition");
    postTransactionItemMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    putTransactionItemMock.expectedMessageCount(1);
    putTransactionItemMock.message(0).jsonpath("$.exhibition").isEqualTo("put_exhibition");
    putTransactionItemMock.expectedHeaderReceived(Exchange.HTTP_PATH, "IT-XL484LKZJNK");
    putTransactionItemMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Exhibition");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", transactionItemCsvContent, Exchange.FILE_NAME, "test.csv");

    postTransactionItemMock.assertIsSatisfied();
    putTransactionItemMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 0);
    assertEquals(successfulRecords.size(), 2);
  }

  @Test
  public void testPostPutTransactionEventFailure() throws Exception {
    camelContext.getRouteDefinition("push-transaction-item-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:items*").skipSendToOriginalEndpoint()
                .to("mock:postTransactionItem");
            interceptSendToEndpoint("rest:PUT:items*").skipSendToOriginalEndpoint()
                .to("mock:putTransactionItem");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postTransactionItemMock.expectedMessageCount(1);
    postTransactionItemMock.message(0).jsonpath("$.exhibition").isEqualTo("post_exhibition");
    postTransactionItemMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    putTransactionItemMock.expectedMessageCount(1);
    putTransactionItemMock.message(0).jsonpath("$.exhibition").isEqualTo("put_exhibition");
    putTransactionItemMock.expectedHeaderReceived(Exchange.HTTP_PATH, "IT-XL484LKZJNK");
    putTransactionItemMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Exhibition");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", transactionItemCsvContent, Exchange.FILE_NAME, "test.csv");

    postTransactionItemMock.assertIsSatisfied();
    putTransactionItemMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 2);
    assertEquals(successfulRecords.size(), 0);

  }

}
