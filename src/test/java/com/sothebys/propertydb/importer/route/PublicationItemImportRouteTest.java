package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.PropertyImportApplication;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

/**
 * @author Minhtri Tran
 */
@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class PublicationItemImportRouteTest extends CamelTestSupport {

  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:postPublicationItem")
  private MockEndpoint postPublicationItemMock;

  @EndpointInject(uri = "mock:putPublicationItem")
  private MockEndpoint putPublicationItemMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  private byte[] publicationCsvContent;

  @Before
  public void setUp() throws Exception {
    File csvFile = new File("src/test/resources/publication-item-import-test.csv");
    publicationCsvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));
  }

  @After
  public void teardown() {
    postPublicationItemMock.reset();
    putPublicationItemMock.reset();
    sendImportReportMock.reset();
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testPostPutPublicationItem() throws Exception {
    camelContext.getRouteDefinition("push-publication-item-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:items*").skipSendToOriginalEndpoint()
                .to("mock:postPublicationItem");
            interceptSendToEndpoint("rest:PUT:items*").skipSendToOriginalEndpoint()
                .to("mock:putPublicationItem");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postPublicationItemMock.expectedMessageCount(1);
    postPublicationItemMock.message(0).jsonpath("$.provenance").isEqualTo("The artist; Alexina Duchamp, France");
    postPublicationItemMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    putPublicationItemMock.expectedMessageCount(1);
    putPublicationItemMock.message(0).jsonpath("$.provenance").isEqualTo("The artist; Alexina Duchamp, France");
    putPublicationItemMock.expectedHeaderReceived(Exchange.HTTP_PATH, "IP-ZX89597KVXV");
    putPublicationItemMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Publication_Item_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", publicationCsvContent, Exchange.FILE_NAME, "test.csv");

    postPublicationItemMock.assertIsSatisfied();
    putPublicationItemMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 0);
    assertEquals(successfulRecords.size(), 2);
  }

  @Test
  public void testPostPutPublicationItemFailure() throws Exception {
    camelContext.getRouteDefinition("push-publication-item-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:items*").skipSendToOriginalEndpoint()
                .to("mock:postPublicationItem");
            interceptSendToEndpoint("rest:PUT:items*").skipSendToOriginalEndpoint()
                .to("mock:putPublicationItem");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postPublicationItemMock.expectedMessageCount(1);
    postPublicationItemMock.message(0).jsonpath("$.provenance").isEqualTo("The artist; Alexina Duchamp, France");
    postPublicationItemMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    putPublicationItemMock.expectedMessageCount(1);
    putPublicationItemMock.message(0).jsonpath("$.provenance").isEqualTo("The artist; Alexina Duchamp, France");
    putPublicationItemMock.expectedHeaderReceived(Exchange.HTTP_PATH, "IP-ZX89597KVXV");
    putPublicationItemMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Publication_Item_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", publicationCsvContent, Exchange.FILE_NAME, "test.csv");

    postPublicationItemMock.assertIsSatisfied();
    putPublicationItemMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 2);
    assertEquals(successfulRecords.size(), 0);

  }

}
