package com.sothebys.propertydb.importer.route;

import static com.sothebys.propertydb.importer.support.PropertyImporterConstants.CSV;

import com.sothebys.propertydb.importer.PropertyImportApplication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.aws.s3.S3Constants;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.impl.DefaultMessage;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

/**
 * @author Minhtri Tran
 */
@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class SendImportReportRouteTest extends CamelTestSupport {

  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:awsS3")
  private MockEndpoint awsS3Mock;

  @EndpointInject(uri = "mock:sendEmail")
  private MockEndpoint sendEmailMock;


  @Before
  public void setUp() throws Exception {
    camelContext.getRouteDefinition("send-import-report").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            interceptSendToEndpoint("aws-s3:*").skipSendToOriginalEndpoint()
                .to("mock:awsS3");
            interceptSendToEndpoint("smtp*").skipSendToOriginalEndpoint().to("mock:sendEmail");
          }
        });
  }

  @After
  public void teardown() {
    awsS3Mock.reset();
    sendEmailMock.reset();
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testSendImportReportWithSomeFailedRows() throws Exception {
    camelContext.start();

    awsS3Mock.expectedMessageCount(2);
    awsS3Mock.message(0).body().convertToString().contains("SOT-NYC");
    awsS3Mock.message(0).header(S3Constants.KEY).contains("test_fileName_errors_");
    awsS3Mock.message(0).header(S3Constants.KEY).endsWith(CSV);
    awsS3Mock.message(0).header(S3Constants.CANNED_ACL).contains("PublicRead");

    awsS3Mock.message(1).body().convertToString().contains("SE-75NNX4W04QQ");
    awsS3Mock.message(1).header(S3Constants.KEY).contains("test_fileName_successful_");
    awsS3Mock.message(1).header(S3Constants.KEY).endsWith(".csv");
    awsS3Mock.message(1).header(S3Constants.CANNED_ACL).contains("PublicRead");

    sendEmailMock.expectedMessageCount(1);
    sendEmailMock.message(0).header(Exchange.CONTENT_TYPE).isEqualToIgnoreCase("text/html");
    sendEmailMock.message(0).body().convertToString().contains("test fileName.csv");
    sendEmailMock.message(0).body().convertToString()
        .contains("Total number of records in file:2");
    sendEmailMock.message(0).body().convertToString()
        .contains("Total number of records successfully imported:1");
    sendEmailMock.message(0).body().convertToString()
        .contains("Total number of records with errors:1");
    sendEmailMock.message(0).body().convertToString().contains("Please download the Error records file from");
    sendEmailMock.message(0).body().convertToString().contains("Please download the Successful records file from");

    Message message = new DefaultMessage(camelContext);
    message.setHeader(Exchange.FILE_NAME, "test fileName.csv");
    message.setBody("some,csv,content");

    List<String> badRow = new ArrayList<>();
    badRow.add("SOT-NYC");
    
    List<String> successfulRow = new ArrayList<>();
    successfulRow.add("SE-75NNX4W04QQ");

    Exchange exchange = new DefaultExchange(camelContext);
    exchange.setProperty("badCsvData", Collections.singleton(badRow));
    exchange.setProperty("badRowsBody", Collections.singleton(badRow));
    exchange.setProperty("sucessfulRowsBody", Collections.singleton(successfulRow));
    exchange.setProperty("numberOfCSVRows", 2);
    exchange.setIn(message);
    producerTemplate.send("direct:sendImportReport", exchange);


    awsS3Mock.assertIsSatisfied();
    sendEmailMock.assertIsSatisfied();

  }

  @Test
  public void testSendImportReportWithAllSuccessfulRows() throws Exception {
    camelContext.start();

    awsS3Mock.expectedMessageCount(1);
    awsS3Mock.message(0).body().convertToString().contains("SOT-NYC");
    awsS3Mock.message(0).header(S3Constants.KEY).contains("test_fileName_successful_");
    awsS3Mock.message(0).header(S3Constants.KEY).endsWith(".csv");
    awsS3Mock.message(0).header(S3Constants.CANNED_ACL).contains("PublicRead");

    sendEmailMock.expectedMessageCount(1);
    sendEmailMock.message(0).header(Exchange.CONTENT_TYPE).isEqualToIgnoreCase("text/html");
    sendEmailMock.message(0).body().convertToString().contains("test fileName.csv");
    sendEmailMock.message(0).body().convertToString()
        .contains("Total number of records in file:1");
    sendEmailMock.message(0).body().convertToString()
        .contains("Total number of records successfully imported:1");
    sendEmailMock.message(0).body().convertToString()
        .contains("Total number of records with errors:0");
    sendEmailMock.message(0).body().convertToString().contains("Please download the Successful records file from");

    Message message = new DefaultMessage(camelContext);
    message.setHeader(Exchange.FILE_NAME, "test fileName.csv");
    
    List<String> sucessfulRows = new ArrayList<>();
    sucessfulRows.add("SOT-NYC");

    Exchange exchange = new DefaultExchange(camelContext);
    exchange.setProperty("badCsvData", new ArrayList<>());
    exchange.setProperty("sucessfulRowsBody", sucessfulRows);
    exchange.setProperty("numberOfCSVRows", 1);
    exchange.setIn(message);
    producerTemplate.send("direct:sendImportReport", exchange);

    awsS3Mock.assertIsSatisfied();
    sendEmailMock.assertIsSatisfied();
  }
  
  
  @Test
  public void testSendImportReportWithAllErrorRows() throws Exception {
    camelContext.start();

    awsS3Mock.expectedMessageCount(1);
    awsS3Mock.message(0).body().convertToString().contains("SOT-NYC");
    awsS3Mock.message(0).header(S3Constants.KEY).contains("test_fileName_errors_");
    awsS3Mock.message(0).header(S3Constants.KEY).endsWith(".csv");
    awsS3Mock.message(0).header(S3Constants.CANNED_ACL).contains("PublicRead");

    sendEmailMock.expectedMessageCount(1);
    sendEmailMock.message(0).header(Exchange.CONTENT_TYPE).isEqualToIgnoreCase("text/html");
    sendEmailMock.message(0).body().convertToString().contains("test fileName.csv");
    sendEmailMock.message(0).body().convertToString()
        .contains("Total number of records in file:1");
    sendEmailMock.message(0).body().convertToString()
        .contains("Total number of records successfully imported:0");
    sendEmailMock.message(0).body().convertToString()
        .contains("Total number of records with errors:1");
    sendEmailMock.message(0).body().convertToString().contains("Please download the Error records file from");

    Message message = new DefaultMessage(camelContext);
    message.setHeader(Exchange.FILE_NAME, "test fileName.csv");
    
    List<String> badRows = new ArrayList<>();
    badRows.add("SOT-NYC");

    Exchange exchange = new DefaultExchange(camelContext);
    exchange.setProperty("badCsvData", badRows);
    exchange.setProperty("badRowsBody", badRows);
    exchange.setProperty("numberOfCSVRows", 1);
    exchange.setIn(message);
    producerTemplate.send("direct:sendImportReport", exchange);

    awsS3Mock.assertIsSatisfied();
    sendEmailMock.assertIsSatisfied();
  }
  
}