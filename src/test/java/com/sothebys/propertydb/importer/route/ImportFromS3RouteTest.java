package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.PropertyImportApplication;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.aws.s3.S3Constants;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

/**
 * @author Minhtri Tran
 */
@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
public class ImportFromS3RouteTest extends CamelTestSupport {

  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:sendEmail")
  private MockEndpoint sendEmailMock;

  @EndpointInject(uri = "mock:file")
  private MockEndpoint fileMock;

  @EndpointInject(uri = "mock:result")
  private MockEndpoint resultMock;

  @Before
  public void setUp() throws Exception {
    if (Files.exists(Paths.get(ImportFromS3Route.FILE_REPO_PATH))) {
      Files.delete(Paths.get(ImportFromS3Route.FILE_REPO_PATH));
    }

    camelContext.getRouteDefinition("import-from-s3").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("smtp*").skipSendToOriginalEndpoint().to("mock:sendEmail");
            interceptSendToEndpoint("file*").skipSendToOriginalEndpoint().to("mock:file");
            weaveAddLast().to("mock:result");
          }
        });
    camelContext.start();
  }

  @After
  public void teardown() {
    MockEndpoint.resetMocks(camelContext);
  }

  @Test
  @DirtiesContext
  public void testReadFromS3() throws Exception {
    sendEmailMock.expectedMessageCount(0);

    fileMock.expectedMessageCount(1);
    fileMock.expectedHeaderReceived(S3Constants.E_TAG, "1234");
    fileMock.expectedHeaderReceived(S3Constants.KEY, "property-filename.csv");

    resultMock.expectedBodiesReceived("gregor,zurowski");
    resultMock.expectedMessageCount(1);

    Map<String, Object> headers = new HashMap<>();
    headers.put(S3Constants.E_TAG, "1234");
    headers.put(S3Constants.KEY, "property-filename.csv");

    producerTemplate.sendBodyAndHeaders("direct:start", "gregor,zurowski", headers);

    fileMock.assertIsSatisfied();
    resultMock.assertIsSatisfied();
  }

  /**
   * Test the route that handles the import from S3. Verifies that the same input files are not
   * processed twice.
   */
  @Test
  @DirtiesContext
  public void testReadFromS3DuplicateFile() throws Exception {
    sendEmailMock.expectedMessageCount(1);
    sendEmailMock.message(0).header(Exchange.CONTENT_TYPE).isEqualToIgnoreCase("text/html");
    sendEmailMock.message(0).body().convertToString()
        .contains("We did not process the import for property-filename.csv");

    resultMock.expectedBodiesReceived("gregor,zurowski");
    resultMock.expectedMessageCount(1);

    Map<String, Object> headers = new HashMap<>();
    headers.put(S3Constants.E_TAG, "1234");
    headers.put(S3Constants.KEY, "property-filename.csv");

    producerTemplate.sendBodyAndHeaders("direct:start", "gregor,zurowski", headers);
    producerTemplate.sendBodyAndHeaders("direct:start", "gregor,zurowski", headers);

    sendEmailMock.assertIsSatisfied();
    resultMock.assertIsSatisfied();
  }
}