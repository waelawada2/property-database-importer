package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.PropertyImportApplication;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

/**
 * @author Minhtri Tran
 */
@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class TransactionEventImportRouteTest extends CamelTestSupport {

  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:postEvent")
  private MockEndpoint postEventMock;

  @EndpointInject(uri = "mock:putEvent")
  private MockEndpoint putEventMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  private byte[] transactionEventCsvContent;

  @Before
  public void setUp() throws Exception {
    camelContext.getRouteDefinition("push-transaction-event-csv-to-service")
        .adviceWith(camelContext,
            new AdviceWithRouteBuilder() {
              @Override
              public void configure() throws Exception {
                replaceFromWith("direct:start");
                interceptSendToEndpoint("rest:POST:events*").skipSendToOriginalEndpoint()
                    .to("mock:postEvent");
                interceptSendToEndpoint("rest:PUT:events*").skipSendToOriginalEndpoint()
                    .to("mock:putEvent");
                interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                    .to("mock:sendImportReport");
              }
            });

    File csvFile = new File("src/test/resources/transaction-event-import-test.csv");
    transactionEventCsvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));
  }

  @After
  public void teardown() {
    postEventMock.reset();
    putEventMock.reset();
    sendImportReportMock.reset();
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testPostPutTransactionEvent() throws Exception {
    camelContext.start();

    postEventMock.expectedMessageCount(1);
    postEventMock.message(0).jsonpath("$.title").isEqualTo("Latin American Art");
    postEventMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    putEventMock.expectedMessageCount(1);
    putEventMock.message(0).jsonpath("$.title").isEqualTo("2012 Private Sale");
    putEventMock.expectedHeaderReceived(Exchange.HTTP_PATH, "ET-PXKK41P12Y9");
    putEventMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Transaction_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", transactionEventCsvContent,
        Exchange.FILE_NAME, "test.csv");

    postEventMock.assertIsSatisfied();
    putEventMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 0);
    assertEquals(successfulRecords.size(), 2);
  }

  @Test
  public void testPostPutTransactionEventFailure() throws Exception {
    camelContext.start();

    postEventMock.expectedMessageCount(1);
    postEventMock.message(0).jsonpath("$.title").isEqualTo("Latin American Art");
    postEventMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    putEventMock.expectedMessageCount(1);
    putEventMock.message(0).jsonpath("$.title").isEqualTo("2012 Private Sale");
    putEventMock.expectedHeaderReceived(Exchange.HTTP_PATH, "ET-PXKK41P12Y9");
    putEventMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Transaction_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", transactionEventCsvContent,
        Exchange.FILE_NAME, "test.csv");

    postEventMock.assertIsSatisfied();
    putEventMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 2);
    assertEquals(successfulRecords.size(), 0);

  }
}
