package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.PropertyImportApplication;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class ConsignmentPurchaseEventImportRouteTest extends CamelTestSupport {

  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:postEvent")
  private MockEndpoint postEventMock;

  @EndpointInject(uri = "mock:putEvent")
  private MockEndpoint putEventMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  private byte[] ConsignmentPurchaseCsvContent;


  @Before
  public void setUp() throws Exception {
    File csvFile = new File("src/test/resources/consignment-purchase-event-import-test.csv");
    ConsignmentPurchaseCsvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));
  }

  @After
  public void teardown() {
    postEventMock.reset();
    putEventMock.reset();
    sendImportReportMock.reset();
  }


  @Override
  public boolean isUseAdviceWith() {
    return true;
  }


  @Test
  public void testPostPutConsignmentPurchaseSuccess() throws Exception {
    camelContext.getRouteDefinition("push-consignment-purchase-event-csv-to-service")
        .adviceWith(camelContext, new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:events*").skipSendToOriginalEndpoint()
                .to("mock:postEvent");
            interceptSendToEndpoint("rest:PUT:events*").skipSendToOriginalEndpoint()
                .to("mock:putEvent");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postEventMock.expectedMessageCount(1);
    postEventMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ID-123\"}");
    });

    putEventMock.expectedMessageCount(1);
    putEventMock.expectedHeaderReceived(Exchange.HTTP_PATH, "EVENT-ID");
    putEventMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"EVENT-ID\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Consignment_Purchase_Code");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", ConsignmentPurchaseCsvContent,
        Exchange.FILE_NAME, "test.csv");

    postEventMock.assertIsSatisfied();
    putEventMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);

    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 0);
    assertEquals(successfulRecords.size(), 2);
  }

  @Test
  public void testPostPutConsignmentPurchaseFail() throws Exception {
    camelContext.getRouteDefinition("push-consignment-purchase-event-csv-to-service")
        .adviceWith(camelContext, new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:events*").skipSendToOriginalEndpoint()
                .to("mock:postEvent");
            interceptSendToEndpoint("rest:PUT:events*").skipSendToOriginalEndpoint()
                .to("mock:putEvent");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postEventMock.expectedMessageCount(1);
    postEventMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    putEventMock.expectedMessageCount(1);
    putEventMock.expectedHeaderReceived(Exchange.HTTP_PATH, "EVENT-ID");
    putEventMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Consignment_Purchase_Code");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", ConsignmentPurchaseCsvContent,
        Exchange.FILE_NAME, "test.csv");

    postEventMock.assertIsSatisfied();
    putEventMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);

    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 2);
    assertEquals(successfulRecords.size(), 0);
  }



}
