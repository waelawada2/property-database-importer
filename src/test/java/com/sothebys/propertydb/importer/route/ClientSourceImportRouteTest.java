package com.sothebys.propertydb.importer.route;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import com.sothebys.propertydb.importer.PropertyImportApplication;

@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class ClientSourceImportRouteTest extends CamelTestSupport {
  
  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  @EndpointInject(uri = "mock:postClientSource")
  private MockEndpoint postClientSourceMock;

  @EndpointInject(uri = "mock:putClientSource")
  private MockEndpoint putClientSourceMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  private byte[] ClientSourceCsvContent;

  @Before
  public void setUp() throws Exception {
    File csvFile = new File("src/test/resources/SourceName-test.csv");
    ClientSourceCsvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));
  }

  @After
  public void teardown() {
    postClientSourceMock.reset();
    putClientSourceMock.reset();
    sendImportReportMock.reset();
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testPostPutClientSource() throws Exception {
    camelContext.getRouteDefinition("push-client-source-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:client-sources*").skipSendToOriginalEndpoint()
                .to("mock:postClientSource");
            interceptSendToEndpoint("rest:PUT:client-sources*").skipSendToOriginalEndpoint()
                .to("mock:putClientSource");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postClientSourceMock.expectedMessageCount(1);
    postClientSourceMock.message(0).jsonpath("$.name").isEqualTo("Valuation");
    postClientSourceMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"ABC123\"}");
    });

    putClientSourceMock.expectedMessageCount(1);
    putClientSourceMock.message(0).jsonpath("$.name").isEqualTo("CAT");
    putClientSourceMock.expectedHeaderReceived(Exchange.HTTP_PATH, "SS-KN2W8N7NQQY");
    putClientSourceMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"XYZ789\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Source Name");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", ClientSourceCsvContent, Exchange.FILE_NAME, "test.csv");

    postClientSourceMock.assertIsSatisfied();
    putClientSourceMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 0);
    assertEquals(successfulRecords.size(), 2);
  }

  @Test
  public void testPostClientSourceFailure() throws Exception {
    camelContext.getRouteDefinition("push-client-source-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:client-sources*").skipSendToOriginalEndpoint()
                .to("mock:postClientSource");
            interceptSendToEndpoint("rest:PUT:client-sources*").skipSendToOriginalEndpoint()
                .to("mock:putClientSource");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });
    camelContext.start();

    postClientSourceMock.expectedMessageCount(1);
    postClientSourceMock.message(0).jsonpath("$.name").isEqualTo("Valuation");
    postClientSourceMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    putClientSourceMock.expectedMessageCount(1);
    putClientSourceMock.message(0).jsonpath("$.name").isEqualTo("CAT");
    putClientSourceMock.expectedHeaderReceived(Exchange.HTTP_PATH, "SS-KN2W8N7NQQY");
    putClientSourceMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "test.csv");
    sendImportReportMock.message(0).body().convertToString().contains("Source Name");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", ClientSourceCsvContent, Exchange.FILE_NAME, "test.csv");

    postClientSourceMock.assertIsSatisfied();
    putClientSourceMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();

    Exchange exchange = sendImportReportMock.getExchanges().get(0);
    List badCsvData = exchange.getProperty("badCsvData", List.class);
    List successfulRecords = exchange.getProperty("successfulRecords", List.class);
    assertEquals(badCsvData.size(), 2);
    assertEquals(successfulRecords.size(), 0);

  }

}
