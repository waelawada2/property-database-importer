package com.sothebys.propertydb.importer.route;

import com.sothebys.propertydb.importer.PropertyImportApplication;
import java.io.File;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.http.common.HttpOperationFailedException;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.UseAdviceWith;
import org.apache.http.conn.HttpHostConnectException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

/**
 * @author Minhtri Tran
 */
@RunWith(CamelSpringBootRunner.class)
@UseAdviceWith
@SpringBootTest(classes = PropertyImportApplication.class)
@DirtiesContext
public class ArtistImportRouteTest extends CamelTestSupport {

  @Autowired
  private ModelCamelContext camelContext;

  @Autowired
  private ProducerTemplate producerTemplate;

  private byte[] csvContent;
  private byte[] csvContentBadData;

  @EndpointInject(uri = "mock:artists")
  private MockEndpoint artistsMock;

  @EndpointInject(uri = "mock:updateArtists")
  private MockEndpoint artistsUpdateMock;

  @EndpointInject(uri = "mock:sendImportReport")
  private MockEndpoint sendImportReportMock;

  @Before
  public void setUp() throws Exception {
    camelContext.getRouteDefinition("push-artist-csv-to-service").adviceWith(camelContext,
        new AdviceWithRouteBuilder() {
          @Override
          public void configure() throws Exception {
            replaceFromWith("direct:start");
            interceptSendToEndpoint("rest:POST:artists*").skipSendToOriginalEndpoint()
                .to("mock:artists");
            interceptSendToEndpoint("rest:PUT:artists*").skipSendToOriginalEndpoint()
                .to("mock:updateArtists");
            interceptSendToEndpoint("direct:sendImportReport").skipSendToOriginalEndpoint()
                .to("mock:sendImportReport");
          }
        });

    camelContext.start();

    File csvFile = new File("src/test/resources/artist-import-test.csv");
    csvContent = Files.readAllBytes(Paths.get(csvFile.toURI()));

    File csvFileBadData = new File("src/test/resources/artist-import-test-bad-data.csv");
    csvContentBadData = Files.readAllBytes(Paths.get(csvFileBadData.toURI()));
  }

  @After
  public void teardown() {
    MockEndpoint.resetMocks(camelContext);
  }

  @Override
  public boolean isUseAdviceWith() {
    return true;
  }

  @Test
  public void testPostToArtistServiceRoute() throws Exception {
    artistsMock.expectedMessageCount(1);
    artistsMock.message(0).body().convertToString().contains("1500321612");
    artistsMock.message(0).body().convertToString().contains("Weiwei");
    artistsMock.message(0).body().convertToString().contains("Ai");
    artistsMock.message(0).body().convertToString().contains("1957");
    artistsMock.message(0).body().convertToString().contains("1900");
    artistsMock.message(0).body().convertToString().contains("China");
    artistsMock.message(0).body().convertToString().contains("Male");
    artistsMock.message(0).body().convertToString().contains("Sculpture");
    artistsMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"AA-ABC123\"}");
    });

    artistsUpdateMock.expectedMessageCount(1);
    artistsUpdateMock.message(0).body().convertToString().contains("1500058684");
    artistsUpdateMock.message(0).body().convertToString().contains("AA-0003QW33");
    artistsUpdateMock.message(0).body().convertToString().contains("Francis");
    artistsUpdateMock.message(0).body().convertToString().contains("Bacon");
    artistsUpdateMock.message(0).body().convertToString().contains("1909");
    artistsUpdateMock.message(0).body().convertToString().contains("1992");
    artistsUpdateMock.message(0).body().convertToString().contains("U.S.A.");
    artistsUpdateMock.message(0).body().convertToString().contains("Male");
    artistsUpdateMock.whenAnyExchangeReceived(exchange -> {
      exchange.getIn().setBody("{\"id\": \"AA-XYZ789\"}");
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "artist-import-test.csv");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", csvContent, Exchange.FILE_NAME,
        "artist-import-test.csv");

    artistsMock.assertIsSatisfied();
    artistsUpdateMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
  }

  @Test
  public void testPostToArtistServiceRouteHttpOperationFailedException() throws Exception {
    artistsMock.expectedMessageCount(1);
    artistsMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    artistsUpdateMock.expectedMessageCount(1);
    artistsUpdateMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpOperationFailedException(null, 500, null, null, null, null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "artist-import-test.csv");
    sendImportReportMock.allMessages().body().convertToString().contains("Artist_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", csvContent, Exchange.FILE_NAME,
        "artist-import-test.csv");

    artistsMock.assertIsSatisfied();
    artistsUpdateMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
  }

  @Test
  public void testPostToArtistServiceRouteBadDataFailure() throws Exception {
    artistsMock.expectedMessageCount(0);
    artistsUpdateMock.expectedMessageCount(0);

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock
        .expectedHeaderReceived(Exchange.FILE_NAME, "artist-import-test-bad-data.csv");
    sendImportReportMock.allMessages().body().convertToString().contains("Artist_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", csvContentBadData, Exchange.FILE_NAME,
        "artist-import-test-bad-data.csv");

    artistsMock.assertIsSatisfied();
    artistsUpdateMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
  }

  @Test
  public void testPostToArtistServiceRouteHttpHostConnectException() throws Exception {
    artistsMock.expectedMessageCount(1);
    artistsMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpHostConnectException(null, null, (InetAddress) null);
    });

    artistsUpdateMock.expectedMessageCount(1);
    artistsUpdateMock.whenAnyExchangeReceived(exchange -> {
      throw new HttpHostConnectException(null, null, (InetAddress) null);
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock
        .expectedHeaderReceived(Exchange.FILE_NAME, "artist-import-test.csv");
    sendImportReportMock.allMessages().body().convertToString().contains("Artist_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", csvContent, Exchange.FILE_NAME,
        "artist-import-test.csv");

    artistsMock.assertIsSatisfied();
    artistsUpdateMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
  }

  @Test
  public void testPostToArtistServiceRouteUnknownException() throws Exception {
    artistsMock.expectedMessageCount(1);
    artistsMock.whenAnyExchangeReceived(exchange -> {
      throw new Exception();
    });

    artistsUpdateMock.expectedMessageCount(1);
    artistsUpdateMock.whenAnyExchangeReceived(exchange -> {
      throw new Exception();
    });

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock
        .expectedHeaderReceived(Exchange.FILE_NAME, "artist-import-test.csv");
    sendImportReportMock.allMessages().body().convertToString().contains("Artist_ID");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 2);

    producerTemplate.sendBodyAndHeader("direct:start", csvContent, Exchange.FILE_NAME,
        "artist-import-test.csv");

    artistsMock.assertIsSatisfied();
    artistsUpdateMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
  }

  @Test
  public void testPostToArtistServiceRouteEmptyCsvFile() throws Exception {
    artistsMock.expectedMessageCount(0);
    artistsUpdateMock.expectedMessageCount(0);

    sendImportReportMock.expectedMessageCount(1);
    sendImportReportMock.expectedHeaderReceived(Exchange.FILE_NAME, "empty-file.csv");
    sendImportReportMock.expectedPropertyReceived("numberOfCSVRows", 0);

    producerTemplate.sendBodyAndHeader("direct:start", new byte[0], Exchange.FILE_NAME,
        "empty-file.csv");

    artistsMock.assertIsSatisfied();
    artistsUpdateMock.assertIsSatisfied();
    sendImportReportMock.assertIsSatisfied();
  }
}
