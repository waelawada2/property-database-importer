package com.sothebys.propertydb.importer.support;

import static com.sothebys.propertydb.importer.support.PublicationEventMapper.VERIFIED_VALUE;
import static org.junit.Assert.assertEquals;

import com.sothebys.propertydb.importer.model.event.PublicationEventDto;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PublicationEventMapperTest {

  @InjectMocks
  private PublicationEventMapper publicationEventMapper;

  private List<String> csvRow;

  @Before
  public void setUp() {
    setupCsvRow();
  }

  private void setupCsvRow() {
    csvRow = new ArrayList<>();
    csvRow.add("EP-CODE-TEST");
    csvRow.add("EP-123456");
    csvRow.add("SOME TYPE");
    csvRow.add("06/May/2017");
    csvRow.add("Test Title");
    csvRow.add("Test SubTitle");
    csvRow.add("J.R.R Tolkien");
    csvRow.add("SOT-LON");
    csvRow.add("SE-J89Y1ZP95LN");
    csvRow.add("1");
    csvRow.add("2");
    csvRow.add("150");
    csvRow.add(VERIFIED_VALUE);
    csvRow.add("123456789");
    csvRow.add("library-code");
    csvRow.add("Associated_Exhibition_ID");
    csvRow.add("Associated_Publications_ID");
    csvRow.add("Notes");
    csvRow.add("image.jpg");
    csvRow.add("Y");
  }

  @Test
  public void testMap() {
    PublicationEventDto dto = null;
      dto = publicationEventMapper.map(csvRow);
    assertEquals("EP-CODE-TEST", dto.getCode());
    assertEquals("EP-123456", dto.getId());
    assertEquals("SOME TYPE", dto.getPublicationType().getId());
    assertEquals("Test Title", dto.getTitle());
    assertEquals("Test SubTitle", dto.getSubTitle());
    assertEquals("J.R.R Tolkien", dto.getAuthor());
    assertEquals("SOT-LON", dto.getEstablishment().getCode());
    assertEquals("SE-J89Y1ZP95LN", dto.getEstablishment().getId());
    assertEquals("1", dto.getVolume());
    assertEquals("2", dto.getEdition());
    assertEquals(150, dto.getPages());
    assertEquals(Boolean.valueOf("TRUE"), dto.isVerified());
    assertEquals("123456789", dto.getIsbn());
    assertEquals("library-code", dto.getLibraryCode());
    //assertEquals("Associated_Exhibition_ID", dto.getExhibitionEvents());
    assertEquals("Associated_Publications_ID", dto.getPublicationEvents().get(0).getId());
    assertEquals("Notes", dto.getNotes());
    assertEquals("image.jpg", dto.getImageName());
    assertEquals(Boolean.valueOf("TRUE"), dto.isUpdateImage());
  }
}