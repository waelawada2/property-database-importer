package com.sothebys.propertydb.importer.support;

import static org.junit.Assert.assertEquals;

import com.sothebys.propertydb.importer.model.item.PublicationItemDto;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PublicationItemMapperTest {

  @InjectMocks
  private PublicationItemMapper publicationItemMapper;

  private List<String> csvRow;

  @Before
  public void setUp() {
    setupCsvRow();
  }

  private void setupCsvRow() {
    csvRow = new ArrayList<>();
    csvRow.add("Monet2");
    csvRow.add("IP-ZX89597KVXV");
    csvRow.add("PC-SCH-LON-XXX-1997-002");
    csvRow.add("EP-J89Y4YZ8VW8");
    csvRow.add("");
    csvRow.add("");
    csvRow.add("");
    csvRow.add("12345");
    csvRow.add("447");
    csvRow.add("1");
    csvRow.add("");
    csvRow.add("");
    csvRow.add("");
    csvRow.add("WVPZKWXZ2V5");
    csvRow.add("");
    csvRow.add("");
    csvRow.add("");
    csvRow.add("The artist; Alexina Duchamp, France");
    csvRow.add("");
    csvRow.add("");
    csvRow.add("");
    csvRow.add("");

  }

  @Test
  public void testMap() throws Exception {
    PublicationItemDto dto = null;

    dto = publicationItemMapper.map(csvRow);

    assertEquals("Monet2", dto.getCode());
    assertEquals("IP-ZX89597KVXV", dto.getId());
    assertEquals("PC-SCH-LON-XXX-1997-002", dto.getEvent().getCode());
    assertEquals("12345", dto.getObjects().get(0).getObjectId());
    assertEquals("EP-J89Y4YZ8VW8", dto.getEvent().getId());
    assertEquals("447", dto.getCataloguingPage());
    assertEquals("1", dto.getNumber());
    assertEquals("WVPZKWXZ2V5", dto.getColourType().getId());
    assertEquals("The artist; Alexina Duchamp, France", dto.getProvenance());

  }

}