package com.sothebys.propertydb.importer.support;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.sothebys.propertydb.importer.model.WebsiteImagesCreateRequestDTO;

/**
 * @author SrinivasaRao Batthula
 */
public class WebsiteImagesMapperTest {

  private WebsiteImagesMapper mapper;

  @Before
  public void setUp() {
    mapper = new WebsiteImagesMapper();
  }

  /**
   * Tests the mapping code that maps from the CSV bean to request DTO.
   */
  @Test
  public void testMap() throws Exception {
    List<String> csv = createFullPropertyCsvRecord();
    WebsiteImagesCreateRequestDTO dto = mapper.map(csv);

    assertEquals(csv.get(WebsiteImagesCsvFields.ITEM_ID), dto.getItemId());
    assertEquals(csv.get(WebsiteImagesCsvFields.SALE_SALE_ID), dto.getSaleId());
    assertEquals(csv.get(WebsiteImagesCsvFields.LOTS_LOT_ID_TXT), dto.getLotsNumber());

  }



  private List<String> createFullPropertyCsvRecord() {
    String[] csv = new String[3];

    csv[WebsiteImagesCsvFields.ITEM_ID] = "7FWJ7";
    csv[WebsiteImagesCsvFields.SALE_SALE_ID] = "PF1540";
    csv[WebsiteImagesCsvFields.LOTS_LOT_ID_TXT] = "5038";

    return Arrays.asList(csv);
  }

}

