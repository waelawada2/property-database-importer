package com.sothebys.propertydb.importer.support;

import static org.junit.Assert.*;

import com.sothebys.propertydb.importer.model.event.EstablishmentDto;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EstablishmentMapperTest {

  @InjectMocks
  private EstablishmentMapper establishmentMapper;

  private List<String> csvRow;

  @Before
  public void setUp() {
    setupCsvRow();
  }

  private void setupCsvRow() {
    csvRow = new ArrayList<>();
    csvRow.add("SOT-LON");
    csvRow.add("some id");
    csvRow.add("test");
    csvRow.add("London");
    csvRow.add("State");
    csvRow.add("United Kingdom");
    csvRow.add("Europe");
  }

  @Test
  public void testMap() {
    EstablishmentDto dto = establishmentMapper.map(csvRow);

    assertEquals("SOT-LON", dto.getCode());
    assertEquals("some id", dto.getId());
    assertEquals("test", dto.getName());
    assertEquals("London", dto.getCityName());
    assertEquals("State", dto.getState());
    assertEquals("United Kingdom", dto.getCountryName());
    assertEquals("Europe", dto.getRegion());


  }

}