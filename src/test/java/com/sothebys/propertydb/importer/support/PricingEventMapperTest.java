package com.sothebys.propertydb.importer.support;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.importer.model.event.PricingEventDto;

@RunWith(MockitoJUnitRunner.class)
public class PricingEventMapperTest {
  
  @InjectMocks
  private PricingEventMapper pricingEventMapper;

  private List<String> csvRow;

  @Before
  public void setUp() {
    setupCsvRow();
  }

  private void setupCsvRow() {
    csvRow = new ArrayList<>();
    csvRow.add("CODE-TEST");
    csvRow.add("some id");
    csvRow.add("KN26K0J1X06");
    csvRow.add("08/Nov/2013");
    csvRow.add("70238717");
    csvRow.add("Proposal 4515646134");
    csvRow.add("M8705YJ4KY1");
    csvRow.add("Works on Long Term Loan to Towner");
    csvRow.add("XL4KQQM8Q0Z");
    csvRow.add("");
    csvRow.add("75NQL816K89");
    csvRow.add("client status");
    csvRow.add("");
    csvRow.add("client_id");
    csvRow.add("test designation");
    csvRow.add("XL4KQQM8Q0Z");
    csvRow.add("");
    csvRow.add("SS-5411X81QN4Y");
    csvRow.add("test name");
    csvRow.add("8888888888");
    csvRow.add("1XW20PQ82PK");
  }

  @Test
  public void testMap() throws Exception {
    PricingEventDto dto = pricingEventMapper.map(csvRow);
    assertEquals("CODE-TEST", dto.getCode());
    assertEquals("some id", dto.getId());
    assertEquals("KN26K0J1X06", dto.getValuationOrigin().getId());
    assertEquals(CsvStringConverter.toDate("08/Nov/2013", "d/MMM/yyyy"), dto.getDate());
    assertEquals("70238717", dto.getValuationId());
    assertEquals("Proposal 4515646134", dto.getProposal());
    assertEquals("M8705YJ4KY1", dto.getValuationType().getId());
    assertEquals("Works on Long Term Loan to Towner", dto.getNotes());
    assertEquals("XL4KQQM8Q0Z", dto.getMainClient().getClientSourceType().getId());
    assertEquals("75NQL816K89", dto.getMainClient().getClientSource().getId());
    assertEquals("client status", dto.getMainClient().getClientStatus().getId());
    assertEquals("client_id", dto.getMainClient().getClientId());
    assertEquals("test designation", dto.getMainClient().getDisplay());
    assertEquals("XL4KQQM8Q0Z", dto.getAffiliatedClient().getClientSourceType().getId());
    assertEquals("SS-5411X81QN4Y", dto.getAffiliatedClient().getClientSource().getId());
    assertEquals("1XW20PQ82PK", dto.getAffiliatedClient().getAffiliatedType().getId());
    assertEquals("8888888888", dto.getAffiliatedClient().getClientId());
  }
}
