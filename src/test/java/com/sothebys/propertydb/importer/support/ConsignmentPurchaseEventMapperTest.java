package com.sothebys.propertydb.importer.support;

import static org.junit.Assert.assertEquals;

import com.sothebys.propertydb.importer.exception.ConsignmentPurchaseEventImportException;
import com.sothebys.propertydb.importer.model.event.ConsignmentPurchaseEventDto;
import com.sothebys.propertydb.importer.model.event.EventType;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ConsignmentPurchaseEventMapperTest {
  
  @InjectMocks
  private ConsignmentPurchaseEventMapper consignmentPurchaseEventMapper;

  private List<String> csvRow;
  
  @Before
  public void setUp() {
    setupCsvRow();
  }

  private void setupCsvRow() {
    csvRow = new ArrayList<>();
    csvRow.add("167082927");
    csvRow.add("ID_EVENT");
    csvRow.add("CONSIGNMENT");
    csvRow.add("INTERNAL");
    csvRow.add("CAT");
    csvRow.add("ID-CAT");
    csvRow.add("ACTIVE");
    csvRow.add("Blackpool Overseas Corporation");
    csvRow.add("12345789");
    csvRow.add("DESIGNATION");
    csvRow.add("MEDIA");
    csvRow.add("AFFILIATED_NAME");
    csvRow.add("ID-AFFILIATED");
    csvRow.add("MEDIA-NAME");
    csvRow.add("987654321");
    csvRow.add("SPONSORED-BY");
  }
  
  
  @Test
  public void testMap() throws Exception {
    ConsignmentPurchaseEventDto dto = consignmentPurchaseEventMapper.map(csvRow);
    
    assertEquals("167082927", dto.getCode());
    assertEquals("ID_EVENT", dto.getId());
    assertEquals(EventType.CONSIGNMENT, dto.getType());
    assertEquals("INTERNAL", dto.getMainClient().getClientSourceType().getId());
    assertEquals("ID-CAT", dto.getMainClient().getClientSource().getId());
    assertEquals("ACTIVE", dto.getMainClient().getClientStatus().getId());
    assertEquals("12345789", dto.getMainClient().getClientId());
    assertEquals("DESIGNATION", dto.getMainClient().getDisplay());
    assertEquals("MEDIA", dto.getAffiliatedClient().getClientSourceType().getId());
    assertEquals("ID-AFFILIATED", dto.getAffiliatedClient().getClientSource().getId());
    assertEquals("987654321", dto.getAffiliatedClient().getClientId());
    assertEquals("SPONSORED-BY", dto.getAffiliatedClient().getAffiliatedType().getId());
  }

  @Test(expected = ConsignmentPurchaseEventImportException.class)
  public void testMapInvalidEventType() throws Exception {
    csvRow.set(2, "INVALID-EVENT-TYPE");

    consignmentPurchaseEventMapper.map(csvRow);
  }
  
  @Test(expected = ConsignmentPurchaseEventImportException.class)
  public void testMapEmptyEventType() throws Exception {
    csvRow.set(2, "");

    consignmentPurchaseEventMapper.map(csvRow);
  }

}
