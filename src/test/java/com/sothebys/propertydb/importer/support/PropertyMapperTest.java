package com.sothebys.propertydb.importer.support;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import com.sothebys.propertydb.importer.model.PropertyCreateRequestDTO;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Gregor Zurowski
 */
public class PropertyMapperTest {

  private PropertyMapper mapper;

  private SimpleDateFormat format = new SimpleDateFormat(PropertyMapper.DATE_FORMAT);

  List<String> errorDetailsList = new ArrayList<>();

  @Before
  public void setUp() {
    mapper = new PropertyMapper();
  }

  /**
   * Tests the mapping code that maps from the CSV bean to the request DTO.
   */
  @Test
  public void testMap() throws Exception {
    // Given...
    List<String> csv = createFullPropertyCsvRecord();
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    // Then...
    assertEquals(csv.get(CsvFields.SORT_CODE), dto.getSortCode());
    assertEquals(csv.get(CsvFields.ARTIST_ID), dto.getArtistIds().get(0));
    assertEquals(csv.get(CsvFields.CATALOGUE_RAISONEE_NUMBER),
        dto.getPropertyCatalogueRaisonees().get(0).getNumber());
    assertEquals(csv.get(CsvFields.CATALOGUE_RAISONEE_VOLUME),
        dto.getPropertyCatalogueRaisonees().get(0).getVolume());
    assertEquals(csv.get(CsvFields.ORIGINAL_TITLE), dto.getOriginalTitle());
    assertEquals(csv.get(CsvFields.TITLE), dto.getTitle());
    assertEquals(csv.get(CsvFields.SIGNATURE), dto.getSignature());
    assertEquals(csv.get(CsvFields.MEDIUM), dto.getMedium());
    assertEquals(csv.get(CsvFields.CATEGORY), dto.getCategory());
    assertEquals(csv.get(CsvFields.SUB_CATEGORY), dto.getSubCategory());
    assertTrue(dto.getYearCirca());
    assertEquals(Integer.parseInt(csv.get(CsvFields.YEAR_START)), dto.getYearStart().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.YEAR_END)), dto.getYearEnd().intValue());
    assertTrue(dto.getCastYearCirca());
    assertEquals(Integer.parseInt(csv.get(CsvFields.CAST_YEAR_START)),
        dto.getCastYearStart().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.CAST_YEAR_END)),
        dto.getCastYearEnd().intValue());
    assertTrue(dto.getPosthumous());
    assertEquals(csv.get(CsvFields.YEAR_OVERRIDE), dto.getYearText());
    assertEquals(csv.get(CsvFields.PRESIZE_DESCRIPTION), dto.getPresizeName());
    assertEquals(new BigDecimal(csv.get(CsvFields.HEIGHT_CM)), dto.getHeightCm());
    assertEquals(new BigDecimal(csv.get(CsvFields.HEIGHT_IN)), dto.getHeightIn());
    assertEquals(new BigDecimal(csv.get(CsvFields.DEPTH_CM)), dto.getDepthCm());
    assertEquals(new BigDecimal(csv.get(CsvFields.DEPTH_IN)), dto.getDepthIn());
    assertEquals(new BigDecimal(csv.get(CsvFields.WIDTH_CM)), dto.getWidthCm());
    assertEquals(new BigDecimal(csv.get(CsvFields.WIDTH_IN)), dto.getWidthIn());
    assertEquals(new BigDecimal(csv.get(CsvFields.WEIGHT)), dto.getWeight());
    assertEquals(Integer.parseInt(csv.get(CsvFields.PARTS)), dto.getParts().intValue());
    assertEquals(csv.get(CsvFields.EDITION_STATUS), dto.getEditionSizeTypeName());
    assertEquals(csv.get(CsvFields.EDITION_NUMBER),
        dto.getEditionNumber());
    assertEquals(csv.get(CsvFields.EDITION_TYPE), dto.getEditionName());
    assertEquals(Integer.parseInt(csv.get(CsvFields.ARTIST_PROOF_TOTAL)),
        dto.getArtistProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.BON_A_TIRER_TOTAL)),
        dto.getBonATirerTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.CANCELLATION_PROOF_TOTAL)),
        dto.getCancellationProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.COLOR_TRIAL_PROOF_TOTAL)),
        dto.getColorTrialProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.DEDICATED_PROOF_TOTAL)),
        dto.getDedicatedProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.PRINTERS_PROOF_TOTAL)),
        dto.getPrintersProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.HORS_COMMERCE_TOTAL)),
        dto.getHorsCommerceProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.MUSEUM_PROOF_TOTAL)),
        dto.getMuseumProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.PROGRESSIVE_PROOF_TOTAL)),
        dto.getProgressiveProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.RIGHT_TO_PRODUCE_PROOF_TOTAL)),
        dto.getRightToProduceProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.TRIAL_PROOF_TOTAL)),
        dto.getTrialProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.WORKING_PROOF_TOTAL)),
        dto.getWorkingProofTotal().intValue());
    assertEquals(Integer.parseInt(csv.get(CsvFields.SERIES_TOTAL)),
        dto.getSeriesTotal().intValue());
    assertEquals(csv.get(CsvFields.FOUNDRY), dto.getFoundry());
    assertEquals(csv.get(CsvFields.EDITION_OVERRIDE), dto.getEditionOverride());
    assertEquals(csv.get(CsvFields.EDITION_NOTES), dto.getEditionSizeNotes());
    assertTrue(dto.getSigned());
    assertTrue(dto.getStamped());
    assertTrue(dto.getCertificate());
    assertTrue(dto.getAuthentication());
    assertTrue(dto.getResearchCompleteCheck());
    assertEquals(csv.get(CsvFields.ARCHIVE_NUMBERS),
        dto.getArchiveNumbers().get(0).getArchiveNumber());
    assertTrue(dto.getFlags().getFlagAuthenticity());
    assertTrue(dto.getFlags().getFlagRestitution());
    assertTrue(dto.getFlags().getFlagCondition());
    assertTrue(dto.getFlags().getFlagSfs());
    assertTrue(dto.getFlags().getFlagMedium());
    assertEquals(csv.get(CsvFields.FLAGS_EXPORT_COUNTRY), dto.getFlags().getFlagsCountry());
    assertEquals(csv.get(CsvFields.TAG_SUBJECT), dto.getTags().getImageSubjects().get(0));
    assertEquals(csv.get(CsvFields.TAG_FIGURE), dto.getTags().getImageFigure());
    assertEquals(csv.get(CsvFields.TAG_GENDER), dto.getTags().getImageGenders().get(0));
    assertEquals(csv.get(CsvFields.TAG_POSITION), dto.getTags().getImagePositions().get(0));
    assertEquals(csv.get(CsvFields.TAG_ANIMAL), dto.getTags().getImageAnimals().get(0));
    assertEquals(csv.get(CsvFields.TAG_MAIN_COLOUR), dto.getTags().getImageMainColours().get(0));
    assertEquals(csv.get(CsvFields.TAG_TEXT), dto.getTags().getImageText());
    assertEquals(csv.get(CsvFields.TAG_LOCATION), dto.getTags().getExpertiseLocation());
    assertEquals(csv.get(CsvFields.TAG_SITTER),
        dto.getTags().getExpertiseSitters().get(0).getDisplayName());
    assertEquals(csv.get(CsvFields.TAG_OTHER), dto.getTags().getOtherTags().get(0));
    assertEquals(csv.get(CsvFields.RESEARCHER_NAME), dto.getResearcherName());
    assertEquals(csv.get(CsvFields.RESEARCHER_NOTES_TEXT), dto.getResearcherNotesText());
    assertEquals(format.parse(csv.get(CsvFields.RESEARCHER_NOTES_DATE)),
        dto.getResearcherNotesDate());
    assertEquals(csv.get(CsvFields.MEASUREMENT_OVERRIDE), dto.getSizeText());
    assertEquals(csv.get(CsvFields.OBJECT_CODE),
        dto.getObjectCodes().get(0));
    assertEquals(errorDetailsList, dto.getErrorDetailsList());
  }

  @Test
  public void testArtistIdMappingWithWhitespaces() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_ID, " 1234| 4569  ");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getArtistIds().size(), is(2));
    assertThat(dto.getArtistIds().get(0), is("1234"));
    assertThat(dto.getArtistIds().get(1), is("4569"));
  }

  @Test
  public void testArtistIdMappingWithTrailingEmptyElement() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_ID, "1234|");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getArtistIds().size(), is(1));
    assertThat(dto.getArtistIds().get(0), is("1234"));
  }

  @Test
  public void testArtistIdMappingWithLeadingEmptyElement() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_ID, "|1234");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getArtistIds().size(), is(1));
    assertThat(dto.getArtistIds().get(0), is("1234"));
  }

  @Test
  public void testArtistIdMappingWithAllEmptyEntries() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_ID, "|");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getArtistIds().size(), is(0));
  }

  @Test
  public void testArtistIdMappingWithNullReference() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_ID, null);
    errorDetailsList.add("Artist Id cannot be empty.");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getArtistIds().size(), is(0));
    assertEquals("Artist Id cannot be empty.", dto.getErrorDetailsList().get(0));
  }

  @Test
  public void testUlanIdMappingWithWhitespaces() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_CODE, " 1234| 4569  ");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getUlanIds().size(), is(2));
    assertThat(dto.getUlanIds().get(0), is(1234L));
    assertThat(dto.getUlanIds().get(1), is(4569L));
  }

  @Test
  public void testUlanIdMappingWithTrailingEmptyElement() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_CODE, "1234|");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getUlanIds().size(), is(1));
    assertThat(dto.getUlanIds().get(0), is(1234L));
  }

  @Test
  public void testUlanIdMappingWithLeadingEmptyElement() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_CODE, "|1234");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getUlanIds().size(), is(1));
    assertThat(dto.getUlanIds().get(0), is(1234L));
  }

  @Test
  public void testUlanIdMappingWithAllEmptyEntries() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_CODE, "|");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getUlanIds().size(), is(0));
  }

  @Test
  public void testUlanIdMappingWithNullReference() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.ARTIST_CODE, null);
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getUlanIds().size(), is(0));
  }

  @Test
  public void testImagePathMappingWithNewObject() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.EOS_OBJECT_ID, "");
    csv.set(CsvFields.IMAGE_FILE_NAME, "some.jpg");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getImagePath(), is("some.jpg"));
  }

  @Test
  public void testImagePathMappingWithNoImageUpdate() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.IMAGE_UPDATE_Y_N, "N");
    csv.set(CsvFields.IMAGE_FILE_NAME, "some.jpg");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getImagePath(), nullValue());
  }

  @Test
  public void testImagePathMappingWithWrongImageUpdateFlagValue() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.IMAGE_UPDATE_Y_N, "other");
    csv.set(CsvFields.IMAGE_FILE_NAME, "some.jpg");
    mapper.map(csv, errorDetailsList);
    assertThat(errorDetailsList, contains("Image Update field needs to be set to Y or N"));
  }

  @Test
  public void testImagePathMappingWithImageUpdate() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.IMAGE_UPDATE_Y_N, "Y");
    csv.set(CsvFields.IMAGE_FILE_NAME, "some.jpg");
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getImagePath(), is("some.jpg"));
  }

  @Test
  public void testImagePathMappingWithImageRemove() {
    List<String> csv = createFullPropertyCsvRecord();
    csv.set(CsvFields.IMAGE_UPDATE_Y_N, "Y");
    csv.set(CsvFields.IMAGE_FILE_NAME, null);
    PropertyCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getImagePath(), is(""));
  }

  private List<String> createFullPropertyCsvRecord() {
    String[] csv = new String[82];
    csv[CsvFields.OBJECT_CODE] = "1661";
    csv[CsvFields.EOS_OBJECT_ID] = "12";
    csv[CsvFields.SORT_CODE] = "Impressionist-1to5-Marino Marini-94";
    csv[CsvFields.ARTIST_ID] = "000000M3";
    csv[CsvFields.ARTIST_CODE] = "1500086184";
    csv[CsvFields.ARTIST_NAME] = "Pablo Picasso";
    csv[CsvFields.CATALOGUE_RAISONEE_VOLUME] = "1";
    csv[CsvFields.CATALOGUE_RAISONEE_NUMBER] = "12";
    csv[CsvFields.ORIGINAL_TITLE] = "La Reine] = Le Fou Et Le Cheval";
    csv[CsvFields.TITLE] = "Honeycomb";
    csv[CsvFields.SIGNATURE] = "Executed in 1945 and cast in bronze in an edition of 6. This is the only cast with the ori More ...";
    csv[CsvFields.MEDIUM] = "oil on canvas";
    csv[CsvFields.CATEGORY] = "Work on Paper";
    csv[CsvFields.SUB_CATEGORY] = "Subcategory 1";
    csv[CsvFields.YEAR_CIRCA] = "Y";
    csv[CsvFields.YEAR_START] = "1927";
    csv[CsvFields.YEAR_END] = "1961";
    csv[CsvFields.CAST_YEAR_CIRCA] = "Y";
    csv[CsvFields.CAST_YEAR_START] = "1919";
    csv[CsvFields.CAST_YEAR_END] = "1921";
    csv[CsvFields.POSTHUMOUS] = "Y";
    csv[CsvFields.YEAR_OVERRIDE] = "Year Text";
    csv[CsvFields.PRESIZE_DESCRIPTION] = "Presize description";
    csv[CsvFields.HEIGHT_IN] = "25.8";
    csv[CsvFields.WIDTH_IN] = "3.4";
    csv[CsvFields.DEPTH_IN] = "26.8";
    csv[CsvFields.HEIGHT_CM] = "65.5";
    csv[CsvFields.WIDTH_CM] = "12.1";
    csv[CsvFields.DEPTH_CM] = "68.1";
    csv[CsvFields.MEASUREMENT_OVERRIDE] = "sizetext";
    csv[CsvFields.WEIGHT] = "23";
    csv[CsvFields.PARTS] = "1";
    csv[CsvFields.EDITION_STATUS] = "Unspecified";
    csv[CsvFields.EDITION_NUMBER] = "10";
    csv[CsvFields.EDITION_TYPE] = "Series";
    csv[CsvFields.SERIES_TOTAL] = "9";
    csv[CsvFields.ARTIST_PROOF_TOTAL] = "22";
    csv[CsvFields.BON_A_TIRER_TOTAL] = "-1";
    csv[CsvFields.CANCELLATION_PROOF_TOTAL] = "2";
    csv[CsvFields.COLOR_TRIAL_PROOF_TOTAL] = "3";
    csv[CsvFields.DEDICATED_PROOF_TOTAL] = "4";
    csv[CsvFields.PRINTERS_PROOF_TOTAL] = "5";
    csv[CsvFields.HORS_COMMERCE_TOTAL] = "15";
    csv[CsvFields.MUSEUM_PROOF_TOTAL] = "6";
    csv[CsvFields.PROGRESSIVE_PROOF_TOTAL] = "7";
    csv[CsvFields.RIGHT_TO_PRODUCE_PROOF_TOTAL] = "8";
    csv[CsvFields.TRIAL_PROOF_TOTAL] = "25";
    csv[CsvFields.WORKING_PROOF_TOTAL] = "9";
    csv[CsvFields.FOUNDRY] = "A.A. HŽbrard ed.";
    csv[CsvFields.EDITION_OVERRIDE] = "edition override";
    csv[CsvFields.EDITION_NOTES] = "edition notes";
    csv[CsvFields.STAMPED] = "Y";
    csv[CsvFields.SIGNED] = "Y";
    csv[CsvFields.CERTIFICATE] = "Y";
    csv[CsvFields.AUTHENTICATION] = "Y";
    csv[CsvFields.ARCHIVE_NUMBERS] = "1661/3";
    csv[CsvFields.FLAGS_AUTHENTICITY] = "Y";
    csv[CsvFields.FLAGS_RESTITUTION] = "Y";
    csv[CsvFields.FLAGS_CONDITION] = "Y";
    csv[CsvFields.FLAGS_SFS] = "Y";
    csv[CsvFields.FLAGS_MEDIUM] = "Y";
    csv[CsvFields.FLAGS_EXPORT_COUNTRY] = "Flagge Nr. 1";
    csv[CsvFields.TAG_SUBJECT] = "Abstract";
    csv[CsvFields.TAG_FIGURE] = "20+";
    csv[CsvFields.TAG_GENDER] = "Male";
    csv[CsvFields.TAG_POSITION] = "Standing";
    csv[CsvFields.TAG_ANIMAL] = "Lion";
    csv[CsvFields.TAG_MAIN_COLOUR] = "White";
    csv[CsvFields.TAG_TEXT] = "some text";
    csv[CsvFields.TAG_LOCATION] = "new york";
    csv[CsvFields.TAG_SITTER] = "john doe";
    csv[CsvFields.TAG_OTHER] = "some other tag";
    csv[CsvFields.RESEARCH_COMPLETE_CHECK] = "Y";
    csv[CsvFields.RESEARCHER_NAME] = "Wael Awada";
    csv[CsvFields.RESEARCHER_NOTES_DATE] = "05/19/2017";
    csv[CsvFields.RESEARCHER_NOTES_TEXT] = "This is a sample researcher notes text";
    csv[CsvFields.IMAGE_FILE_NAME] = "";
    csv[CsvFields.IMAGE_UPDATE_Y_N] = "N";
    return Arrays.asList(csv);
  }

}
