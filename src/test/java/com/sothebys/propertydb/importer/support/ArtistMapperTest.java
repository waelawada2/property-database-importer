package com.sothebys.propertydb.importer.support;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import com.sothebys.propertydb.importer.model.ArtistCreateRequestDTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Gregor Zurowski
 */
public class ArtistMapperTest {

  private ArtistMapper mapper;

  List<String> errorDetailsList = new ArrayList<>();

  @Before
  public void setUp() {
    mapper = new ArtistMapper();
  }

  /**
   * Tests the mapping code that maps from the CSV bean to the request DTO.
   */
  @Test
  public void testMap() {
    // Given...
    List<String> csv = createFullArtistCsvRecord();
    // When...
    ArtistCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    // Then...
    assertEquals(Long.valueOf(csv.get(ArtistCsvFields.ULAN_ID)), dto.getUlanId());
    assertEquals(csv.get(ArtistCsvFields.ARTIST_ID), dto.getExternalId());
    assertEquals(csv.get(ArtistCsvFields.FIRST_NAME), dto.getFirstName());
    assertEquals(csv.get(ArtistCsvFields.LAST_NAME), dto.getLastName());
    assertEquals(Integer.valueOf(csv.get(ArtistCsvFields.BIRTH_YEAR)), dto.getBirthYear());
    assertEquals(Integer.valueOf(csv.get(ArtistCsvFields.DEATH_YEAR)), dto.getDeathYear());
    assertEquals(csv.get(ArtistCsvFields.DISPLAY), dto.getDisplay());
    assertEquals(csv.get(ArtistCsvFields.NATIONALITY), dto.getCountries().get(0).getCountryName());
    assertEquals(csv.get(ArtistCsvFields.GENDER), dto.getGender());
    assertEquals(csv.get(ArtistCsvFields.CATALOGUE_RAISONEE_AUTHOR),
        dto.getArtistCatalogueRaisonees().get(0).getAuthor());
    assertEquals(csv.get(ArtistCsvFields.CATALOGUE_RAISONEE_YEAR),
        String.valueOf(dto.getArtistCatalogueRaisonees().get(0).getYear()));
    assertEquals(csv.get(ArtistCsvFields.CATALOGUE_RAISONEE_VOLUMES),
        dto.getArtistCatalogueRaisonees().get(0).getVolumes());
    assertEquals(csv.get(ArtistCsvFields.CATALOGUE_RAISONEE_TYPE),
        dto.getArtistCatalogueRaisonees().get(0).getType());
  }

  @Test
  public void testArtistIdMappingWithWhitespaces() {
    List<String> csv = createFullArtistCsvRecord();
    csv.set(ArtistCsvFields.ULAN_ID, "4569");
    ArtistCreateRequestDTO dto = mapper.map(csv, errorDetailsList);
    assertThat(dto.getUlanId(), is(4569L));
  }

  @Test
  public void testConvertToLong(){
    String ULAN_ID = "1500321612";
    Long convertedValue = ArtistMapper.convertToLong("UlanId", ULAN_ID, errorDetailsList);
    assertNotNull(convertedValue);
  }

  @Test
  public void testConvertToLongWithNullValue(){
    String ULAN_ID = "";
    Long convertedValue = ArtistMapper.convertToLong("UlanId", ULAN_ID, errorDetailsList);
    assertNull(convertedValue);
  }
  
  @Test
  public void testConvertToLongWithNonNumericValue(){
    String ULAN_ID = "ABCD123";
    ArtistMapper.convertToLong("UlanId", ULAN_ID, errorDetailsList);
  }

  @SuppressWarnings("DuplicateStringLiteralInspection")
  private static List<String> createFullArtistCsvRecord() {
    String[] csv = new String[24];
    csv[ArtistCsvFields.ULAN_ID] = "1500321612";
    csv[ArtistCsvFields.ARTIST_ID] = "12";
    csv[ArtistCsvFields.FIRST_NAME] = "Weiwei";
    csv[ArtistCsvFields.LAST_NAME] = "Ai";
    csv[ArtistCsvFields.BIRTH_YEAR] = "1957";
    csv[ArtistCsvFields.DEATH_YEAR] = "1900";
    csv[ArtistCsvFields.DISPLAY] = "Ai Weiwei";
    csv[ArtistCsvFields.NATIONALITY] = "China";
    csv[ArtistCsvFields.GENDER] = "Male";
    csv[ArtistCsvFields.UNKNOWN_CHECK] = "Y";
    csv[ArtistCsvFields.PAINTING_CHECK] = "Y";
    csv[ArtistCsvFields.WORK_ON_PAPER_CHECK] = "Y";
    csv[ArtistCsvFields.MOUNTED_CHECK] = "Y";
    csv[ArtistCsvFields.SCULPTURE_CHECK] = "Y";
    csv[ArtistCsvFields.PHOTOGRAPH_CHECK] = "Y";
    csv[ArtistCsvFields.PRINT_CHECK] = "Y";
    csv[ArtistCsvFields.INSTALLATION_CHECK] = "Y";
    csv[ArtistCsvFields.VIDEO_CHECK] = "Y";
    csv[ArtistCsvFields.OTHER_CHECK] = "Y";
    csv[ArtistCsvFields.CATALOGUE_RAISONEE_AUTHOR] = "Author";
    csv[ArtistCsvFields.CATALOGUE_RAISONEE_YEAR] = "1921";
    csv[ArtistCsvFields.CATALOGUE_RAISONEE_VOLUMES] = "Volume-10";
    csv[ArtistCsvFields.CATALOGUE_RAISONEE_TYPE] = "Type";
    csv[ArtistCsvFields.NOTES] = "Notes";
    return Arrays.asList(csv);
  }

}
