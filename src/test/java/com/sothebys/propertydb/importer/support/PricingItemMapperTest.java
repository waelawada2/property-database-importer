package com.sothebys.propertydb.importer.support;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.importer.model.item.ItemObjectDto;
import com.sothebys.propertydb.importer.model.item.PricingItemDto;

@RunWith(MockitoJUnitRunner.class)
public class PricingItemMapperTest {
  
  @InjectMocks
  private PricingItemMapper pricingItemMapper;

  private List<String> csvRow;

  @Before
  public void setUp() {
    setupCsvRow();
  }

  private void setupCsvRow() {
    csvRow = new ArrayList<>();
    csvRow.add("CODE-TEST");
    csvRow.add("some id");
    csvRow.add("");
    csvRow.add("PricingEvent_ID");
    csvRow.add("");
    csvRow.add("Object_ID");
    csvRow.add("Auction_Currency_ID");
    csvRow.add("100000");
    csvRow.add("9999999");
    csvRow.add("Y");
    csvRow.add("PS_Currency_ID");
    csvRow.add("200000");
    csvRow.add("Insurance_Currency_ID");
    csvRow.add("12345678");
    csvRow.add("FMV_Currency_ID");
    csvRow.add("524154651");
    csvRow.add("Pipeline_Auction_ID");
    csvRow.add("Y");
    csvRow.add("Pipeline_PS_ID");
    csvRow.add("Y");
    csvRow.add("Pipeline_Exhibition_ID");
    csvRow.add("Y");
    csvRow.add("Old_Item_ID");
    csvRow.add("Item Notes");
    csvRow.add("Item Description");
    csvRow.add("Item Provenance");
    csvRow.add("Item Exhibition");
    csvRow.add("Item Literature");
    csvRow.add("Item Condition");
    csvRow.add("Item OD_Notes");    
  }

  @Test
  public void testMap() throws Exception {
    PricingItemDto dto = pricingItemMapper.map(csvRow);
    assertEquals("CODE-TEST", dto.getCode());
    assertEquals("some id", dto.getId());
    assertEquals("PricingEvent_ID", dto.getEvent().getId());
    assertEquals("Object_ID", ((ItemObjectDto)dto.getObjects().get(0)).getObjectId());
    assertEquals("Auction_Currency_ID", dto.getAuctionCurrency().getId());
    assertEquals(new BigDecimal("100000"), dto.getAuctionLowValue());
    assertEquals(new BigDecimal("9999999"), dto.getAuctionHighValue());
    assertEquals(true, dto.isAuctionPlusOne());
    assertEquals("PS_Currency_ID", dto.getPrivateSaleCurrency().getId());
    assertEquals(new BigDecimal("200000"), dto.getPrivateSaleNetSeller());
    assertEquals("Insurance_Currency_ID", dto.getInsuranceCurrency().getId());
    assertEquals(new BigDecimal("12345678"), dto.getInsurancePrice());
    assertEquals("FMV_Currency_ID", dto.getFairMarketValueCurrency().getId());
    assertEquals(new BigDecimal("524154651"), dto.getFairMarketValuePrice());
    assertEquals("Pipeline_Auction_ID", dto.getPipelineAuction().getId());
    assertEquals(true, dto.isPipelineAuctionTarget());
    assertEquals("Pipeline_PS_ID", dto.getPipelinePrivateSale().getId());
    assertEquals(true, dto.isPipelinePrivateSaleTarget());
    assertEquals("Pipeline_Exhibition_ID", dto.getPipelineExhibition().getId());
    assertEquals(true, dto.isPipelineExhibitionTarget());
    assertEquals("Old_Item_ID", dto.getLegacyId());
    assertEquals("Item Notes", dto.getComments());
    assertEquals("Item Description", dto.getDescription());
    assertEquals("Item Provenance", dto.getProvenance());
    assertEquals("Item Exhibition", dto.getExhibition());
    assertEquals("Item Literature", dto.getLiterature());
    assertEquals("Item Condition", dto.getCondition());
    assertEquals("Item OD_Notes", dto.getCataloguingNotes());
  }
}
