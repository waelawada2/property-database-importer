package com.sothebys.propertydb.importer.support;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.importer.model.item.TransactionItemDto;

@RunWith(MockitoJUnitRunner.class)
public class TransactionItemMapperTest {
  
  @InjectMocks
  private TransactionItemMapper transactionItemMapper;

  private List<String> csvRow;

  @Before
  public void setUp() {
    setupCsvRow();
  }

  private void setupCsvRow() {
    csvRow = new ArrayList<>();
    csvRow.add("CODE-TEST");
    csvRow.add("some id");
    csvRow.add("");
    csvRow.add("Transaction_Event_ID");
    csvRow.add("");
    csvRow.add("Consignment_ID");
    csvRow.add("");
    csvRow.add("Purchase_ID");
    csvRow.add("");
    csvRow.add("1234");
    csvRow.add("20125");
    csvRow.add("Y");
    csvRow.add("6000");
    csvRow.add("8000");
    csvRow.add("term_id");
    csvRow.add("2541364");
    csvRow.add("3622414");
    csvRow.add("5214854");
    csvRow.add("Sale_Status");
    csvRow.add("");
    csvRow.add("");
    csvRow.add("8858848");
    csvRow.add("Item_Notes");
    csvRow.add("Description");
    csvRow.add("Provenance");
    csvRow.add("Exhibition");
    csvRow.add("Literature");
    csvRow.add("Condition");
    csvRow.add("OD_Notes");
  }

  @Test
  public void testMap() throws Exception {
    TransactionItemDto dto = transactionItemMapper.map(csvRow);
    assertEquals("CODE-TEST", dto.getCode());
    assertEquals("some id", dto.getId());
    assertEquals("Transaction_Event_ID", dto.getEvent().getId());
    assertEquals("Consignment_ID", dto.getConsignmentEvent().getId());
    assertEquals("Purchase_ID", dto.getPurchaseEvent().getId());
    assertEquals("1234", dto.getObjects().get(0).getObjectId());
    assertEquals("20125", dto.getLotNumber());
    assertEquals(true, dto.getAuction().isEstimateOnRequest());
    assertEquals(new BigDecimal("6000"),dto.getAuction().getLowEstimate());
    assertEquals(new BigDecimal("8000"), dto.getAuction().getHighEstimate());
    assertEquals("term_id", dto.getAuction().getTerm().getId());
    assertEquals("Sale_Status", dto.getStatus().getId());
    assertEquals(new BigDecimal("2541364"), dto.getAuction().getUnsoldHammer());
    assertEquals(new BigDecimal("3622414"), dto.getAuction().getHammer());
    assertEquals(new BigDecimal("5214854"), dto.getAuction().getPremium());
    assertEquals("8858848", dto.getLegacyId());
    assertEquals("Item_Notes", dto.getComments());
    assertEquals("Description", dto.getDescription());
    assertEquals("Provenance", dto.getProvenance());
    assertEquals("Exhibition", dto.getExhibition());
    assertEquals("Literature", dto.getLiterature());
    assertEquals("Condition", dto.getCondition());
    assertEquals("OD_Notes", dto.getCataloguingNotes());
  }
}
