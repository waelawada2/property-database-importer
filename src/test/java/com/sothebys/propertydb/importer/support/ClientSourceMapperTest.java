package com.sothebys.propertydb.importer.support;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.importer.model.event.ClientSourceDto;

@RunWith(MockitoJUnitRunner.class)
public class ClientSourceMapperTest {
  
  @InjectMocks
  private ClientSourceMapper clientSourceMapper;

  private List<String> csvRow;

  @Before
  public void setUp() {
    setupCsvRow();
  }

  private void setupCsvRow() {
    csvRow = new ArrayList<>();
    csvRow.add("SOURCE-ID");
    csvRow.add("name");
  }

  @Test
  public void testMap() throws Exception {
    ClientSourceDto dto = clientSourceMapper.map(csvRow);
    assertEquals("SOURCE-ID", dto.getId());
    assertEquals("name", dto.getName());
  }
}
