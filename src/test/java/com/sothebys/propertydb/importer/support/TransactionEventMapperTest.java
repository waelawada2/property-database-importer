package com.sothebys.propertydb.importer.support;

import static org.junit.Assert.assertEquals;

import com.sothebys.propertydb.importer.model.event.TransactionEventDto;
import com.sothebys.propertydb.importer.model.event.TransactionType;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TransactionEventMapperTest {

  @InjectMocks
  private TransactionEventMapper transactionEventMapper;

  private List<String> csvRow;

  @Before
  public void setUp() {
    setupCsvRow();
  }

  private void setupCsvRow() {
    csvRow = new ArrayList<>();
    csvRow.add("SOT-NYC-16-NOV-2004-LAT-LAT");
    csvRow.add("some id");
    csvRow.add("16/Nov/2004");
    csvRow.add("Auction");
    csvRow.add("Latin American Art");
    csvRow.add("SOT-NYC");
    csvRow.add("SE-123456");
    csvRow.add("some department");
    csvRow.add("N00001");
    csvRow.add("USD");
    csvRow.add("1");
    csvRow.add("some notes");
  }

  @Test
  public void testMap() throws Exception {
    TransactionEventDto dto = transactionEventMapper.map(csvRow);
    assertEquals("SOT-NYC-16-NOV-2004-LAT-LAT", dto.getCode());
    assertEquals("some id", dto.getId());
    assertEquals(LocalDateTime.of(2004, 11, 16, 0, 0).toInstant(ZoneOffset.UTC),
        dto.getDate().toInstant());
    assertEquals(TransactionType.AUCTION, dto.getTransactionType());
    assertEquals("Latin American Art", dto.getTitle());
    assertEquals("SE-123456", dto.getEstablishment().getId());
    assertEquals("some department", dto.getDepartment().getId());
    assertEquals("N00001", dto.getSaleNumber());
    assertEquals("USD", dto.getCurrency().getId());
    assertEquals(new Double(1), dto.getTotalLots());
    assertEquals("some notes", dto.getNotes());


  }

}