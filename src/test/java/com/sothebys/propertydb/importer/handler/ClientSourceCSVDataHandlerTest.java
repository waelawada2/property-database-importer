package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.importer.exception.ClientSourceImportException;
import com.sothebys.propertydb.importer.model.event.ClientSourceDto;
import com.sothebys.propertydb.importer.support.ClientSourceMapper;

@RunWith(MockitoJUnitRunner.class)
public class ClientSourceCSVDataHandlerTest {
  @InjectMocks
  private ClientSourceCSVDataHandler clientSourceCSVDataHandler;
    
  @Mock
  private ClientSourceMapper clientSourceMapper;
  
  private Exchange exchange;
  private CamelContext context;
  
  private ClientSourceDto clientSourceDto;
  private List<String> csvRow;

  @Before
  public void setUp() {
    setupClientSourceCreateRequestDto();
    context = new DefaultCamelContext();
    exchange = new DefaultExchange(context);
  }

  private void setupClientSourceCreateRequestDto() {
    clientSourceDto = new ClientSourceDto();
  }

  @Test
  public void testHandle() {
    Mockito.when(clientSourceMapper.map(csvRow)).thenReturn(clientSourceDto);
    ClientSourceDto dto = clientSourceCSVDataHandler.handle(csvRow, null);

    assertEquals(clientSourceDto, dto);
  }
  
  @Test
  public void testHandlePutMethod() {
    clientSourceDto.setId("ID123");
    Mockito.when(clientSourceMapper.map(csvRow))
        .thenReturn(clientSourceDto);

    ClientSourceDto dto =
        clientSourceCSVDataHandler.handle(csvRow, exchange);

    assertEquals(clientSourceDto, dto);
    assertEquals(exchange.getProperties().get("clientSourceId"), "ID123");
    assertNull(clientSourceDto.getId());
  }
  
  @Test
  public void testHandlePostMethod() {
    clientSourceDto.setId(null);
    Mockito.when(clientSourceMapper.map(csvRow))
        .thenReturn(clientSourceDto);

    ClientSourceDto dto =
        clientSourceCSVDataHandler.handle(csvRow, exchange);

    assertEquals(clientSourceDto, dto);
    assertEquals(exchange.getProperties().get("clientSourceId"), null);
    assertNull(clientSourceDto.getId());
  }

  @Test(expected = ClientSourceImportException.class)
  public void testHandleException() {
    Mockito.when(clientSourceMapper.map(csvRow)).thenThrow(new ArrayIndexOutOfBoundsException());
    clientSourceCSVDataHandler.handle(csvRow, null);
  }
  
}