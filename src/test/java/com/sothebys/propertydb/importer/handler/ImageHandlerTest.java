package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertTrue;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.sothebys.propertydb.importer.exception.PropertyImportException;

import io.findify.s3mock.S3Mock;

import java.io.File;
import java.io.IOException;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.model.ModelCamelContext;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import akka.http.scaladsl.Http;

@RunWith(MockitoJUnitRunner.class)
public class ImageHandlerTest {
  
  @InjectMocks
  private ImageHandler imageHandler;
  
  @Autowired
  private ModelCamelContext camelContext;
  
  private static S3Mock api;
  private static AmazonS3 client;
  private File imageFile;

  @Before
  public void setUp() throws IOException {
    imageFile = new File("src/test/resources/image-test.jpg");
    ReflectionTestUtils.setField(imageHandler, "images3Client", client);
    ReflectionTestUtils.setField(imageHandler, "bucketName", "testbucket");
  }
  
  @BeforeClass
  public static void setUpClient() {
    setUpS3Client();
  }
  
  @AfterClass
  public static void releaseResources() {
    api.stop();
  }
  
  private static void setUpS3Client() {
    api = new S3Mock.Builder().withPort(0).withInMemoryBackend().build();
    Http.ServerBinding binding = api.start();
    int portToUse = binding.localAddress().getPort();
    
    EndpointConfiguration endpoint =
        new EndpointConfiguration("http://localhost:"+portToUse, "us-west-2");
    client = AmazonS3ClientBuilder.standard().withPathStyleAccessEnabled(true)
        .withEndpointConfiguration(endpoint)
        .withCredentials(new AWSStaticCredentialsProvider(new AnonymousAWSCredentials())).build();

    client.createBucket("testbucket");
  }
  
  
  @Test
  public void testSetS3ImageToBodyAsMultiPart() {
    Exchange exchange = new DefaultExchange(camelContext);
    String imageName = "imageName.jpg";
    client.putObject("testbucket", imageName, imageFile);
    
    imageHandler.setS3ImageToBodyAsMultiPart(imageName, exchange);
    
    assertTrue(exchange.getIn().getBody() != null);
    assertTrue(exchange.getIn().getBody().getClass().toString().contains("Multipart"));
  }
  
  @Test
  public void testSetS3ImageToBodyAsMultiPartBlankImage() {
    Exchange exchange = new DefaultExchange(camelContext);
    
    imageHandler.setS3ImageToBodyAsMultiPart(null, exchange);
    
    assertTrue(exchange.getIn().getBody() == null);
  }
  
  
  @Test(expected=PropertyImportException.class)
  public void testSetS3ImageToBodyAsMultiPartImageDoesNotExist() {
    Exchange exchange = new DefaultExchange(camelContext);
    String imageName = "NonExistingImage.jpg";
    
    imageHandler.setS3ImageToBodyAsMultiPart(imageName, exchange);
  }
  

}
