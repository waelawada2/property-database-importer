package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.format.DateTimeParseException;
import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.importer.exception.PricingEventImportException;
import com.sothebys.propertydb.importer.model.event.PricingEventDto;
import com.sothebys.propertydb.importer.support.PricingEventMapper;

@RunWith(MockitoJUnitRunner.class)
public class PricingEventCSVDataHandlerTest {
  @InjectMocks
  private PricingEventCSVDataHandler pricingEventCSVDataHandler;
    
  @Mock
  private PricingEventMapper pricingEventMapper;
  
  private Exchange exchange;
  private CamelContext context;
  
  private PricingEventDto pricingEventDto;
  private List<String> csvRow;

  @Before
  public void setUp() {
    setupPricingEventCreateRequestDto();
    context = new DefaultCamelContext();
    exchange = new DefaultExchange(context);
  }

  private void setupPricingEventCreateRequestDto() {
    pricingEventDto = new PricingEventDto();
  }

  @Test
  public void testHandle() {
    Mockito.when(pricingEventMapper.map(csvRow)).thenReturn(pricingEventDto);
    PricingEventDto dto = pricingEventCSVDataHandler.handle(csvRow, null);

    assertEquals(pricingEventDto, dto);
  }
  
  @Test
  public void testHandlePutMethod() {
    pricingEventDto.setId("ID123");
    Mockito.when(pricingEventMapper.map(csvRow))
        .thenReturn(pricingEventDto);

    PricingEventDto dto =
        pricingEventCSVDataHandler.handle(csvRow, exchange);

    assertEquals(pricingEventDto, dto);
    assertEquals(exchange.getProperties().get("eventId"), "ID123");
    assertNull(pricingEventDto.getId());
  }
  
  @Test
  public void testHandlePostMethod() {
    pricingEventDto.setId(null);
    Mockito.when(pricingEventMapper.map(csvRow))
        .thenReturn(pricingEventDto);

    PricingEventDto dto =
        pricingEventCSVDataHandler.handle(csvRow, exchange);

    assertEquals(pricingEventDto, dto);
    assertEquals(exchange.getProperties().get("eventId"), null);
    assertNull(pricingEventDto.getId());
  }

  @Test(expected = PricingEventImportException.class)
  public void testHandleDateTimeParseException() {
    Mockito.when(pricingEventMapper.map(csvRow)).thenThrow(new DateTimeParseException(null, "", 0));
    pricingEventCSVDataHandler.handle(csvRow, null);
  }

  @Test(expected = PricingEventImportException.class)
  public void testHandleNumberFormatException() {
    Mockito.when(pricingEventMapper.map(csvRow)).thenThrow(new NumberFormatException());
    pricingEventCSVDataHandler.handle(csvRow, null);
  }

  @Test(expected = PricingEventImportException.class)
  public void testHandleException() {
    Mockito.when(pricingEventMapper.map(csvRow)).thenThrow(new ArrayIndexOutOfBoundsException());
    pricingEventCSVDataHandler.handle(csvRow, null);
  }
  
}