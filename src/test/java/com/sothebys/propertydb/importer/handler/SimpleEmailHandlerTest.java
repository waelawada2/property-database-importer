package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SimpleEmailHandlerTest {

  @InjectMocks
  private SimpleEmailHandler simpleEmailHandler;

  @Test
  public void testPrepareEmailBody() {
    String emailBody = simpleEmailHandler
        .prepareEmailBody("someFilename.csv", "someFilename_error.csv", "someFilename_success", 3, 1);

    assertTrue(emailBody.contains("someFilename.csv"));
    assertTrue(emailBody.contains("Total number of records in file:3"));
    assertTrue(emailBody.contains("Total number of records successfully imported:2"));
    assertTrue(emailBody.contains("Total number of records with errors:1"));
    assertTrue(emailBody.contains("someFilename_error.csv"));
  }

  @Test
  public void testPrepareSaveListStatusEmailBody() {
    String emailBody = simpleEmailHandler
        .prepareSaveListStatusEmailBody("someFilename.csv", "SL-123", 3, null);

    assertTrue(emailBody.contains("someFilename"));
    assertTrue(emailBody.contains("Saved list ID: SL-123"));
    assertTrue(emailBody.contains("Saved list property count: 3"));
  }

  @Test
  public void testPrepareSaveListStatusEmailBodyErrorMessage() {
    String emailBody = simpleEmailHandler
        .prepareSaveListStatusEmailBody("someFilename.csv", "SL-123", 0, "some error occurred");

    assertTrue(emailBody.contains("someFilename"));
    assertTrue(emailBody.contains("Error message: some error occurred"));
  }

  @Test
  public void testPrepareDuplicateFileEmailBody() {
    String emailBody = simpleEmailHandler.prepareDuplicateFileEmailBody("someFilename.csv");
    assertTrue(emailBody.contains("We did not process the import for someFilename.csv"));
  }
}