package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;

import com.sothebys.propertydb.importer.exception.EnumNotFoundException;
import com.sothebys.propertydb.importer.exception.TransactionEventImportException;
import com.sothebys.propertydb.importer.model.event.TransactionEventDto;
import com.sothebys.propertydb.importer.support.TransactionEventMapper;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TransactionEventCSVDataHandlerTest {

  @InjectMocks
  private TransactionEventCSVDataHandler transactionEventCSVDataHandler;

  @Mock
  private TransactionEventMapper transactionEventMapper;

  private TransactionEventDto transactionEventDto;
  private Exchange exchange;
  private List<String> csvRow;

  @Before
  public void setUp() {
    setupTransactionEventCreateRequestDto();
    setupExchange();
    csvRow = Collections.emptyList();
  }

  private void setupTransactionEventCreateRequestDto() {
    transactionEventDto = new TransactionEventDto();
    transactionEventDto.setId("123");
  }

  private void setupExchange() {
    exchange = new DefaultExchange(new DefaultCamelContext());
  }

  @Test
  public void testHandle() {
    Mockito.when(transactionEventMapper.map(csvRow)).thenReturn(transactionEventDto);
    TransactionEventDto dto = transactionEventCSVDataHandler.handle(csvRow, exchange);

    assertEquals(transactionEventDto, dto);
    assertEquals("123", exchange.getProperty("eventId"));
  }

  @Test(expected = TransactionEventImportException.class)
  public void testHandleEnumNotFoundException() {
    Mockito.when(transactionEventMapper.map(csvRow)).thenThrow(new EnumNotFoundException());
    transactionEventCSVDataHandler.handle(csvRow, exchange);
  }

  @Test(expected = TransactionEventImportException.class)
  public void testHandleDateTimeParseException() {
    Mockito.when(transactionEventMapper.map(csvRow)).thenThrow(new DateTimeParseException(null, "", 0));
    transactionEventCSVDataHandler.handle(csvRow, exchange);
  }

  @Test(expected = TransactionEventImportException.class)
  public void testHandleNumberFormatException() {
    Mockito.when(transactionEventMapper.map(csvRow)).thenThrow(new NumberFormatException());
    transactionEventCSVDataHandler.handle(csvRow, exchange);
  }

  @Test(expected = TransactionEventImportException.class)
  public void testHandleException() {
    Mockito.when(transactionEventMapper.map(csvRow)).thenThrow(new ArrayIndexOutOfBoundsException());
    transactionEventCSVDataHandler.handle(csvRow, exchange);
  }
}
