package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.sothebys.propertydb.importer.handler.StringHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StringHandlerTest {

  @InjectMocks
  private StringHandler stringHandler;

  @Test
  public void trimAndReplaceWhitespace() {
    String filenameNoWhitespace = stringHandler.trimAndReplaceWhitespace(" test file name.csv");
    assertEquals("test_file_name.csv", filenameNoWhitespace);
  }

  @Test
  public void trimAndReplaceWhitespaceNull() {
    String filenameNoWhitespace = stringHandler.trimAndReplaceWhitespace(null);
    assertNull(filenameNoWhitespace);
  }
}