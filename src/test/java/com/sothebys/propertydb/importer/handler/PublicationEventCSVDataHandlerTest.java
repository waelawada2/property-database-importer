package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;

import com.sothebys.propertydb.importer.exception.PublicationEventImportException;
import com.sothebys.propertydb.importer.model.event.PublicationEventDto;
import com.sothebys.propertydb.importer.support.PublicationEventMapper;
import java.text.ParseException;
import java.util.List;
import org.apache.camel.Exchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PublicationEventCSVDataHandlerTest {

  @InjectMocks
  private PublicationEventCSVDataHandler publicationEventCSVDataHandler;

  @Mock
  private PublicationEventMapper publicationEventMapper;

  @Mock
  private Exchange exchange;

  private PublicationEventDto publicationEventDto;
  private List<String> csvRow;


  @Before
  public void setUp() {
    publicationEventDto = new PublicationEventDto();
    publicationEventDto.setId("123");
  }


  @Test
  public void testHandle() {
    Mockito.when(publicationEventMapper.map(csvRow)).thenReturn(publicationEventDto);

    PublicationEventDto dto = publicationEventCSVDataHandler.handle(csvRow, exchange);

    assertEquals(publicationEventDto, dto);
  }

  @Test(expected = PublicationEventImportException.class)
  public void testHandleFailure() {
    Mockito.when(publicationEventMapper.map(csvRow)).thenThrow(new ArrayIndexOutOfBoundsException());

    publicationEventCSVDataHandler.handle(csvRow, exchange);
  }

}