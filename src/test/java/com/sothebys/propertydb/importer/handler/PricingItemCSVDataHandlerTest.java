package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.importer.exception.PricingItemImportException;
import com.sothebys.propertydb.importer.model.item.PricingItemDto;
import com.sothebys.propertydb.importer.support.PricingItemMapper;

@RunWith(MockitoJUnitRunner.class)
public class PricingItemCSVDataHandlerTest {
  @InjectMocks
  private PricingItemCSVDataHandler pricingItemCSVDataHandler;
  
  @Mock
  private PricingItemMapper pricingItemMapper;
  
  private Exchange exchange;
  private CamelContext context;
  
  private PricingItemDto pricingItemDto;
  private List<String> csvRow;
  
  @Before
  public void setUp() {
    setupPricingItemCreateRequestDto();
    context = new DefaultCamelContext();
    exchange = new DefaultExchange(context);
  }

  private void setupPricingItemCreateRequestDto() {
    pricingItemDto = new PricingItemDto();
  }
  
  @Test
  public void testHandlePutMethod() {
    pricingItemDto.setId("ID123");
    Mockito.when(pricingItemMapper.map(csvRow))
    .thenReturn(pricingItemDto);
    
    PricingItemDto dto =
        pricingItemCSVDataHandler.handle(csvRow, exchange);
    
    assertEquals(pricingItemDto, dto);
    assertEquals(exchange.getProperties().get("itemId"), "ID123");
    assertNull(pricingItemDto.getId());
  }
  
  @Test
  public void testHandlePostMethod() {
    pricingItemDto.setId(null);
    Mockito.when(pricingItemMapper.map(csvRow))
        .thenReturn(pricingItemDto);

    PricingItemDto dto =
        pricingItemCSVDataHandler.handle(csvRow, exchange);

    assertEquals(pricingItemDto, dto);
    assertEquals(exchange.getProperties().get("eventId"), null);
    assertNull(pricingItemDto.getId());
  }
  
  @Test(expected = PricingItemImportException.class)
  public void testHandleException() {
    Mockito.when(pricingItemMapper.map(csvRow)).thenThrow(new ArrayIndexOutOfBoundsException());
    pricingItemCSVDataHandler.handle(csvRow, null);
  }
    
}
