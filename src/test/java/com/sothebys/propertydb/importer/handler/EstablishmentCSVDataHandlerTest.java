package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;

import com.sothebys.propertydb.importer.exception.EstablishmentImportException;
import com.sothebys.propertydb.importer.model.event.EstablishmentDto;
import com.sothebys.propertydb.importer.support.EstablishmentMapper;
import java.util.Collections;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EstablishmentCSVDataHandlerTest {

  @InjectMocks
  private EstablishmentCSVDataHandler establishmentCSVDataHandler;

  @Mock
  private EstablishmentMapper establishmentMapper;

  private EstablishmentDto establishmentDto;
  private Exchange exchange;
  private List<String> csvRow;

  @Before
  public void setUp() {
    setupEstablishmentDto();
  }

  private void setupEstablishmentDto() {
    establishmentDto = new EstablishmentDto();
    establishmentDto.setId("123");
    setupExchange();
    csvRow = Collections.emptyList();
  }

  private void setupExchange() {
    exchange = new DefaultExchange(new DefaultCamelContext());
  }

  @Test
  public void testHandle() {
    Mockito.when(establishmentMapper.map(csvRow)).thenReturn(establishmentDto);
    EstablishmentDto dto = establishmentCSVDataHandler.handle(csvRow, exchange);

    assertEquals(establishmentDto, dto);
    assertEquals("123", exchange.getProperty("establishmentId"));
  }

  @Test(expected = EstablishmentImportException.class)
  public void testHandleFailure() {
    Mockito.when(establishmentMapper.map(csvRow)).thenThrow(new ArrayIndexOutOfBoundsException());
    establishmentCSVDataHandler.handle(csvRow, exchange);
  }

}