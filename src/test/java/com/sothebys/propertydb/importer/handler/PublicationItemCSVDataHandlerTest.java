package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;

import com.sothebys.propertydb.importer.exception.PublicationItemImportException;
import com.sothebys.propertydb.importer.model.item.PublicationItemDto;
import com.sothebys.propertydb.importer.support.PublicationItemMapper;
import java.util.List;
import org.apache.camel.Exchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PublicationItemCSVDataHandlerTest {

  @InjectMocks
  private PublicationItemCSVDataHandler publicationItemCSVDataHandler;

  @Mock
  private PublicationItemMapper publicationItemMapper;

  @Mock
  private Exchange exchange;

  private PublicationItemDto publicationItemDto;
  private List<String> csvRow;


  @Before
  public void setUp() {
    publicationItemDto = new PublicationItemDto();
    publicationItemDto.setId("123");
  }


  @Test
  public void testHandle() throws Exception{
    Mockito.when(publicationItemMapper.map(csvRow)).thenReturn(publicationItemDto);

    PublicationItemDto dto = publicationItemCSVDataHandler.handle(csvRow, exchange);

    assertEquals(publicationItemDto, dto);
  }

  @Test(expected = PublicationItemImportException.class)
  public void testHandleFailure() throws Exception{
    Mockito.when(publicationItemMapper.map(csvRow)).thenThrow(new ArrayIndexOutOfBoundsException());

    publicationItemCSVDataHandler.handle(csvRow, exchange);
  }

}