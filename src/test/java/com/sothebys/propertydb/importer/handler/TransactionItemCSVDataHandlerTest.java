package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.importer.exception.TransactionItemImportException;
import com.sothebys.propertydb.importer.model.item.TransactionItemDto;
import com.sothebys.propertydb.importer.support.TransactionItemMapper;

@RunWith(MockitoJUnitRunner.class)
public class TransactionItemCSVDataHandlerTest {
  @InjectMocks
  private TransactionItemCSVDataHandler transactionItemCSVDataHandler;
  
  @Mock
  private TransactionItemMapper transactionItemMapper;
  
  private Exchange exchange;
  private CamelContext context;
  
  private TransactionItemDto transactionItemDto;
  private List<String> csvRow;
  
  @Before
  public void setUp() {
    setupTransactionItemCreateRequestDto();
    context = new DefaultCamelContext();
    exchange = new DefaultExchange(context);
  }

  private void setupTransactionItemCreateRequestDto() {
    transactionItemDto = new TransactionItemDto();
  }
  
  @Test
  public void testHandlePutMethod() {
    transactionItemDto.setId("ID123");
    Mockito.when(transactionItemMapper.map(csvRow))
    .thenReturn(transactionItemDto);
    
    TransactionItemDto dto =
        transactionItemCSVDataHandler.handle(csvRow, exchange);
    
    assertEquals(transactionItemDto, dto);
    assertEquals(exchange.getProperties().get("itemId"), "ID123");
    assertNull(transactionItemDto.getId());
  }
  
  @Test
  public void testHandlePostMethod() {
    transactionItemDto.setId(null);
    Mockito.when(transactionItemMapper.map(csvRow))
        .thenReturn(transactionItemDto);

    TransactionItemDto dto =
        transactionItemCSVDataHandler.handle(csvRow, exchange);

    assertEquals(transactionItemDto, dto);
    assertEquals(exchange.getProperties().get("eventId"), null);
    assertNull(transactionItemDto.getId());
  }
  
  @Test(expected = TransactionItemImportException.class)
  public void testHandleException() {
    Mockito.when(transactionItemMapper.map(csvRow)).thenThrow(new ArrayIndexOutOfBoundsException());
    transactionItemCSVDataHandler.handle(csvRow, null);
  }
    
}
