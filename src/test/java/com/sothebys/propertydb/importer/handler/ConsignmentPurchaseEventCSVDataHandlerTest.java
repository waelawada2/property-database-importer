package com.sothebys.propertydb.importer.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.sothebys.propertydb.importer.exception.ConsignmentPurchaseEventImportException;
import com.sothebys.propertydb.importer.exception.EnumNotFoundException;
import com.sothebys.propertydb.importer.model.event.ConsignmentPurchaseEventDto;
import com.sothebys.propertydb.importer.model.event.EventType;
import com.sothebys.propertydb.importer.support.ConsignmentPurchaseEventMapper;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ConsignmentPurchaseEventCSVDataHandlerTest {

  @InjectMocks
  private ConsignmentPurchaseEventCSVDataHandler consignmentPurchaseEventCSVDataHandler;

  @Mock
  private ConsignmentPurchaseEventMapper consignmentPurchaseEventMapper;

  private Exchange exchange;
  private CamelContext context;
  private ConsignmentPurchaseEventDto consignmentPurchaseEventDto;
  private List<String> csvRow;

  @Before
  public void setUp() {
    consignmentPurchaseEventDto = new ConsignmentPurchaseEventDto(EventType.CONSIGNMENT);
    context = new DefaultCamelContext();
    exchange = new DefaultExchange(context);
  }


  @Test
  public void testHandlePutMethod() {
    consignmentPurchaseEventDto.setId("ID123");
    Mockito.when(consignmentPurchaseEventMapper.map(csvRow))
        .thenReturn(consignmentPurchaseEventDto);

    ConsignmentPurchaseEventDto dto =
        consignmentPurchaseEventCSVDataHandler.handle(csvRow, exchange);

    assertEquals(consignmentPurchaseEventDto, dto);
    assertEquals(exchange.getProperties().get("id"), "ID123");
    assertEquals(exchange.getIn().getHeader(Exchange.HTTP_PATH), "ID123");
    assertNull(consignmentPurchaseEventDto.getId());
  }

  @Test
  public void testHandlePostMethod() {
    consignmentPurchaseEventDto.setId(null);
    Mockito.when(consignmentPurchaseEventMapper.map(csvRow))
        .thenReturn(consignmentPurchaseEventDto);

    ConsignmentPurchaseEventDto dto =
        consignmentPurchaseEventCSVDataHandler.handle(csvRow, exchange);

    assertEquals(consignmentPurchaseEventDto, dto);
    assertEquals(exchange.getProperties().get("id"), null);
    assertEquals(exchange.getIn().getHeader(Exchange.HTTP_PATH), null);
    assertNull(consignmentPurchaseEventDto.getId());
  }
}
