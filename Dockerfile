FROM openjdk:8-jre-alpine
ADD target/property-database-importer*.jar app.jar
EXPOSE 8090
ENV PORT=8090
ENV JAVA_OPTS="-Xmx512M"
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
