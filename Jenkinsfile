#!/usr/bin/env groovy

properties([
        buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5')),
        pipelineTriggers([pollSCM('H/10 * * * *')])
])

@Library('sothebys-jenkins-shared-library@2.x') _

def utils = new com.sothebys.jenkins.DeploymentUtils()

try {

  node {

    stage('Preparation') {
      deleteDir()
    }

    stage('Checkout') {
      checkoutProject {}
    }

    stage('Build') {
      buildProject {}
    }

    stage('Create Docker Image') {
      createDockerImage {}
    }

    stage('Integration Tests') {
      runIntegrationTests {
        withMajorVersion = false
      }
    }

    // Publish to snapshot repository
    if (utils.isSnapshot()) {
      stage('Publish artifacts') {
        publishArtifact {}
      }
    }

    // Continuous integration for deployable branches only
    if (utils.isDeployableBranch()) {
      stage('Trigger DEV deployment') {
        triggerDeployment {
          environment = 'dev'
        }
      }
    }

  } // node

} catch (e) {
  currentBuild.result = 'FAILURE'
  throw e
} finally {
  sendBuildNotification {}
}
